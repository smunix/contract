package org.contract.type;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.base.IndexI;
import org.contract.utils.Pointer;
import org.contract.utils.functional.Thunk2;
import org.contract.utils.functional.Thunk3;
import org.contract.utils.functional.Thunk4;

public abstract class Type<E extends IndexI> {
    public E create(final UnsafeBuffer unsafeBuffer, Pointer<Long> index) {
        final E r = ctor.apply(unsafeBuffer, index);
        return r;
    }
    public E create(final long count, final UnsafeBuffer unsafeBuffer, Pointer<Long> index) {
        final E r = dynArrCtor.apply(count, unsafeBuffer, index);
        return r;
    }
    Thunk2<E, UnsafeBuffer, Pointer<Long>> ctor;
    Thunk3<E, Long, UnsafeBuffer, Pointer<Long>> dynArrCtor;
    Thunk4<E, Long, Type, UnsafeBuffer, Pointer<Long>> CountFunctorCtor;

    protected Type(Thunk3<E, Long, UnsafeBuffer, Pointer<Long>> dynArrCtor) { this.dynArrCtor = dynArrCtor; }
    protected Type(Thunk2<E,  UnsafeBuffer, Pointer<Long>> ctor) { this.ctor = ctor; }
    protected Type(Thunk4<E, Long, Type, UnsafeBuffer, Pointer<Long>> CountFunctorCtor) { this.CountFunctorCtor = CountFunctorCtor; }

    public boolean isArray() {
        return false;
    }

    public abstract long offsetOf();
    public abstract Type<E> offsetOf(long offset);
    public abstract long sizeOf(long eCount);
    public abstract Desc describe();
    public long sizeOf(){ return sizeOf(1); }
    public abstract long alignOf();

    public Boolean from(long off, Desc desc) {
        return false;
    }
}
