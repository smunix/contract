package org.contract.type;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.Bool;
import org.contract.field.Int;
import org.contract.field.Long;
import org.contract.field.Unit;
import org.contract.field.base.IndexWire;
import org.contract.utils.Pointer;
import org.contract.utils.functional.Thunk0;
import org.contract.utils.functional.Thunk1;
import org.contract.utils.functional.Thunk2;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public abstract class Desc {
    private static final byte True = 1;
    private static final byte False = 0;

    static final int NONE = -1;
    static final int PRIM = 0;
    static final int VAR = 2;
    static final int FARR = 4;
    static final int ARR = 5;
    static final int VARIANT = 6;
    static final int STRUCT = 7;
    static final int NAT = 11;
    static final int APP = 12;
    static final int RECURSIVE = 13;
    static final int FN = 15;

    public Boolean with(byte[] name, Thunk2<Boolean, java.lang.Long, Desc> action) {
        // new IllegalFieldNotFound(Desc.string(name)).printStackTrace(System.err);
        return false;
    }

    public Boolean with(String name, Thunk2<Boolean, java.lang.Long, Desc> action) {
        return with(name.getBytes(), action);
    }

    // string manipulations
    public static class Bytes extends IndexWire {
        public Bytes(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> position) {
            super(unsafeBuffer, position);
        }
        public Bytes set(byte[] bytes) {
            new Long.Meta().type().create(wire(), write()).set(bytes.length);
            wire().putBytes(Math.toIntExact(write().get()), bytes);
            write(write().get() + bytes.length);
            return this;
        }
        public long length() {
            return wire().getLong((int) index());
        }

        public byte[] get() {
            byte[] dst = new byte[(int)new Long.Meta().type().create(wire(), write()).get()];
            wire().getBytes((int)(index() + Long.Meta.Type.sizeOf()), dst);
            write(write().get() + dst.length);
            return dst;
        }
    }

    enum Enum {
        None(NONE),
        Prim(PRIM),
        Var(VAR),
        FArr(FARR),
        Arr(ARR),
        Variant(VARIANT),
        Struct(STRUCT),
        Nat(NAT),
        App(APP),
        Recursive(RECURSIVE),
        Fn(FN)
        ;

        public final int value;
        Enum(final int value) {
            this.value = value;
        }
        public int get() {
            return value;
        }
    }
    public Enum tid;
    Desc(final Enum tid) {
        this.tid = tid;
    }

    public PrintStream show(PrintStream printStream) {
        Thunk0<PrintStream> empty = () -> {
            printStream.print("?");
            return printStream;
        };
        return match(
                ()  -> empty.apply(),
                (prim) -> {
                    printStream.print(string(prim.n));
                    if (null != prim.repr) {
                        printStream.print(" (");
                        prim.repr.show(printStream);
                        printStream.print(')');
                    }
                    return printStream;
                },
                (var) -> {
                    printStream.print(string(var.n));
                    return printStream;
                },
                (farr) -> {
                    printStream.print("[");
                    farr.t.show(printStream);
                    printStream.print("|");
                    farr.len.show(printStream);
                    printStream.print("]");
                    return printStream;
                },
                (arr) -> {
                    printStream.print("[");
                    arr.t.show(printStream);
                    printStream.print("]");
                    return printStream;
                },
                (variant) -> {
                    printStream.print("|");
                    Iterator<Variant.Ctor> it = variant.ctors.iterator();
                    while (it.hasNext()) {
                        Variant.Ctor ctor = it.next();
                        printStream.print(string(ctor.name));
                        printStream.print(":");
                        ctor.desc.show(printStream);
                        if (it.hasNext())
                            printStream.print(',');
                    }
                    printStream.print("|");
                    return printStream;
                },
                (struct) -> {
                    printStream.print("{");
                    Iterator<Struct.Field> it = struct.fields.iterator();
                    while (it.hasNext()) {
                        Struct.Field ctor = it.next();
                        printStream.print(string(ctor.name));
                        printStream.print(":");
                        ctor.desc.show(printStream);
                        if (it.hasNext())
                            printStream.print(',');
                    }
                    printStream.print("}");
                    return printStream;
                },
                (nat) -> {
                    printStream.print(nat.x);
                    return printStream;
                },
                (app) -> app.evaluate(() -> printStream, (body) -> body.show(printStream)),
                (recursive) -> {
                    printStream.print("^" + string(recursive.x) + ".");
                    recursive.t.show(printStream);
                    return printStream;
                },
                (fn) -> {
                    printStream.print('\\');
                    Iterator<byte[]> it = fn.args.iterator();
                    while (it.hasNext())
                        printStream.print(string(it.next()) + " ");
                    printStream.print("-> ");
                    fn.body.show(printStream);
                    return printStream;
                }
        );
    }

    public <R> R evaluate(Thunk0<R> def, Thunk1<R, Desc> th) {
        return match(
                () -> def.apply(),
                (x) -> def.apply(),
                (x) -> def.apply(),
                (x) -> def.apply(),
                (x) -> def.apply(),
                (x) -> def.apply(),
                (x) -> def.apply(),
                (x) -> def.apply(),
                (app) -> app.f.match(
                        () -> def.apply(),
                        (x) -> def.apply(),
                        (x) -> def.apply(),
                        (x) -> def.apply(),
                        (x) -> def.apply(),
                        (x) -> def.apply(),
                        (x) -> def.apply(),
                        (x) -> def.apply(),
                        (x) -> def.apply(),
                        (x) -> def.apply(),
                        (fn) -> th.apply(fn.body.substitute(fn.args, app.args))
                ),
                (x) -> def.apply(),
                (x) -> def.apply()
        );
    }

    protected Desc substitute(Iterable<byte[]> vars, Iterable<Desc> args) {
        return this;
    }
    public static String string(byte[] str) {
        return new String(str);
    }

    public Unit encode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
        new Int.Meta().type().create(unsafeBuffer, index).set(tid.get());
        return invokeEncode(unsafeBuffer, index);
    }
    public Unit encode(UnsafeBuffer unsafeBuffer) {
        return encode(unsafeBuffer, Pointer.create(0L));
    }
    public abstract Unit invokeEncode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index);

    static long decodeLong(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
        Long l = new Long.Meta().type().create(unsafeBuffer, index);
        return l.get();
    }
    static int decodeInt(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
        Int i = new Int.Meta().type().create(unsafeBuffer, index);
        return i.get();
    }
    public static byte[] decodeString(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
        byte[] str = new Bytes(unsafeBuffer, index).get();
        return str;
    }
    public static boolean decodeBool(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
        return new Bool.Meta().type().create(unsafeBuffer, index).get();
    }
    public static Desc decode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> pos) {
        return
                decode(
                        unsafeBuffer,
                        pos,
                        () -> new Desc(Enum.None) {
                            @Override
                            protected Desc substitute(Iterable<byte[]> vars, Iterable<Desc> args) {
                                return this;
                            }

                            @Override
                            public Unit invokeEncode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
                                return Unit.unit;
                            }

                            @Override
                            public <R> R match(Thunk0<R> none, Thunk1<R, Prim> prim, Thunk1<R, Var> var, Thunk1<R, FArr> farr, Thunk1<R, Arr> arr, Thunk1<R, Variant> variant, Thunk1<R, Struct> struct, Thunk1<R, Nat> nat, Thunk1<R, App> app, Thunk1<R, Recursive> recursive, Thunk1<R, Fn> fn) {
                                return none.apply();
                            }
                        },
                        (ub, ndx) -> {
                            // prim;
                            byte[] n = decodeString(ub, ndx);
                            Desc repr = null;
                            if (decodeBool(ub, ndx))
                                repr = decode(ub, ndx);
                            return prim(n, repr);
                        },
                        (ub, ndx) -> {
                            // var;
                            byte[] n = decodeString(ub, ndx);
                            return var(n);
                        },
                        (ub, ndx) -> {
                            // farr;
                            Desc t = decode(ub, ndx);
                            Desc len = decode(ub, ndx);
                            return arr(t, len);
                        },
                        (ub, ndx) -> {
                            // arr;
                            Desc t = decode(ub, ndx);
                            return arr(t);
                        },
                        (ub, ndx) -> {
                            // variant;
                            long count = decodeLong(ub, ndx);
                            Vector<Variant.Ctor> ctors = new Vector<>();
                            while (count > 0) {
                                byte[] n = decodeString(ub, ndx);
                                int i = decodeInt(ub, ndx);
                                Desc t = decode(ub, ndx);
                                ctors.add(new Variant.Ctor(n, i, t));
                                --count;
                            }
                            return variant(ctors);
                        },
                        (ub, ndx) -> {
                            // record;
                            long count = decodeLong(ub, ndx);
                            Vector<Struct.Field> fields = new Vector<>();
                            while (count > 0) {
                                byte[] n = decodeString(ub, ndx);
                                int o = decodeInt(ub, ndx);
                                Desc t = decode(ub, ndx);
                                fields.add(new Struct.Field(n, o, t));
                                --count;
                            }
                            return record(fields);
                        },
                        (ub, ndx) -> {
                            // nat;
                            return nat(decodeLong(ub, ndx));
                        },
                        (ub, ndx) -> {
                            // app;
                            Desc f = decode(ub, ndx);
                            long count = decodeLong(ub, ndx);
                            Vector<Desc> args = new Vector<>();
                            while (count > 0) {
                                args.add(decode(ub, ndx));
                                --count;
                            }
                            return app(f, args);
                        },
                        (ub, ndx) -> {
                            // recursive;
                            byte[] x = decodeString(ub, ndx);
                            Desc t = decode(ub, ndx);
                            return recursive(x, t);
                        },
                        (ub, ndx) -> {
                            // fn;
                            long count = decodeLong(ub, ndx);
                            Vector<byte[]> args = new Vector<>();
                            while (count > 0) {
                                args.add(decodeString(ub, ndx));
                                --count;
                            }
                            Desc t = decode(ub, ndx);
                            return fn(args, t);
                        });
    }
    public static Desc decode(UnsafeBuffer unsafeBuffer) {
        return decode(unsafeBuffer, Pointer.create(0L));
    }
    public static Fn fn(List<byte[]> args, Desc t) {
        return new Fn(args, t);
    }

    public static Recursive recursive(byte[] x, Desc t) {
        return new Recursive(x, t);
    }
    public static Recursive recursive(String x, Desc t) {
        return recursive(x.getBytes(), t);
    }
    public static Recursive recursive(Desc t) {
        return recursive("x", t);
    }

    public static App app(Desc f, List<Desc> args) {
        return new App(f, args);
    }

    public static Nat nat(long x) {
        return new Nat(x);
    }

    public static Struct record(List<Struct.Field> fields) {
        return new Struct(fields);
    }

    public static Variant variant(List<Variant.Ctor> ctors) {
        return new Variant(ctors);
    }

    public static Arr arr(Desc t) {
        return new Arr(t);
    }

    public static FArr arr(Desc t, Desc len) {
        return new FArr(t, len);
    }

    public static Prim prim(final byte[] n) {
        return new Prim(n, null);
    }
    public static Prim prim(final String n) {
        return prim(n.getBytes());
    }
    public static Prim prim(final byte[] n, final Desc repr) {
        return new Prim(n, repr);
    }
    public static Prim prim(final String n, final Desc repr) {
        return prim(n.getBytes(), repr);
    }

    public static Var var(byte[] n) {
        return new Var(n);
    }
    public static Var var(String n) {
        return var(n.getBytes());
    }

    public static Struct.Field field(final byte[] name, final int offset, final Desc desc) { return new Struct.Field(name, offset, desc); }
    public static Struct.Field field(final String name, final int offset, final Desc desc) { return field(name.getBytes(), offset, desc); }
    public static Struct.Field field(final byte[] name, final long offset, final Desc desc) { return field(name, Math.toIntExact(offset), desc); }
    public static Struct.Field field(final String name, final long offset, final Desc desc) { return field(name.getBytes(), offset, desc); }

    public static Variant.Ctor ctor(final byte[] name, final int tag, final Desc desc) { return new Variant.Ctor(name, tag, desc); }
    public static Variant.Ctor ctor(final String name, final int tag, final Desc desc) { return ctor(name.getBytes(), tag, desc); }

    static <R extends Desc> R decode(UnsafeBuffer unsafeBuffer,
                                     Pointer<java.lang.Long> index,
                                     Thunk0<R> none,
                                     Thunk2<Prim, UnsafeBuffer, Pointer<java.lang.Long>> prim,
                                     Thunk2<Var, UnsafeBuffer, Pointer<java.lang.Long>> var,
                                     Thunk2<FArr, UnsafeBuffer, Pointer<java.lang.Long>> farr,
                                     Thunk2<Arr, UnsafeBuffer, Pointer<java.lang.Long>> arr,
                                     Thunk2<Variant, UnsafeBuffer, Pointer<java.lang.Long>> variant,
                                     Thunk2<Struct, UnsafeBuffer, Pointer<java.lang.Long>> struct,
                                     Thunk2<Nat, UnsafeBuffer, Pointer<java.lang.Long>> nat,
                                     Thunk2<App, UnsafeBuffer, Pointer<java.lang.Long>> app,
                                     Thunk2<Recursive, UnsafeBuffer, Pointer<java.lang.Long>> recursive,
                                     Thunk2<Fn, UnsafeBuffer, Pointer<java.lang.Long>> fn) {
        final int tyCase = new Int.Meta().type().create(unsafeBuffer, index).get();
        switch (tyCase) {
            case PRIM : return (R) prim.apply(unsafeBuffer, index);
            case VAR : return (R) var.apply(unsafeBuffer, index);
            case FARR : return (R) farr.apply(unsafeBuffer, index);
            case ARR : return (R) arr.apply(unsafeBuffer, index);
            case VARIANT : return (R) variant.apply(unsafeBuffer, index);
            case STRUCT : return (R) struct.apply(unsafeBuffer, index);
            case NAT : return (R) nat.apply(unsafeBuffer, index);
            case APP : return (R) app.apply(unsafeBuffer, index);
            case RECURSIVE : return (R) recursive.apply(unsafeBuffer, index);
            case FN : return (R) fn.apply(unsafeBuffer, index);
            default: return none.apply();
        }
    }

    public static Unit encodeBytes(UnsafeBuffer unsafeBuffer, final byte[] n, Pointer<java.lang.Long> index) {
        new Bytes(unsafeBuffer, index).set(n);
        return Unit.unit;
    }

    public static Unit encodeBool(UnsafeBuffer unsafeBuffer, final boolean v, Pointer<java.lang.Long> index) {
        new Bool.Meta().type().create(unsafeBuffer, index).set(v);
        return Unit.unit;
    }
    public static Unit encodeInt(UnsafeBuffer unsafeBuffer, final int i, Pointer<java.lang.Long> index) {
        new Int.Meta().type().create(unsafeBuffer, index).set(i);
        return Unit.unit;
    }
    public static Unit encodeLong(UnsafeBuffer unsafeBuffer, final long l, Pointer<java.lang.Long> index) {
        new Long.Meta().type().create(unsafeBuffer, index).set(l);
        return Unit.unit;
    }
    public Unit encode(final Desc desc, UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
        new Int.Meta().type().create(unsafeBuffer, index).set(desc.tid.get());
        return
                match(
                        () -> Unit.unit,
                        (prim) -> prim.encode(unsafeBuffer, index),
                        (var) -> var.encode(unsafeBuffer, index),
                        (farr) -> farr.encode(unsafeBuffer, index),
                        (arr) -> arr.encode(unsafeBuffer, index),
                        (variant) -> variant.encode(unsafeBuffer, index),
                        (struct) -> struct.encode(unsafeBuffer, index),
                        (nat) -> nat.encode(unsafeBuffer, index),
                        (app) -> app.encode(unsafeBuffer, index),
                        (recursive) -> recursive.encode(unsafeBuffer, index),
                        (fn) -> fn.encode(unsafeBuffer, index)
                );
    }
    public abstract <R> R match(Thunk0<R> none,
                                Thunk1<R, Prim> prim,
                                Thunk1<R, Var> var,
                                Thunk1<R, FArr> farr,
                                Thunk1<R, Arr> arr,
                                Thunk1<R, Variant> variant,
                                Thunk1<R, Struct> struct,
                                Thunk1<R, Nat> nat,
                                Thunk1<R, App> app,
                                Thunk1<R, Recursive> recursive,
                                Thunk1<R, Fn> fn);

    public static class Prim extends Desc {
        public byte[] n;
        public Desc repr;
        Prim(final byte[] n, final Desc repr) {
            super(Enum.Prim);
            this.n = n;
            this.repr = repr;
        }

        @Override
        protected Desc substitute(Iterable<byte[]> vars, Iterable<Desc> args) {
            if (null != repr)
                return prim(n, repr.substitute(vars, args));
            return this;
        }

        @Override
        public Unit invokeEncode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
            encodeBytes(unsafeBuffer, n, index);
            if (null != repr) {
                encodeBool(unsafeBuffer, true, index);
                repr.encode(unsafeBuffer, index);
            } else {
                encodeBool(unsafeBuffer, false, index);
            }
            return Unit.unit;
        }

        @Override
        public <R> R match(Thunk0<R> none, Thunk1<R, Prim> prim, Thunk1<R, Var> var, Thunk1<R, FArr> farr, Thunk1<R, Arr> arr, Thunk1<R, Variant> variant, Thunk1<R, Struct> struct, Thunk1<R, Nat> nat, Thunk1<R, App> app, Thunk1<R, Recursive> recursive, Thunk1<R, Fn> fn) {
            return prim.apply(this);
        }
    }

    public static class Var extends Desc {
        public byte[] n;
        Var(final byte[] n) {
            super(Enum.Var);
            this.n = n;
        }

        @Override
        protected Desc substitute(Iterable<byte[]> vars, Iterable<Desc> args) {
            Iterator<byte[]> v = vars.iterator();
            Iterator<Desc> d = args.iterator();
            while (v.hasNext() && d.hasNext()) {
                byte[] bytes = v.next();
                Desc desc = d.next();
                if (string(bytes).equals(string(n))) {
                    return desc;
                }
            }
            return this;
        }

        @Override
        public Unit invokeEncode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
            encodeBytes(unsafeBuffer, n, index);
            return Unit.unit;
        }

        @Override
        public <R> R match(Thunk0<R> none, Thunk1<R, Prim> prim, Thunk1<R, Var> var, Thunk1<R, FArr> farr, Thunk1<R, Arr> arr, Thunk1<R, Variant> variant, Thunk1<R, Struct> struct, Thunk1<R, Nat> nat, Thunk1<R, App> app, Thunk1<R, Recursive> recursive, Thunk1<R, Fn> fn) {
            return var.apply(this);
        }
    }
    public static class FArr extends Desc {
        public final Desc t;
        public final Desc len;

        FArr(final Desc t, final Desc len) {
            super(Enum.FArr);
            this.t = t;
            this.len = len;
        }

        @Override
        protected Desc substitute(Iterable<byte[]> vars, Iterable<Desc> args) {
            return arr(t.substitute(vars, args), len.substitute(vars, args));
        }

        @Override
        public Unit invokeEncode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
            t.encode(unsafeBuffer, index);
            len.encode(unsafeBuffer, index);
            return Unit.unit;
        }

        @Override
        public <R> R match(Thunk0<R> none, Thunk1<R, Prim> prim, Thunk1<R, Var> var, Thunk1<R, FArr> farr, Thunk1<R, Arr> arr, Thunk1<R, Variant> variant, Thunk1<R, Struct> struct, Thunk1<R, Nat> nat, Thunk1<R, App> app, Thunk1<R, Recursive> recursive, Thunk1<R, Fn> fn) {
            return farr.apply(this);
        }
    }
    public static class Arr extends Desc {
        public final Desc t;

        Arr(final Desc t) {
            super(Enum.Arr);
            this.t = t;
        }

        @Override
        protected Desc substitute(Iterable<byte[]> vars, Iterable<Desc> args) {
            return arr(t.substitute(vars, args));
        }

        @Override
        public Unit invokeEncode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
            t.encode(unsafeBuffer, index);
            return Unit.unit;
        }

        @Override
        public <R> R match(Thunk0<R> none, Thunk1<R, Prim> prim, Thunk1<R, Var> var, Thunk1<R, FArr> farr, Thunk1<R, Arr> arr, Thunk1<R, Variant> variant, Thunk1<R, Struct> struct, Thunk1<R, Nat> nat, Thunk1<R, App> app, Thunk1<R, Recursive> recursive, Thunk1<R, Fn> fn) {
            return arr.apply(this);
        }
    }
    public static class Variant extends Desc {
        public final List<Ctor> ctors;

        @Override
        protected Desc substitute(Iterable<byte[]> vars, Iterable<Desc> args) {
            Vector<Ctor> nctors = new Vector<>();
            for (Ctor c: ctors)
                nctors.add(ctor(c.name, c.tag, c.desc.substitute(vars, args)));
            return variant(nctors);
        }

        @Override
        public Unit invokeEncode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
            encodeLong(unsafeBuffer, ctors.size(), index);
            for (Ctor c : ctors) {
                encodeBytes(unsafeBuffer, c.name, index);
                encodeInt(unsafeBuffer, c.tag, index);
                c.desc.encode(unsafeBuffer, index);
            }
            return Unit.unit;
        }

        @Override
        public <R> R match(Thunk0<R> none, Thunk1<R, Prim> prim, Thunk1<R, Var> var, Thunk1<R, FArr> farr, Thunk1<R, Arr> arr, Thunk1<R, Variant> variant, Thunk1<R, Struct> struct, Thunk1<R, Nat> nat, Thunk1<R, App> app, Thunk1<R, Recursive> recursive, Thunk1<R, Fn> fn) {
            return variant.apply(this);
        }

        public static class Ctor {
            final byte[] name;
            final int tag;
            final Desc desc;

            Ctor(final byte[] name, final int tag, final Desc desc) {
                this.name = name;
                this.tag = tag;
                this.desc = desc;
            }
        }
        Variant(final List<Ctor> ctors) {
            super(Enum.Variant);
            this.ctors = ctors;
        }
    }
    public static class Struct extends Desc {
        public final List<Field> fields;

        @Override
        protected Desc substitute(Iterable<byte[]> vars, Iterable<Desc> args) {
            Vector<Field> nctors = new Vector<>();
            for (Field c: fields)
                nctors.add(field(c.name, c.offset, c.desc.substitute(vars, args)));
            return record(nctors);
        }

        @Override
        public Unit invokeEncode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
            encodeLong(unsafeBuffer, fields.size(), index);
            for (Field f: fields) {
                encodeBytes(unsafeBuffer, f.name, index);
                encodeInt(unsafeBuffer, f.offset, index);
                f.desc.encode(unsafeBuffer, index);
            }
            return Unit.unit;
        }

        @Override
        public <R> R match(Thunk0<R> none, Thunk1<R, Prim> prim, Thunk1<R, Var> var, Thunk1<R, FArr> farr, Thunk1<R, Arr> arr, Thunk1<R, Variant> variant, Thunk1<R, Struct> struct, Thunk1<R, Nat> nat, Thunk1<R, App> app, Thunk1<R, Recursive> recursive, Thunk1<R, Fn> fn) {
            return struct.apply(this);
        }

        @Override
        public Boolean with(byte[] name, Thunk2<Boolean, java.lang.Long, Desc> action) {
            for(final Field f: fields) {
                if (Desc.string(name).equals(f.name())) {
                    return action.apply((long) f.offset, f.desc);
                }
            }
            return super.with(name, action);
        }

        public static class Field {
            final byte[] name;
            final int offset;
            final Desc desc;

            Field(final byte[] name, final int offset, final Desc desc) {
                this.name = name;
                this.offset = offset;
                this.desc = desc;
            }

            public String name() {
                return Desc.string(this.name);
            }
        }
        Struct(final List<Field> fields) {
            super(Enum.Struct);
            this.fields = fields;
        }
    }
    public static class Nat extends Desc {
        public long x;
        Nat(final long x) {
            super(Enum.Nat);
            this.x = x;
        }

        @Override
        public Unit invokeEncode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
            encodeLong(unsafeBuffer, x, index);
            return Unit.unit;
        }

        @Override
        public <R> R match(Thunk0<R> none, Thunk1<R, Prim> prim, Thunk1<R, Var> var, Thunk1<R, FArr> farr, Thunk1<R, Arr> arr, Thunk1<R, Variant> variant, Thunk1<R, Struct> struct, Thunk1<R, Nat> nat, Thunk1<R, App> app, Thunk1<R, Recursive> recursive, Thunk1<R, Fn> fn) {
            return nat.apply(this);
        }
    }
    public static class App extends Desc {
        public Desc f;
        public List<Desc> args;
        App(final Desc f, final List<Desc> args) {
            super(Enum.App);
            this.f = f;
            this.args = args;
        }

        @Override
        protected Desc substitute(Iterable<byte[]> vars, Iterable<Desc> args) {
            Vector<Desc> nargs = new Vector<>();
            for (Desc d: this.args)
                nargs.add(d.substitute(vars, args));
            return app(f.substitute(vars, args), nargs);
        }

        @Override
        public Unit invokeEncode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
            f.encode(unsafeBuffer, index);
            encodeLong(unsafeBuffer, args.size(), index);
            for (Desc arg : args)
                arg.encode(unsafeBuffer, index);
            return Unit.unit;
        }

        @Override
        public <R> R match(Thunk0<R> none, Thunk1<R, Prim> prim, Thunk1<R, Var> var, Thunk1<R, FArr> farr, Thunk1<R, Arr> arr, Thunk1<R, Variant> variant, Thunk1<R, Struct> struct, Thunk1<R, Nat> nat, Thunk1<R, App> app, Thunk1<R, Recursive> recursive, Thunk1<R, Fn> fn) {
            return app.apply(this);
        }
    }
    public static class Recursive extends Desc {
        public byte[] x;
        public Desc t;

        Recursive(final byte[] x, final Desc t) {
            super(Enum.Recursive);
            this.x = x;
            this.t = t;
        }

        @Override
        protected Desc substitute(Iterable<byte[]> vars, Iterable<Desc> args) {
            return recursive(x, t.substitute(vars, args));
        }

        @Override
        public Unit invokeEncode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
            encodeBytes(unsafeBuffer, x, index);
            t.encode(unsafeBuffer, index);
            return Unit.unit;
        }

        @Override
        public <R> R match(Thunk0<R> none, Thunk1<R, Prim> prim, Thunk1<R, Var> var, Thunk1<R, FArr> farr, Thunk1<R, Arr> arr, Thunk1<R, Variant> variant, Thunk1<R, Struct> struct, Thunk1<R, Nat> nat, Thunk1<R, App> app, Thunk1<R, Recursive> recursive, Thunk1<R, Fn> fn) {
            return recursive.apply(this);
        }
    }
    public static class Fn extends Desc {
        public List<byte[]> args;
        public Desc body;

        Fn(final List<byte[]> args, final Desc body) {
            super(Enum.Fn);
            this.args = args;
            this.body = body;
        }

        @Override
        protected Desc substitute(final Iterable<byte[]> vars, final Iterable<Desc> args) {
            final Thunk1<Boolean, byte[]> isFree = (bs) -> {
                for (byte[] b: this.args)
                    if (string(b).equals(string(bs)))
                        return false;
                    return true;
            };

            Iterable<Thunk1<Unit, Thunk2<Unit, byte[], Desc>>> zip = new Iterable<Thunk1<Unit, Thunk2<Unit, byte[], Desc>>>() {
                Iterator<byte[]> varsIt = vars.iterator();
                Iterator<Desc> argsIt = args.iterator();

                @Override
                public Iterator<Thunk1<Unit, Thunk2<Unit, byte[], Desc>>> iterator() {
                    return new Iterator<Thunk1<Unit, Thunk2<Unit, byte[], Desc>>>() {
                        @Override
                        public boolean hasNext() {
                            return varsIt.hasNext() && argsIt.hasNext();
                        }

                        @Override
                        public Thunk1<Unit, Thunk2<Unit, byte[], Desc>> next() {
                            byte[] bs = varsIt.next();
                            Desc d = argsIt.next();
                            if (! isFree.apply(bs))
                                return (th) -> th.apply(bs, d);
                            else
                                return (th) -> th.apply(bs, var(bs));
                        }
                    };
                }
            };

            Vector<byte[]> nvars = new Vector<>();
            Vector<Desc> nargs = new Vector<>();

            for (Thunk1<Unit, Thunk2<Unit, byte[], Desc>> th : zip) {
                th.apply((bs, d) -> {
                    nvars.add(bs);
                    nargs.add(d);
                    return Unit.unit;
                });
            }
            return fn(this.args, body.substitute(nvars, nargs));
        }

        @Override
        public Unit invokeEncode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
            encodeLong(unsafeBuffer, args.size(), index);
            for (final byte[] arg : args)
                encodeBytes(unsafeBuffer, arg, index);
            body.encode(unsafeBuffer, index);
            return Unit.unit;
        }

        @Override
        public <R> R match(Thunk0<R> none, Thunk1<R, Prim> prim, Thunk1<R, Var> var, Thunk1<R, FArr> farr, Thunk1<R, Arr> arr, Thunk1<R, Variant> variant, Thunk1<R, Struct> struct, Thunk1<R, Nat> nat, Thunk1<R, App> app, Thunk1<R, Recursive> recursive, Thunk1<R, Fn> fn) {
            return fn.apply(this);
        }
    }
}
