package org.contract.type;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.Int;
import org.contract.field.base.IndexWire;
import org.contract.utils.Pointer;

public class Tagged extends IndexWire {
    private Int tag;
    public int tag() {
        return tag.get();
    }

    public Tagged(int tag, UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
        super(unsafeBuffer, position);
        this.tag = new Int.Meta().type().create(unsafeBuffer, position);
        this.tag.set(tag);
    }
    public Tagged(UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
        super(unsafeBuffer, position);
        this.tag = new Int.Meta().type().create(unsafeBuffer, position);
    }
}
