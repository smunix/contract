package org.contract.utils;

import org.contract.type.Desc;

// References in Java
public class Pointer<T> {
    public T value;
    public Pointer(T value) {
        this.value = value;
    }

    public static <T> Pointer<T> create(T v) {
        return new Pointer<>(v);
    }

    public Pointer<T> set(T value) {
        this.value = value;
        return this;
    }
    public T get() {
        return value;
    }
}
