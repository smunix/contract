package org.contract.utils.functional;

@FunctionalInterface
public interface Thunk3<R, A0, A1, A2> {
    R apply(A0 a0, A1 a1, A2 a2);
}
