package org.contract.utils;

public class AlignOf {
    public static long apply(long pos, long alignment) {
        if (0 == alignment) return pos;
        if ((pos % alignment) == 0) return pos;
        return alignment * (1 + (pos / alignment));
    }
}
