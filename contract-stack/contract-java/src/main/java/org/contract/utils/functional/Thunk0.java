package org.contract.utils.functional;

@FunctionalInterface
public interface Thunk0<R> {
    R apply();
}
