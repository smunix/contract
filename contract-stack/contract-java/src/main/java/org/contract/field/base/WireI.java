package org.contract.field.base;

import org.agrona.concurrent.UnsafeBuffer;

public interface WireI {
    UnsafeBuffer wire();
    WireI wire(UnsafeBuffer unsafeBuffer);
}
