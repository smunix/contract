package org.contract.field;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.array.Dynamic;
import org.contract.field.base.IndexWire;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.Pointer;
import org.contract.utils.functional.Thunk2;

import java.util.Arrays;

public class String extends IndexWire {
    public static Meta Meta = new Meta();
    public static class Meta {
        public Type<String> Type = type();
        public Type<String> type() {
            return type(0);
        }
        public Type<String> type(final long offset) {
            return new Type<String>(String::new) {
                long off = offset;

                @Override
                public long offsetOf() {
                    return off;
                }

                @Override
                public Type<String> offsetOf(long offset) {
                    off = offset;
                    return this;
                }

                @Override
                public String create(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
                    String r = super.create(unsafeBuffer, index);
                    r.write(r.index());
                    r.write(r.index() + sizeOf());
                    return r;
                }

                @Override
                public long sizeOf(final long count) {
                    return count * new Long.Meta().type().sizeOf();
                }

                @Override
                public Desc describe() {
                    return Desc.app(Desc.fn(Arrays.asList("a".getBytes()), Desc.arr(Desc.var("a".getBytes()))), Arrays.asList(Desc.prim("char".getBytes())));
                }

                @Override
                public long alignOf() {
                    return Long.Meta.Type.alignOf();
                }
            };
        }

    }
    private long offset;

    public String(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> position) {
        super(unsafeBuffer, position);
        this.offset = 0;
        init(Meta);
    }
    public void init(final Meta meta) {
        write(index());
        write(index() + Long.Meta.Type.sizeOf());
    }
    public long offset() {
        return offset;
    }
    public void offset(long offset) {
        this.offset = offset;
    }
    public void offset(int offset) {
        this.offset = offset;
    }

    public String assign(byte[] bytes) {
        if (0 == offset()) {
            offset(write().get() - index());
            wire().putLong(Math.toIntExact(write().get()), bytes.length);
            write(write().get() + Long.Meta.Type.sizeOf());
            wire().putBytes(Math.toIntExact(write().get()), bytes);
            write(write().get() + bytes.length);
        }
        return this;
    }

    public long length() {
        if (0 != offset()) {
            return wire().getLong((int)(index() + offset()));
        }
        return 0;
    }

    public byte[] get() {
        if (0 != offset()) {
            final long len = wire().getLong((int)(index() + offset()));
            byte[] dst = new byte[(int)len];
            wire().getBytes((int)(index() + offset() + Dynamic.Meta.type(Byte.Meta.Type).sizeOf()), dst);
            return dst;
        }
        return null;
    }
}
