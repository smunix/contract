package org.contract.field.array;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.Long;
import org.contract.field.base.IndexI;
import org.contract.field.base.IndexWire;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.Pointer;

import java.util.Arrays;
import java.util.Iterator;

public class Dynamic<E extends IndexI> extends IndexWire {
    public static Meta Meta = new Meta();
    public static class Meta {
        public <F extends IndexI> Type<Dynamic<F>> type(final Type<F> eType) {
            return type(0, eType);
        }
        public <F extends IndexI> Type<Dynamic<F>> type(final long offset, final Type<F> eType) {
            return new Type<Dynamic<F>>((count, unsafeBuffer, base) -> new Dynamic<>(count, eType, unsafeBuffer, base)) {
                long off = offset;

                @Override
                public long offsetOf() {
                    return off;
                }

                @Override
                public Type<Dynamic<F>> offsetOf(long offset) {
                    off = offset;
                    return this;
                }

                @Override
                public long sizeOf(long count) { return Long.Meta.Type.sizeOf(count); }

                @Override
                public Desc describe() {
                    return Desc.app(Desc.fn(Arrays.asList("a".getBytes()), Desc.arr(Desc.var("a".getBytes()))), Arrays.asList(eType.describe()));
                }
                @Override
                public long alignOf() { return Math.max(Long.Meta.type().alignOf(), eType.alignOf()); }

                @Override
                public boolean isArray() {
                    return true;
                }
            };
        }
    }

    private Type<E> eType;

    public Dynamic(final long count, final Type<E> eType, UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> position) {
        super(unsafeBuffer, position);
        this.eType = eType;
        if (0 == wire().getLong((int) index())) {
            wire().putLong((int) index(), count);
            write(index() + Long.Meta.Type.sizeOf() + eType.sizeOf(count));
        }
    }
    public long length() {
        return wire().getLong(Math.toIntExact(index()));
    }

    public Iterable<E> iterate() {
        final long count = length();
        if (0 < count) {
            return new Iterable<E>() {
                final long begin = index() + Long.Meta.Type.sizeOf();
                final long end = begin + eType.sizeOf(count);
                @Override
                public Iterator<E> iterator() {
                    return new Iterator<E>() {
                        long i = begin;
                        final long e = end;
                        @Override
                        public boolean hasNext() {
                            return i < e;
                        }
                        @Override
                        public E next() {
                            try {
                                E r = eType.create(wire(), Pointer.create(i));
                                r.write(write());
                                i += eType.sizeOf();
                                return r;
                            } catch (Throwable throwable) {
                                throwable.printStackTrace();
                            }
                            return null;
                        }
                    };
                }
            };
        }
        return () -> new Iterator<E>() {
            @Override
            public boolean hasNext() {
                return false;
            }
            @Override
            public E next() {
                return null;
            }
        };
    }
}
