package org.contract.field;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.exception.IllegalOffsetError;
import org.contract.field.array.Dynamic;
import org.contract.field.base.IndexI;
import org.contract.field.base.IndexWire;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.Pointer;

import java.util.Arrays;

public class Maybe <E extends IndexI> extends IndexWire {
    public static Meta Meta = new Meta();
    public static class Meta {
        public <F extends IndexI> Type<Maybe<F>> type(final Type<F> eType) {
            return type(eType, 0);
        }
        public <F extends IndexI> Type<Maybe<F>> type(final Type<F> eType, final long offset) {
            return new Type<Maybe<F>>((unsafeBuffer, base) -> new Maybe<>(eType, unsafeBuffer, base)) {
                long off = offset;

                @Override
                public Maybe<F> create(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
                    Maybe<F> r = super.create(unsafeBuffer, index);
                    r.offset(0);
                    r.type = eType;
                    r.write(r.index());
                    r.write(r.index() + sizeOf());
                    return r;
                }

                @Override
                public long offsetOf() {
                    return off;
                }

                @Override
                public Type<Maybe<F>> offsetOf(long offset) {
                    off = offset;
                    return this;
                }

                @Override
                public long sizeOf(final long count) {
                    return count * new Long.Meta().type().sizeOf();
                }
                @Override
                public Desc describe() {
                    return Desc.app(Desc.fn(Arrays.asList("a".getBytes()), Desc.prim("ref".getBytes(), Desc.var("a".getBytes()))), Arrays.asList(eType.describe()));
                }
                @Override
                public long alignOf() {
                    return Long.Meta.Type.alignOf();
                }
            };
        }
    }
    private long offset;
    private Type<E> type;

    private Maybe(Type<E> type, UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> position) {
        super(unsafeBuffer, position);
        this.offset = 0;
        this.type = type;
    }
    public long offset() {
        return offset;
    }
    public void offset(long offset) {
        this.offset = offset;
    }
    public void offset(int offset) {
        this.offset = offset;
    }

    public Maybe<E> set(E e) throws IllegalOffsetError {
        if (0 == offset()) {
            offset(e.index() - this.index());
        } else
            throw new IllegalOffsetError("attempt to set a field twice");
        return this;
    }

    public E array() {
        return array(0);
    }
    public E array(long count) {
        if (0 == offset()) {
            offset(write().get() - index());
            return type.create(count, wire(), write());
        }
        E r = type.create(count, wire(), Pointer.create(index()+offset()));
        r.write(write());
        return r;
    }

    public E assign() {
        if (0 == offset()) {
            offset(write().get() - index());
            return type.create(wire(), write());
        }
        E r = type.create(wire(), Pointer.create(index() + offset()));
        r.write(write());
        return r;
    }
}
