package org.contract.field;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.exception.IllegalConversion;
import org.contract.field.base.IndexWire;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.Pointer;
import org.contract.utils.functional.Thunk1;
import org.contract.utils.functional.Thunk2;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Bool extends IndexWire {
    public static Meta Meta = new Meta();
    public static class Meta {
        public Type<Bool> Type = type();
        public Type<Bool> type() {
            return type(0);
        }
        public Type<Bool> type(final long offset) {
            return new Type<Bool>(Bool::new) {
                long off = offset;

                @Override
                public Bool create(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
                    Bool r = super.create(unsafeBuffer, index);
                    r.write(r.index());
                    r.write(r.index() + sizeOf());
                    return r;
                }

                @Override
                public long offsetOf() {
                    return off;
                }

                @Override
                public Type<Bool> offsetOf(long offset) {
                    off = offset;
                    return this;
                }

                @Override
                public long sizeOf(final long count) {
                    return count * 1;
                }
                @Override
                public Desc describe() {
                    return Desc.prim("bool".getBytes());
                }
                @Override
                public long alignOf() {
                    return 1;
                }
                @Override
                public Boolean from(long off, Desc desc) {
                    Thunk1<Boolean, Desc> err = (d -> {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        d.show(new PrintStream(baos));
                        new IllegalConversion(baos.toString()).printStackTrace(System.err);
                        return false;
                    });
                    Thunk1<Boolean, Boolean> ret = (b -> {
                        this.offsetOf(off);
                        return b;
                    });
                    return desc.match(
                            () -> false,
                            (prim -> ret.apply("bool".equals(Desc.string(prim.n)))),
                            (var -> err.apply(var)),
                            (farr -> err.apply(farr)),
                            (arr -> err.apply(arr)),
                            (variant -> err.apply(variant)),
                            (struct -> err.apply(struct)),
                            (nat -> err.apply(nat)),
                            (app -> err.apply(app)),
                            (recursive -> err.apply(recursive)),
                            (fn -> err.apply(fn))
                    );
                }
            };
        }
    }

    public Bool(final UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> position) {
        super(unsafeBuffer, position);
    }
    public boolean get() { return wire().getByte((int) index()) == 1; }
    public Bool set(boolean v) {
        wire().putByte((int) index(), v ?
                java.lang.Byte.parseByte("1") :
                java.lang.Byte.parseByte("0"));
        return this;
    }
}
