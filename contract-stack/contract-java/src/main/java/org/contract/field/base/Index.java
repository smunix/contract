package org.contract.field.base;

import org.contract.utils.Pointer;

public abstract class Index implements IndexI {
    long index;
    Pointer<Long> write;

    public Index(final Pointer<Long> index) {
        this.index = index.get();
        this.write = index;
    }

    @Override
    public long index() {
        return index;
    }

    @Override
    public void index(final long position) {
        this.index = position;
    }

    @Override
    public void index(final int position) {
        this.index = position;
    }
}
