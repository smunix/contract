package org.contract.field.array;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.exception.IllegalConversion;
import org.contract.field.base.IndexI;
import org.contract.field.base.IndexWire;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.AlignOf;
import org.contract.utils.Pointer;
import org.contract.utils.compile.Long.Static;
import org.contract.utils.functional.Thunk1;
import org.contract.utils.functional.Thunk2;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

public class Fixed<E extends IndexI, S extends Static> extends IndexWire {
    private S sz;
    private Type<E> eType;

    public static Meta Meta = new Meta();
    public static class Meta {
        public <E extends IndexI, S extends Static> Type<Fixed<E, S>> type(final Type<E> eType, final S sz) {
            return type(0, eType, sz);
        }
        public <E extends IndexI, S extends Static> Type<Fixed<E, S>> type(final long offset, final Type<E> eType, final S sz) {
            return new Type<Fixed<E, S>>((unsafeBuffer, base) -> new Fixed<>(sz, eType, unsafeBuffer, base)) {
                long off = offset;

                @Override
                public long offsetOf() {
                    return off;
                }

                @Override
                public Type<Fixed<E, S>> offsetOf(long offset) {
                    off = offset;
                    return this;
                }

                @Override
                public long sizeOf(final long count) {
                    return count * AlignOf.apply(eType.sizeOf(sz.get()), eType.alignOf());
                }

                @Override
                public Desc describe() {
                    return Desc.app(Desc.fn(Arrays.asList("a".getBytes(), "n".getBytes()), Desc.arr(Desc.var("a".getBytes()), Desc.var("n".getBytes()))), Arrays.asList(eType.describe(), Desc.nat(sz.get())));
                }

                @Override
                public Boolean from(long off, Desc desc) {
                    Thunk1<Boolean, Desc> err = (d -> {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        d.show(new PrintStream(baos));
                        new IllegalConversion(baos.toString()).printStackTrace(System.err);
                        return false;
                    });

                    Thunk1<Boolean, Boolean> ret = (b -> {
                        this.offsetOf(off);
                        return b;
                    });

                    return desc.match(
                            () -> false,
                            (prim -> err.apply(prim)),
                            (var -> err.apply(var)),
                            (farr -> err.apply(farr)),
                            (arr -> err.apply(arr)),
                            (variant -> err.apply(variant)),
                            (struct -> err.apply(struct)),
                            (nat -> err.apply(nat)),
                            (app -> app.f.match(
                                    () -> false,
                                    (prim -> err.apply(prim)),
                                    (var -> err.apply(var)),
                                    (farr -> err.apply(desc)),
                                    (arr -> err.apply(arr)),
                                    (variant -> err.apply(variant)),
                                    (struct -> err.apply(struct)),
                                    (nat -> err.apply(nat)),
                                    (app1 -> err.apply(app1)),
                                    (recursive1 -> err.apply(recursive1)),
                                    (fn1 -> fn1.body.match(
                                            (() -> false),
                                            (prim -> err.apply(prim)),
                                            (var -> err.apply(var)),
                                            (fArr -> {
                                                if (app.args.get(1).match(
                                                        () -> false,
                                                        (prim -> err.apply(prim)),
                                                        (var -> err.apply(var)),
                                                        (farr1 -> err.apply(farr1)),
                                                        (arr -> err.apply(arr)),
                                                        (variant -> err.apply(variant)),
                                                        (struct -> err.apply(struct)),
                                                        (nat -> nat.x == sz.get()),
                                                        (app1 -> err.apply(app1)),
                                                        (recursive -> err.apply(recursive)),
                                                        (fn -> err.apply(fn))
                                                )) {
                                                    return ret.apply(eType.from(0, app.args.get(0)));
                                                };
                                                return err.apply(desc);
                                            }),
                                            (arr -> err.apply(arr)),
                                            (variant -> err.apply(variant)),
                                            (struct -> err.apply(struct)),
                                            (nat -> err.apply(nat)),
                                            (app2 -> err.apply(app2)),
                                            (recursive -> err.apply(recursive)),
                                            (fn2 -> err.apply(fn2))
                                    ))
                            )),
                            (recursive -> err.apply(recursive)),
                            (fn -> err.apply(fn))
                    );
                }

                @Override
                public long alignOf() {
                    return eType.alignOf();
                }
            };
        }
    }

    public Fixed(final S sz, final Type<E> eType, UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
        super(unsafeBuffer, position);
        this.sz = sz;
        this.eType = eType;
        init(Meta);
    }
    public void init(Meta meta) {
        write(index() + meta.type(eType, sz).sizeOf());
    }
    public void init(final long offset, Meta meta) {
        write(index() + meta.type(offset, eType, sz).sizeOf());
    }
}
