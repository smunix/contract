package org.contract.generated;

/* file : tests/input_dir/complex.con */
import org.agrona.concurrent.UnsafeBuffer;

public class Complex extends org.contract.field.base.IndexWire {
  public static Meta Meta = new Meta();
  public static class Meta {
    // field meta-descriptors
    public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Int>>> iarr = new org.contract.field.Ref.Meta().type(new org.contract.field.array.Dynamic.Meta().type(0, new org.contract.field.Int.Meta().type(0)), 0);
    public final org.contract.type.Type<org.contract.field.array.Fixed<org.contract.field.Char, org.contract.utils.compile.Long._10>> cfarr = new org.contract.field.array.Fixed.Meta().type(8, new org.contract.field.Char.Meta().type(0), new org.contract.utils.compile.Long._10());
    public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.Float>> fref = new org.contract.field.Ref.Meta().type(new org.contract.field.Float.Meta().type(0), 24);
    public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Double>>> dref = new org.contract.field.Ref.Meta().type(new org.contract.field.array.Dynamic.Meta().type(0, new org.contract.field.Double.Meta().type(0)), 32);

    // type descriptor for record org.contract.generated.Complex
    public org.contract.type.Type<Complex> Type = type();
    public org.contract.type.Type<Complex> type() { return type(0); }
    public org.contract.type.Type<Complex> type(final long offset) {
      return new org.contract.type.Type<Complex>(Complex::new) {
        long off = offset;
        @Override
        public long offsetOf() { return off; }
        @Override
        public org.contract.type.Type<Complex> offsetOf(long offset) {
          off = offset;
          return this;
        }
        @Override
        public Complex create(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> index) {
          Complex r = super.create(unsafeBuffer, index);
          r.write(r.index());
          r.iarr = iarr.create(r.wire(), r.write(r.index() + iarr.offsetOf()));
          r.cfarr = cfarr.create(r.wire(), r.write(r.index() + cfarr.offsetOf()));
          r.fref = fref.create(r.wire(), r.write(r.index() + fref.offsetOf()));
          r.dref = dref.create(r.wire(), r.write(r.index() + dref.offsetOf()));
          r.write(r.index() + sizeOf());
          return r;
        }
        @Override
        public long sizeOf(long eCount) {
          long r = 0;
          r = org.contract.utils.AlignOf.apply(r, iarr.alignOf()) + iarr.sizeOf();
          r = org.contract.utils.AlignOf.apply(r, cfarr.alignOf()) + cfarr.sizeOf();
          r = org.contract.utils.AlignOf.apply(r, fref.alignOf()) + fref.sizeOf();
          r = org.contract.utils.AlignOf.apply(r, dref.alignOf()) + dref.sizeOf();
          return eCount * org.contract.utils.AlignOf.apply(r, alignOf());
        }
        @Override
        public org.contract.type.Desc describe() {
          return org.contract.type.Desc.record(java.util.Arrays.asList(
            org.contract.type.Desc.field("iarr", iarr.offsetOf(), iarr.describe()),
            org.contract.type.Desc.field("cfarr", cfarr.offsetOf(), cfarr.describe()),
            org.contract.type.Desc.field("fref", fref.offsetOf(), fref.describe()),
            org.contract.type.Desc.field("dref", dref.offsetOf(), dref.describe())));
        }
        @Override
        public long alignOf() {
          long r = 0;
          r = Math.max(r, iarr.alignOf());
          r = Math.max(r, cfarr.alignOf());
          r = Math.max(r, fref.alignOf());
          r = Math.max(r, dref.alignOf());
          return r;
        }
        @Override
        public Boolean from(long off, org.contract.type.Desc desc) {
          offsetOf(off);

          boolean r = true;
          r &= desc.with("iarr", iarr::from);
          r &= desc.with("cfarr", cfarr::from);
          r &= desc.with("fref", fref::from);
          r &= desc.with("dref", dref::from);
          return r;
        }
      };
    }
  }
  // all fields
  public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Int>> iarr;
  public org.contract.field.array.Fixed<org.contract.field.Char, org.contract.utils.compile.Long._10> cfarr;
  public org.contract.field.Ref<org.contract.field.Float> fref;
  public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Double>> dref;
  // constructor.
  private Complex(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> position) {
    super(unsafeBuffer, position);
  }
  // process new wire buffer.
  @Override
  public org.contract.field.base.WireI wire(UnsafeBuffer unsafeBuffer) {
    iarr.wire(unsafeBuffer);
    cfarr.wire(unsafeBuffer);
    fref.wire(unsafeBuffer);
    dref.wire(unsafeBuffer);
    return super.wire(unsafeBuffer);
  }
}
