package org.contract.generated.nested2;

/* file : tests/input_dir/contract.2.con */
import org.agrona.concurrent.UnsafeBuffer;

public class Nested extends org.contract.field.base.IndexWire {
  public static class Inner extends org.contract.field.base.IndexWire {
    public static class InnerInner extends org.contract.field.base.IndexWire {
      public static Meta Meta = new Meta();
      public static class Meta {
        // field meta-descriptors
        public final org.contract.type.Type<org.contract.field.Int> salaire = new org.contract.field.Int.Meta().type(0);

        // type descriptor for record org.contract.generated.nested2.Nested.Inner.InnerInner
        public org.contract.type.Type<InnerInner> Type = type();
        public org.contract.type.Type<InnerInner> type() { return type(0); }
        public org.contract.type.Type<InnerInner> type(final long offset) {
          return new org.contract.type.Type<InnerInner>(InnerInner::new) {
            long off = offset;
            @Override
            public long offsetOf() { return off; }
            @Override
            public org.contract.type.Type<InnerInner> offsetOf(long offset) {
              off = offset;
              return this;
            }
            @Override
            public InnerInner create(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> index) {
              InnerInner r = super.create(unsafeBuffer, index);
              r.write(r.index());
              r.salaire = salaire.create(r.wire(), r.write(r.index() + salaire.offsetOf()));
              r.write(r.index() + sizeOf());
              return r;
            }
            @Override
            public long sizeOf(long eCount) {
              long r = 0;
              r = org.contract.utils.AlignOf.apply(r, salaire.alignOf()) + salaire.sizeOf();
              return eCount * org.contract.utils.AlignOf.apply(r, alignOf());
            }
            @Override
            public org.contract.type.Desc describe() {
              return org.contract.type.Desc.record(java.util.Arrays.asList(
                org.contract.type.Desc.field("salaire", salaire.offsetOf(), salaire.describe())));
            }
            @Override
            public long alignOf() {
              long r = 0;
              r = Math.max(r, salaire.alignOf());
              return r;
            }
            @Override
            public Boolean from(long off, org.contract.type.Desc desc) {
              offsetOf(off);

              boolean r = true;
              r &= desc.with("salaire", salaire::from);
              return r;
            }
          };
        }
      }
      // all fields
      public org.contract.field.Int salaire;
      // constructor.
      private InnerInner(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> position) {
        super(unsafeBuffer, position);
      }
      // process new wire buffer.
      @Override
      public org.contract.field.base.WireI wire(UnsafeBuffer unsafeBuffer) {
        salaire.wire(unsafeBuffer);
        return super.wire(unsafeBuffer);
      }
    }
    public static Meta Meta = new Meta();
    public static class Meta {
      // field meta-descriptors
      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Int>>> iarr = new org.contract.field.Ref.Meta().type(new org.contract.field.array.Dynamic.Meta().type(0, new org.contract.field.Int.Meta().type(0)), 0);
      public final org.contract.type.Type<org.contract.field.array.Fixed<org.contract.field.Char, org.contract.utils.compile.Long._10>> cfarr = new org.contract.field.array.Fixed.Meta().type(8, new org.contract.field.Char.Meta().type(0), new org.contract.utils.compile.Long._10());
      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.Float>> fref = new org.contract.field.Ref.Meta().type(new org.contract.field.Float.Meta().type(0), 24);

      // type descriptor for record org.contract.generated.nested2.Nested.Inner
      public org.contract.type.Type<Inner> Type = type();
      public org.contract.type.Type<Inner> type() { return type(0); }
      public org.contract.type.Type<Inner> type(final long offset) {
        return new org.contract.type.Type<Inner>(Inner::new) {
          long off = offset;
          @Override
          public long offsetOf() { return off; }
          @Override
          public org.contract.type.Type<Inner> offsetOf(long offset) {
            off = offset;
            return this;
          }
          @Override
          public Inner create(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> index) {
            Inner r = super.create(unsafeBuffer, index);
            r.write(r.index());
            r.iarr = iarr.create(r.wire(), r.write(r.index() + iarr.offsetOf()));
            r.cfarr = cfarr.create(r.wire(), r.write(r.index() + cfarr.offsetOf()));
            r.fref = fref.create(r.wire(), r.write(r.index() + fref.offsetOf()));
            r.write(r.index() + sizeOf());
            return r;
          }
          @Override
          public long sizeOf(long eCount) {
            long r = 0;
            r = org.contract.utils.AlignOf.apply(r, iarr.alignOf()) + iarr.sizeOf();
            r = org.contract.utils.AlignOf.apply(r, cfarr.alignOf()) + cfarr.sizeOf();
            r = org.contract.utils.AlignOf.apply(r, fref.alignOf()) + fref.sizeOf();
            return eCount * org.contract.utils.AlignOf.apply(r, alignOf());
          }
          @Override
          public org.contract.type.Desc describe() {
            return org.contract.type.Desc.record(java.util.Arrays.asList(
              org.contract.type.Desc.field("iarr", iarr.offsetOf(), iarr.describe()),
              org.contract.type.Desc.field("cfarr", cfarr.offsetOf(), cfarr.describe()),
              org.contract.type.Desc.field("fref", fref.offsetOf(), fref.describe())));
          }
          @Override
          public long alignOf() {
            long r = 0;
            r = Math.max(r, iarr.alignOf());
            r = Math.max(r, cfarr.alignOf());
            r = Math.max(r, fref.alignOf());
            return r;
          }
          @Override
          public Boolean from(long off, org.contract.type.Desc desc) {
            offsetOf(off);

            boolean r = true;
            r &= desc.with("iarr", iarr::from);
            r &= desc.with("cfarr", cfarr::from);
            r &= desc.with("fref", fref::from);
            return r;
          }
        };
      }
    }
    // all fields
    public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Int>> iarr;
    public org.contract.field.array.Fixed<org.contract.field.Char, org.contract.utils.compile.Long._10> cfarr;
    public org.contract.field.Ref<org.contract.field.Float> fref;
    // constructor.
    private Inner(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> position) {
      super(unsafeBuffer, position);
    }
    // process new wire buffer.
    @Override
    public org.contract.field.base.WireI wire(UnsafeBuffer unsafeBuffer) {
      iarr.wire(unsafeBuffer);
      cfarr.wire(unsafeBuffer);
      fref.wire(unsafeBuffer);
      return super.wire(unsafeBuffer);
    }
  }
  public static Meta Meta = new Meta();
  public static class Meta {
    // field meta-descriptors
    public final org.contract.type.Type<org.contract.field.Char> c = new org.contract.field.Char.Meta().type(0);
    public final org.contract.type.Type<org.contract.field.array.Fixed<org.contract.field.array.Fixed<Inner.InnerInner, org.contract.utils.compile.Long._10>, org.contract.utils.compile.Long._10>> innerinnerfarr = new org.contract.field.array.Fixed.Meta().type(4, new org.contract.field.array.Fixed.Meta().type(0, new Inner.InnerInner.Meta().type(0), new org.contract.utils.compile.Long._10()), new org.contract.utils.compile.Long._10());
    public final org.contract.type.Type<org.contract.field.Double> d = new org.contract.field.Double.Meta().type(408);

    // type descriptor for record org.contract.generated.nested2.Nested
    public org.contract.type.Type<Nested> Type = type();
    public org.contract.type.Type<Nested> type() { return type(0); }
    public org.contract.type.Type<Nested> type(final long offset) {
      return new org.contract.type.Type<Nested>(Nested::new) {
        long off = offset;
        @Override
        public long offsetOf() { return off; }
        @Override
        public org.contract.type.Type<Nested> offsetOf(long offset) {
          off = offset;
          return this;
        }
        @Override
        public Nested create(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> index) {
          Nested r = super.create(unsafeBuffer, index);
          r.write(r.index());
          r.c = c.create(r.wire(), r.write(r.index() + c.offsetOf()));
          r.innerinnerfarr = innerinnerfarr.create(r.wire(), r.write(r.index() + innerinnerfarr.offsetOf()));
          r.d = d.create(r.wire(), r.write(r.index() + d.offsetOf()));
          r.write(r.index() + sizeOf());
          return r;
        }
        @Override
        public long sizeOf(long eCount) {
          long r = 0;
          r = org.contract.utils.AlignOf.apply(r, c.alignOf()) + c.sizeOf();
          r = org.contract.utils.AlignOf.apply(r, innerinnerfarr.alignOf()) + innerinnerfarr.sizeOf();
          r = org.contract.utils.AlignOf.apply(r, d.alignOf()) + d.sizeOf();
          return eCount * org.contract.utils.AlignOf.apply(r, alignOf());
        }
        @Override
        public org.contract.type.Desc describe() {
          return org.contract.type.Desc.record(java.util.Arrays.asList(
            org.contract.type.Desc.field("c", c.offsetOf(), c.describe()),
            org.contract.type.Desc.field("innerinnerfarr", innerinnerfarr.offsetOf(), innerinnerfarr.describe()),
            org.contract.type.Desc.field("d", d.offsetOf(), d.describe())));
        }
        @Override
        public long alignOf() {
          long r = 0;
          r = Math.max(r, c.alignOf());
          r = Math.max(r, innerinnerfarr.alignOf());
          r = Math.max(r, d.alignOf());
          return r;
        }
        @Override
        public Boolean from(long off, org.contract.type.Desc desc) {
          offsetOf(off);

          boolean r = true;
          r &= desc.with("c", c::from);
          r &= desc.with("innerinnerfarr", innerinnerfarr::from);
          r &= desc.with("d", d::from);
          return r;
        }
      };
    }
  }
  // all fields
  public org.contract.field.Char c;
  public org.contract.field.array.Fixed<org.contract.field.array.Fixed<Inner.InnerInner, org.contract.utils.compile.Long._10>, org.contract.utils.compile.Long._10> innerinnerfarr;
  public org.contract.field.Double d;
  // constructor.
  private Nested(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> position) {
    super(unsafeBuffer, position);
  }
  // process new wire buffer.
  @Override
  public org.contract.field.base.WireI wire(UnsafeBuffer unsafeBuffer) {
    c.wire(unsafeBuffer);
    innerinnerfarr.wire(unsafeBuffer);
    d.wire(unsafeBuffer);
    return super.wire(unsafeBuffer);
  }
}
