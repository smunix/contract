package org.contract.generated;

/* file : tests/input_dir/simple.1.con */
import org.agrona.concurrent.UnsafeBuffer;

public class Simple extends org.contract.field.base.IndexWire {
  public static Meta Meta = new Meta();
  public static class Meta {
    // field meta-descriptors
    public final org.contract.type.Type<org.contract.field.Int> i = new org.contract.field.Int.Meta().type(0);
    public final org.contract.type.Type<org.contract.field.Char> c = new org.contract.field.Char.Meta().type(4);
    public final org.contract.type.Type<org.contract.field.Float> f = new org.contract.field.Float.Meta().type(8);
    public final org.contract.type.Type<org.contract.field.Double> d = new org.contract.field.Double.Meta().type(16);

    // type descriptor for record org.contract.generated.Simple
    public org.contract.type.Type<Simple> Type = type();
    public org.contract.type.Type<Simple> type() { return type(0); }
    public org.contract.type.Type<Simple> type(final long offset) {
      return new org.contract.type.Type<Simple>(Simple::new) {
        long off = offset;
        @Override
        public long offsetOf() { return off; }
        @Override
        public org.contract.type.Type<Simple> offsetOf(long offset) {
          off = offset;
          return this;
        }
        @Override
        public Simple create(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> index) {
          Simple r = super.create(unsafeBuffer, index);
          r.write(r.index());
          r.i = i.create(r.wire(), r.write(r.index() + i.offsetOf()));
          r.c = c.create(r.wire(), r.write(r.index() + c.offsetOf()));
          r.f = f.create(r.wire(), r.write(r.index() + f.offsetOf()));
          r.d = d.create(r.wire(), r.write(r.index() + d.offsetOf()));
          r.write(r.index() + sizeOf());
          return r;
        }
        @Override
        public long sizeOf(long eCount) {
          long r = 0;
          r = org.contract.utils.AlignOf.apply(r, i.alignOf()) + i.sizeOf();
          r = org.contract.utils.AlignOf.apply(r, c.alignOf()) + c.sizeOf();
          r = org.contract.utils.AlignOf.apply(r, f.alignOf()) + f.sizeOf();
          r = org.contract.utils.AlignOf.apply(r, d.alignOf()) + d.sizeOf();
          return eCount * org.contract.utils.AlignOf.apply(r, alignOf());
        }
        @Override
        public org.contract.type.Desc describe() {
          return org.contract.type.Desc.record(java.util.Arrays.asList(
            org.contract.type.Desc.field("i", i.offsetOf(), i.describe()),
            org.contract.type.Desc.field("c", c.offsetOf(), c.describe()),
            org.contract.type.Desc.field("f", f.offsetOf(), f.describe()),
            org.contract.type.Desc.field("d", d.offsetOf(), d.describe())));
        }
        @Override
        public long alignOf() {
          long r = 0;
          r = Math.max(r, i.alignOf());
          r = Math.max(r, c.alignOf());
          r = Math.max(r, f.alignOf());
          r = Math.max(r, d.alignOf());
          return r;
        }
        @Override
        public Boolean from(long off, org.contract.type.Desc desc) {
          offsetOf(off);

          boolean r = true;
          r &= desc.with("i", i::from);
          r &= desc.with("c", c::from);
          r &= desc.with("f", f::from);
          r &= desc.with("d", d::from);
          return r;
        }
      };
    }
  }
  // all fields
  public org.contract.field.Int i;
  public org.contract.field.Char c;
  public org.contract.field.Float f;
  public org.contract.field.Double d;
  // constructor.
  private Simple(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> position) {
    super(unsafeBuffer, position);
  }
  // process new wire buffer.
  @Override
  public org.contract.field.base.WireI wire(UnsafeBuffer unsafeBuffer) {
    i.wire(unsafeBuffer);
    c.wire(unsafeBuffer);
    f.wire(unsafeBuffer);
    d.wire(unsafeBuffer);
    return super.wire(unsafeBuffer);
  }
}
