package org.contract.type;

import org.agrona.concurrent.UnsafeBuffer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.contract.field.Char;
import org.contract.field.Unit;
import org.contract.field.array.Dynamic;
import org.contract.field.array.Fixed;
import org.contract.record.Record;
import org.contract.record.Struct;
import org.contract.utils.Pointer;
import org.contract.utils.compile.Long._1;
import org.contract.utils.compile.Long._10;
import org.contract.utils.compile.Long._20;
import org.contract.utils.functional.Thunk0;
import org.contract.utils.functional.Thunk1;
import org.contract.variant.Color;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class DescTest {
    ByteBuffer byteBuffer;
    UnsafeBuffer unsafeBuffer;
    Pointer<java.lang.Long> w, r;

    Desc descNone = new Desc(Desc.Enum.None) {
        @Override
        public Unit invokeEncode(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> index) {
            return Unit.unit;
        }
        @Override
        public <R> R match(Thunk0<R> none, Thunk1<R, Prim> prim, Thunk1<R, Var> var, Thunk1<R, FArr> farr, Thunk1<R, Arr> arr, Thunk1<R, Variant> variant, Thunk1<R, Struct> struct, Thunk1<R, Nat> nat, Thunk1<R, App> app, Thunk1<R, Recursive> recursive, Thunk1<R, Fn> fn) {
            return none.apply();
        }
    };
    
    Thunk0<Unit> bottom = Assertions::fail;

    @BeforeEach
    void setUp() {
        w = Pointer.create((long)0);
        r = Pointer.create((long)0);
        byteBuffer = ByteBuffer.allocateDirect(4096);
        unsafeBuffer = new UnsafeBuffer(byteBuffer);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void decodeLong() {
        final long l = 1234;
        Desc.encodeLong(unsafeBuffer, l, w);
        assertEquals(8, w.get());
        assertEquals(l, Desc.decodeLong(unsafeBuffer, r));
        assertEquals(8, r.get());
    }

    @Test
    void decodeInt() {
        final int i = 1234;
        Desc.encodeInt(unsafeBuffer, i, w);
        assertEquals(4, w.get());
        assertEquals(i, Desc.decodeInt(unsafeBuffer, r));
        assertEquals(4, r.get());
    }

    @Test
    void decodeString() {
        final byte[] bytes = "Je ne suis pas un homme libre, P. Abrams".getBytes();
        Desc.encodeBytes(unsafeBuffer, bytes, w);
        assertEquals(48, w.get());
        assertArrayEquals(bytes, Desc.decodeString(unsafeBuffer, r));
        assertEquals(48, r.get());
    }

    @Test
    void decodeBool() {
        Desc.encodeBool(unsafeBuffer, true, w);
        assertEquals(1, w.get());
        assertEquals(true, Desc.decodeBool(unsafeBuffer, r));
        assertEquals(1, r.get());

        Desc.encodeBool(unsafeBuffer, false, w);
        assertEquals(2, w.get());
        assertEquals(false, Desc.decodeBool(unsafeBuffer, r));
        assertEquals(2, r.get());
    }

    @Test
    void fn() {
        {
            Desc.fn(Arrays.asList("x".getBytes(), "y".getBytes()),
                    Desc.prim("data".getBytes(),
                            Desc.var("x".getBytes())))
                    .encode(unsafeBuffer, w);
            Desc d = Desc.decode(unsafeBuffer, r);
            d.match(
                    ()-> descNone,
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (fn) -> {
                        Iterator<byte[]> expected = Arrays.asList("x".getBytes(), "y".getBytes()).iterator();
                        Iterator<byte[]> args = fn.args.iterator();
                        while (expected.hasNext() && args.hasNext()) {
                            assertArrayEquals(expected.next(), args.next());
                        }
                        while (expected.hasNext()) {
                            assertArrayEquals(expected.next(), new byte[]{});
                        }
                        while (args.hasNext()) {
                            assertArrayEquals(new byte[]{}, args.next());
                        }
                        fn.body.match(
                                () -> descNone,
                                (prim) -> {
                                    assertArrayEquals("data".getBytes(), prim.n);
                                    prim.repr.match(
                                            () -> descNone,
                                            (x) -> bottom.apply(),
                                            (var) -> {
                                                assertArrayEquals("x".getBytes(), var.n);
                                                return Unit.unit;
                                            },
                                            (x) -> bottom.apply(),
                                            (x) -> bottom.apply(),
                                            (x) -> bottom.apply(),
                                            (x) -> bottom.apply(),
                                            (x) -> bottom.apply(),
                                            (x) -> bottom.apply(),
                                            (x) -> bottom.apply(),
                                            (x) -> bottom.apply()
                                    );
                                    return Unit.unit;
                                    },
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply()
                        );
                        return Unit.unit;
                    }
            );
        }
    }

    @Test
    void recursive() {
    }

    @Test
    void app() {
    }

    @Test
    void nat() {
    }

    @Test
    void record() {
        {
            Struct.Meta.Type.describe().encode(unsafeBuffer, w);
            Desc d = Desc.decode(unsafeBuffer, r);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            d.show(new PrintStream(baos));
            System.out.println("struct() => " + baos.toString());
            assertEquals("^x.{d:double,i:int,nextM:ref (x),prevM:ref (x),b:bool,s:short,l:long,rdc:ref ([char]),bte:byte,f:float,p:{c:char,d:double,i:int,fi:[int|10]},dataArr:ref ([{c:char,d:double,i:int,fi:[int|10]}]),dataR:ref ({c:char,d:double,i:int,fi:[int|10]})}", baos.toString());
        }
        {
            Record.Meta.Type.describe().encode(unsafeBuffer, w);
            Desc d = Desc.decode(unsafeBuffer, r);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            d.show(new PrintStream(baos));
            System.out.println("record() => " + baos.toString());
            assertEquals("{c:char,d:double,i:int,fi:[int|10]}", baos.toString());
        }
    }

    @Test
    void variant() {
        Color.Meta.Type.describe().encode(unsafeBuffer, w);
        Desc d = Desc.decode(unsafeBuffer, r);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        d.show(new PrintStream(baos));
        System.out.println("variant() => " + baos.toString());
        assertEquals("|red:{author:[char]},green:{pixels:int,author:[char]},blue:{pixels:int}|", baos.toString());
    }

    @Test
    void arr() {
        {
            Fixed.Meta.type(Char.Meta.Type, new _10()).describe().encode(unsafeBuffer, w);
            Desc d = Desc.decode(unsafeBuffer, r);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            d.show(new PrintStream(baos));
            System.out.println("arr() => " + baos.toString());
            assertEquals("[char|10]", baos.toString());

        }
        {
            Fixed.Meta.type(Fixed.Meta.type(Char.Meta.Type, new _20()), new _10()).describe().encode(unsafeBuffer, w);
            Desc d = Desc.decode(unsafeBuffer, r);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            d.show(new PrintStream(baos));
            System.out.println("arr() => " + baos.toString());
            assertEquals("[[char|20]|10]", baos.toString());
        }
        {
            Fixed.Meta.type(Dynamic.Meta.type(Char.Meta.Type), new _10()).describe().encode(unsafeBuffer, w);
            Desc d = Desc.decode(unsafeBuffer, r);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            d.show(new PrintStream(baos));
            System.out.println("arr() => " + baos.toString());
            assertEquals("[[char]|10]", baos.toString());
        }
        {
            Dynamic.Meta.type(Fixed.Meta.type(Char.Meta.Type, new _10())).describe().encode(unsafeBuffer, w);
            Desc d = Desc.decode(unsafeBuffer, r);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            d.show(new PrintStream(baos));
            System.out.println("arr() => " + baos.toString());
            assertEquals("[[char|10]]", baos.toString());
        }
    }

    @Test
    void testShow() {
        {
            Desc.fn(Arrays.asList("x".getBytes(), "y".getBytes()),
                    Desc.prim("data".getBytes(),
                            Desc.var("x".getBytes())))
                    .encode(unsafeBuffer, w);
            Desc d = Desc.decode(unsafeBuffer, r);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            d.show(new PrintStream(baos));
            assertEquals("\\x y -> data (x)", baos.toString());
        }
    }

    @Test
    void prim() {
        {
            byte[] n = "data".getBytes();
            Desc prim = Desc.prim(n, Desc.prim("int".getBytes()));
            prim.encode(unsafeBuffer, w);
            Desc.decode(unsafeBuffer, r).match(
                    ()-> descNone,
                    (primTy) -> {
                        assertArrayEquals(n, primTy.n);
                        return Unit.unit;
                    },
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply()
            );
        }
        {
            byte[] n = "data".getBytes();
            Desc prim = Desc.prim(n, Desc.prim("int".getBytes()));
            prim.encode(unsafeBuffer, w);
            Desc d = Desc.decode(unsafeBuffer, r);
            d.match(
                    ()-> descNone,
                    (primTy) -> {
                        assertArrayEquals(n, primTy.n);
                        primTy.repr.match(()-> descNone,
                                (repr) -> {
                                    assertArrayEquals("int".getBytes(), repr.n);
                                    return Unit.unit;
                                },
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply(),
                                (x) -> bottom.apply());
                        return Unit.unit;
                    },
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply(),
                    (x) -> bottom.apply()
            );
        }
    }
}
