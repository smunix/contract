package org.contract.variant;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.Int;
import org.contract.field.base.IndexI;
import org.contract.field.base.WireI;
import org.contract.field.String;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.AlignOf;
import org.contract.utils.Pointer;
import org.contract.utils.functional.Thunk1;
import org.contract.utils.functional.Thunk2;
import org.contract.type.Tagged;

import java.util.Arrays;

// type Color = |red:{author:[char]}, green:{pixels:int, author:[char]}, blue:{pixels:int}|
public interface Color extends IndexI, WireI {
    enum Value {
        None(-1),
        Red(0),
        Green(1),
        Blue(2)
        ;
        Thunk2<Color, UnsafeBuffer, Pointer<Long>> assigner;
        Value(final int value) {
            assigner = (unsafeBuffer, index) -> {
                if (-1 == value) return getColor(new Tagged(unsafeBuffer, index).tag(), unsafeBuffer, index);
                return getColor(value, unsafeBuffer, index);
            };
        }

        Color getColor(int value, UnsafeBuffer unsafeBuffer, Pointer<Long> index) {
            switch (value) {
                case 0: return new Red(value, unsafeBuffer, index);
                case 1: return new Green(value, unsafeBuffer, index);
                case 2: return new Blue(value, unsafeBuffer, index);
            }
            return new Color() {
                Pointer<Long> ndx = Pointer.create(index.get());
                @Override
                public <R> R match(Thunk1<R, Color.Red> red, Thunk1<R, Color.Green> green, Thunk1<R, Color.Blue> blue) {
                    return null;
                }
                @Override
                public long index() {
                    return ndx.get();
                }
                @Override
                public void index(long position) {

                }
                @Override
                public void index(int position) {

                }

                @Override
                public Pointer<Long> write() {
                    return ndx;
                }

                @Override
                public Pointer<Long> write(Pointer<Long> pointer) {
                    return pointer;
                }

                @Override
                public UnsafeBuffer wire() {
                    return unsafeBuffer;
                }

                @Override
                public WireI wire(UnsafeBuffer unsafeBuffer) {
                    return this;
                }
            };
        }

        Color assign(final UnsafeBuffer unsafeBuffer, final Pointer<Long> position) {
            return assigner.apply(unsafeBuffer, position);
        }
    };

    Color.Meta Meta = new Meta();
    class Meta {
        public Type<Color> Type = type();
        public Type<Color> type() {
            return type(0);
        }
        Type<Color> type(final long offset) {
            return new Type<Color>(Color::assign) {
                long off = offset;

                @Override
                public long offsetOf() {
                    return off;
                }

                @Override
                public Type<Color> offsetOf(long offset) {
                    off = offset;
                    return this;
                }

                @Override
                public long sizeOf(long eCount) {
                    long r = 0;
                    r = Math.max(r, Red.Meta.Type.sizeOf());
                    r = Math.max(r, Green.Meta.Type.sizeOf());
                    r = Math.max(r, Blue.Meta.Type.sizeOf());
                    return eCount * AlignOf.apply(r, alignOf());
                }

                @Override
                public Desc describe() {
                    return Desc.variant(Arrays.asList(
                            Desc.ctor("red".getBytes(), (int) offsetOf(), Red.Meta.type().describe()),
                            Desc.ctor("green".getBytes(), (int) offsetOf(), Green.Meta.type().describe()),
                            Desc.ctor("blue".getBytes(), (int) offsetOf(), Blue.Meta.type().describe())));
                }

                @Override
                public long alignOf() {
                    long r = 0;
                    r = Math.max(r, Red.Meta.Type.alignOf());
                    r = Math.max(r, Green.Meta.Type.alignOf());
                    r = Math.max(r, Blue.Meta.Type.alignOf());
                    return r;
                }
            };
        }
    }

    class Red extends Tagged implements Color {
        public String author;

        public static Red.Meta Meta = new Meta();
        static class Meta {
            public Type<String> author = String.Meta.type(4);

            public Type<Red> Type = type();
            public Type<Red> type() {
                return type(0);
            }
            public Type<Red> type(final long offset) {
                return new Type<Red>((unsafeBuffer, base) -> new Red(0, unsafeBuffer, base)) {
                    long off = offset;

                    @Override
                    public long offsetOf() {
                        return off;
                    }

                    @Override
                    public Type<Red> offsetOf(long offset) {
                        off = offset;
                        return this;
                    }

                    @Override
                    public long sizeOf(long eCount) {
                        long r = 4;
                        r = AlignOf.apply(r, author.alignOf()) + author.sizeOf();
                        return AlignOf.apply(r, alignOf()) * eCount;
                    }

                    @Override
                    public Desc describe() {
                        return Desc.record(Arrays.asList(Desc.field("author".getBytes(), (int) author.offsetOf(), author.describe())));
                    }

                    @Override
                    public long alignOf() {
                        long r = 4;
                        r = Math.max(r, author.alignOf());
                        return r;
                    }
                };
            }
        }

        Red(final int tag, final UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
            super(tag, unsafeBuffer, position);
            init(Red.Meta);
        }
        public void init(Red.Meta meta) {
            author = meta.author.create(wire(), write());
            write(index() + Color.Meta.Type.sizeOf());
        }

        @Override
        public WireI wire(UnsafeBuffer unsafeBuffer) {
            author.wire(unsafeBuffer);
            return super.wire(unsafeBuffer);
        }

        @Override
        public <R> R match(Thunk1<R, Red> red, Thunk1<R, Green> green, Thunk1<R, Blue> blue) {
            return red.apply(this);
        }
    }
    class Blue extends Tagged implements Color {
        public Int pixels;

        public static Blue.Meta Meta = new Meta();
        static class Meta {
            public Type<Int> pixels = Int.Meta.type(4);

            public Type<Blue> Type = type();
            public Type<Blue> type() {
                return type(0);
            }
            public Type<Blue> type(final long offset) {
                return new Type<Blue>((unsafeBuffer, base) -> new Blue(2, unsafeBuffer, base)) {
                    long off = offset;

                    @Override
                    public long offsetOf() {
                        return off;
                    }

                    @Override
                    public Type<Blue> offsetOf(long offset) {
                        off = offset;
                        return this;
                    }

                    @Override
                    public long sizeOf(long eCount) {
                        long r = 4;
                        r = AlignOf.apply(r, pixels.alignOf()) + pixels.sizeOf();
                        return eCount * AlignOf.apply(r, alignOf());
                    }

                    @Override
                    public Desc describe() {
                        return Desc.record(Arrays.asList(Desc.field("pixels".getBytes(), (int) pixels.offsetOf(), pixels.describe())));
                    }

                    @Override
                    public long alignOf() {
                        long r = 4;
                        r = Math.max(r, pixels.alignOf());
                        return r;
                    }
                };
            }
        }

        Blue(final int tag, final UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
            super(tag, unsafeBuffer, position);
            init(Blue.Meta);
        }
        public void init(Blue.Meta meta) {
            pixels = meta.pixels.create(wire(), write());
            write(index() + Color.Meta.Type.sizeOf());
        }

        @Override
        public WireI wire(UnsafeBuffer unsafeBuffer) {
            pixels.wire(unsafeBuffer);
            return super.wire(unsafeBuffer);
        }

        @Override
        public <R> R match(Thunk1<R, Red> red, Thunk1<R, Green> green, Thunk1<R, Blue> blue) {
            return blue.apply(this);
        }
    }
    class Green extends Tagged implements Color {
        public Int pixels;
        public String author;

        public static Green.Meta Meta = new Meta();
        static class Meta {
            public Type<Int> pixels = Int.Meta.type(4);
            public Type<String> author = String.Meta.type(8);
            public Type<Green> Type = type();
            public Type<Green> type() {
                return type(0);
            }
            public Type<Green> type(final long offset) {
                return new Type<Green>((unsafeBuffer, longPointer) -> new Green(1, unsafeBuffer, longPointer)) {
                    long off = offset;

                    @Override
                    public long offsetOf() {
                        return off;
                    }

                    @Override
                    public Type<Green> offsetOf(long offset) {
                        off = offset;
                        return this;
                    }

                    @Override
                    public long sizeOf(long eCount) {
                        long r = 4;
                        r = AlignOf.apply(r, pixels.alignOf()) + pixels.sizeOf();
                        r = AlignOf.apply(r, author.alignOf()) + author.sizeOf();
                        return r;
                    }

                    @Override
                    public Desc describe() {
                        return Desc.record(Arrays.asList(
                                Desc.field("pixels".getBytes(), (int) pixels.offsetOf(), pixels.describe()),
                                Desc.field("author".getBytes(), (int) author.offsetOf(), author.describe())));
                    }

                    @Override
                    public long alignOf() {
                        long r = 4;
                        r = Math.max(r, pixels.alignOf());
                        r = Math.max(r, author.alignOf());
                        return r;
                    }
                };
            }
        }

        Green(final int tag, final UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
            super(tag, unsafeBuffer, position);
            init(Green.Meta);
        }
        public void init(Green.Meta meta) {
            pixels = meta.pixels.create(wire(), write());
            author = meta.author.create(wire(), write());
            write(index() + Color.Meta.Type.sizeOf());
        }

        @Override
        public WireI wire(UnsafeBuffer unsafeBuffer) {
            pixels.wire(unsafeBuffer);
            author.wire(unsafeBuffer);
            return super.wire(unsafeBuffer);
        }

        @Override
        public <R> R match(Thunk1<R, Red> red, Thunk1<R, Green> green, Thunk1<R, Blue> blue) {
            return green.apply(this);
        }
    }
    // introduction - read /  get
    static Color assign(final UnsafeBuffer unsafeBuffer, final Pointer<Long> position) {
        return Value.None.assign(unsafeBuffer, position);
    }
    static Color assign(final UnsafeBuffer unsafeBuffer) {
        return Value.None.assign(unsafeBuffer, Pointer.create(8L));
    }
    // introduction
    static Color red(final UnsafeBuffer unsafeBuffer, final Pointer<Long> position) {
        return Value.Red.assign(unsafeBuffer, position);
    }
    static Color red(final UnsafeBuffer unsafeBuffer) {
        return red(unsafeBuffer, Pointer.create(8L));
    }
    static Color green(UnsafeBuffer unsafeBuffer, final Pointer<Long> position) {
        return Value.Green.assign(unsafeBuffer, position);
    }
    static Color green(final UnsafeBuffer unsafeBuffer) {
        return green(unsafeBuffer, Pointer.create(8L));
    }
    static Color blue(UnsafeBuffer unsafeBuffer, final Pointer<Long> position) {
        return Value.Blue.assign(unsafeBuffer, position);
    }
    static Color blue(final UnsafeBuffer unsafeBuffer) {
        return blue(unsafeBuffer, Pointer.create(8L));
    }

    // elimination
    <R> R match(final Thunk1<R, Red> red, final Thunk1<R, Green> green, final Thunk1<R, Blue> blue);
}
