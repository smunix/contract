package org.contract.variant;

import org.agrona.concurrent.UnsafeBuffer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.contract.field.Unit;
import org.contract.utils.Pointer;
import org.contract.utils.functional.Thunk0;

import java.nio.ByteBuffer;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

// type Color = |red:{author:[char]}, green:{pixels:int, author:[char]}, blue:{pixels:int}|
class ColorTest {
    Color red;
    Color green;
    Color blue;

    final String pete = "I am not a free man. Peter Abrahms, 1956";
    final String paulo = "Sur le bord de la riviere Piedra, je me suis assise et j'ai pleure. Paulo Coelho, 2008";
    final int p1080 = 1080;
    final int p720 = 720;

    final Thunk0<Unit> bottom = Assertions::fail;

    @BeforeEach
    void setUp() {
        // init r
        red = Color.red(new UnsafeBuffer(ByteBuffer.allocateDirect(4096)));
        red.match(
                (red) -> {
                    red.author.assign(pete.getBytes());
                    return Unit.unit;
                },
                (green) -> {
                    green.author.assign(paulo.getBytes());
                    green.pixels.set(p1080);
                    return Unit.unit;
                },
                (blue) -> {
                    blue.pixels.set(p720);
                    return Unit.unit;
                }
        );

        // init g
        green = Color.green(new UnsafeBuffer(ByteBuffer.allocateDirect(4096)));
        green.match(
                (red) -> {
                    red.author.assign(pete.getBytes());
                    return Unit.unit;
                },
                (green) -> {
                    green.author.assign(paulo.getBytes());
                    green.pixels.set(p1080);
                    return Unit.unit;
                },
                (blue) -> {
                    blue.pixels.set(p720);
                    return Unit.unit;
                }
        );
        // init b
        blue = Color.blue(new UnsafeBuffer(ByteBuffer.allocateDirect(4096)));
        blue.match(
                (red) -> {
                    red.author.assign(pete.getBytes());
                    return Unit.unit;
                },
                (green) -> {
                    green.author.assign(paulo.getBytes());
                    green.pixels.set(p1080);
                    return Unit.unit;
                },
                (blue) -> {
                    blue.pixels.set(p720);
                    return Unit.unit;
                }
        );
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testTags() {
        red.match((r)-> { assertEquals(0, r.tag()); return Unit.unit; }, (g)-> bottom.apply(), (b)-> bottom.apply());
        green.match((r)-> bottom.apply(), (g)-> { assertEquals(1, g.tag()); return Unit.unit; }, (b)-> bottom.apply());
        blue.match((r)-> bottom.apply(), (g)-> bottom.apply(), (b)-> { assertEquals(2, b.tag()); return Unit.unit; });

        Color.assign(red.wire(), Pointer.create(8L)).match((r)-> { assertEquals(0, r.tag()); return Unit.unit; }, (g)-> bottom.apply(), (b)-> bottom.apply());
        Color.assign(green.wire(), Pointer.create(8L)).match((r)-> bottom.apply(), (g)-> { assertEquals(1, g.tag()); return Unit.unit; }, (b)-> bottom.apply());
        Color.assign(blue.wire(), Pointer.create(8L)).match((r)-> bottom.apply(), (g)-> bottom.apply(), (b)-> { assertEquals(2, b.tag()); return Unit.unit; });
    }

    @Test
    void testAssignments() {
        // asserts
        red.match(
                (red) -> {
                    assertArrayEquals(pete.getBytes(), red.author.get());
                    return Unit.unit;
                },
                (green) -> bottom.apply(),
                (blue) -> bottom.apply()
        );
        green.match(
                (red) -> bottom.apply(),
                (green) -> {
                    assertArrayEquals(paulo.getBytes(), green.author.get());
                    assertEquals(p1080, green.pixels.get());
                    return Unit.unit;
                },
                (blue) -> bottom.apply()
        );
        blue.match(
                (red) -> bottom.apply(),
                (green) -> bottom.apply(),
                (blue) -> {
                    assertEquals(p720, blue.pixels.get());
                    return Unit.unit;
                }
        );
    }
}
