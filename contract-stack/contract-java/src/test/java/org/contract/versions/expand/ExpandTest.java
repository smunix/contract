package org.contract.versions.expand;

import org.agrona.concurrent.UnsafeBuffer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.contract.type.Desc;
import org.contract.utils.Pointer;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExpandTest {
    UnsafeBuffer unsafeBuffer;
    A1 a1;
    A2 a2;

    @BeforeEach
    void setUp() {
        unsafeBuffer = new UnsafeBuffer(ByteBuffer.allocateDirect(4096));
        a1 = new A1.Meta().type().create(unsafeBuffer, Pointer.create(8L));
        a2 = new A2.Meta().type().create(unsafeBuffer, Pointer.create(8L));
    }

    @Test
    void rearrangeFields() {
        a1.c.set('z');
        a1.n.c.set('n');
        a1.n.i.set(1234);
        a1.d.set(3.14);
        a1.i.set(8282);

        assertEquals('z', a1.c.get());
        assertEquals('n', a1.n.c.get());
        assertEquals(1234, a1.n.i.get());
        assertEquals(3.14, a1.d.get());
        assertEquals(8282, a1.i.get());

        A2.Meta m2 = new A2.Meta();
        m2.c.offsetOf(A1.Meta.c.offsetOf());
        m2.i.offsetOf(A1.Meta.i.offsetOf());
        {
            m2.nMeta.i.offsetOf(A1.Meta.nMeta.i.offsetOf());
            m2.nMeta.c.offsetOf(A1.Meta.nMeta.c.offsetOf());
            m2.n.offsetOf(A1.Meta.n.offsetOf());
        }
        m2.d.offsetOf(A1.Meta.d.offsetOf());

        a2 = m2.type().create(unsafeBuffer, Pointer.create(8L));
        assertEquals(3.14, a2.d.get());
        assertEquals(8282, a2.i.get());
        assertEquals('n', a2.n.c.get());
        assertEquals(1234, a2.n.i.get());
        assertEquals('z', a2.c.get());
    }

    @Test
    void a1() {
        A1.Meta.type().describe().encode(unsafeBuffer);
        Desc d = Desc.decode(unsafeBuffer);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        d.show(new PrintStream(baos));
        System.out.println("A1 => " + baos.toString());
        assertEquals("{c:char,n:{c:char,d:double,i:int},i:int,d:double,fn:[{c:char,d:double,i:int}|10]}", baos.toString());
    }

    @Test
    void a2() {
        A2.Meta.type().describe().encode(unsafeBuffer);
        Desc d = Desc.decode(unsafeBuffer);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        d.show(new PrintStream(baos));
        System.out.println("A2 => " + baos.toString());
        assertEquals("{c:char,i:int,n:{i:int,c:char},d:double,fn:[{i:int,c:char}|10]}", baos.toString());
    }
}
