package org.contract.versions.expand;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.Char;
import org.contract.field.Double;
import org.contract.field.Int;
import org.contract.field.array.Fixed;
import org.contract.field.base.IndexWire;
import org.contract.field.base.WireI;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.AlignOf;
import org.contract.utils.Pointer;
import org.contract.utils.compile.Long._10;

import java.util.Arrays;

public class A1 extends IndexWire {
    public static class Nested extends IndexWire {
        public static Meta Meta = new Meta();
        public static class Meta {
            public Char.Meta cMeta = new Char.Meta();
            public Double.Meta dMeta = new Double.Meta();
            public Int.Meta iMeta = new Int.Meta();

            public Type<Char> c = cMeta.type(0);
            public Type<Double> d = dMeta.type(8);
            public Type<Int> i = iMeta.type(16);

            public Type<Nested> Type = type();
            public Type<Nested> type() {
                return type(0);
            }
            public Type<Nested> type(final long offset) {
                return new Type<Nested>(Nested::new) {
                    long off = offset;

                    @Override
                    public Nested create(UnsafeBuffer unsafeBuffer, Pointer<Long> index) {
                        Nested r = super.create(unsafeBuffer, index);
                        r.write(r.index());
                        r.c = c.create(r.wire(), r.write(r.index() + c.offsetOf()));
                        r.d = d.create(r.wire(), r.write(r.index() + d.offsetOf()));
                        r.i = i.create(r.wire(), r.write(r.index() + i.offsetOf()));
                        r.write(r.index() + sizeOf());
                        return r;
                    }

                    @Override
                    public long offsetOf() {
                        return off;
                    }

                    @Override
                    public Type<Nested> offsetOf(long offset) {
                        off = offset;
                        return this;
                    }

                    @Override
                    public long sizeOf(long eCount) {
                        long r = 0;
                        r = AlignOf.apply(r, c.alignOf()) + c.sizeOf();
                        r = AlignOf.apply(r, d.alignOf()) + d.sizeOf();
                        r = AlignOf.apply(r, i.alignOf()) + i.sizeOf();
                        return eCount * AlignOf.apply(r, alignOf());
                    }

                    @Override
                    public Desc describe() {
                        return Desc.record(Arrays.asList(
                                Desc.field("c", c.offsetOf(), c.describe()),
                                Desc.field("d", d.offsetOf(), d.describe()),
                                Desc.field("i", i.offsetOf(), i.describe())
                        ));
                    }

                    @Override
                    public long alignOf() {
                        long r = 0;
                        r = Math.max(r, c.alignOf());
                        r = Math.max(r, d.alignOf());
                        r = Math.max(r, i.alignOf());
                        return r;
                    }
                };
            }
        }

        public Char c;
        public Double d;
        public Int i;

        private Nested(UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
            super(unsafeBuffer, position);
        }

        @Override
        public WireI wire(UnsafeBuffer unsafeBuffer) {
            c.wire(unsafeBuffer);
            d.wire(unsafeBuffer);
            i.wire(unsafeBuffer);
            return super.wire(unsafeBuffer);
        }
    }

    public static Meta Meta = new Meta();
    public static class Meta {
        public final Char.Meta cMeta = new Char.Meta();
        public final Nested.Meta nMeta = new Nested.Meta();
        public final Int.Meta iMeta = new Int.Meta();
        public final Double.Meta dMeta = new Double.Meta();
        public final Fixed.Meta fnMeta = new Fixed.Meta();

        public final Type<Char> c = cMeta.type(0);
        public final Type<Nested> n = nMeta.type(8);
        public final Type<Int> i = iMeta.type(32);
        public final Type<Double> d = dMeta.type(36);
        public final Type<Fixed<Nested, _10>> fn = fnMeta.type(40, new Nested.Meta().type(0), new _10());

        public Type<A1> Type = type();
        public Type<A1> type() {
            return type(0);
        }
        public Type<A1> type(final long offset) {
            return new Type<A1>(A1::new) {
                long off = offset;

                @Override
                public A1 create(UnsafeBuffer unsafeBuffer, Pointer<Long> index) {
                    A1 r = super.create(unsafeBuffer, index);
                    r.write(r.index());
                    r.c = c.create(r.wire(), r.write(r.index() + c.offsetOf()));
                    r.n = n.create(r.wire(), r.write(r.index() + n.offsetOf()));
                    r.i = i.create(r.wire(), r.write(r.index() + i.offsetOf()));
                    r.d = d.create(r.wire(), r.write(r.index() + d.offsetOf()));
                    r.fn = fn.create(r.wire(), r.write(r.index() + fn.offsetOf()));
                    r.write(r.index() + sizeOf());
                    return r;
                }

                @Override
                public long offsetOf() {
                    return offset;
                }

                @Override
                public Type<A1> offsetOf(long offset) {
                    off = offset;
                    return this;
                }

                @Override
                public long sizeOf(long eCount) {
                    long r = 0;
                    r = AlignOf.apply(r, c.alignOf()) + c.sizeOf();
                    r = AlignOf.apply(r, n.alignOf()) + n.sizeOf();
                    r = AlignOf.apply(r, i.alignOf()) + i.sizeOf();
                    r = AlignOf.apply(r, d.alignOf()) + d.sizeOf();
                    r = AlignOf.apply(r, fn.alignOf()) + fn.sizeOf();
                    return eCount * AlignOf.apply(r, alignOf());
                }

                @Override
                public Desc describe() {
                    return Desc.record(Arrays.asList(
                            Desc.field("c", c.offsetOf(), c.describe()),
                            Desc.field("n", n.offsetOf(), n.describe()),
                            Desc.field("i", i.offsetOf(), i.describe()),
                            Desc.field("d", d.offsetOf(), d.describe()),
                            Desc.field("fn", fn.offsetOf(), fn.describe())
                    ));
                }

                @Override
                public long alignOf() {
                    long r = 0;
                    r = Math.max(r, c.alignOf());
                    r = Math.max(r, n.alignOf());
                    r = Math.max(r, i.alignOf());
                    r = Math.max(r, d.alignOf());
                    r = Math.max(r, fn.alignOf());
                    return r;
                }
            };
        }
    }
    // fields
    public Char c;
    public Nested n;
    public Int i;
    public Double d;
    public Fixed<Nested, _10> fn;

    private A1(UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
        super(unsafeBuffer, position);
    }

    @Override
    public WireI wire(UnsafeBuffer unsafeBuffer) {
        c.wire(unsafeBuffer);
        n.wire(unsafeBuffer);
        i.wire(unsafeBuffer);
        d.wire(unsafeBuffer);
        fn.wire(unsafeBuffer);
        return super.wire(unsafeBuffer);
    }

    public void init(Meta meta) {
        /*
        write(index());
        c = meta.c.create(wire(), write(index() + meta.c.offsetOf()));
        n = meta.n.create(wire(), write(index() + meta.n.offsetOf()));
        n.init(meta.nMeta);
        i = meta.i.create(wire(), write(index() + meta.i.offsetOf()));
        d = meta.d.create(wire(), write(index() + meta.d.offsetOf()));
        fn = meta.fn.create(wire(), write(index() + meta.fn.offsetOf()));

         */
    }
}
