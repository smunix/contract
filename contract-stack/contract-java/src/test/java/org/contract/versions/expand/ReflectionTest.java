package org.contract.versions.expand;

import org.agrona.concurrent.UnsafeBuffer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.contract.type.Desc;
import org.contract.utils.Pointer;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;

public class ReflectionTest {
    UnsafeBuffer transportBuffer;
    UnsafeBuffer handshakeBuffer;
    A1 a1;
    A2 a2;
    A1 a3;
    A2 a4;

    @BeforeEach
    void setUp() {
        transportBuffer = new UnsafeBuffer(ByteBuffer.allocateDirect(4096));
        handshakeBuffer = new UnsafeBuffer(ByteBuffer.allocateDirect(4096));
        a1 = new A1.Meta().type().create(transportBuffer, Pointer.create(8L));
        a2 = new A2.Meta().type().create(new UnsafeBuffer(ByteBuffer.allocateDirect(4096)), Pointer.create(8L));
        a3 = new A1.Meta().type().create(new UnsafeBuffer(ByteBuffer.allocateDirect(4096)), Pointer.create(8L));
        a4 = new A2.Meta().type().create(new UnsafeBuffer(ByteBuffer.allocateDirect(4096)), Pointer.create(8L));
    }

    @Test
    void rearrangeFields() {
        a1.c.set('z');
        a1.n.c.set('n');
        a1.n.i.set(1234);
        a1.d.set(3.14);
        a1.i.set(8282);

        assertEquals('z', a1.c.get());
        assertEquals('n', a1.n.c.get());
        assertEquals(1234, a1.n.i.get());
        assertEquals(3.14, a1.d.get());
        assertEquals(8282, a1.i.get());

        A2.Meta m2 = new A2.Meta();
        m2.c.offsetOf(A1.Meta.c.offsetOf());
        m2.i.offsetOf(A1.Meta.i.offsetOf());
        {
            m2.nMeta.i.offsetOf(A1.Meta.nMeta.i.offsetOf());
            m2.nMeta.c.offsetOf(A1.Meta.nMeta.c.offsetOf());
            m2.n.offsetOf(A1.Meta.n.offsetOf());
        }
        m2.d.offsetOf(A1.Meta.d.offsetOf());

        a2 = m2.type().create(a1.wire(), Pointer.create(8L));
        a2.wire(a1.wire());
        assertEquals(3.14, a2.d.get());
        assertEquals(8282, a2.i.get());
        assertEquals('n', a2.n.c.get());
        assertEquals(1234, a2.n.i.get());
        assertEquals('z', a2.c.get());
    }


    @Test
    void a1() {
        A1.Meta.type().describe().encode(handshakeBuffer);
        Desc d = Desc.decode(handshakeBuffer);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        d.show(new PrintStream(baos));
        System.out.println("A1 => " + baos.toString());
        assertEquals("{c:char,n:{c:char,d:double,i:int},i:int,d:double,fn:[{c:char,d:double,i:int}|10]}", baos.toString());
    }

    @Test
    void a2() {
        A2.Meta.type().describe().encode(handshakeBuffer);
        Desc d = Desc.decode(handshakeBuffer);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        d.show(new PrintStream(baos));
        System.out.println("A2 => " + baos.toString());
        assertEquals("{c:char,i:int,n:{i:int,c:char},d:double,fn:[{i:int,c:char}|10]}", baos.toString());
    }

    @Test
    void reflect() {
        a1.c.set('z');
        a1.n.c.set('n');
        a1.n.i.set(1234);
        a1.n.d.set(2.6868);
        a1.d.set(3.14);
        a1.i.set(8282);

        a3.c.set('q');
        a3.n.c.set('m');
        a3.n.i.set(4321);
        a3.n.d.set(6868.2);
        a3.d.set(41.3);
        a3.i.set(2828);

        a4.c.set('t');
        a4.n.c.set('k');
        a4.n.i.set(4325);
        a4.d.set(45.3);
        a4.i.set(2929);

        {
            new A1.Meta().type().describe().encode(handshakeBuffer);
            A2.Meta metaA1 = new A2.Meta();
            assertTrue(metaA1.type().from(0, Desc.decode(handshakeBuffer)));
            // now reading from v1
            a2 = metaA1.type().create(new UnsafeBuffer(ByteBuffer.allocateDirect(4096)), Pointer.create(8L));
            {
                // new buffer (v1) : conversion from A1.1 -> A2.1
                a2.wire(new UnsafeBuffer(a1.wire().byteBuffer().duplicate()));
                assertEquals(3.14, a2.d.get());
                assertEquals(8282, a2.i.get());
                assertEquals('n', a2.n.c.get());
                assertEquals(1234, a2.n.i.get());
                assertEquals('z', a2.c.get());
            }
            {
                // new buffer (v1) : conversion from A3.1 -> A2.1
                a2.wire(new UnsafeBuffer(a3.wire().byteBuffer().duplicate()));
                a1.c.set('a');
                a1.n.c.set('m');
                a1.n.i.set(1235);
                a1.n.d.set(3.6868);
                a1.d.set(4.14);
                a1.i.set(8283);

                assertEquals(41.3, a2.d.get());
                assertEquals(2828, a2.i.get());
                assertEquals('m', a2.n.c.get());
                assertEquals(4321, a2.n.i.get());
                assertEquals('q', a2.c.get());
            }
            // now reading from v2
            A2.Meta.type().describe().encode(handshakeBuffer);
            A2.Meta metaA2 = new A2.Meta();
            metaA2.type().from(0, Desc.decode(handshakeBuffer));
            a2 = metaA2.type().create(new UnsafeBuffer(ByteBuffer.allocateDirect(4096)), Pointer.create(8L));
            {
                // new buffer (v2) : conversion from A4.2 -> A2.2
                a2.wire(new UnsafeBuffer(a4.wire().byteBuffer().duplicate()));
                assertEquals(45.3, a2.d.get());
                assertEquals(2929, a2.i.get());
                assertEquals('k', a2.n.c.get());
                assertEquals(4325, a2.n.i.get());
                assertEquals('t', a2.c.get());
            }
        }
    }
}
