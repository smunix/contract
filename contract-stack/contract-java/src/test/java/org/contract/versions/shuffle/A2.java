package org.contract.versions.shuffle;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.Char;
import org.contract.field.Double;
import org.contract.field.Int;
import org.contract.field.base.IndexWire;
import org.contract.field.base.WireI;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.AlignOf;
import org.contract.utils.Pointer;

import java.util.Arrays;

public class A2 extends IndexWire {
    public static Meta Meta = new Meta();
    public static class Meta {
        public final Type<Double> d = Double.Meta.type(0);
        public final Type<Int> i = Int.Meta.type(8);
        public final Type<Char> c = Char.Meta.type(12);

        public Type<A2> Type = type();
        public Type<A2> type() {
            return type(0);
        }
        public Type<A2> type(final long offset) {
            return new Type<A2>(A2::new) {
                long off = offset;

                @Override
                public long offsetOf() {
                    return off;
                }

                @Override
                public org.contract.type.Type<A2> offsetOf(long offset) {
                    off = offset;
                    return this;
                }

                @Override
                public A2 create(UnsafeBuffer unsafeBuffer, Pointer<Long> index) {
                    A2 r = super.create(unsafeBuffer, index);
                    r.write(r.index());
                    r.d = d.create(r.wire(), r.write(r.index() + d.offsetOf()));
                    r.i = i.create(r.wire(), r.write(r.index() + i.offsetOf()));
                    r.c = c.create(r.wire(), r.write(r.index() + c.offsetOf()));
                    r.write(r.index() + sizeOf());
                    return r;
                }

                @Override
                public long sizeOf(long eCount) {
                    long r = 0;
                    r = AlignOf.apply(r, d.alignOf()) + d.sizeOf();
                    r = AlignOf.apply(r, i.alignOf()) + i.sizeOf();
                    r = AlignOf.apply(r, c.alignOf()) + c.sizeOf();
                    return eCount*AlignOf.apply(r, alignOf());
                }

                @Override
                public Desc describe() {
                    return Desc.record(Arrays.asList(
                            Desc.field("d", d.offsetOf(), d.describe()),
                            Desc.field("i", i.offsetOf(), i.describe()),
                            Desc.field("c", c.offsetOf(), c.describe())
                    ));
                }

                @Override
                public long alignOf() {
                    long r = 0;
                    r = Math.max(r, d.alignOf());
                    r = Math.max(r, i.alignOf());
                    r = Math.max(r, c.alignOf());
                    return r;
                }
            };
        }
    }

    public Double d;
    public Int i;
    public Char c;

    public A2(UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
        super(unsafeBuffer, position);
    }

    @Override
    public WireI wire(UnsafeBuffer unsafeBuffer) {
        d.wire(unsafeBuffer);
        i.wire(unsafeBuffer);
        c.wire(unsafeBuffer);
        return super.wire(unsafeBuffer);
    }
}
