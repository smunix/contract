package org.contract.versions.nested;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.Char;
import org.contract.field.Double;
import org.contract.field.Int;
import org.contract.field.base.IndexWire;
import org.contract.field.base.WireI;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.AlignOf;
import org.contract.utils.Pointer;

import java.util.Arrays;

public class A2 extends IndexWire {
    public static class Nested extends IndexWire {
        public static Meta Meta = new Meta();
        public static class Meta {
            public Type<Int> i = Int.Meta.type(0);
            public Type<Char> c = Char.Meta.type(4);
            public Type<Nested> type(final long offset) {
                return new Type<Nested>(Nested::new) {
                    long off = offset;

                    @Override
                    public long offsetOf() {
                        return off;
                    }

                    @Override
                    public Type<Nested> offsetOf(long offset) {
                        off = offset;
                        return this;
                    }

                    @Override
                    public Nested create(UnsafeBuffer unsafeBuffer, Pointer<Long> index) {
                        Nested r = super.create(unsafeBuffer, index);
                        r.write(r.index());
                        r.i = i.create(r.wire(), r.write(r.index() + i.offsetOf()));
                        r.c = c.create(r.wire(), r.write(r.index() + c.offsetOf()));
                        r.write(r.index() + sizeOf());
                        return r;
                    }

                    @Override
                    public long sizeOf(long eCount) {
                        long r = 0;
                        r = AlignOf.apply(r, i.alignOf()) + i.sizeOf();
                        r = AlignOf.apply(r, c.alignOf()) + c.sizeOf();
                        return eCount*AlignOf.apply(r, alignOf());
                    }

                    @Override
                    public Desc describe() {
                        return Desc.record(Arrays.asList(
                                Desc.field("i", i.offsetOf(), i.describe()),
                                Desc.field("c", c.offsetOf(), c.describe())
                        ));
                    }

                    @Override
                    public long alignOf() {
                        long r = 0;
                        r = Math.max(r, i.alignOf());
                        r = Math.max(r, c.alignOf());
                        return r;
                    }
                };
            }
        }

        public Int i;
        public Char c;

        public Nested(UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
            super(unsafeBuffer, position);
            init(Meta);
        }

        @Override
        public WireI wire(UnsafeBuffer unsafeBuffer) {
            i.wire(unsafeBuffer);
            c.wire(unsafeBuffer);
            return super.wire(unsafeBuffer);
        }

        public void init(Meta meta) {
            write(index());
            i = meta.i.create(wire(), write(index() + meta.i.offsetOf()));
            c = meta.c.create(wire(), write(index() + meta.c.offsetOf()));
        }
    }

    public static Meta Meta = new Meta();
    public static class Meta {
        public final A2.Nested.Meta Nested = new A2.Nested.Meta();

        public final Type<Char> c = Char.Meta.type(0);
        public final Type<Int> i = Int.Meta.type(4);
        public final Type<Nested> n = Nested.type(8);
        public final Type<Double> d = Double.Meta.type(16);

        public Type<A2> Type = type();
        public Type<A2> type() {
            return type(0);
        }
        public Type<A2> type(final long offset) {
            return new Type<A2>(A2::new) {
                long off = offset;

                @Override
                public long offsetOf() {
                    return offset;
                }

                @Override
                public Type<A2> offsetOf(long offset) {
                    off = offset;
                    return this;
                }

                @Override
                public A2 create(UnsafeBuffer unsafeBuffer, Pointer<Long> index) {
                    A2 r = super.create(unsafeBuffer, index);
                    r.write(r.index());
                    r.c = c.create(r.wire(), r.write(r.index() + c.offsetOf()));
                    r.i = i.create(r.wire(), r.write(r.index() + i.offsetOf()));
                    r.n = n.create(r.wire(), r.write(r.index() + n.offsetOf()));
                    r.d = d.create(r.wire(), r.write(r.index() + d.offsetOf()));
                    r.write(r.index() + sizeOf());
                    return r;
                }

                @Override
                public long sizeOf(long eCount) {
                    long r = 0;
                    r = AlignOf.apply(r, c.alignOf()) + c.sizeOf();
                    r = AlignOf.apply(r, i.alignOf()) + i.sizeOf();
                    r = AlignOf.apply(r, n.alignOf()) + n.sizeOf();
                    r = AlignOf.apply(r, d.alignOf()) + d.sizeOf();
                    return eCount*AlignOf.apply(r, alignOf());
                }

                @Override
                public Desc describe() {
                    return Desc.record(Arrays.asList(
                            Desc.field("c", c.offsetOf(), c.describe()),
                            Desc.field("i", i.offsetOf(), i.describe()),
                            Desc.field("n", n.offsetOf(), n.describe()),
                            Desc.field("d", d.offsetOf(), d.describe())
                    ));
                }

                @Override
                public long alignOf() {
                    long r = 0;
                    r = Math.max(r, c.alignOf());
                    r = Math.max(r, i.alignOf());
                    r = Math.max(r, n.alignOf());
                    r = Math.max(r, d.alignOf());
                    return r;
                }
            };
        }
    }
    // fields
    public Char c;
    public Int i;
    public Nested n;
    public Double d;

    private A2(UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
        super(unsafeBuffer, position);
    }

    @Override
    public WireI wire(UnsafeBuffer unsafeBuffer) {
        c.wire(unsafeBuffer);
        i.wire(unsafeBuffer);
        n.wire(unsafeBuffer);
        d.wire(unsafeBuffer);
        return super.wire(unsafeBuffer);
    }
}
