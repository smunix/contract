package org.contract.versions.shuffle;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.Char;
import org.contract.field.Double;
import org.contract.field.Int;
import org.contract.field.base.IndexWire;
import org.contract.field.base.WireI;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.AlignOf;
import org.contract.utils.Pointer;

import java.util.Arrays;

public class A1 extends IndexWire {
    public static Meta Meta = new Meta();
    public static class Meta {
        public final Type<Char> c = Char.Meta.type(0);
        public final Type<Int> i = Int.Meta.type(4);
        public final Type<Double> d = Double.Meta.type(8);

        public Type<A1> Type = type();
        public Type<A1> type() {
            return type(0);
        }
        public Type<A1> type(final long offset) {
            return new Type<A1>(A1::new) {
                long off = offset;

                @Override
                public long offsetOf() {
                    return offset;
                }

                @Override
                public Type<A1> offsetOf(long offset) {
                    off = offset;
                    return this;
                }

                @Override
                public A1 create(UnsafeBuffer unsafeBuffer, Pointer<Long> index) {
                    A1 r = super.create(unsafeBuffer, index);
                    r.write(r.index());
                    r.c = c.create(r.wire(), r.write(r.index() + c.offsetOf()));
                    r.i = i.create(r.wire(), r.write(r.index() + i.offsetOf()));
                    r.d = d.create(r.wire(), r.write(r.index() + d.offsetOf()));
                    r.write(r.index() + sizeOf());
                    return r;
                }

                @Override
                public long sizeOf(long eCount) {
                    long r = 0;
                    r = AlignOf.apply(r, c.alignOf()) + c.sizeOf();
                    r = AlignOf.apply(r, i.alignOf()) + i.sizeOf();
                    r = AlignOf.apply(r, d.alignOf()) + d.sizeOf();
                    return eCount*AlignOf.apply(r, alignOf());
                }

                @Override
                public Desc describe() {
                    return Desc.record(Arrays.asList(
                            Desc.field("c", c.offsetOf(), c.describe()),
                            Desc.field("i", i.offsetOf(), i.describe()),
                            Desc.field("d", d.offsetOf(), d.describe())
                    ));
                }

                @Override
                public long alignOf() {
                    long r = 0;
                    r = Math.max(r, c.alignOf());
                    r = Math.max(r, i.alignOf());
                    r = Math.max(r, d.alignOf());
                    return r;
                }
            };
        }
    }
    // fields
    public Char c;
    public Int i;
    public Double d;

    private A1(UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
        super(unsafeBuffer, position);
    }

    @Override
    public WireI wire(UnsafeBuffer unsafeBuffer) {
        c.wire(unsafeBuffer);
        i.wire(unsafeBuffer);
        d.wire(unsafeBuffer);
        return super.wire(unsafeBuffer);
    }
}
