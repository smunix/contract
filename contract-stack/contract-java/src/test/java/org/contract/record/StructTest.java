package org.contract.record;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.utils.Pointer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.contract.field.Char;
import org.contract.field.Double;
import org.contract.field.Int;
import org.contract.field.Maybe;
import org.contract.field.array.Dynamic;
import org.contract.field.array.Fixed;
import org.contract.utils.AlignOf;
import org.contract.utils.compile.Long._1;
import org.contract.utils.compile.Long._10;
import org.contract.utils.compile.Long._2;

import java.nio.ByteBuffer;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StructTest {

    Struct struct;

    @BeforeEach
    void setUp() {
        struct = new Struct.Meta().type().create(new UnsafeBuffer(ByteBuffer.allocateDirect(4096)), Pointer.create(8L));
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testInt() {
        struct.setInt(10);
        assertEquals(10, struct.getInt());
        struct.setInt(30);
        assertEquals(30, struct.getInt());
    }

    @Test
    void testDouble() {
        struct.setDouble(10.0);
        assertEquals(10.0, struct.getDouble());
        struct.setDouble(30.0);
        assertEquals(30, struct.getDouble());
    }

    @Test
    void testMaybeRef() throws Throwable {
        assertEquals(0, struct.getNextMaybe().offset());
        Struct nextOrder = struct.getNextMaybe().assign();
        assertEquals(128, struct.getNextMaybe().offset());

        nextOrder.setDouble(60.0);
        nextOrder.setInt(70);
        assertEquals(60.0, nextOrder.getDouble());
        assertEquals(70, nextOrder.getInt());

        struct.setDouble(20.0);
        struct.setInt(30);
        assertEquals(0, nextOrder.getPrevMaybe().offset());
        nextOrder.getPrevMaybe().set(struct);
        assertEquals(-168, nextOrder.getPrevMaybe().offset());
        assertEquals(20.0, nextOrder.getPrevMaybe().assign().getDouble());
        assertEquals(30, nextOrder.getPrevMaybe().assign().getInt());
    }

    @Test
    void testBool() {
        struct.setBool(true);
        assertEquals(true, struct.getBool());
        struct.setBool(false);
        assertEquals(false, struct.getBool());
    }

    @Test
    void testShort() {
        struct.setShort((short)10);
        assertEquals(10, struct.getShort());

        struct.setShort((short)50000);
        // todo : salumup : Java short max value is 2^15-1; i.e. ~32700
        assertEquals(-15536, struct.getShort());

        struct.setShort((short)32700);
        assertEquals(32700, struct.getShort());
    }

    @Test
    void testLong() {
        struct.setLong(10);
        assertEquals(10, struct.getLong());

        struct.setLong(50000);
        assertEquals(50000, struct.getLong());

        struct.setLong(32700);
        assertEquals(32700, struct.getLong());
    }

    @Test
    void testRefDynamicChar() {
        int i;
        assertEquals(0, struct.getRDC().offset());

        i = 0;
        struct.getRDC().array(10).iterate();
        for (final Char c : struct.getRDC().array(10).iterate()) {
            c.set((char) ((int) 'A' + i));
            i++;
        }
        assertEquals(10, struct.getRDC().array(10).length());
        i = 0;
        for (final Char c : struct.getRDC().array().iterate()) {
            assertEquals((char) ((int) 'A' + i), c.get());
            i++;
        }

        final Dynamic<Char> dyn = struct.getRDC().array();
        final Iterable<Char> seq = dyn.iterate();
        final Iterator<Char> it = seq.iterator();

        if (it.hasNext()) assertEquals('A', it.next().get());
        if (it.hasNext()) assertEquals('B', it.next().get());
        if (it.hasNext()) assertEquals('C', it.next().get());
        if (it.hasNext()) assertEquals('D', it.next().get());
        if (it.hasNext()) assertEquals('E', it.next().get());
        if (it.hasNext()) assertEquals('F', it.next().get());
        if (it.hasNext()) assertEquals('G', it.next().get());
        if (it.hasNext()) assertEquals('H', it.next().get());
        if (it.hasNext()) assertEquals('I', it.next().get());
        if (it.hasNext()) assertEquals('J', it.next().get());
        if (it.hasNext()) assertEquals('Z', it.next().get());
    }

    @Tag("org.contract.record.StructTest.testRefDynamicData")
    @Test
    void testRefDynamicData() {
        int i;
        assertEquals(0, struct.getDataArr().offset());

        i = 0;
        for (final Record d : struct.getDataArr().array(10).iterate()) {
            d.setI(i).setC((char) ((int) 'A' + i)).setD(i);
            i++;
        }

        i = 0;
        for (final Record d : struct.getDataArr().array().iterate()) {
            assertEquals(i, d.getI());
            assertEquals((char) ((int) 'A' + i), d.getC());
            assertEquals(i, d.getD());
            i++;
        }

        i = 0;
        for (final Char c : struct.getRDC().array().iterate()) {
            assertEquals((char) ((int) 'A' + i), c.get());
            i++;
        }

        final Dynamic<Record> dyn = struct.getDataArr().array();
        final Iterable<Record> seq = dyn.iterate();
        final Iterator<Record> it = seq.iterator();

        if (it.hasNext()) assertEquals('A', it.next().getC());
        if (it.hasNext()) assertEquals('B', it.next().getC());
        if (it.hasNext()) assertEquals('C', it.next().getC());
        if (it.hasNext()) assertEquals('D', it.next().getC());
        if (it.hasNext()) assertEquals('E', it.next().getC());
        if (it.hasNext()) assertEquals('F', it.next().getC());
        if (it.hasNext()) assertEquals('G', it.next().getC());
        if (it.hasNext()) assertEquals('H', it.next().getC());
        if (it.hasNext()) assertEquals('I', it.next().getC());
        if (it.hasNext()) assertEquals('J', it.next().getC());
        if (it.hasNext()) assertEquals('Z', it.next().getC());
    }

    @Test
    void testRefData() {
        assertEquals(0, struct.getDataR().offset());
        struct.getDataR().assign().setD(10.0).setC('P').setI(20);

        final Record d = struct.getDataR().assign();
        assertEquals(20, d.getI());
        assertEquals(10.0, d.getD());
        assertEquals('P', d.getC());

        d.setI(2*d.getI()).setD(2*d.getD()).setC('p');
        assertEquals(40, d.getI());
        assertEquals(20.0, d.getD());
        assertEquals('p', d.getC());

        struct.getDataR().assign().setI(2*d.getI()).setD(2*d.getD()).setC('s');
        assertEquals(80, struct.getDataR().assign().getI());
        assertEquals(40.0, struct.getDataR().assign().getD());
        assertEquals('s', struct.getDataR().assign().getC());
    }

    @Test
    void testSizeOf() {
        assertEquals(144, Struct.Meta.Type.sizeOf(1));
        assertEquals(8, Struct.Meta.Type.alignOf());
        assertEquals(8, Fixed.Meta.type(Struct.Meta.Type, new _1()).alignOf());
        assertEquals(144, Fixed.Meta.type(Struct.Meta.Type, new _1()).sizeOf(1));
        assertEquals(288, Fixed.Meta.type(Struct.Meta.Type, new _2()).sizeOf(1));
        assertEquals(576, Fixed.Meta.type(Struct.Meta.Type, new _2()).sizeOf(2));
        assertEquals(288, Fixed.Meta.type(Struct.Meta.Type, new _1()).sizeOf(2));
        assertEquals(1440, Fixed.Meta.type(Struct.Meta.Type, new _10()).sizeOf(1));
    }

    @Test
    void testPos() {
        long pos = 8; // must be multiple of 8

        pos = AlignOf.apply(pos, Double.Meta.Type.alignOf());
        assertEquals(8, pos);
        pos += Double.Meta.Type.sizeOf(1);
        assertEquals(16, pos);

        pos = AlignOf.apply(pos, Int.Meta.Type.alignOf());
        assertEquals(16, pos);
        pos += Int.Meta.Type.sizeOf(1);
        assertEquals(20, pos);

        assertEquals(8, Maybe.Meta.type(Struct.Meta.Type).alignOf());
        assertEquals(24, Maybe.Meta.type(Struct.Meta.Type).alignOf()*(1+(pos/Maybe.Meta.type(Struct.Meta.Type).alignOf())));

        pos = AlignOf.apply(pos, Maybe.Meta.type(Struct.Meta.Type).alignOf());
        assertEquals(24, pos);
        pos += Maybe.Meta.type(Struct.Meta.Type).sizeOf(1);
        assertEquals(32, pos);

        assertEquals(8, struct.index());
    }
}
