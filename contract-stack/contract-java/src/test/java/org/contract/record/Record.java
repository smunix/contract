package org.contract.record;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.Char;
import org.contract.field.Double;
import org.contract.field.Int;
import org.contract.field.array.Fixed;
import org.contract.field.base.IndexWire;
import org.contract.field.base.WireI;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.AlignOf;
import org.contract.utils.Pointer;
import org.contract.utils.compile.Long._10;

import java.util.Arrays;

public class Record extends IndexWire {
    public static Meta Meta = new Meta();
    public static class Meta {
        final Char.Meta cMeta = new Char.Meta();
        final Double.Meta dMeta = new Double.Meta();
        final Int.Meta iMeta = new Int.Meta();
        final Fixed.Meta fiMeta = new Fixed.Meta();

        final Type<Char> c = cMeta.type(0);
        final Type<Double> d = dMeta.type(8);
        final Type<Int> i = iMeta.type(16);
        final Type<Fixed<Int, _10>> fi = fiMeta.type(20, Int.Meta.Type, new _10());

        public Type<Record> Type = type();
        public Type<Record> type() {
            return type(0);
        }
        public Type<Record> type(final long offset) {
            return new Type<Record>(Record::new) {
                long off = offset;

                @Override
                public long offsetOf() {
                    return off;
                }

                @Override
                public Type<Record> offsetOf(long offset) {
                    off = offset;
                    return this;
                }

                @Override
                public Record create(UnsafeBuffer unsafeBuffer, Pointer<Long> index) {
                    Record r = super.create(unsafeBuffer, index);
                    r.write(r.index());
                    r.c = c.create(r.wire(), r.write(r.index() + c.offsetOf()));
                    r.d = d.create(r.wire(), r.write(r.index() + d.offsetOf()));
                    r.i = i.create(r.wire(), r.write(r.index() + i.offsetOf()));
                    r.fi = fi.create(r.wire(), r.write(r.index() + fi.offsetOf()));
                    r.write(r.index() + sizeOf());
                    return r;
                }

                @Override
                public long sizeOf(long eCount) {
                    long r = 0;
                    r = AlignOf.apply(r, c.alignOf()) + c.sizeOf();
                    r = AlignOf.apply(r, d.alignOf()) + d.sizeOf();
                    r = AlignOf.apply(r, i.alignOf()) + i.sizeOf();
                    r = AlignOf.apply(r, fi.alignOf()) + fi.sizeOf();
                    return eCount * AlignOf.apply(r, alignOf());
                }

                @Override
                public Desc describe() {
                    return Desc.record(Arrays.asList(
                            Desc.field("c".getBytes(), c.offsetOf(), c.describe()),
                            Desc.field("d".getBytes(), d.offsetOf(), d.describe()),
                            Desc.field("i".getBytes(), i.offsetOf(), i.describe()),
                            Desc.field("fi".getBytes(), fi.offsetOf(), fi.describe())
                    ));
                }

                @Override
                public long alignOf() {
                    long r = 0;
                    r = Math.max(r, c.alignOf());
                    r = Math.max(r, d.alignOf());
                    r = Math.max(r, i.alignOf());
                    r = Math.max(r, fi.alignOf());
                    return r;
                }
            };
        }
    }

    private Char c; // {0:0-0}
    private Double d;   // {8:8-15}
    private Int i; // {4:16-19}
    private Fixed<Int, _10> fi;

    public char getC() {
        return c.get();
    }

    public Record setC(char c) {
        this.c.set(c);
        return this;
    }

    public double getD() {
        return d.get();
    }

    public Record setD(double d) {
        this.d.set(d);
        return this;
    }

    public int getI() {
        return i.get();
    }

    public Record setI(int i) {
        this.i.set(i);
        return this;
    }

    public Fixed<Int, _10> getFi() {
        return fi;
    }

    private Record(UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
        super(unsafeBuffer, position);
    }

    @Override
    public WireI wire(UnsafeBuffer unsafeBuffer) {
        c.wire(unsafeBuffer);
        d.wire(unsafeBuffer);
        i.wire(unsafeBuffer);
        fi.wire(unsafeBuffer);
        return super.wire(unsafeBuffer);
    }
}
