package org.contract.string;

import org.agrona.concurrent.UnsafeBuffer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.contract.field.String;
import org.contract.type.Type;
import org.contract.utils.Pointer;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class StringTest {
    String string;

    @BeforeEach
    void setUp() {
        Type<String> typeString = String.Meta.Type;
        string = typeString.create(new UnsafeBuffer(ByteBuffer.allocateDirect(4096)), Pointer.create (8L));
    }

    @AfterEach
    void tearDown() {
    }

    // References in Java
    public class R<T> {
        public T v;
        public R(T v) {
            this.v = v;
        }
    }
    <T> R<T> r(T v) {
        return new R<T>(v);
    }
    void fn(R<Integer> i) {
        i.v = 0;
    }
    @Test
    void testSizeOf() {
        assertEquals(16, string.write().get());
        assertEquals(8, String.Meta.Type.sizeOf());
        final byte[] text = "Je ne suis pas un homme libre".getBytes();
        string.assign(text);
        assertArrayEquals(text, string.get());
        R<Integer> i = r(new Integer(10));
        assertEquals(10, i.v);
        fn(i);
        assertEquals(0, i.v);
    }
}
