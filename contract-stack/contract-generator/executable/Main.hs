-- It is generally a good idea to keep all your business logic in your library
-- and only use it in the executable. Doing so allows others to use what you
-- wrote in their libraries.

import qualified Example

import qualified Debug.Trace.ByteString as Debug

import qualified Language.Contract.Generator.Normalize as N
import qualified Language.Contract.Generator.Java as Java

import Control.Monad.IO.Class
import Data.Functor.Identity
import Data.String.Encode          (convertString)
import GHC.Generics                ( Generic, Generic1 )
import Control.Monad               ( mapM, void, when )
import Control.DeepSeq             ( NFData )
import Data.Data                   ( Data )
import Data.Typeable               ( Typeable )
import Data.Coerce                 ( coerce )
import Data.List (foldl', foldr)
import qualified System.IO as IO
import qualified Data.Time.LocalTime as LocalTime
import qualified Data.HashMap.Strict as HM
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString.Lazy.Char8 as LC
import qualified Data.ByteString.Builder as B
import Data.ByteString.Short as B ( fromShort
                                  , toShort
                                  , ShortByteString
                                  )

import qualified Language.Contract.Parser as Parser
import qualified Language.Contract.Parser.Internal as Internal
import Language.Contract.Data hiding (Ident, name)

import Path ( Path
            , Abs
            , Dir
            , File
            , (</>)
            , toFilePath
            , fromAbsFile
            )
import qualified Path
import qualified Path.IO as Path
import Options.Applicative as Opt

data Language where
  Java :: Language
  Cpp :: Language
  Python :: Language
  deriving (Show)

data Config where
  Config :: { cfgLanguage :: Language
            , cfgOutputDir :: !(Path Abs Dir)
            , cfgInputFiles :: ![(Path Abs File)]
            , cfgForce :: Bool
            } -> Config
  deriving (Show)

rootedDir :: Path Abs Dir -> ReadM (Path Abs Dir)
rootedDir root = either id (root </>) <$> (absDir <|> relDir)
  where
    absDir = Left <$> maybeReader Path.parseAbsDir
    relDir = Right <$> maybeReader Path.parseRelDir

rootedFile :: Path Abs Dir -> ReadM (Path Abs File)
rootedFile root = either id (root </>) <$> (absFile <|> relFile)
  where
    absFile = Left <$> maybeReader Path.parseAbsFile
    relFile = Right <$> maybeReader Path.parseRelFile

parseJavaLanguage :: ParserInfo Language
parseJavaLanguage = info (helper <*> javaLanguageParser) (progDesc "generate Java based contracts")
  where
    javaLanguageParser :: Parser Language
    javaLanguageParser = pure Java

parseCppLanguage :: ParserInfo Language
parseCppLanguage =  info (helper <*> cppLanguageParser) (progDesc "generate Cpp based contracts")
  where
    cppLanguageParser :: Parser Language
    cppLanguageParser = pure Cpp

parsePythonLanguage :: ParserInfo Language
parsePythonLanguage =  info (helper <*> pythonLanguageParser) (progDesc "generate Python based contracts")
  where
    pythonLanguageParser :: Parser Language
    pythonLanguageParser = pure Python

languageParser :: Parser Language
languageParser = subparser ( command "java" parseJavaLanguage
                            <> command "cpp" parseCppLanguage
                            <> command "python" parsePythonLanguage
                           )

outputDirParser :: Path Abs Dir -> Parser (Path Abs Dir)
outputDirParser root = option
  (rootedDir root)
  ( long "output_dir"
    <> short 'o'
    <> metavar "OUTPUT_DIR"
    <> help "output directory"
  )

inputFilesParser :: Path Abs Dir -> Parser [Path Abs File]
inputFilesParser root = some $ option
                        (rootedFile root)
                        ( long "input_file"
                          <> short 'i'
                          <> metavar "INPUT_FILE"
                          <> help "contract input file"
                        )

forceParser :: Parser Bool
forceParser = flag False True ( long "force"
                              <> short 'f'
                              <> help "force write output files"
                              )

configParser :: Path Abs Dir -> Parser Config
configParser root = do
  cfgLanguage <- languageParser
  cfgOutputDir <- outputDirParser root
  cfgInputFiles <- inputFilesParser root
  cfgForce <- forceParser
  pure $ Config{..}

version :: Parser (a -> a)
version = infoOption
  "Generate Contract version 0.0.0"
  ( long "version"
  <> short 'v'
  <> help "Display Contract version"
  )

argsParser :: Path Abs Dir -> ParserInfo Config
argsParser root = info
  (version <*> helper <*> configParser root)
  (fullDesc <> progDesc "Generate Contract for Java, Cpp, Python...")

parseConfig :: (MonadIO m) => m Config
parseConfig = do
  root <- Path.getCurrentDir
  liftIO $ execParser (argsParser root)

main :: IO ()
main = do
  ztime <- LocalTime.getZonedTime
  cfg@Config{..} <- parseConfig
  Path.createDirIfMissing True cfgOutputDir
  flip mapM cfgInputFiles do
    \file -> do
      fileExist <- Path.doesFileExist file
      when
        fileExist
        do
          IO.withFile
            (Path.fromAbsFile file)
            IO.ReadMode
            \h -> do
              is <- Debug.traceEvent (C.pack $ show file) $ hReadInputStream h
              let
                parsedFile = Debug.traceEvent (C.pack $ inputStreamToString is) $ Parser.run Internal.parseFile is initPosition

              either
                (putStrLn . show)
                (\fileExpr -> do
                    let
                      expr' = N.toExpr N.Java Nothing fileExpr
                      env' = N.Env $ N.toExprEnv N.Java <> N.toExprEnv expr'
                      -- ^ Expression Virtual Filesystem
                      evfs = Java.run env' (Java.contractGen expr')

                    Debug.traceEvent (C.pack $ show fileExpr) $ either
                      (putStrLn. show)
                      (\ (Java.VFS vfs) ->
                          HM.foldlWithKey'
                          (\ acc fp Java.FileContent{..} -> do
                              case Path.parseRelFile fp of
                                Just relFile -> when (maybe False ( == ".java") $ Path.fileExtension relFile) do
                                  let
                                    outputFile :: Path Abs File
                                    outputFile = cfgOutputDir </> relFile
                                    rwMode
                                      | cfgForce = IO.ReadWriteMode
                                      | otherwise = IO.ReadMode

                                  Path.createDirIfMissing True (Path.parent outputFile)
                                  Debug.traceEvent (C.pack $ show outputFile) $ IO.withFile
                                    (Path.fromAbsFile outputFile)
                                    rwMode
                                    (\h -> foldr
                                           (\ !ln !acc -> B.hPutBuilder h ln >> acc)
                                           (pure ())
                                           fcStore
                                    )
                                _ -> do
                                  putStrLn $ "<" <> show fp <> "> isn't a relative path."
                                  return ()
                              acc
                          )
                          (pure ())
                          vfs
                      )
                      evfs
                )
                parsedFile
              return ()
  Example.main
