#pragma once
#include <org/contract/field/Prim.h>

namespace org {
  namespace contract {
    namespace field {
      using Long = Prim<int64_t>;
    }
  }
}
