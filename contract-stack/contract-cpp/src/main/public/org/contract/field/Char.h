#pragma once
#include <org/contract/field/Prim.h>

namespace org {
  namespace contract {
    namespace field {
      using Char = Prim<char>;
    }
  }
}
