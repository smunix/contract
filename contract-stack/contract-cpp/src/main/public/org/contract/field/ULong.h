#pragma once
#include <org/contract/field/Prim.h>

namespace org {
  namespace contract {
    namespace field {
      using ULong = Prim<uint64_t>;
    }
  }
}
