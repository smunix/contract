#pragma once
#include <org/contract/Utils.h>
#include <org/contract/field/ULong.h>
#include <org/contract/field/array/Fixed.h>

namespace org {
  namespace contract {
    namespace field {
      namespace array {
        template<class T>
        struct Dynamic {
          using type_t = T;
          using EMeta = typename type_t::Meta;
          using EType = typename EMeta::Type;

          using rank_n_type = utils::_1;

          struct Meta : std::enable_shared_from_this<Meta> {
            struct Type : std::enable_shared_from_this<Type> {
              // Type ctor
              static auto create(SPtr<Meta> meta, off_t off, SPtr<EType> eType) -> SPtr<Type> {
                auto r = SPtr<Type>(new Type(meta, off, eType));
                return r;
              }
              // Type properties
              // create(...)
              auto create(LValue<Wire> wirev, RValue<index_t> ndx) -> Dynamic {
                index_t index = ndx;
                return create(wirev, index);
              }
              auto create(LValue<Wire> wirev, LValue<index_t> index) -> Dynamic {
                auto tmp = index + offsetOf();
                auto r = Dynamic::create(this->shared_from_this(), wirev, index = (tmp));
                index = (tmp + sizeOf(1, r.size()));
                return r;
              }
              auto create(sz_t sz, LValue<Wire> wirev, RValue<index_t> ndx) -> Dynamic {
                index_t index = ndx;
                return create(sz, wirev, index);
              }
              auto create(sz_t sz, LValue<Wire> wirev, LValue<index_t> index) -> Dynamic {
                auto tmp = index + offsetOf();
                auto r = Dynamic::create(this->shared_from_this(), sz, wirev, index = (tmp));
                index = (tmp + sizeOf(1, sz));
                return r;
              }
              // sizeOf(...)
              auto sizeOf() const -> sz_t {
                return sizeOf(1, 0);
              }
              auto sizeOf(sz_t eCount) const -> sz_t {
                return sizeOf(1, eCount);
              }
              auto sizeOf(sz_t count, sz_t eCount) const -> sz_t {
                return count * (lenType()->sizeOf() + elementType()->sizeOf(eCount));
              }
              // offsetOf(...)
              auto offsetOf() const -> off_t {
                return off;
              }
              auto offsetOf(off_t off) -> void {
                this->off = off;
              }
              // alignOf(...)
              auto alignOf() const -> sz_t {
                return field::Long::Meta::create()->type()->sizeOf();
              }
              // element type()
              auto elementType() const -> SPtr<EType> {
                return eType;
              }
              // len type()
              auto lenType() const -> SPtr<ULong::Meta::Type> {
                return lenTy;
              }
            private:
              Type(SPtr<Meta> meta, off_t off, SPtr<EType> eType) : meta(meta),
                                                                    off(off),
                                                                    lenTy(ULong::Meta::create()->type(off)),
                                                                    eType(eType)
              {}

              SPtr<Meta> meta; // meta description
              off_t off; // offset
              SPtr<ULong::Meta::Type> lenTy;
              SPtr<typename type_t::Meta::Type> eType;
            };
            // Meta ctors
            static auto create() -> SPtr<Meta> {
              return SPtr<Meta>(new Meta);
            }
            // get type object at a given offset off
            auto type(SPtr<EType> eType) -> SPtr<Type> {
              return Meta::type(0, eType);
            }
            auto type(off_t off, SPtr<EType> eType) -> SPtr<Type> {
              return Type::create(this->shared_from_this(), off, eType);
            }
          private:
            Meta() = default;
          };
          // getter and setter
          auto get(sz_t j) -> type_t {
            return type()->elementType()->create(wirev, index() + len.type()->sizeOf() + (type()->elementType()->sizeOf() * std::min(size(), j)));
          }
          auto get(sz_t j) const -> type_t const {
            return type()->elementType()->create(wirev, index() + len.type()->sizeOf() + (type()->elementType()->sizeOf() * std::min(size(), j)));
          }
          // apply a function to each slot
          template<class Fn>
          auto forEach(Fn &&fn) -> void {
            if (auto sz = size()) {
              auto pos = index() + len.type()->sizeOf();
              for (auto j = 0; j < sz; ++j) {
                auto iter = type()->elementType()->create(wirev, pos);
                fn(iter);
              }
            }
          }
          // apply a function to each slot
          template<class Fn>
          auto forEach(Fn &&fn) const -> void {
            if (auto sz = size()) {
              auto pos = index() + len.type()->sizeOf();
              for (auto j = 0; j < sz; ++j) {
                auto iter = type()->elementType()->create(wirev, pos);
                fn(iter);
              }
            }
          }
          // size and length
          auto size() const -> sz_t {
            return len;
          }
          auto length() const -> sz_t {
            return size();
          }
          // new wire
          auto wire(LValue<Wire> wirev) -> void {
            this->wirev = wirev;
            len.wire(wirev);
          }
          // change index
          auto index() -> LValue<index_t> {
            return this->indexv;
          }
          auto index() const -> LValue<index_t const> {
            return this->indexv;
          }
          auto index(index_t indexv) -> void {
            index() = indexv + type()->offsetOf();
            len.index() = index() + len.type()->offsetOf();
          }
          // type
          auto type() const -> SPtr<typename Meta::Type> {
            return tY;
          }
          // ctors
          Dynamic() {
            index_t i = 0L;
            new (this) Dynamic(Meta::create()->type(utils::tyCon<type_t>::create())->create(Wire::null(), i));
          }
          Dynamic(LValue<Dynamic const>) = default;
        private:
          auto length(sz_t sz) -> void {
            size(sz);
          }
          auto size(sz_t sz) -> void {
            len = (sz);
          }
          // friends are declared here
          friend class Meta::Type;

          // create a new dynamic for reading
          static auto create(SPtr<typename Meta::Type> tY,
                             LValue<Wire> wirev,
                             LValue<index_t> indexv) -> Dynamic {
            auto r = Dynamic(tY, wirev, indexv);
            r.len = tY->lenType()->create(wirev, indexv);
            return r;
          }
          // create a new dynamic for writing
          static auto create(SPtr<typename Meta::Type> tY,
                             sz_t sz,
                             LValue<Wire> wirev,
                             LValue<index_t> indexv) -> Dynamic {
            auto r = Dynamic(tY, wirev, indexv);
            r.len = tY->lenType()->create(wirev, indexv);
            r.size(sz);
            return r;
          }
        private:
          Dynamic(SPtr<typename Meta::Type> tY,
                  LValue<Wire> wirev,
                  LValue<index_t> indexv) : tY(tY),
                                         wirev(wirev),
                                         indexv(indexv) {}
          ULong len;
          SPtr<typename Meta::Type> tY;
          Wire wirev;
          index_t indexv = 0;
        };
      }
    }
  }
}
