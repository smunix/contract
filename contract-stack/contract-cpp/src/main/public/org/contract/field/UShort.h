#pragma once
#include <org/contract/field/Prim.h>

namespace org {
  namespace contract {
    namespace field {
      using UShort = Prim<uint16_t>;
    }
  }
}
