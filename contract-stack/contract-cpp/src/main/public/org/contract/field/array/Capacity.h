#pragma once
#include <org/contract/Utils.h>
#include <org/contract/field/ULong.h>
#include <org/contract/field/array/Fixed.h>

namespace org {
  namespace contract {
    namespace field {
      namespace array {
        template<class T, sz_t N>
        struct Capacity {
          using type_t = T;
          using EMeta = typename type_t::Meta;
          using EType = typename EMeta::Type;

          using rank_n_type = utils::_1;

          struct Meta : std::enable_shared_from_this<Meta> {
            struct Type : std::enable_shared_from_this<Type> {
              // Type ctor
              static auto create(SPtr<Meta> meta, off_t off, SPtr<EType> eType) -> SPtr<Type> {
                auto r = SPtr<Type>(new Type(meta, off, eType));
                return r;
              }
              // Type properties
              // create(...)
              auto create(LValue<Wire> wirev, RValue<index_t> ndx) -> Capacity {
                index_t index = ndx;
                return create(wirev, index);
              }
              auto create(LValue<Wire> wirev, LValue<index_t> index) -> Capacity {
                auto tmp = index + offsetOf();
                auto r = Capacity::create(this->shared_from_this(), wirev, index = (tmp));
                index = (tmp + sizeOf());
                return r;
              }
              // sizeOf(...)
              auto sizeOf() const -> sz_t {
                return sizeOf(1);
              }
              auto sizeOf(sz_t count) const -> sz_t {
                return count * (lenType()->sizeOf() + payloadType()->sizeOf());
              }
              // offsetOf(...)
              auto offsetOf() const -> off_t {
                return off;
              }
              auto offsetOf(off_t off) -> void {
                this->off = off;
              }
              // alignOf(...)
              auto alignOf() const -> sz_t {
                return field::Long::Meta::create()->type()->sizeOf();
              }
              // element type()
              auto elementType() const -> SPtr<EType> {
                return payloadType()->elementType();
              }
              // len type()
              auto lenType() const -> SPtr<ULong::Meta::Type> {
                return lenTy;
              }
              // payload type()
              auto payloadType() const -> SPtr<typename Fixed<type_t, N>::Meta::Type> {
                return payloadTy;
              }
            private:
              Type(SPtr<Meta> meta, off_t off, SPtr<EType> eType) : meta(meta),
                                                                    off(off),
                                                                    lenTy(ULong::Meta::create()->type(off)),
                                                                    payloadTy(Fixed<type_t, N>::Meta::create()->type(off + lenTy->sizeOf(), eType))
              {}

              SPtr<Meta> meta; // meta description
              off_t off; // offset
              SPtr<ULong::Meta::Type> lenTy;
              SPtr<typename Fixed<type_t, N>::Meta::Type> payloadTy;
            };
            // Meta ctors
            static auto create() -> SPtr<Meta> {
              return SPtr<Meta>(new Meta);
            }
            // get type object at a given offset off
            auto type(SPtr<EType> eType) -> SPtr<Type> {
              return Meta::type(0, eType);
            }
            auto type(off_t off, SPtr<EType> eType) -> SPtr<Type> {
              return Type::create(this->shared_from_this(), off, eType);
            }
          private:
            Meta() = default;
          };
          // getter and setter
          auto get(sz_t j) -> type_t {
            index_t pos = index();
            auto payload = tY->payloadType()->create(wirev, pos);
            return payload.get(j);
          }
          auto get(sz_t j) const -> type_t const {
            index_t pos = index();
            auto payload = tY->payloadType()->create(wirev, pos);
            return payload.get(j);
          }
          template<class Fn>
          auto forEach(Fn &&fn) -> void {
            if (auto sz = size()) {
              auto eTy = type()->elementType();
              index_t pos = index() + type()->payloadType()->offsetOf();
              for (auto j = 0; j < sz; ++j) {
                auto iter = eTy->create(wirev, pos);
                fn(iter);
              }
            }
          }
          template<class Fn>
          auto forEach(Fn &&fn) const -> void {
            if (auto sz = size()) {
              auto eTy = type()->elementType();
              index_t pos = index() + type()->payloadType()->offsetOf();
              for (auto j = 0; j < sz; ++j) {
                auto iter = eTy->create(wirev, pos);
                fn(iter);
              }
            }
          }
          // size and length
          auto size() const -> sz_t {
            return len;
          }
          auto size(sz_t sz) -> void {
            len = (std::min(sz, N));
          }
          auto length() const -> sz_t {
            return size();
          }
          auto length(sz_t sz) -> void {
            size(sz);
          }
          // new wire
          auto wire(LValue<Wire> wirev) -> void {
            this->wirev = wirev;
            len.wire(wirev);
          }
          // change index
          auto index() -> LValue<index_t> {
            return this->indexv;
          }
          auto index() const -> LValue<index_t const> {
            return this->indexv;
          }
          auto index(index_t indexv) -> void {
            index() = indexv + type()->offsetOf();
            len.index() = index() + len.type()->offsetOf();
          }
          // type
          auto type() const -> SPtr<typename Meta::Type> {
            return tY;
          }
          // ctors
          Capacity() {
            new (this) Capacity(Meta::create()->type(utils::tyCon<type_t>::create())->create(Wire::null(), 0L));
          }
          Capacity(LValue<Capacity const>) = default;
        private:
          // friends are declared here
          friend class Meta::Type;

          // create a new capacity
          static auto create(SPtr<typename Meta::Type> tY,
                           LValue<Wire> wirev,
                           LValue<index_t> indexv) -> Capacity {
            auto r = Capacity(tY, wirev, indexv);
            r.len = tY->lenType()->create(wirev, indexv);
            if (not wirev.isNull())
              r.len = (0UL);
            return r;
          }
          // create a new capacity
          static auto create(SPtr<typename Meta::Type> tY,
                               LValue<Wire> wirev,
                               LValue<Pointer<index_t>> indexv) -> UPtr<Capacity> {
            auto r = UPtr<Capacity>(new Capacity(tY, wirev, indexv));
            r->len = tY->lenType()->create(wirev, indexv);
            if (not wirev.isNull())
              r->len = (0UL);
            return r;
          }
        private:
          Capacity(SPtr<typename Meta::Type> tY,
                   LValue<Wire> wirev,
                   LValue<index_t> indexv) : tY(tY),
                                          wirev(wirev),
                                          indexv(indexv) {}

          ULong len;
          SPtr<typename Meta::Type> tY;
          Wire wirev;
          index_t indexv = 0;
        };
      }
    }
  }
}
