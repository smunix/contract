#pragma once
#include <org/contract/field/Prim.h>

namespace org {
  namespace contract {
    namespace field {
      using Byte = Prim<uint8_t>;
    }
  }
}
