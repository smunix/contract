#pragma once
#include <org/contract/Utils.h>

namespace org {
  namespace contract {
    namespace field {
      template<class T>
      struct Prim {
        using rank_n_type = utils::_0;

        using type_t = T;
        struct Meta : std::enable_shared_from_this<Meta> {
          using SP = SPtr<Meta>;
          struct Type : std::enable_shared_from_this<Type> {
            using SP = SPtr<Type>;

            // Type ctor
            static auto create(SPtr<Meta> meta, off_t off) -> SPtr<Type> {
              auto r = SPtr<Type>(new Type(meta, off));
              return r;
            }
            // Type properties
            // create(...) in the stack
            auto create(LValue<Wire> wirev, RValue<index_t> ndx) -> Prim {
              index_t index = ndx;
              return create(wirev, index);
            }
            auto create(LValue<Wire> wirev, LValue<index_t> index) -> Prim {
              auto tmp = index + offsetOf();
              auto r = Prim::create(this->shared_from_this(), wirev, index = (tmp));
              index = (tmp + sizeOf());
              return r;
            }
            // sizeOf(...)
            auto sizeOf() const -> sz_t {
              return sizeOf(1);
            }
            auto sizeOf(sz_t count) const -> sz_t {
              return count * sizeof(type_t);
            }
            // offsetOf(...)
            auto offsetOf() const -> off_t {
              return off;
            }
            auto offsetOf(off_t off) -> void {
              this->off = off;
            }
            // alignOf(...)
            auto alignOf() const -> sz_t {
              return sizeOf();
            }
          private:
            Type(SPtr<Meta> meta, off_t off) : meta(meta), off(off) {}
            SPtr<Meta> meta; // meta description
            off_t off; // offset
          };
          // Meta ctors
          static auto create() -> SPtr<Meta> {
            return SPtr<Meta>(new Meta);
          }
          // get type object at a given offset off
          auto type(off_t off = 0UL) -> SPtr<Type> {
            return Type::create(this->shared_from_this(), off);
          }
        private:
          Meta() = default;
        };
        // getter
        auto get() -> LValue<type_t> {
          return *rcast<type_t>(wirev.index(indexv));
        }
        auto get() const -> LValue<type_t const> {
          return *rcast<type_t const>(wirev.index(indexv));
        }
        operator LValue<type_t> () {
          return get();
        }
        operator LValue<type_t const> () const {
          return get();
        }
        // setter
        auto set(LValue<type_t const> v) -> LValue<Prim> {
          get() = v;
          return *this;
        }
        auto operator=(LValue<type_t const> v) -> LValue<Prim> {
          return set(v);
        }
        // new wire
        auto wire(LValue<Wire> wirev) -> LValue<Prim> {
          this->wirev = wirev;
          return *this;
        }
        // change index
        auto index() -> LValue<index_t> {
          return this->indexv;
        }
        auto index() const -> LValue<index_t const> {
          return this->indexv;
        }
        auto index(index_t ndx) -> LValue<Prim> {
          index() = ndx + type()->offsetOf();
          return *this;
        }
        // type
        auto type() const -> SPtr<typename Meta::Type> {
          return tY;
        }
        Prim() {
          new (this) Prim(Meta::create()->type()->create(Wire::null(), 0L));
        }
        Prim(LValue<Prim const>) = default;
      private:
        // friends are declared here
        friend class Meta::Type;

        //create a new prim
        static auto create(SPtr<typename Meta::Type> tY, LValue<Wire> wirev, LValue<index_t> indexv) -> Prim {
          return Prim(tY, wirev, indexv);
        }
        static auto create(SPtr<typename Meta::Type> tY, LValue<Wire> wirev, LValue<Pointer<index_t>> indexv) -> Prim {
          return Prim(tY, wirev, indexv);
        }
      private:
        Prim(SPtr<typename Meta::Type> tY, LValue<Wire> wirev, LValue<Pointer<index_t>> indexv) : tY(tY), wirev(wirev), indexv(indexv.get()) {}
        Prim(SPtr<typename Meta::Type> tY, LValue<Wire> wirev, LValue<index_t> indexv) : tY(tY), wirev(wirev), indexv(indexv) {}

        SPtr<typename Meta::Type> tY;
        Wire wirev;
        index_t indexv = 0;
      };
    }
  }
}
