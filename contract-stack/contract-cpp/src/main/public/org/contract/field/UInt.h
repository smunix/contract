#pragma once
#include <org/contract/field/Prim.h>

namespace org {
  namespace contract {
    namespace field {
      using UInt = Prim<uint32_t>;
    }
  }
}
