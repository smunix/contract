#pragma once
#include <org/contract/Utils.h>

namespace org {
  namespace contract {
    namespace field {
      namespace array {
        template<class T, sz_t N>
        struct Fixed {
          using type_t = T;
          using EMeta = typename type_t::Meta;
          using EType = typename EMeta::Type;
          using SP = SPtr<Fixed>;

          using rank_n_type = utils::_1;

          struct Meta : std::enable_shared_from_this<Meta> {
            using SP = SPtr<Meta>;
            struct Type : std::enable_shared_from_this<Type> {
              using SP = SPtr<Type>;
              // Type ctor
              static auto create(SPtr<Meta> meta, off_t off, SPtr<EType> eType) -> SPtr<Type> {
                auto r = SPtr<Type>(new Type(meta, off, eType));
                return r;
              }
              // Type properties
              // create(...) in stack
              auto create(LValue<Wire> wirev, RValue<index_t> ndx) -> Fixed {
                index_t index = ndx;
                return create(wirev, index);
              }
              auto create(LValue<Wire> wirev, LValue<index_t> index) -> Fixed {
                auto tmp = index + offsetOf();
                auto r = Fixed::create(this->shared_from_this(), wirev, index = (tmp));
                index = (tmp + sizeOf());
                return r;
              }
              // sizeOf(...)
              auto sizeOf() const -> sz_t {
                return sizeOf(1);
              }
              auto sizeOf(sz_t count) const -> sz_t {
                return count * eType->sizeOf(N);
              }
              // offsetOf(...)
              auto offsetOf() const -> off_t {
                return off;
              }
              auto offsetOf(off_t off) -> void {
                this->off = off;
              }
              // alignOf(...)
              auto alignOf() const -> sz_t {
                return eType->sizeOf();
              }
              // element type()
              auto elementType() const -> SPtr<EType> {
                return eType;
              }
            private:
              Type(SPtr<Meta> meta, off_t off, SPtr<EType> eType) : meta(meta),
                                                                    off(off),
                                                                    eType(eType) {}

              SPtr<Meta> meta; // meta description
              off_t off; // offset
              SPtr<EType> eType; // element type
            };
            // Meta ctors
            static auto create() -> SPtr<Meta> {
              return SPtr<Meta>(new Meta);
            }
            // get type object at a given offset off
            auto type(SPtr<EType> eType) -> SPtr<Type> {
              return type(0, eType);
            }
            auto type(off_t off, SPtr<EType> eType) -> SPtr<Type> {
              return Type::create(this->shared_from_this(), off, eType);
            }
          private:
            Meta() = default;
          };
          // getter and setter
          auto get(sz_t j) -> type_t {
            auto eTy = tY->elementType();
            auto step = eTy->sizeOf();
            index_t ndx = index();
            auto iter = eTy->create(wirev, ndx);
            iter.index(indexv + j*step);
            return iter;
          }
          auto get(sz_t j) const -> type_t const {
            auto eTy = tY->elementType();
            auto step = eTy->sizeOf();
            index_t ndx = index();
            auto iter = eTy->create(wirev, ndx);
            iter.index(indexv + j*step);
            return iter;
          }
          template<class Fn>
          auto forEach(Fn &&fn) -> void {
            auto eTy = tY->elementType();
            index_t pos = index();
            for (auto j = 0; j < N; ++j) {
              auto iter = eTy->create(wirev, pos);
              fn(iter);
            }
          }
          template<class Fn>
          auto forEach(Fn &&fn) const -> void {
            auto eTy = tY->elementType();
            index_t pos = index();
            for (auto j = 0; j < N; ++j) {
              auto iter = eTy->create(wirev, pos);
              fn(iter);
            }
          }
          // size and length
          auto size() const -> sz_t {
            return N;
          }
          auto length() const -> sz_t {
            return size();
          }
          // new wire
          auto wire(LValue<Wire> wirev) -> void {
            this->wirev = wirev;
          }
          // change index
          auto index() -> LValue<index_t> {
            return this->indexv;
          }
          auto index() const -> LValue<index_t const> {
            return this->indexv;
          }
          auto index(index_t indexv) -> void {
            index() = indexv + type()->offsetOf();
          }
          // type
          auto type() const -> SPtr<typename Meta::Type> {
            return tY;
          }
          // ctors
          Fixed() {
            new (this) Fixed(Meta::create()->type(utils::tyCon<type_t>::create())->create(Wire::null(), 0L));
          }
          Fixed(LValue<Fixed const>) = default;
        private:
          // friends are declared here
          friend class Meta::Type;
          template<class, sz_t> friend class Capacity;

          // create a new fixed
          static auto create(SPtr<typename Meta::Type> tY, LValue<Wire> wirev, LValue<index_t> indexv) -> Fixed {
            return Fixed(tY, wirev, indexv);
          }
        private:
          Fixed(SPtr<typename Meta::Type> tY, LValue<Wire> wirev, LValue<index_t> indexv) : tY(tY), wirev(wirev), indexv(indexv) {}

          SPtr<typename Meta::Type> tY;
          Wire wirev;
          index_t indexv = 0;
        };
      }
    }
  }
}
