#pragma once
#include <org/contract/field/Prim.h>

namespace org {
  namespace contract {
    namespace field {
      using Int = Prim<int32_t>;
    }
  }
}
