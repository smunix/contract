#pragma once
#include <org/contract/field/Prim.h>

namespace org {
  namespace contract {
    namespace field {
      using Short = Prim<int16_t>;
    }
  }
}
