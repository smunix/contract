#pragma once
#include <org/contract/Utils.h>
#include <org/contract/field/Long.h>
#include <org/contract/field/ULong.h>

namespace org {
  namespace contract {
    namespace field {
      template<class T>
      struct Maybe {
        using type_t = T;
        using EMeta = typename type_t::Meta;
        using EType = typename EMeta::Type;

        using rank_n_type = utils::_1;

        struct Meta : std::enable_shared_from_this<Meta> {
          using SP = SPtr<Meta>;
          struct Type : std::enable_shared_from_this<Type> {
            using SP = SPtr<Type>;

            // Type ctor
            static auto create(SPtr<Meta> meta, off_t off, SPtr<EType> eType) -> SPtr<Type> {
              auto r = SPtr<Type>(new Type(meta, off, eType));
              return r;
            }
            // Type properties
            auto create(LValue<Wire> wirev, RValue<index_t> ndx) -> Maybe {
              index_t index = ndx;
              return create(wirev, index);
            }
            auto create(LValue<Wire> wirev, LValue<index_t> index) -> Maybe {
              auto tmp = index + offsetOf();
              auto r = Maybe::create(this->shared_from_this(), wirev, index = (tmp));
              index = (tmp + sizeOf());
              return r;
            }
            // sizeOf(...)
            auto sizeOf() const -> sz_t {
              return sizeOf(1);
            }
            auto sizeOf(sz_t count) const -> sz_t {
              return count * distanceType()->sizeOf();
            }
            // offsetOf(...)
            auto offsetOf() const -> off_t {
              return off;
            }
            auto offsetOf(off_t off) -> void {
              this->off = off;
            }
            // alignOf(...)
            auto alignOf() const -> sz_t {
              return distanceTy->alignOf();
            }
            // element type()
            auto elementType() const -> SPtr<EType> {
              return eType;
            }
            // distance type()
            auto distanceType() const -> SPtr<Long::Meta::Type> {
              return distanceTy;
            }
          private:
            Type(SPtr<Meta> meta, off_t off, SPtr<EType> eType) : meta(meta),
                                                                  off(off),
                                                                  distanceTy(Long::Meta::create()->type()),
                                                                  eType(eType)
            {}
            SPtr<Meta> meta; // meta description
            off_t off; // offset
            SPtr<Long::Meta::Type> distanceTy;
            SPtr<EType> eType;
          };
          // Meta ctors
          static auto create() -> SPtr<Meta> {
            return SPtr<Meta>(new Meta);
          }
          // get type object at a given offset off
          auto type(SPtr<EType> eType) -> SPtr<Type> {
            return Meta::type(0, eType);
          }
          auto type(off_t off, SPtr<EType> eType) -> SPtr<Type> {
            return Type::create(this->shared_from_this(), off, eType);
          }
        private:
          Meta() = default;
        };
        // inspect the maybe value
        auto isJust() const -> bool {
          return not isNothing();
        }
        auto isNothing() const -> bool {
          return 0L == distance;
        }
        // deconstruct the maybe value
        template<class nothingFn, class justFn>
        auto match(nothingFn &&nFn, justFn && jFn) -> utils::enable<utils::same<decltype(jFn(std::declval<LValue<type_t>>())),
                                                                                decltype(nFn())>,
                                                                    decltype(nFn())> {
          if (isJust()) {
            index_t i = index() + distance;
            auto val = type()->elementType()->create(wirev, i);
            return jFn(val);
          }
          else
            return nFn();
        }
        // setters
        template<class Fn>
        auto set(sz_t sz, Fn &&fn) -> void {
          match([&]() -> void {
                  thread_local auto ulongTy = field::ULong::Meta::create()->type();
                  auto wireLenField = ulongTy->create(wirev, 0L);
                  index_t ndx = wireLenField.wire(wirev);
                  off_t nPos = ndx;
                  off_t cPos = index();
                  auto eArr = type()->elementType()->create(sz, wirev, ndx);
                  distance = nPos - cPos;
                  wireLenField = ndx;
                  eArr.forEach(fn);
                },
            [](LValue<type_t>) -> void {});
        }
        template<class Fn>
        auto set(Fn &&fn) -> void {
          match([&]() -> void {
                  thread_local auto ulongTy = field::ULong::Meta::create()->type();
                  auto wireLenField = ulongTy->create(wirev, 0L);
                  index_t ndx = wireLenField.wire(wirev);
                  off_t nPos = ndx;
                  off_t cPos = index();
                  auto eVal = type()->elementType()->create(wirev, ndx);
                  distance = nPos - cPos;
                  wireLenField = ndx;
                  fn(eVal);
                },
            [](LValue<type_t>) -> void {});
        }
        // new wire
        auto wire(LValue<Wire> wirev) -> void {
          this->wirev = wirev;
          distance.wire(wirev);
        }
        // change index
        auto index() -> LValue<index_t> {
          return this->indexv;
        }
        auto index() const -> LValue<index_t const> {
          return this->indexv;
        }
        auto index(index_t ndx) -> void {
          index() = ndx + type()->offsetOf();
          distance.index(index() + type()->distanceType()->offsetOf());
        }
        // type
        auto type() const -> SPtr<typename Meta::Type> {
          return tY;
        }
        // ctors
        Maybe() {
          index_t i = 0L;
          new (this) Maybe(Meta::create()->type(utils::tyCon<type_t>::create())->create(Wire::null(), i));
        }
        Maybe(LValue<Maybe const>) = default;
      private:
        // friends are declared here
        friend class Meta::Type;

        //create a new maybe
        static auto create(SPtr<typename Meta::Type> tY, LValue<Wire> wirev, LValue<index_t> indexv) -> Maybe {
          return Maybe(tY, wirev, indexv);
        }
        static auto create(SPtr<typename Meta::Type> tY, LValue<Wire> wirev, LValue<Pointer<index_t>> indexv) -> Maybe {
          return Maybe(tY, wirev, indexv);
        }
      private:
        Maybe(SPtr<typename Meta::Type> tY, LValue<Wire> wirev, LValue<index_t> indexv) : tY(tY),
                                                                                    wirev(wirev),
                                                                                    indexv(indexv)
        {}
        Maybe(SPtr<typename Meta::Type> tY, LValue<Wire> wirev, LValue<Pointer<index_t>> indexv) : tY(tY),
                                                                                             wirev(wirev),
                                                                                             indexv(indexv)
        {}

        SPtr<typename Meta::Type> tY;
        Wire wirev;
        index_t indexv = 0;
        Long distance;
      };
    }
  }
}
