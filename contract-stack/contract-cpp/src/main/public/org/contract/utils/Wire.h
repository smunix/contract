#pragma once
#include <org/contract/utils/Utils.h>

namespace org {
  namespace contract {
    namespace utils {
      // a wire is a buffer as sent or received from a communication media
      struct Wire {
        struct Position {
          Position(LValue<Wire> w) : w(w) {}
          Position(LValue<Wire const> w) : w(ccast(w)) {}
          template<class S>
          auto operator= (S &&sz) -> LValue<Position> {
            w.size(std::forward<S>(sz));
            return *this;
          }
          operator sz_t () const {
            return w.size();
          }
          template<class V>
          auto operator+=(V&& v) -> LValue<Position> {
            w.size(w.size() + v);
            return *this;
          }
          template<class V>
          auto operator-(V&& v) -> LValue<Position> {
            return *this = sz_t(*this) - v;
          }
        private:
          LValue<Wire> w;
        };
        auto position() const -> Position {
          return Position(*this);
        }
        // create a new wire
        static auto create(Ptr<buff_t> buff, len_t len) -> Wire {
          auto r = Wire(buff, len);
          return r;
        }
        static auto create(Ptr<char_t> buff, len_t len) -> Wire {
          auto r = Wire(rcast<buff_t>(buff), len);
          return r;
        }
        static auto create(Ptr<char_t const> buff, len_t len) -> Wire {
          auto r = Wire(rcast<buff_t>(ccast(buff)), len);
          return r;
        }
        template<class T, uint64_t N>
        static auto create(FArr<T, N> buff) -> Wire {
          return create(rcast<char_t>(addrOf(buff)), N * sizeof(T));
        }
        template<class T, uint64_t N>
        static auto create(FArr<T const, N> buff) -> Wire {
          return create(rcast<char_t const>(addrOf(buff)), N * sizeof(T));
        }
        // attach a new wire
        static auto attach(Ptr<buff_t> buff, len_t len) -> Wire {
          return Wire(buff, len);
        }
        static auto attach(Ptr<char_t> buff, len_t len) -> Wire {
          return Wire(rcast<buff_t>(buff), len);
        }
        static auto attach(Ptr<char_t const> buff, len_t len) -> Wire {
          return Wire(rcast<buff_t>(ccast(buff)), len);
        }

        // indexing at a specific position
        auto index(index_t index) -> Ptr<buff_t> {
          return buff+index;
        }
        auto index(index_t index) const -> Ptr<buff_t const> {
          return buff+index;
        }
        // isNull ?
        auto isNull() const -> bool {
          return this->buff == Wire::null().buff;
        }
        static auto null() -> LValue<Wire> {
          thread_local sz_t sz = 0UL;
          thread_local auto w = Wire::create(rcast<char>(addrOf(sz)), 0);
          return w;
        }
        // capacity
        auto capacity() const -> len_t {
          return len;
        }
        auto size() const -> sz_t {
          return *rcast<sz_t const>(index(0UL));
        }
        // ctors
        Wire(Wire const& o) : buff(o.buff), len(o.len) {}
        Wire() : buff(null().buff), len(null().len) {};
      private:
        // change size
        auto size(sz_t sz) -> LValue<Wire> {
          if (not isNull())
            *rcast<sz_t>(index(0UL)) = sz;
          return *this;
        }
      private:
        Wire(Ptr<buff_t> buff, len_t len) : buff(buff), len(len) {}

        Ptr<buff_t> buff = nullptr;
        len_t len = 0UL;
      };
    }
  }
}
