#pragma once
#include <org/contract/utils/Utils.h>

namespace org {
  namespace contract {
    namespace utils {
      template<class T>
      struct Pointer {
        // smart ctors
        template<class ...Us>
        static auto create(Us&& ...us) -> Pointer {
          return std::move(Pointer(std::forward<Us>(us)...));
        }
        // setters
        template<class U>
        auto set(U&& u) -> LValue<Pointer> {
          get() = std::forward<U>(u);
          return *this;
        }
        template<class U>
        auto operator=(U&& u) -> LValue<Pointer> {
          return this->template set(std::forward<U>(u));
        }
        // getters
        auto get() -> LValue<T> {
          return (*t);
        }
        auto get() const -> LValue<T const> {
          return (*t);
        }
        operator LValue<T> () {
          return get();
        }
        operator LValue<T const> () const {
          return get();
        }

        template<class ...Us>
        Pointer(Us&& ...us) : t(new T(std::forward<Us>(us)...)) {}
      private:
        SPtr<T> t;
      };
    }
  }
}
