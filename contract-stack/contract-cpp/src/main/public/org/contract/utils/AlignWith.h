#pragma once
#include <org/contract/utils/Utils.h>

namespace org {
  namespace contract {
    namespace utils {
      static auto alignWith(sz_t v, sz_t a) -> sz_t {
        if (0UL == a) return v;
        if (0UL == v % a) return v;
        return (1 + v / a) * a;
      };
    }
  }
}

