#pragma once
#include <memory>
#include <type_traits>
#include <iostream>
#define cout(x) do { std::cout << x << std::endl; } while(0)

namespace org {
  namespace contract {
    namespace utils {
      template<class T> using UPtr = std::unique_ptr<T>;
      template<class T> using SPtr = std::shared_ptr<T>;
      template<class T> using Ptr = T*;
      template<class T> using LValue = T&;
      template<class T> using RValue = T&&;
      template<class T, uint64_t N> using FArr = T[N];
      template<class T> using Arr = T[];

      template<class T> using LV = LValue<T>;
      template<class T> using RV = RValue<T>;

      template<class B, class T = void>
      using enable = typename std::enable_if<B::value, T>::type;

      template<class A, class B>
      using same = std::is_same<A, B>;

      template<class B, class T, class F>
      using ife = typename std::conditional<B::value, T, F>::type;

      using index_t = uint64_t;
      using buff_t = uint8_t;
      using char_t = char;
      using len_t = uint64_t;
      using off_t = int64_t;
      using sz_t = size_t;

      using _n1 = std::integral_constant<int, -1>;
      using _0 = std::integral_constant<int, 0>;
      using _1 = std::integral_constant<int, 1>;
      using _2 = std::integral_constant<int, 2>;

      // type constructors for first and high rank types
      template<class U, class V = typename U::rank_n_type>
      struct tyCon {};
      // first rank type ctor
      template<class U>
      struct tyCon<U, _0> {
        static auto create() -> SPtr<typename U::Meta::Type> {
          return U::Meta::create()->type();
        }
      };
      // high rank type ctor
      template<class U>
      struct tyCon<U, _1> {
        static auto create() -> SPtr<typename U::Meta::Type> {
          return U::Meta::create()->type(tyCon<typename U::type_t>::create());
        }
      };

      template<class U>
      auto addrOf(U u) -> Ptr<U> {
        return std::addressof(u);
      }
      // const casts
      template<class U>
      auto ccast(Ptr<U const> u) -> Ptr<U> {
        return const_cast<Ptr<U>>(u);
      }
      template<class U>
      auto ccast(Ptr<U> u) -> Ptr<U const> {
        return const_cast<Ptr<U const>>(u);
      }
      template<class U>
      auto ccast(LValue<U const> u) -> LValue<U> {
        return const_cast<LValue<U>>(u);
      }
      template<class U>
      auto ccast(LValue<U> u) -> LValue<U const> {
        return const_cast<LValue<U const>>(u);
      }
      // static casts
      template<class V, class U>
      auto scast(Ptr<U> u) -> Ptr<V> {
        return static_cast<V>(u);
      }
      // reinterpret casts
      template<class V, class U>
      auto rcast(Ptr<U> u) -> Ptr<V> {
        return reinterpret_cast<Ptr<V>>(u);
      }
      template<class V, class U>
      auto rcast(LValue<U> u) -> LValue<V> {
        return reinterpret_cast<LValue<V>>(u);
      }
    }
  }
}
