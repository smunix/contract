#pragma once
#include <org/contract/utils/Pointer.h>
#include <org/contract/utils/Utils.h>
#include <org/contract/utils/AlignWith.h>
#include <org/contract/utils/Wire.h>

namespace org {
  namespace contract {
    // boxing (boxed fields are created like Thunk)
    template<class T> using Pointer = utils::Pointer<T>;

    // type aliasing
    template<class T> using UPtr = utils::UPtr<T>;
    template<class T> using SPtr = utils::SPtr<T>;
    template<class T> using Ptr = utils::Ptr<T>;
    template<class T> using LValue = utils::LValue<T>;
    template<class T> using RValue = utils::RValue<T>;
    template<class T> using LV = LValue<T>;
    template<class T> using RV = RValue<T>;
    template<class T, uint64_t N> using FArr = utils::FArr<T, N>;
    template<class T> using Arr = utils::Arr<T>;

    // reusable types
    using index_t = utils::index_t;
    using buff_t = utils::buff_t;
    using char_t = utils::char_t;
    using len_t = utils::len_t;
    using off_t = utils::off_t;
    using sz_t = utils::sz_t;

    // pointer manipulations
    using utils::addrOf;
    using utils::ccast;
    using utils::scast;
    using utils::rcast;

    // wire manipulations
    using Wire = utils::Wire;
  }
}
