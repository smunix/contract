#pragma once
#include <org/contract/Utils.h>
#include <org/contract/Field.h>

namespace org {
  namespace contract {
    template<class D> struct Contract {
      using Derived = D;

      template<class T> using SPtr = org::contract::utils::SPtr<T>;
      template<class T> using UPtr = org::contract::utils::UPtr<T>;
      template<class T> using Ptr = org::contract::utils::Ptr<T>;
      template<class T> using LValue = org::contract::utils::LValue<T>;
      template<class T> using RValue = org::contract::utils::RValue<T>;
      template<class T> using Pointer = org::contract::utils::Pointer<T>;

      template<class T> using LV = LValue<T>;
      template<class T> using RV = RValue<T>;

      using SP = SPtr<D>;
      using UP = UPtr<D>;
      // using R = LValue<D>;
      // using P = Ptr<D>;

      using Wire = org::contract::utils::Wire;
      using index_t = org::contract::utils::index_t;
      using sz_t = org::contract::utils::sz_t;

      using Bool = org::contract::field::Bool;
      using Char = org::contract::field::Char;
      using Byte = org::contract::field::Byte;
      using Short = org::contract::field::Short;
      using UShort = org::contract::field::UShort;
      using Int = org::contract::field::Int;
      using UInt = org::contract::field::UInt;
      using Float = org::contract::field::Float;
      using Long = org::contract::field::Long;
      using ULong = org::contract::field::ULong;
      using Double = org::contract::field::Double;

      template<class T, sz_t N> using Fixed = org::contract::field::array::Fixed<T, N>;
      template<class T, sz_t N> using Capacity = org::contract::field::array::Capacity<T, N>;
      template<class T> using Dynamic = org::contract::field::array::Dynamic<T>;
      template<class T, sz_t N> using FArr = Fixed<T, N>;
      template<class T, sz_t N> using CArr = Capacity<T, N>;
      template<class T> using DArr = Dynamic<T>;
      template<class T> using Maybe = org::contract::field::Maybe<T>;

      static auto alignWith(sz_t v, sz_t a) -> sz_t {
        return utils::alignWith(v, a);
      }

      // generic record Type base description
      struct Type : public std::enable_shared_from_this<typename Derived::Meta::Type> {
        using DMeta  = typename Derived::Meta;
        using DMType = typename DMeta::Type;
        using SP     = SPtr<DMType>;

        // create(...) data in the stack
        auto create(LValue<Wire> wirev, RV<index_t> indexv) -> Derived {
          index_t index = indexv;
          return type()->create(wirev, index);
        }
        // sizeOf() computation
        auto sizeOf() const -> sz_t {
          return sizeOf(1);
        }
        auto sizeOf(sz_t count) const -> sz_t {
          return count * currSizeOf();
        }
        // alignOf() computation
        auto alignOf() const -> sz_t {
          return currAlignOf();
        }
        // offsetOf(...)
        auto offsetOf() const -> off_t {
          return off;
        }
        auto offsetOf(off_t off) -> void {
          this->off = off;
        }
        // post init
        auto init() -> decltype(this->shared_from_this()) {
          auto d = this->shared_from_this();
          d->setAlignOf();
          d->setSizeOf();
          return d;
        }
        // exec batch (useless :))
        template<class ...Fns>
        auto execBatch(Fns&& ...fns) -> void {}
        template<class Fn>
        auto execBatch(Fn&& fn) -> decltype(fn()) {
          return fn();
        }
        template<class Fn, class ...Fns>
        auto execBatch(Fn&& fn, Fns&& ...fns) -> decltype(execBatch(std::forward<Fns>(fns)...)) {
          execBatch(std::forward<Fn>(fn));
          return execBatch(std::forward<Fns>(fns)...);
        }
      protected:
        // child type desc
        auto type() const -> decltype(this->shared_from_this()) {
          return this->shared_from_this();
        }
        auto type() -> decltype(this->shared_from_this()) {
          return this->shared_from_this();
        }
        // sizeOf() computation
        auto resetSizeOf() -> void {
          currSizeOf() = 0UL;
        }
        auto addSizeOf(sz_t o, sz_t s) -> void {
          currSizeOf() = o + s;
        }
        auto currSizeOf() -> LV<sz_t> {
          return _sizeOf;
        }
        auto currSizeOf() const -> LV<sz_t const> {
          return _sizeOf;
        }

        // alignOf() computation
        auto resetAlignOf() -> void {
          currAlignOf() = 0UL;
        }
        auto addAlignOf(sz_t a) -> void {
          currAlignOf() = std::max(currAlignOf(), a);
        }
        auto currAlignOf() -> LV<sz_t> {
          return _alignOf;
        }
        auto currAlignOf() const -> LV<sz_t const> {
          return _alignOf;
        }

        Type(SPtr<DMeta> meta, off_t off) : meta(meta), off(off) {}

        SPtr<DMeta> meta; // meta description
        off_t off; // offset
        sz_t _alignOf = 0UL;
        sz_t _sizeOf = 0UL;
      };
      // get index
      auto index() -> LValue<index_t> {
        return this->indexv;
      }
      auto index() const -> LValue<index_t const> {
        return this->indexv;
      }
      // get wire
      auto wire() -> LValue<Wire> {
        return wirev;
      }
      auto wire() const -> LValue<Wire const> {
        return wirev;
      }
    protected:
      Contract() : wirev(Wire::null()),
                   indexv(0L)
      {}
      Contract(LValue<Wire> wirev,
               LValue<index_t> indexv) : wirev(wirev),
                                         indexv(indexv) {}

      Wire wirev;
      index_t indexv = 0;
    };
  }
}
