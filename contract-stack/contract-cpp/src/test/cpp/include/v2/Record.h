#include <org/contract/contract.h>

namespace org {
  namespace test {
    namespace v2 {
      /* {d:double, i:int, c:char} */
      struct Record : public org::contract::Contract<Record> {
        using Base = typename org::contract::Contract<Record>;
        struct Meta : std::enable_shared_from_this<Meta> {
          struct Type : public org::contract::Contract<Record>::Type {
            using Base = typename org::contract::Contract<Record>::Type;

            typename Double::Meta::Type::SP d;
            typename Int::Meta::Type::SP i;
            typename Char::Meta::Type::SP c;

            // Type ctor
            static auto create(SPtr<Meta> meta, off_t off) -> SPtr<Type> {
              auto r = SPtr<Type>(new Type(meta, off));
              r->d = Double::Meta::create()->type(0);
              r->i = Int::Meta::create()->type(8);
              r->c = Char::Meta::create()->type(12);
              return r->init();
            }
            // Type properties
            // create(...) data in the stack
            using Base::create;
            auto create(LValue<Wire> wirev, LValue<index_t> index) -> Record {
              auto tmp = index + offsetOf();
              auto r = Record::create(this->shared_from_this(), wirev, index = (tmp));
              r.d = d->create(wirev, index = (tmp));
              r.i = i->create(wirev, index = (tmp));
              r.c = c->create(wirev, index = (tmp));
              index = (tmp + sizeOf());
              return r;
            }
            // sizeOf(...)
            auto setSizeOf() -> void {
              resetSizeOf();
              addSizeOf(alignWith(currSizeOf(), d->alignOf()), d->sizeOf());
              addSizeOf(alignWith(currSizeOf(), i->alignOf()), i->sizeOf());
              addSizeOf(alignWith(currSizeOf(), c->alignOf()), c->sizeOf());
              currSizeOf() = alignWith(currSizeOf(), alignOf());
            }
            // alignOf(...)
            auto setAlignOf() -> void {
              resetAlignOf();
              addAlignOf(d->alignOf());
              addAlignOf(i->alignOf());
              addAlignOf(c->alignOf());
            }
          private:
            Type(SPtr<Meta> meta, off_t off) : org::contract::Contract<Record>::Type(meta, off) {}
          };
          // Meta ctors
          static auto create() -> SPtr<Meta> {
            return SPtr<Meta>(new Meta);
          }
          // get type object at a given offset off
          auto type(off_t off = 0UL) -> SPtr<Type> {
            return Type::create(this->shared_from_this(), off);
          }
        private:
          Meta() = default;
        };
        // new wire
        using Base::wire;
        auto wire(LValue<Wire> wirev) -> LValue<Record> {
          wire() = wirev;
          d.wire(wire());
          i.wire(wire());
          c.wire(wire());
          return *this;
        }
        // change index
        using Base::index;
        auto index(index_t indexv) -> LValue<Record> {
          index() = indexv + type()->offsetOf();
          d.index(index());
          i.index(index());
          c.index(index());
          return *this;
        }
        // type
        auto type() const -> SPtr<typename Meta::Type> {
          return tY;
        }
        // ctor
        Record() {
          new (this) Record(Meta::create()->type()->org::contract::Contract<Record>::Type::create(Wire::null(), 0L));
        }
        Record(LValue<Record const>) = default;

        // fields
        Double d;
        Int i;
        Char c;
      private:
        // create a new record
        static auto create(SPtr<typename Meta::Type> tY,
                           LValue<Wire> wirev,
                           LValue<index_t> indexv) -> Record {
          auto r = Record(tY, wirev, indexv);
          return r;
        }
      private:
        Record(SPtr<typename Meta::Type> tY, LValue<Wire> wirev, LValue<index_t> indexv) : org::contract::Contract<Record>(wirev, indexv), tY(tY)
        {}
        SPtr<typename Derived::Meta::Type> tY;
      };
    }
  }
}
