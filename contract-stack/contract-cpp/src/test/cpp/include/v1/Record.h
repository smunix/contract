#include <org/contract/contract.h>

namespace org {
  namespace test {
    namespace v1 {
      /* {i:int, c:char, d:double} */
      struct Record : public org::contract::Contract<Record> {
        using Base = typename org::contract::Contract<Record>;
        struct Meta : std::enable_shared_from_this<Meta> {
          struct Type : public org::contract::Contract<Record>::Type {
            using Base = typename org::contract::Contract<Record>::Type;

            typename Int::Meta::Type::SP i;
            typename Char::Meta::Type::SP c;
            typename Double::Meta::Type::SP d;
            typename Maybe<Double>::Meta::Type::SP dM;
            typename Maybe<Dynamic<Double>>::Meta::Type::SP dDArrM;

            // Type ctor
            static auto create(SPtr<Meta> meta, off_t off) -> SPtr<Type> {
              auto r = SPtr<Type>(new Type(meta, off));
              r->i = Int::Meta::create()->type(0);
              r->c = Char::Meta::create()->type(4);
              r->d = Double::Meta::create()->type(8);
              r->dM = Maybe<Double>::Meta::create()->type(16, Double::Meta::create()->type());
              r->dDArrM = Maybe<Dynamic<Double>>::Meta::create()->type(24, Dynamic<Double>::Meta::create()->type(Double::Meta::create()->type()));
              return r->init();
            }
            // create(...) data in the stack
            using Base::create;
            auto create(LValue<Wire> wirev, LValue<index_t> index) -> Record {
              auto tmp = index + offsetOf();
              auto r = Record::create(type(), wirev, index = tmp);
              cout("record.index=" << tmp);
              r.i = i->create(wirev, index = tmp);
              cout("record.i.index=" << r.i.index());
              r.c = c->create(wirev, index = tmp);
              cout("record.c.index=" << r.c.index());
              r.d = d->create(wirev, index = tmp);
              cout("record.d.index=" << r.d.index());
              r.dM = dM->create(wirev, index = tmp);
              cout("record.dM.index=" << r.dM.index());
              r.dDArrM = dDArrM->create(wirev, index = tmp);
              cout("record.dDArrM.index=" << r.dDArrM.index());
              index = (tmp + sizeOf());
              cout("record.index=" << sizeOf());
              return r;
            }
            // Type properties
            // sizeOf(...)
            auto setSizeOf() -> void {
              resetSizeOf();
              addSizeOf(alignWith(currSizeOf(), i->alignOf()), i->sizeOf());
              addSizeOf(alignWith(currSizeOf(), c->alignOf()), c->sizeOf());
              addSizeOf(alignWith(currSizeOf(), d->alignOf()), d->sizeOf());
              addSizeOf(alignWith(currSizeOf(), dM->alignOf()), dM->sizeOf());
              addSizeOf(alignWith(currSizeOf(), dDArrM->alignOf()), dDArrM->sizeOf());
              currSizeOf() = alignWith(currSizeOf(), alignOf());
            }
            // alignOf(...)
            auto setAlignOf() -> void {
              resetAlignOf();
              addAlignOf(i->alignOf());
              addAlignOf(c->alignOf());
              addAlignOf(d->alignOf());
              addAlignOf(dM->alignOf());
              addAlignOf(dDArrM->alignOf());
            }
          private:
            Type(SPtr<Meta> meta, off_t off) : org::contract::Contract<Record>::Type(meta, off) {}
          };
          // Meta ctors
          static auto create() -> SPtr<Meta> {
            return SPtr<Meta>(new Meta);
          }
          // get type object at a given offset off
          auto type(off_t off = 0UL) -> SPtr<Type> {
            return Type::create(this->shared_from_this(), off);
          }
        private:
          Meta() = default;
        };
        // new wire
        using Base::wire;
        auto wire(LValue<Wire> wirev) -> LValue<Record> {
          wire() = wirev;
          i.wire(wire());
          c.wire(wire());
          d.wire(wire());
          dM.wire(wire());
          dDArrM.wire(wire());
          return *this;
        }
        // change index
        using Base::index;
        auto index(index_t indexv) -> LValue<Record> {
          index() = indexv + type()->offsetOf();
          i.index(index());
          c.index(index());
          d.index(index());
          dM.index(index());
          dDArrM.index(index());
          return *this;
        }
        // type
        auto type() const -> SPtr<typename Meta::Type> {
          return tY;
        }
        // ctor
        Record() {
          new (this) Record(Meta::create()->type()->org::contract::Contract<Record>::Type::create(Wire::null(), 0L));
        }
        Record(LValue<Record const>) = default;

        // fields
        Int i;
        Char c;
        Double d;
        Maybe<Double> dM;
        Maybe<Dynamic<Double>> dDArrM;
        Dynamic<Double> dDArr;
        Fixed<Double, 20> dFArr;
        Capacity<Double, 20> dCArr;
        // Maybe<Dynamic<Capacity<Fixed<Double, 10>, 10>>> dM;
        // Maybe<Maybe<Double>> dM;
        // Maybe<Maybe<Maybe<Double>>> dM;
      private:
        // create a new record
        static auto create(SPtr<typename Meta::Type> tY,
                           LValue<Wire> wirev,
                           LValue<index_t> indexv) -> Record {
          auto r = Record(tY, wirev, indexv);
          return r;
        }

        Record(SPtr<typename Meta::Type> tY, LValue<Wire> wirev, LValue<index_t> indexv) : org::contract::Contract<Record>(wirev, indexv), tY(tY)
        {}
        SPtr<typename Derived::Meta::Type> tY;
      };
    }
  }
}
