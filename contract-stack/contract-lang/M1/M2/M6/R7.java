package M1.M2.M6;

/*
2019-11-13 23:42:03.893615 EST
*/

import org.contract.utils.function.Thunk5;
import org.contract.field.array.Dynamic;
import org.contract.utils.function.Thunk2;
import org.contract.exception.IllegalConversion;
import org.contract.utils.AlignOf;
import org.contract.utils.function.Thunk3;
import org.contract.utils.compile.Long._10;
import org.contract.utils.Pointer;
import java.io.PrintStream;
import org.contract.utils.function.Thunk1;
import org.contract.field.Char;
import java.util.Arrays;
import org.contract.field.array.Fixed;
import org.contract.field.Ref;
import org.contract.field.Int;
import org.contract.field.Double;
import java.io.ByteArrayOutputStream;
import org.contract.utils.function.Thunk0;
import org.agrona.concurrent.UnsafeBuffer;
import org.contract.type.Type;
import org.contract.type.Desc;
import org.contract.field.array.Capacity;
import org.contract.utils.compile.Long._5;
import org.contract.field.Long;

public static class R7 extends IndexWire {
  public static Meta Meta = new Meta();
  public static class Meta {
    // field meta-descriptors
    public final org.contract.type.Type<org.contract.field.Int> b = org.contract.field.Int.Meta.type(0);
    public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Ref<M2.M3.R1.R2>, _5>, _10>> r2r1m3m2reffarr5carr10 = org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Ref<M2.M3.R1.R2>, _5>, _10>.Meta.type(8);

    // type descriptor for record M1.M2.M6.R7
    public org.contract.type.Type<R7> Type = type();
    public org.contract.type.Type<R7> type() = { return type(0); }
    public org.contract.type.Type<R7> type(final long offset) {
      return new org.contract.type.Type<R7>(R7::new) {
        long off = offset;
        @Override
        public long offsetOf() { return off; }
        @Override
        public org.contract.type.Type<R7> offsetOf(long offset) {
          off = offset;
          return this;
        }
        @Override
        public long sizeOf(long eCount) {
          long r = 0;
          r = AlignOf.apply(r, b.alignOf()) + b.sizeOf();
          r = AlignOf.apply(r, r2r1m3m2reffarr5carr10.alignOf()) + r2r1m3m2reffarr5carr10.sizeOf();
          return eCount * AlignOf.apply(r, alignOf());
        }
        @Override
        public org.contract.type.Desc describe() {
          return org.contract.type.Desc.record(Arrays.asList(
            org.contract.type.Desc.field("b", b.offsetOf(), b.describe()),
            org.contract.type.Desc.field("r2r1m3m2reffarr5carr10", r2r1m3m2reffarr5carr10.offsetOf(), r2r1m3m2reffarr5carr10.describe())));
        }
        @Override
        public long alignOf() {
          long r = 0;
          r = Math.max(r, b.alignOf());
          r = Math.max(r, r2r1m3m2reffarr5carr10.alignOf());
          return r;
        }
        @Override
        public Boolean from(long off, org.contract.type.Desc desc) {
          offsetOf(off);

          boolean r = true;
          r &= desc.with("b", b::from);
          r &= desc.with("r2r1m3m2reffarr5carr10", r2r1m3m2reffarr5carr10::from);
          return r;
        }
      }
    }
  }
  // all fields
  public org.contract.field.Int b;
  public org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Ref<M2.M3.R1.R2>, _5>, _10> r2r1m3m2reffarr5carr10;
  // constructor.
  public R7(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> position) {
    super(unsafeBuffer, position);
    init(Meta);
  }
  // meta init.
  public void init(Meta meta) {
    write(index());
    b = meta.b.reify(wire(), write(index() + meta.b.offsetOf()));
    r2r1m3m2reffarr5carr10 = meta.r2r1m3m2reffarr5carr10.reify(wire(), write(index() + meta.r2r1m3m2reffarr5carr10.offsetOf()));
  }
  // process new wire buffer.
  @Override
  public WireI wire(UnsafeBuffer unsafeBuffer) {
    b.wire(unsafeBuffer);
    r2r1m3m2reffarr5carr10.wire(unsafeBuffer);
    return super.wire(unsafeBuffer);
  }
}
