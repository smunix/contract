package M1.M2.M3;

/*
2019-11-13 23:42:03.893615 EST
*/

import org.contract.utils.function.Thunk5;
import org.contract.field.array.Dynamic;
import org.contract.utils.function.Thunk2;
import org.contract.exception.IllegalConversion;
import org.contract.utils.AlignOf;
import org.contract.utils.function.Thunk3;
import org.contract.utils.compile.Long._10;
import org.contract.utils.Pointer;
import java.io.PrintStream;
import org.contract.utils.function.Thunk1;
import org.contract.field.Char;
import java.util.Arrays;
import org.contract.field.array.Fixed;
import org.contract.field.Ref;
import org.contract.field.Int;
import org.contract.field.Double;
import java.io.ByteArrayOutputStream;
import org.contract.utils.function.Thunk0;
import org.agrona.concurrent.UnsafeBuffer;
import org.contract.type.Type;
import org.contract.type.Desc;
import org.contract.field.array.Capacity;
import org.contract.utils.compile.Long._5;
import org.contract.field.Long;

public static class R4 extends IndexWire {
  public static class R5 extends IndexWire {
    public static Meta Meta = new Meta();
    public static class Meta {
      // field meta-descriptors
      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Long>>> u64arr = org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Long>>.Meta.type(0);
      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>>> u64refarr = org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>>.Meta.type(8);
      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>>> u64optarr = org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>>.Meta.type(16);
      public final org.contract.type.Type<org.contract.field.array.Fixed<org.contract.field.Long, _10>> u64farr10 = org.contract.field.array.Fixed<org.contract.field.Long, _10>.Meta.type(24);
      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, _10>>> u64farr10ref = org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, _10>>.Meta.type(104);
      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, _10>>> u64farr10opt = org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, _10>>.Meta.type(112);
      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _10>>> u64optfarr10opt = org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _10>>.Meta.type(120);
      public final org.contract.type.Type<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _10>> u64reffarr10 = org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _10>.Meta.type(128);
      public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.Long, _10>> u64carr10 = org.contract.field.array.Capacity<org.contract.field.Long, _10>.Meta.type(208);
      public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, _5>, _10>> u64farr5carr10 = org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, _5>, _10>.Meta.type(288);
      public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, _5>, _10>> testu64farr5carr10 = org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, _5>, _10>.Meta.type(688);
      public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _5>, _10>> testu64reffarr5carr10 = org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _5>, _10>.Meta.type(1088);

      // type descriptor for record M1.M2.M3.R4.R5
      public org.contract.type.Type<R5> Type = type();
      public org.contract.type.Type<R5> type() = { return type(0); }
      public org.contract.type.Type<R5> type(final long offset) {
        return new org.contract.type.Type<R5>(R5::new) {
          long off = offset;
          @Override
          public long offsetOf() { return off; }
          @Override
          public org.contract.type.Type<R5> offsetOf(long offset) {
            off = offset;
            return this;
          }
          @Override
          public long sizeOf(long eCount) {
            long r = 0;
            r = AlignOf.apply(r, u64arr.alignOf()) + u64arr.sizeOf();
            r = AlignOf.apply(r, u64refarr.alignOf()) + u64refarr.sizeOf();
            r = AlignOf.apply(r, u64optarr.alignOf()) + u64optarr.sizeOf();
            r = AlignOf.apply(r, u64farr10.alignOf()) + u64farr10.sizeOf();
            r = AlignOf.apply(r, u64farr10ref.alignOf()) + u64farr10ref.sizeOf();
            r = AlignOf.apply(r, u64farr10opt.alignOf()) + u64farr10opt.sizeOf();
            r = AlignOf.apply(r, u64optfarr10opt.alignOf()) + u64optfarr10opt.sizeOf();
            r = AlignOf.apply(r, u64reffarr10.alignOf()) + u64reffarr10.sizeOf();
            r = AlignOf.apply(r, u64carr10.alignOf()) + u64carr10.sizeOf();
            r = AlignOf.apply(r, u64farr5carr10.alignOf()) + u64farr5carr10.sizeOf();
            r = AlignOf.apply(r, testu64farr5carr10.alignOf()) + testu64farr5carr10.sizeOf();
            r = AlignOf.apply(r, testu64reffarr5carr10.alignOf()) + testu64reffarr5carr10.sizeOf();
            return eCount * AlignOf.apply(r, alignOf());
          }
          @Override
          public org.contract.type.Desc describe() {
            return org.contract.type.Desc.record(Arrays.asList(
              org.contract.type.Desc.field("u64arr", u64arr.offsetOf(), u64arr.describe()),
              org.contract.type.Desc.field("u64refarr", u64refarr.offsetOf(), u64refarr.describe()),
              org.contract.type.Desc.field("u64optarr", u64optarr.offsetOf(), u64optarr.describe()),
              org.contract.type.Desc.field("u64farr10", u64farr10.offsetOf(), u64farr10.describe()),
              org.contract.type.Desc.field("u64farr10ref", u64farr10ref.offsetOf(), u64farr10ref.describe()),
              org.contract.type.Desc.field("u64farr10opt", u64farr10opt.offsetOf(), u64farr10opt.describe()),
              org.contract.type.Desc.field("u64optfarr10opt", u64optfarr10opt.offsetOf(), u64optfarr10opt.describe()),
              org.contract.type.Desc.field("u64reffarr10", u64reffarr10.offsetOf(), u64reffarr10.describe()),
              org.contract.type.Desc.field("u64carr10", u64carr10.offsetOf(), u64carr10.describe()),
              org.contract.type.Desc.field("u64farr5carr10", u64farr5carr10.offsetOf(), u64farr5carr10.describe()),
              org.contract.type.Desc.field("testu64farr5carr10", testu64farr5carr10.offsetOf(), testu64farr5carr10.describe()),
              org.contract.type.Desc.field("testu64reffarr5carr10", testu64reffarr5carr10.offsetOf(), testu64reffarr5carr10.describe())));
          }
          @Override
          public long alignOf() {
            long r = 0;
            r = Math.max(r, u64arr.alignOf());
            r = Math.max(r, u64refarr.alignOf());
            r = Math.max(r, u64optarr.alignOf());
            r = Math.max(r, u64farr10.alignOf());
            r = Math.max(r, u64farr10ref.alignOf());
            r = Math.max(r, u64farr10opt.alignOf());
            r = Math.max(r, u64optfarr10opt.alignOf());
            r = Math.max(r, u64reffarr10.alignOf());
            r = Math.max(r, u64carr10.alignOf());
            r = Math.max(r, u64farr5carr10.alignOf());
            r = Math.max(r, testu64farr5carr10.alignOf());
            r = Math.max(r, testu64reffarr5carr10.alignOf());
            return r;
          }
          @Override
          public Boolean from(long off, org.contract.type.Desc desc) {
            offsetOf(off);

            boolean r = true;
            r &= desc.with("u64arr", u64arr::from);
            r &= desc.with("u64refarr", u64refarr::from);
            r &= desc.with("u64optarr", u64optarr::from);
            r &= desc.with("u64farr10", u64farr10::from);
            r &= desc.with("u64farr10ref", u64farr10ref::from);
            r &= desc.with("u64farr10opt", u64farr10opt::from);
            r &= desc.with("u64optfarr10opt", u64optfarr10opt::from);
            r &= desc.with("u64reffarr10", u64reffarr10::from);
            r &= desc.with("u64carr10", u64carr10::from);
            r &= desc.with("u64farr5carr10", u64farr5carr10::from);
            r &= desc.with("testu64farr5carr10", testu64farr5carr10::from);
            r &= desc.with("testu64reffarr5carr10", testu64reffarr5carr10::from);
            return r;
          }
        }
      }
    }
    // all fields
    public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Long>> u64arr;
    public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>> u64refarr;
    public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>> u64optarr;
    public org.contract.field.array.Fixed<org.contract.field.Long, _10> u64farr10;
    public org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, _10>> u64farr10ref;
    public org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, _10>> u64farr10opt;
    public org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _10>> u64optfarr10opt;
    public org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _10> u64reffarr10;
    public org.contract.field.array.Capacity<org.contract.field.Long, _10> u64carr10;
    public org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, _5>, _10> u64farr5carr10;
    public org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, _5>, _10> testu64farr5carr10;
    public org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _5>, _10> testu64reffarr5carr10;
    // constructor.
    public R5(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> position) {
      super(unsafeBuffer, position);
      init(Meta);
    }
    // meta init.
    public void init(Meta meta) {
      write(index());
      u64arr = meta.u64arr.reify(wire(), write(index() + meta.u64arr.offsetOf()));
      u64refarr = meta.u64refarr.reify(wire(), write(index() + meta.u64refarr.offsetOf()));
      u64optarr = meta.u64optarr.reify(wire(), write(index() + meta.u64optarr.offsetOf()));
      u64farr10 = meta.u64farr10.reify(wire(), write(index() + meta.u64farr10.offsetOf()));
      u64farr10ref = meta.u64farr10ref.reify(wire(), write(index() + meta.u64farr10ref.offsetOf()));
      u64farr10opt = meta.u64farr10opt.reify(wire(), write(index() + meta.u64farr10opt.offsetOf()));
      u64optfarr10opt = meta.u64optfarr10opt.reify(wire(), write(index() + meta.u64optfarr10opt.offsetOf()));
      u64reffarr10 = meta.u64reffarr10.reify(wire(), write(index() + meta.u64reffarr10.offsetOf()));
      u64carr10 = meta.u64carr10.reify(wire(), write(index() + meta.u64carr10.offsetOf()));
      u64farr5carr10 = meta.u64farr5carr10.reify(wire(), write(index() + meta.u64farr5carr10.offsetOf()));
      testu64farr5carr10 = meta.testu64farr5carr10.reify(wire(), write(index() + meta.testu64farr5carr10.offsetOf()));
      testu64reffarr5carr10 = meta.testu64reffarr5carr10.reify(wire(), write(index() + meta.testu64reffarr5carr10.offsetOf()));
    }
    // process new wire buffer.
    @Override
    public WireI wire(UnsafeBuffer unsafeBuffer) {
      u64arr.wire(unsafeBuffer);
      u64refarr.wire(unsafeBuffer);
      u64optarr.wire(unsafeBuffer);
      u64farr10.wire(unsafeBuffer);
      u64farr10ref.wire(unsafeBuffer);
      u64farr10opt.wire(unsafeBuffer);
      u64optfarr10opt.wire(unsafeBuffer);
      u64reffarr10.wire(unsafeBuffer);
      u64carr10.wire(unsafeBuffer);
      u64farr5carr10.wire(unsafeBuffer);
      testu64farr5carr10.wire(unsafeBuffer);
      testu64reffarr5carr10.wire(unsafeBuffer);
      return super.wire(unsafeBuffer);
    }
  }
  public static Meta Meta = new Meta();
  public static class Meta {
    // field meta-descriptors
    public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Int>>> a = org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Int>>.Meta.type(0);
    public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Long>>> u64arr = org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Long>>.Meta.type(8);
    public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>>> u64refarr = org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>>.Meta.type(16);
    public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>>> u64optarr = org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>>.Meta.type(24);
    public final org.contract.type.Type<org.contract.field.array.Fixed<org.contract.field.Long, _10>> u64farr10 = org.contract.field.array.Fixed<org.contract.field.Long, _10>.Meta.type(32);
    public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, _10>>> u64farr10ref = org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, _10>>.Meta.type(112);
    public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, _10>>> u64farr10opt = org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, _10>>.Meta.type(120);
    public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _10>>> u64optfarr10opt = org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _10>>.Meta.type(128);
    public final org.contract.type.Type<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _10>> u64reffarr10 = org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _10>.Meta.type(136);
    public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.Long, _10>> u64carr10 = org.contract.field.array.Capacity<org.contract.field.Long, _10>.Meta.type(216);
    public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, _5>, _10>> u64farr5carr10 = org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, _5>, _10>.Meta.type(296);
    public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, _5>, _10>> testu64farr5carr10 = org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, _5>, _10>.Meta.type(696);
    public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _5>, _10>> testu64reffarr5carr10 = org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _5>, _10>.Meta.type(1096);
    public final org.contract.type.Type<org.contract.field.Int> b = org.contract.field.Int.Meta.type(1496);
    public final org.contract.type.Type<org.contract.field.Bool> bool = org.contract.field.Bool.Meta.type(1500);
    public final org.contract.type.Type<org.contract.field.Int> int = org.contract.field.Int.Meta.type(1504);
    public final org.contract.type.Type<org.contract.field.Char> char = org.contract.field.Char.Meta.type(1508);
    public final org.contract.type.Type<org.contract.field.Long> i64 = org.contract.field.Long.Meta.type(1512);
    public final org.contract.type.Type<org.contract.field.Int> tag = org.contract.field.Int.Meta.type(1520);
    public final org.contract.type.Type<org.contract.field.Int> group = org.contract.field.Int.Meta.type(1524);

    // type descriptor for record M1.M2.M3.R4
    public org.contract.type.Type<R4> Type = type();
    public org.contract.type.Type<R4> type() = { return type(0); }
    public org.contract.type.Type<R4> type(final long offset) {
      return new org.contract.type.Type<R4>(R4::new) {
        long off = offset;
        @Override
        public long offsetOf() { return off; }
        @Override
        public org.contract.type.Type<R4> offsetOf(long offset) {
          off = offset;
          return this;
        }
        @Override
        public long sizeOf(long eCount) {
          long r = 0;
          r = AlignOf.apply(r, a.alignOf()) + a.sizeOf();
          r = AlignOf.apply(r, u64arr.alignOf()) + u64arr.sizeOf();
          r = AlignOf.apply(r, u64refarr.alignOf()) + u64refarr.sizeOf();
          r = AlignOf.apply(r, u64optarr.alignOf()) + u64optarr.sizeOf();
          r = AlignOf.apply(r, u64farr10.alignOf()) + u64farr10.sizeOf();
          r = AlignOf.apply(r, u64farr10ref.alignOf()) + u64farr10ref.sizeOf();
          r = AlignOf.apply(r, u64farr10opt.alignOf()) + u64farr10opt.sizeOf();
          r = AlignOf.apply(r, u64optfarr10opt.alignOf()) + u64optfarr10opt.sizeOf();
          r = AlignOf.apply(r, u64reffarr10.alignOf()) + u64reffarr10.sizeOf();
          r = AlignOf.apply(r, u64carr10.alignOf()) + u64carr10.sizeOf();
          r = AlignOf.apply(r, u64farr5carr10.alignOf()) + u64farr5carr10.sizeOf();
          r = AlignOf.apply(r, testu64farr5carr10.alignOf()) + testu64farr5carr10.sizeOf();
          r = AlignOf.apply(r, testu64reffarr5carr10.alignOf()) + testu64reffarr5carr10.sizeOf();
          r = AlignOf.apply(r, b.alignOf()) + b.sizeOf();
          r = AlignOf.apply(r, bool.alignOf()) + bool.sizeOf();
          r = AlignOf.apply(r, int.alignOf()) + int.sizeOf();
          r = AlignOf.apply(r, char.alignOf()) + char.sizeOf();
          r = AlignOf.apply(r, i64.alignOf()) + i64.sizeOf();
          r = AlignOf.apply(r, tag.alignOf()) + tag.sizeOf();
          r = AlignOf.apply(r, group.alignOf()) + group.sizeOf();
          return eCount * AlignOf.apply(r, alignOf());
        }
        @Override
        public org.contract.type.Desc describe() {
          return org.contract.type.Desc.record(Arrays.asList(
            org.contract.type.Desc.field("a", a.offsetOf(), a.describe()),
            org.contract.type.Desc.field("u64arr", u64arr.offsetOf(), u64arr.describe()),
            org.contract.type.Desc.field("u64refarr", u64refarr.offsetOf(), u64refarr.describe()),
            org.contract.type.Desc.field("u64optarr", u64optarr.offsetOf(), u64optarr.describe()),
            org.contract.type.Desc.field("u64farr10", u64farr10.offsetOf(), u64farr10.describe()),
            org.contract.type.Desc.field("u64farr10ref", u64farr10ref.offsetOf(), u64farr10ref.describe()),
            org.contract.type.Desc.field("u64farr10opt", u64farr10opt.offsetOf(), u64farr10opt.describe()),
            org.contract.type.Desc.field("u64optfarr10opt", u64optfarr10opt.offsetOf(), u64optfarr10opt.describe()),
            org.contract.type.Desc.field("u64reffarr10", u64reffarr10.offsetOf(), u64reffarr10.describe()),
            org.contract.type.Desc.field("u64carr10", u64carr10.offsetOf(), u64carr10.describe()),
            org.contract.type.Desc.field("u64farr5carr10", u64farr5carr10.offsetOf(), u64farr5carr10.describe()),
            org.contract.type.Desc.field("testu64farr5carr10", testu64farr5carr10.offsetOf(), testu64farr5carr10.describe()),
            org.contract.type.Desc.field("testu64reffarr5carr10", testu64reffarr5carr10.offsetOf(), testu64reffarr5carr10.describe()),
            org.contract.type.Desc.field("b", b.offsetOf(), b.describe()),
            org.contract.type.Desc.field("bool", bool.offsetOf(), bool.describe()),
            org.contract.type.Desc.field("int", int.offsetOf(), int.describe()),
            org.contract.type.Desc.field("char", char.offsetOf(), char.describe()),
            org.contract.type.Desc.field("i64", i64.offsetOf(), i64.describe()),
            org.contract.type.Desc.field("tag", tag.offsetOf(), tag.describe()),
            org.contract.type.Desc.field("group", group.offsetOf(), group.describe())));
        }
        @Override
        public long alignOf() {
          long r = 0;
          r = Math.max(r, a.alignOf());
          r = Math.max(r, u64arr.alignOf());
          r = Math.max(r, u64refarr.alignOf());
          r = Math.max(r, u64optarr.alignOf());
          r = Math.max(r, u64farr10.alignOf());
          r = Math.max(r, u64farr10ref.alignOf());
          r = Math.max(r, u64farr10opt.alignOf());
          r = Math.max(r, u64optfarr10opt.alignOf());
          r = Math.max(r, u64reffarr10.alignOf());
          r = Math.max(r, u64carr10.alignOf());
          r = Math.max(r, u64farr5carr10.alignOf());
          r = Math.max(r, testu64farr5carr10.alignOf());
          r = Math.max(r, testu64reffarr5carr10.alignOf());
          r = Math.max(r, b.alignOf());
          r = Math.max(r, bool.alignOf());
          r = Math.max(r, int.alignOf());
          r = Math.max(r, char.alignOf());
          r = Math.max(r, i64.alignOf());
          r = Math.max(r, tag.alignOf());
          r = Math.max(r, group.alignOf());
          return r;
        }
        @Override
        public Boolean from(long off, org.contract.type.Desc desc) {
          offsetOf(off);

          boolean r = true;
          r &= desc.with("a", a::from);
          r &= desc.with("u64arr", u64arr::from);
          r &= desc.with("u64refarr", u64refarr::from);
          r &= desc.with("u64optarr", u64optarr::from);
          r &= desc.with("u64farr10", u64farr10::from);
          r &= desc.with("u64farr10ref", u64farr10ref::from);
          r &= desc.with("u64farr10opt", u64farr10opt::from);
          r &= desc.with("u64optfarr10opt", u64optfarr10opt::from);
          r &= desc.with("u64reffarr10", u64reffarr10::from);
          r &= desc.with("u64carr10", u64carr10::from);
          r &= desc.with("u64farr5carr10", u64farr5carr10::from);
          r &= desc.with("testu64farr5carr10", testu64farr5carr10::from);
          r &= desc.with("testu64reffarr5carr10", testu64reffarr5carr10::from);
          r &= desc.with("b", b::from);
          r &= desc.with("bool", bool::from);
          r &= desc.with("int", int::from);
          r &= desc.with("char", char::from);
          r &= desc.with("i64", i64::from);
          r &= desc.with("tag", tag::from);
          r &= desc.with("group", group::from);
          return r;
        }
      }
    }
  }
  // all fields
  public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Int>> a;
  public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Long>> u64arr;
  public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>> u64refarr;
  public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>> u64optarr;
  public org.contract.field.array.Fixed<org.contract.field.Long, _10> u64farr10;
  public org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, _10>> u64farr10ref;
  public org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, _10>> u64farr10opt;
  public org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _10>> u64optfarr10opt;
  public org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _10> u64reffarr10;
  public org.contract.field.array.Capacity<org.contract.field.Long, _10> u64carr10;
  public org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, _5>, _10> u64farr5carr10;
  public org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, _5>, _10> testu64farr5carr10;
  public org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, _5>, _10> testu64reffarr5carr10;
  public org.contract.field.Int b;
  public org.contract.field.Bool bool;
  public org.contract.field.Int int;
  public org.contract.field.Char char;
  public org.contract.field.Long i64;
  public org.contract.field.Int tag;
  public org.contract.field.Int group;
  // constructor.
  public R4(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> position) {
    super(unsafeBuffer, position);
    init(Meta);
  }
  // meta init.
  public void init(Meta meta) {
    write(index());
    a = meta.a.reify(wire(), write(index() + meta.a.offsetOf()));
    u64arr = meta.u64arr.reify(wire(), write(index() + meta.u64arr.offsetOf()));
    u64refarr = meta.u64refarr.reify(wire(), write(index() + meta.u64refarr.offsetOf()));
    u64optarr = meta.u64optarr.reify(wire(), write(index() + meta.u64optarr.offsetOf()));
    u64farr10 = meta.u64farr10.reify(wire(), write(index() + meta.u64farr10.offsetOf()));
    u64farr10ref = meta.u64farr10ref.reify(wire(), write(index() + meta.u64farr10ref.offsetOf()));
    u64farr10opt = meta.u64farr10opt.reify(wire(), write(index() + meta.u64farr10opt.offsetOf()));
    u64optfarr10opt = meta.u64optfarr10opt.reify(wire(), write(index() + meta.u64optfarr10opt.offsetOf()));
    u64reffarr10 = meta.u64reffarr10.reify(wire(), write(index() + meta.u64reffarr10.offsetOf()));
    u64carr10 = meta.u64carr10.reify(wire(), write(index() + meta.u64carr10.offsetOf()));
    u64farr5carr10 = meta.u64farr5carr10.reify(wire(), write(index() + meta.u64farr5carr10.offsetOf()));
    testu64farr5carr10 = meta.testu64farr5carr10.reify(wire(), write(index() + meta.testu64farr5carr10.offsetOf()));
    testu64reffarr5carr10 = meta.testu64reffarr5carr10.reify(wire(), write(index() + meta.testu64reffarr5carr10.offsetOf()));
    b = meta.b.reify(wire(), write(index() + meta.b.offsetOf()));
    bool = meta.bool.reify(wire(), write(index() + meta.bool.offsetOf()));
    int = meta.int.reify(wire(), write(index() + meta.int.offsetOf()));
    char = meta.char.reify(wire(), write(index() + meta.char.offsetOf()));
    i64 = meta.i64.reify(wire(), write(index() + meta.i64.offsetOf()));
    tag = meta.tag.reify(wire(), write(index() + meta.tag.offsetOf()));
    group = meta.group.reify(wire(), write(index() + meta.group.offsetOf()));
  }
  // process new wire buffer.
  @Override
  public WireI wire(UnsafeBuffer unsafeBuffer) {
    a.wire(unsafeBuffer);
    u64arr.wire(unsafeBuffer);
    u64refarr.wire(unsafeBuffer);
    u64optarr.wire(unsafeBuffer);
    u64farr10.wire(unsafeBuffer);
    u64farr10ref.wire(unsafeBuffer);
    u64farr10opt.wire(unsafeBuffer);
    u64optfarr10opt.wire(unsafeBuffer);
    u64reffarr10.wire(unsafeBuffer);
    u64carr10.wire(unsafeBuffer);
    u64farr5carr10.wire(unsafeBuffer);
    testu64farr5carr10.wire(unsafeBuffer);
    testu64reffarr5carr10.wire(unsafeBuffer);
    b.wire(unsafeBuffer);
    bool.wire(unsafeBuffer);
    int.wire(unsafeBuffer);
    char.wire(unsafeBuffer);
    i64.wire(unsafeBuffer);
    tag.wire(unsafeBuffer);
    group.wire(unsafeBuffer);
    return super.wire(unsafeBuffer);
  }
}
