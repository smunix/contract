package M1.M2;

/*
2019-11-13 23:42:03.893615 EST
*/

import org.contract.utils.function.Thunk5;
import org.contract.field.array.Dynamic;
import org.contract.utils.function.Thunk2;
import org.contract.exception.IllegalConversion;
import org.contract.utils.AlignOf;
import org.contract.utils.function.Thunk3;
import org.contract.utils.compile.Long._10;
import org.contract.utils.Pointer;
import java.io.PrintStream;
import org.contract.utils.function.Thunk1;
import org.contract.field.Char;
import java.util.Arrays;
import org.contract.field.array.Fixed;
import org.contract.field.Ref;
import org.contract.field.Int;
import org.contract.field.Double;
import java.io.ByteArrayOutputStream;
import org.contract.utils.function.Thunk0;
import org.agrona.concurrent.UnsafeBuffer;
import org.contract.type.Type;
import org.contract.type.Desc;
import org.contract.field.array.Capacity;
import org.contract.utils.compile.Long._5;
import org.contract.field.Long;

public static class R extends IndexWire {
  public static class N1 extends IndexWire {
    public static Meta Meta = new Meta();
    public static class Meta {
      // field meta-descriptors
      public final org.contract.type.Type<org.contract.field.Int> i = org.contract.field.Int.Meta.type(0);
      public final org.contract.type.Type<org.contract.field.Char> c = org.contract.field.Char.Meta.type(4);

      // type descriptor for record M1.M2.R.N1
      public org.contract.type.Type<N1> Type = type();
      public org.contract.type.Type<N1> type() = { return type(0); }
      public org.contract.type.Type<N1> type(final long offset) {
        return new org.contract.type.Type<N1>(N1::new) {
          long off = offset;
          @Override
          public long offsetOf() { return off; }
          @Override
          public org.contract.type.Type<N1> offsetOf(long offset) {
            off = offset;
            return this;
          }
          @Override
          public long sizeOf(long eCount) {
            long r = 0;
            r = AlignOf.apply(r, i.alignOf()) + i.sizeOf();
            r = AlignOf.apply(r, c.alignOf()) + c.sizeOf();
            return eCount * AlignOf.apply(r, alignOf());
          }
          @Override
          public org.contract.type.Desc describe() {
            return org.contract.type.Desc.record(Arrays.asList(
              org.contract.type.Desc.field("i", i.offsetOf(), i.describe()),
              org.contract.type.Desc.field("c", c.offsetOf(), c.describe())));
          }
          @Override
          public long alignOf() {
            long r = 0;
            r = Math.max(r, i.alignOf());
            r = Math.max(r, c.alignOf());
            return r;
          }
          @Override
          public Boolean from(long off, org.contract.type.Desc desc) {
            offsetOf(off);

            boolean r = true;
            r &= desc.with("i", i::from);
            r &= desc.with("c", c::from);
            return r;
          }
        }
      }
    }
    // all fields
    public org.contract.field.Int i;
    public org.contract.field.Char c;
    // constructor.
    public N1(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> position) {
      super(unsafeBuffer, position);
      init(Meta);
    }
    // meta init.
    public void init(Meta meta) {
      write(index());
      i = meta.i.reify(wire(), write(index() + meta.i.offsetOf()));
      c = meta.c.reify(wire(), write(index() + meta.c.offsetOf()));
    }
    // process new wire buffer.
    @Override
    public WireI wire(UnsafeBuffer unsafeBuffer) {
      i.wire(unsafeBuffer);
      c.wire(unsafeBuffer);
      return super.wire(unsafeBuffer);
    }
  }
  public static class N2 extends IndexWire {
    public static Meta Meta = new Meta();
    public static class Meta {
      // field meta-descriptors
      public final org.contract.type.Type<org.contract.field.Char> c = org.contract.field.Char.Meta.type(0);
      public final org.contract.type.Type<org.contract.field.Int> i = org.contract.field.Int.Meta.type(4);

      // type descriptor for record M1.M2.R.N2
      public org.contract.type.Type<N2> Type = type();
      public org.contract.type.Type<N2> type() = { return type(0); }
      public org.contract.type.Type<N2> type(final long offset) {
        return new org.contract.type.Type<N2>(N2::new) {
          long off = offset;
          @Override
          public long offsetOf() { return off; }
          @Override
          public org.contract.type.Type<N2> offsetOf(long offset) {
            off = offset;
            return this;
          }
          @Override
          public long sizeOf(long eCount) {
            long r = 0;
            r = AlignOf.apply(r, c.alignOf()) + c.sizeOf();
            r = AlignOf.apply(r, i.alignOf()) + i.sizeOf();
            return eCount * AlignOf.apply(r, alignOf());
          }
          @Override
          public org.contract.type.Desc describe() {
            return org.contract.type.Desc.record(Arrays.asList(
              org.contract.type.Desc.field("c", c.offsetOf(), c.describe()),
              org.contract.type.Desc.field("i", i.offsetOf(), i.describe())));
          }
          @Override
          public long alignOf() {
            long r = 0;
            r = Math.max(r, c.alignOf());
            r = Math.max(r, i.alignOf());
            return r;
          }
          @Override
          public Boolean from(long off, org.contract.type.Desc desc) {
            offsetOf(off);

            boolean r = true;
            r &= desc.with("c", c::from);
            r &= desc.with("i", i::from);
            return r;
          }
        }
      }
    }
    // all fields
    public org.contract.field.Char c;
    public org.contract.field.Int i;
    // constructor.
    public N2(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> position) {
      super(unsafeBuffer, position);
      init(Meta);
    }
    // meta init.
    public void init(Meta meta) {
      write(index());
      c = meta.c.reify(wire(), write(index() + meta.c.offsetOf()));
      i = meta.i.reify(wire(), write(index() + meta.i.offsetOf()));
    }
    // process new wire buffer.
    @Override
    public WireI wire(UnsafeBuffer unsafeBuffer) {
      c.wire(unsafeBuffer);
      i.wire(unsafeBuffer);
      return super.wire(unsafeBuffer);
    }
  }
  public static Meta Meta = new Meta();
  public static class Meta {
    // field meta-descriptors
    public final org.contract.type.Type<org.contract.field.Char> c = org.contract.field.Char.Meta.type(0);
    public final N1.Meta n1Meta = new N1.Meta();
    public final org.contract.type.Type<N1> n1 = n1Meta.type(4);
    public final org.contract.type.Type<org.contract.field.Int> i = org.contract.field.Int.Meta.type(12);
    public final N2.Meta n2Meta = new N2.Meta();
    public final org.contract.type.Type<N2> n2 = n2Meta.type(16);
    public final org.contract.type.Type<org.contract.field.Double> d = org.contract.field.Double.Meta.type(24);

    // type descriptor for record M1.M2.R
    public org.contract.type.Type<R> Type = type();
    public org.contract.type.Type<R> type() = { return type(0); }
    public org.contract.type.Type<R> type(final long offset) {
      return new org.contract.type.Type<R>(R::new) {
        long off = offset;
        @Override
        public long offsetOf() { return off; }
        @Override
        public org.contract.type.Type<R> offsetOf(long offset) {
          off = offset;
          return this;
        }
        @Override
        public long sizeOf(long eCount) {
          long r = 0;
          r = AlignOf.apply(r, c.alignOf()) + c.sizeOf();
          r = AlignOf.apply(r, n1.alignOf()) + n1.sizeOf();
          r = AlignOf.apply(r, i.alignOf()) + i.sizeOf();
          r = AlignOf.apply(r, n2.alignOf()) + n2.sizeOf();
          r = AlignOf.apply(r, d.alignOf()) + d.sizeOf();
          return eCount * AlignOf.apply(r, alignOf());
        }
        @Override
        public org.contract.type.Desc describe() {
          return org.contract.type.Desc.record(Arrays.asList(
            org.contract.type.Desc.field("c", c.offsetOf(), c.describe()),
            org.contract.type.Desc.field("n1", n1.offsetOf(), n1.describe()),
            org.contract.type.Desc.field("i", i.offsetOf(), i.describe()),
            org.contract.type.Desc.field("n2", n2.offsetOf(), n2.describe()),
            org.contract.type.Desc.field("d", d.offsetOf(), d.describe()),
        }
        @Override
        public long alignOf() {
          long r = 0;
          r = Math.max(r, c.alignOf());
          r = Math.max(r, n1.alignOf());
          r = Math.max(r, i.alignOf());
          r = Math.max(r, n2.alignOf());
          r = Math.max(r, d.alignOf());
          return r;
        }
        @Override
        public Boolean from(long off, org.contract.type.Desc desc) {
          offsetOf(off);

          boolean r = true;
          r &= desc.with("c", c::from);
          r &= desc.with("n1", n1::from);
          r &= desc.with("i", i::from);
          r &= desc.with("n2", n2::from);
          r &= desc.with("d", d::from);
          return r;
        }
      }
    }
  }
  // all fields
  public org.contract.field.Char c;
  public N1 n1;
  public org.contract.field.Int i;
  public N2 n2;
  public org.contract.field.Double d;
  // constructor.
  public R(UnsafeBuffer unsafeBuffer, Pointer<java.lang.Long> position) {
    super(unsafeBuffer, position);
    init(Meta);
  }
  // meta init.
  public void init(Meta meta) {
    write(index());
    c = meta.c.reify(wire(), write(index() + meta.c.offsetOf()));
    n1 = meta.n1.reify(wire(), write(index() + meta.n1.offsetOf()));
    n1.init(meta.n1Meta);
    i = meta.i.reify(wire(), write(index() + meta.i.offsetOf()));
    n2 = meta.n2.reify(wire(), write(index() + meta.n2.offsetOf()));
    n2.init(meta.n2Meta);
    d = meta.d.reify(wire(), write(index() + meta.d.offsetOf()));
  }
  // process new wire buffer.
  @Override
  public WireI wire(UnsafeBuffer unsafeBuffer) {
    c.wire(unsafeBuffer);
    n1.wire(unsafeBuffer);
    i.wire(unsafeBuffer);
    n2.wire(unsafeBuffer);
    d.wire(unsafeBuffer);
    return super.wire(unsafeBuffer);
  }
}
