-- It is generally a good idea to keep all your business logic in your library
-- and only use it in the executable. Doing so allows others to use what you
-- wrote in their libraries.

import qualified Language.Contract.Generator.Normalize as N
import qualified Language.Contract.Generator.Java as Java

import Control.Monad.IO.Class
import Data.Functor.Identity
import Data.String.Encode          (convertString)
import GHC.Generics                ( Generic, Generic1 )
import Control.Monad               ( mapM, void, when )
import Control.DeepSeq             ( NFData )
import Data.Data                   ( Data )
import Data.Typeable               ( Typeable )
import Data.Coerce                 ( coerce )
import qualified GHC.Exts as G

import qualified Control.Effect as Eff
import qualified Control.Effect.Error as Eff
import qualified Control.Effect.State as Eff
import Data.Coerce (coerce)
import Data.List (foldl', foldr)
import qualified System.IO as IO
-- import qualified System.EasyFile as IO
import qualified Data.Time.LocalTime as LocalTime
import qualified Data.HashMap.Strict as HM
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString.Lazy.Char8 as LC
import qualified Data.ByteString.Builder as B
import Data.ByteString.Short as B ( fromShort
                                  , toShort
                                  , ShortByteString
                                  )

import qualified Language.Contract.Syntax as Syn
import qualified Language.Contract.Parser as Parser
import qualified Language.Contract.Parser.Internal as Internal
import Language.Contract.Data hiding (Ident, name)
import qualified Language.Contract.Data as Data

import Path ( Path
            , Abs
            , Dir
            , File
            , (</>)
            )
import qualified Path
import qualified Path.IO as Path
import Options.Applicative as Opt

data Command where
  Java :: Command
  Cpp :: Command
  Python :: Command
  deriving (Show)

data Config where
  Config :: { cfgCommand :: Command
            , cfgOutputDir :: !(Path Abs Dir)
            } -> Config
  deriving (Show)

rootedDir :: Path Abs Dir -> ReadM (Path Abs Dir)
rootedDir root = either id (root </>) <$> (absDir <|> relDir)
  where
    absDir = Left <$> maybeReader Path.parseAbsDir
    relDir = Right <$> maybeReader Path.parseRelDir

rootedFile :: Path Abs Dir -> ReadM (Path Abs File)
rootedFile root = either id (root </>) <$> (absFile <|> relFile)
  where
    absFile = Left <$> maybeReader Path.parseAbsFile
    relFile = Right <$> maybeReader Path.parseRelFile

main :: IO ()
main = do
  ztime <- LocalTime.getZonedTime
  return ()
