-- It is generally a good idea to keep all your business logic in your library
-- and only use it in the executable. Doing so allows others to use what you
-- wrote in their libraries.

import qualified Language.Contract.Generator.Normalize as N
import qualified Language.Contract.Generator.Java as Java

import Control.Monad.IO.Class
import Data.Functor.Identity
import Data.String.Encode          (convertString)
import GHC.Generics                ( Generic, Generic1 )
import Control.Monad               ( mapM, void, when )
import Control.DeepSeq             ( NFData )
import Data.Data                   ( Data )
import Data.Typeable               ( Typeable )
import Data.Coerce                 ( coerce )
import qualified GHC.Exts as G

import qualified Control.Effect as Eff
import qualified Control.Effect.Error as Eff
import qualified Control.Effect.State as Eff
import Data.Coerce (coerce)
import Data.List (foldl', foldr)
import qualified System.IO as IO
import qualified System.EasyFile as IO
import qualified Data.Time.LocalTime as LocalTime
import qualified Data.HashMap.Strict as HM
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString.Lazy.Char8 as LC
import qualified Data.ByteString.Builder as B
import Data.ByteString.Short as B ( fromShort
                                  , toShort
                                  , ShortByteString
                                  )

import qualified Language.Contract.Syntax as Syn
import qualified Language.Contract.Parser as Parser
import qualified Language.Contract.Parser.Internal as Internal
import Language.Contract.Data hiding (Ident, name)
import qualified Language.Contract.Data as Data

program :: (Show a) => a -> N.Str
program a = preambleStr <> expandStr <> moduleStr
  where
    preambleStr :: N.Str
    preambleStr = "language(java) {%\n"
                  <> "/*\n"
                  <> G.fromString (show a)
                  <> "\n"
                  <> "*/\n"
                  <> "\n"
                  <> "%}\n"
                  <> "language(cpp) {%\n"
                  <> "#  include <iostream>;\n"
                  <> "#  include <cpp/lang/String>;\n"
                  <> "#  include <cpp/lang/Long>;\n"
                  <> "#  include <cpp/lang/Type>;\n"
                  <> "#  include \"local/cpp/lang/Type.H\">;\n"
                  <> "%}\n"
                  <> "language(python) {%\n"
                  <> "import org.py.String\n"
                  <> "import org.py.Long\n"
                  <> "import org.py.Type\n"
                  <> "%}\n"

    expandStr :: N.Str
    expandStr = "  module M1 {\n"
                <> "  module M2{\n"
                <> "    record R{\n"
                <> "      record N1{\n"
                <> "        i :: i32;\n"
                <> "        c :: i8;\n"
                <> "      };\n"
                <> "      c :: i8;\n"
                <> "      n1 :: N1;\n"
                <> "      i :: i32;\n"
                <> "      n2 :: N2;\n"
                <> "      d :: f64;\n"
                <> "      record N2{\n"
                <> "        c :: i8;\n"
                <> "        i :: i32;\n"
                <> "      };\n"
                <> "    }\n"
                <> "    record A1 {\n"
                <> "      record N1 {\n"
                <> "        i :: i32;\n"
                <> "        c :: i8;\n"
                <> "        record N2 {\n"
                <> "          i :: i32;\n"
                <> "          c :: i8;\n"
                <> "          record N3 {\n"
                <> "            i :: i32;\n"
                <> "            l :: u64;\n"
                <> "          };\n"
                <> "        };\n"
                <> "      };\n"
                <> "      c :: i8;\n"
                <> "      n1 :: @[N3|10];\n"
                <> "      i :: i32;\n"
                <> "      n2 :: N2;\n"
                <> "      n3 :: N3;\n"
                <> "      d :: f64;\n"
                <> "      record N6 {\n"
                <> "        c :: i8;\n"
                <> "        i :: i32;\n"
                <> "      };\n"
                <> "      n6 :: N6;\n"
                <> "    }\n"
                <> "  }\n"
                <> "}\n"

    moduleStr :: N.Str
    moduleStr = "   module M1 {\n"
                <> "  module M2 {\n"
                <> "    module M3 {\n"
                <> "      record R4 {\n"
                <> "        a :: [int];\n"
                <> "        record R5 {\n"
                <> "          u64arr                :: [u64];\n"
                <> "          u64refarr             :: [@u64] group(20);\n"
                <> "          u64optarr             :: [?u64];\n"
                <> "          u64farr10             :: [u64|10];\n"
                <> "          u64farr10ref          :: @[u64|10];\n"
                <> "          u64farr10opt          :: ?[u64|10];\n"
                <> "          u64optfarr10opt       :: ?[?u64|10];\n"
                <> "          u64reffarr10          :: [@u64|10];\n"
                <> "          u64carr10             :: [|u64|10];\n"
                <> "          u64farr5carr10        :: [|[u64|5]|10];\n"
                <> "          testu64farr5carr10    :: [|[test.u64|5]|10];\n"
                <> "          testu64reffarr5carr10 :: [|[@test.u64|5]|10];\n"
                <> "        };\n"
                <> "        u64arr                :: [u64];\n"
                <> "        u64refarr             :: [@u64] group(20);\n"
                <> "        u64optarr             :: [?u64];\n"
                <> "        u64farr10             :: [u64|10];\n"
                <> "        u64farr10ref          :: @[u64|10];\n"
                <> "        u64farr10opt          :: ?[u64|10];\n"
                <> "        u64optfarr10opt       :: ?[?u64|10];\n"
                <> "        u64reffarr10          :: [@u64|10];\n"
                <> "        u64carr10             :: [|u64|10];\n"
                <> "        u64farr5carr10        :: [|[u64|5]|10];\n"
                <> "        testu64farr5carr10    :: [|[test.u64|5]|10];\n"
                <> "        testu64reffarr5carr10 :: [|[@test.u64|5]|10];\n"
                <> "        b     :: std.int tag(10);\n"
                <> "        bool  :: bool;\n"
                <> "        int   :: int;\n"
                <> "        char  :: char;\n"
                <> "        i64   :: i64;\n"
                <> "        tag   :: int;\n"
                <> "        group :: std.contract.int;\n"
                <> "      }\n"
                <> "    }\n"
                <> "    module M6 {\n"
                <> "      record R7 {\n"
                <> "        b :: int;\n"
                <> "        r2r1m3m2reffarr5carr10 :: [|[@M2.M3.R1.R2|5]|10];\n"
                <> "      }\n"
                <> "    };\n"
                <> "  }\n"
                <> "}\n"


main :: IO ()
main = do
  ztime <- LocalTime.getZonedTime
  either
    (putStrLn . show)
    (\e ->
       do
        let
          expr' = N.toExpr N.Java Nothing $ e
          env' = N.Env $ N.toExprEnv N.Java <> N.toExprEnv expr'
          evfs = Java.run env' (Java.contractGen expr')

        either
          (putStrLn . show)
          (\ (Java.VFS vfs) ->
             do
               HM.foldlWithKey'
                 (\ acc fp Java.FileContent{..} ->
                    do
                      putStrLn . show $ fp
                      when
                        (C.isSuffixOf (C.pack ".java") (C.pack fp))
                        (do
                            let
                              (d, _) = IO.splitFileName fp
                            IO.createDirectoryIfMissing True d
                            IO.withFile
                              fp
                              IO.ReadWriteMode
                              (\h ->
                                  do
                                    foldr
                                      (\ !ln !acc ->
                                         do
                                           B.hPutBuilder h ln
                                           acc
                                      )
                                      (pure ())
                                      fcStore
                              )
                        )
                      acc
                 )
                 (pure ())
                 vfs
          )
          evfs
    ) (Parser.run Internal.parseFile (coerce (program ztime)) initPosition)
