module Language.Contract.Syntax.Ast ( File(..)
                              , PreambleL(..)
                              , Module(..)
                              , Record(..)
                              , Field(..)
                              , Type(..)
                              , Tag(..)
                              , Lang(..)
                              ) where

import Language.Contract.Data.Ident      ( Ident, Name )
import Language.Contract.Data.Position
import Language.Contract.Data.Reversed   ( Reversed )
import Language.Contract.Syntax.Token    ( Delim, Token )
import qualified Language.Contract.Syntax.Token as Tok

import GHC.Generics                ( Generic, Generic1 )

import Control.DeepSeq             ( NFData )
import Data.Data                   ( Data )
import Data.Typeable               ( Typeable )

import Data.Char                   ( ord )
import Data.List                   ( partition )
import Data.List.NonEmpty          ( NonEmpty(..) )
import Data.Semigroup              ( Semigroup(..) )
import Data.Monoid                 ( Monoid(..) )
import Data.Word                   ( Word8 )
import Data.Hashable               ( Hashable )
import Data.HashMap.Strict         ( HashMap(..), unionWith )

data Lang where
  CppL :: Lang
  JavaL :: Lang
  PythonL :: Lang
  deriving (Enum, Bounded, Eq, Ord, Show, Typeable, Data, Generic, NFData)
instance Hashable Lang

data Type a where
  NoType :: Type a
  Type :: Spanned Tok.Token
       -> Type a
       -> Type a
  Ref :: Type a
      -> Type a
  Array :: Type Span
        -> Type a
  FArray :: Type Span
         -> Spanned Int
         -> Type a
  CArray :: Type Span
         -> Spanned Int
         -> Type a
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

data Tag a where
  NoTag :: Tag a
  Tag :: Spanned Tok.Token
      -> Tag a
  Group :: Spanned Tok.Token
        -> Tag a
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

data Field a where
  Field :: Spanned Tok.Token
        -> Type a
        -> Tag a
        -> Field a
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

data Record a where
  Record :: Spanned Tok.Token
         -> [ Either (Record Span) (Field Span) ]
         -> Record a
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

data Module a where
  Module :: Spanned Tok.Token
         -> [ Either (Module Span) (Record Span) ]
         -> Module a
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

data PreambleL a where
  NoPreambleL :: PreambleL a
  PreambleL :: HashMap Lang [String]
           -> PreambleL a
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

instance Monoid (PreambleL a) where
  mempty = NoPreambleL
  mappend = (<>)

instance Semigroup (PreambleL a) where
  x <> NoPreambleL = x
  NoPreambleL <> x = x
  PreambleL x <> PreambleL y = PreambleL $ unionWith (<>) x y

data File a where
  File :: PreambleL a
       -> [ Either (Module a) (Record a) ]
       -> File a
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)
