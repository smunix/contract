module Language.Contract.Generator.Effects ( Indent(..)
                                           , VFSP(..)
                                           , VFS(..)
                                           , FST(..)
                                           , FileSystem(..)
                                           , runFS
                                           , EachRecord(..)
                                           , runEachRecord
                                           , EachField(..)
                                           , runEachField
                                           , FileContent(..)
                                           , FilePreamble(..)
                                           , ContractGen(..)
                                           , ContractGenT
                                           , runContractGen
                                           , ContractGenRecordInfo(..)
                                           , ContractGenRecord(..)
                                           , ContractGenRecordT(..)
                                           , withPreamble
                                           , withRecord
                                           ) where

import qualified Language.Contract.Generator.Normalize as N

import Control.Monad.IO.Class
import Control.Monad.Trans.Control (liftWith, restoreT)
import Control.Monad.Trans.Class   (lift)
import Data.Functor.Identity
import Data.String.Encode          (convertString)
import GHC.Generics                ( Generic, Generic1 )
import Control.Monad               ( mapM, void )
import Control.DeepSeq             ( NFData )
import Data.Data                   ( Data )
import Data.Typeable               ( Typeable )
import Data.Coerce                 ( coerce )
import qualified GHC.Exts as G

import qualified Control.Effect as Eff
import qualified Control.Effect.Error as Eff
import qualified Control.Effect.State as Eff
import Data.Coerce (coerce)
import Data.List (foldl')
import qualified System.IO as IO

import qualified Data.HashMap.Strict as HM
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString.Lazy.Char8 as LC
import qualified Data.ByteString.Builder as B
import Data.ByteString.Short as B ( fromShort
                                  , toShort
                                  , ShortByteString
                                  )

import qualified Language.Contract.Syntax as Syn
import qualified Language.Contract.Parser as Parser
import qualified Language.Contract.Parser.Internal as Internal
import Language.Contract.Data hiding (Ident, name)
import qualified Language.Contract.Data as Data

instance Show B.Builder where
  showsPrec d = showsPrec d . LC.unpack . B.toLazyByteString

instance Eq B.Builder where
  a == b = (LC.unpack . B.toLazyByteString $ a) == (LC.unpack . B.toLazyByteString $ b)

newtype FilePreamble = FilePreamble [String]
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

instance Semigroup FilePreamble where
  FilePreamble a <> FilePreamble b = coerce (a <> b)

instance Monoid FilePreamble where
  mempty = FilePreamble mempty
  mappend = (<>)

data FileContent a where
  FileContent :: { fcStore :: a
                 , fcIndent :: Indent
                 }
              -> FileContent a
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

instance (Semigroup a) => Semigroup (FileContent a) where
  FileContent s i <> FileContent s' i' = FileContent (s <> s') (max i i')

instance (Monoid a) => Monoid (FileContent a) where
  mempty = FileContent mempty mempty
  mappend = (<>)

instance (G.IsString a) => G.IsString (FileContent [a]) where
  fromString s = FileContent{..}
    where
      fcStore = [G.fromString s]
      fcIndent = mempty

data VFS ioh where
  VFS :: { vfs :: (HM.HashMap FilePath (FileContent ioh)) }
      -> VFS ioh
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

instance (Semigroup ioh) => Semigroup (VFS ioh) where
  VFS a <> VFS b = VFS $ HM.unionWith (<>) a b

instance (Semigroup ioh) => Monoid (VFS ioh) where
  mempty = VFS HM.empty
  mappend = (<>)

data ContractGenRecordInfo where
  ContractGenRecordInfo :: { riFilePath :: FilePath
                     , riRecord :: N.Expr
                     }
                  -> ContractGenRecordInfo
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

-- | EachField : effectful fold on each record field
class (Monad m) => EachField m where
  eachField :: m b -- ^ default computation (failure ?)
            -> m a -- ^ begin
            -> (N.MemLayout -> N.Node N.Expr -> N.Type -> N.Tag -> m b) -- ^ action on fields
            -> (N.Expr -> m b) -- ^ action on records
            -> m a -- ^ terminate
            -> m a -- ^ end
            -> m b

instance (Monad (t m), Eff.Send EachField t m) => EachField (Eff.EffT t m) where
  eachField def b actionField actionRecord t e = Eff.sendWith @EachField
    (eachField (Eff.runEffT def) (Eff.runEffT b) (\ml e t tg -> Eff.runEffT $ actionField ml e t tg)  (\r -> Eff.runEffT $ actionRecord r) (Eff.runEffT t) (Eff.runEffT e))
    (do
        s <- liftWith $
             \lower -> eachField
                       (lower . Eff.runEffT $ def)
                       (lower . Eff.runEffT $ b)
                       (\ml e t tg -> lower . Eff.runEffT $ actionField ml e t tg)
                       (\r -> lower . Eff.runEffT $ actionRecord r)
                       (lower . Eff.runEffT $ t)
                       (lower . Eff.runEffT $ e)
        restoreT (pure s)
    )

-- EachFieldPure pure handler
data EachFieldPure
type EachFieldT = Eff.HandlerT EachFieldPure '[Eff.StateT ContractGenRecordInfo, Eff.StateT N.MemLayout, Eff.StateT N.ExprEnv]
type instance Eff.Handles EachFieldT eff = eff Eff.== EachField

instance (Monad m) => EachField (EachFieldT m) where
  eachField def b actionField actionRecord t e = Eff.HandlerT $ do
    ri@ContractGenRecordInfo{..} <- Eff.get @ContractGenRecordInfo
    eEnv@(N.ExprEnv eEnvHM) <- Eff.get @N.ExprEnv
    let
      N.RecordE rin@(N.Node{..}) = riRecord
      -- | apply to a field record
      fieldFn (N.FieldE child tp tg) =
        do
          ml@N.MemLayout{..} <- Eff.get @N.MemLayout
          N.MemLayout _ al' <- N.addMemLayout @"type" rin eEnv mempty tp
          Eff.put @N.MemLayout $ ml{ N.mlSizeOf = N.alignOf mlSizeOf al' }

          ml@N.MemLayout{..} <- Eff.get @N.MemLayout
          ret <- Eff.runHandlerT $ actionField ml child tp tg
          N.MemLayout sz' _ <- N.addMemLayout @"type" rin eEnv mempty tp
          Eff.put @N.MemLayout ml{ N.mlSizeOf = mlSizeOf + sz' }

          _ <- Eff.runHandlerT $ t
          pure ret
      -- | apply to a nested record
      fieldFn (r@N.RecordE{}) =
        do
          Eff.runHandlerT $ actionRecord r

    _ <- Eff.runHandlerT $ b
    foldl'
      (\s f ->
         do
           _ <- s
           _ <- fieldFn f
           pure ()
      )
      (pure ())
      (init . reverse $ childrenList)

    case last . reverse $ childrenList of
      N.FieldE lchild ltp ltg ->
        do
          ml@N.MemLayout{..} <- Eff.get @N.MemLayout
          N.MemLayout _ al' <- N.addMemLayout @"type" rin eEnv mempty ltp
          Eff.put @N.MemLayout ml{ N.mlSizeOf = N.alignOf mlSizeOf al' }
      _ -> pure ()

    ml@N.MemLayout{..} <- Eff.get @N.MemLayout
    (case last . reverse $ childrenList of
       r@N.RecordE{} ->
         do
           Eff.runHandlerT $ actionRecord r

       N.FieldE lchild ltp ltg ->
         do
           s'' <- Eff.runHandlerT $ actionField ml lchild ltp ltg
           ml@N.MemLayout{..} <- Eff.get @N.MemLayout
           N.MemLayout sz' _ <- N.addMemLayout @"type" rin eEnv mempty ltp
           Eff.put @N.MemLayout ml{ N.mlSizeOf = mlSizeOf + sz' }
           _ <- Eff.runHandlerT $ e
           return s''

       _ ->
         do
           Eff.runHandlerT $ def
      )

runEachField :: (Monad m) => N.ExprEnv
             -> ContractGenRecordInfo
             -> Eff.EffT EachFieldT m a
             -> m a
runEachField eEnv ri = Eff.evalState eEnv . Eff.evalState mempty . Eff.evalState ri . Eff.runHandlerT . Eff.runEffT

-- | EachRecord : effectful fold on each nested record
class (Monad m) => EachRecord m where
  eachRecord :: m b -- ^ default computation
             -> m a -- ^ begin
             -> (N.Expr -> m b) -- ^ action
             -> m a -- ^ terminate
             -> m a -- ^ end
             -> m b

instance (Monad (t m), Eff.Send EachRecord t m) => EachRecord (Eff.EffT t m) where
  eachRecord def b action t e = Eff.sendWith @EachRecord
    (eachRecord (Eff.runEffT def) (Eff.runEffT b) (Eff.runEffT . action) (Eff.runEffT t) (Eff.runEffT e))
    (do
        s <- liftWith $
             \lower -> eachRecord
                       (lower . Eff.runEffT $ def)
                       (lower . Eff.runEffT $ b)
                       (lower . Eff.runEffT . action)
                       (lower . Eff.runEffT $ t)
                       (lower . Eff.runEffT $ e)
        restoreT (pure s)
    )

-- EachRecordPure pure handler
data EachRecordPure
type EachRecordT = Eff.HandlerT EachRecordPure '[Eff.StateT ContractGenRecordInfo]
type instance Eff.Handles EachRecordT eff = eff Eff.== EachRecord

instance (Monad m) => EachRecord (EachRecordT m) where
  eachRecord def b actionRecord t e = Eff.HandlerT $ do
    ri@ContractGenRecordInfo{..} <- Eff.get @ContractGenRecordInfo
    let
      N.RecordE (N.Node{..}) = riRecord
      fieldFn (N.FieldE child tp tg) =
        do
          Eff.runHandlerT $ def

      fieldFn (r@N.RecordE{}) =
        do
          ret <- Eff.runHandlerT $ actionRecord r
          _ <- Eff.runHandlerT $ t
          pure ret

    _ <- Eff.runHandlerT $ b

    foldl'
      (\s f ->
         do
           _ <- s
           _ <- fieldFn f
           pure ()
      )
      (pure ())
      (init . reverse $ childrenList)

    ret <- (case last . reverse $ childrenList of
              N.FieldE lchild ltp ltg ->
                do
                  Eff.runHandlerT $ def

              r@N.RecordE{} ->
                do
                  Eff.runHandlerT $ actionRecord r
           )
    _ <- Eff.runHandlerT $ e
    pure ret

runEachRecord :: (Monad m) => ContractGenRecordInfo
              -> Eff.EffT EachRecordT m a
              -> m a
runEachRecord ri = Eff.evalState ri . Eff.runHandlerT . Eff.runEffT

-- | ContractGenRecord effect for record generation
class (Monad m) => ContractGenRecord m where
  beginTopRecord    :: m ()
  beginNestedRecord :: m ()
  endRecord         :: m ()
  beginMeta         :: m ()
  endMeta           :: m ()
  beginFields       :: m ()
  endFields         :: m ()
  beginConstructors :: m ()
  endConstructors   :: m ()

instance (Monad (t m), Eff.Send ContractGenRecord t m) => ContractGenRecord (Eff.EffT t m) where
  beginTopRecord    = Eff.send @ContractGenRecord (beginTopRecord)
  beginNestedRecord = Eff.send @ContractGenRecord (beginNestedRecord)
  endRecord         = Eff.send @ContractGenRecord (endRecord)
  beginMeta         = Eff.send @ContractGenRecord (beginMeta)
  endMeta           = Eff.send @ContractGenRecord (endMeta)
  beginFields       = Eff.send @ContractGenRecord (beginFields)
  endFields         = Eff.send @ContractGenRecord (endFields)
  beginConstructors = Eff.send @ContractGenRecord (beginConstructors)
  endConstructors   = Eff.send @ContractGenRecord (endConstructors)

-- ContractGenRecordPure Pure handler
data ContractGenRecordPure
type ContractGenRecordT = Eff.HandlerT ContractGenRecordPure '[FST, Eff.StateT ContractGenRecordInfo]
type instance Eff.Handles ContractGenRecordT eff = eff Eff.== ContractGenRecord

runContractGenRecord :: (Monad m) => ContractGenRecordInfo
               -> Eff.EffT ContractGenRecordT m a
               -> m a
runContractGenRecord ri = Eff.evalState ri . runFS . Eff.runHandlerT . Eff.runEffT

-- | Contract effect generator
class (Monad m) => ContractGen m where
  -- | create leaf module
  createRecordInModule :: N.Expr -- ^ record : r@(N.RecordE (N.Node{..}))
                       -> N.Expr -- ^ parent module : m@(N.ModuleE{})
                       -> FilePreamble -- ^ File header to write in file
                       -> m ()
  -- | create nested record
  createRecordNested :: N.Expr -- ^ nested record : me@(N.RecordE (N.Node{..}))
                     -> N.Expr -- ^ parent record : pr@(N.RecordE{})
                     -> m ()
  -- | create root record
  createRecordRoot :: N.Expr -- ^ nested record : me@(N.RecordE (N.Node{..}))
                   -> N.Expr -- ^ parent root : pr@(N.RootE{})
                   -> m ()
  -- | generate package directive
  writePackageDirective :: FilePath -- ^ file to write into
                        -> N.Expr -- ^ parent module
                        -> m ()
  writeFilePreamble :: FilePath -- ^ file to write into
                    -> FilePreamble -- ^ file header to write
                    -> m ()
  collectImports :: N.Expr -- ^ collect and build all necessary imports from this expression
                 -> m ()
  writeImports :: FilePath
               -> N.Import
               -> m ()

instance (Monad (t m), Eff.Send ContractGen t m) => ContractGen (Eff.EffT t m) where
  createRecordInModule r m fh = Eff.send @ContractGen (createRecordInModule r m fh)
  createRecordNested r m      = Eff.send @ContractGen (createRecordNested r m)
  createRecordRoot r m        = Eff.send @ContractGen (createRecordRoot r m)
  writePackageDirective fp m  = Eff.send @ContractGen (writePackageDirective fp m)
  writeFilePreamble fp fh     = Eff.send @ContractGen (writeFilePreamble fp fh)
  collectImports r            = Eff.send @ContractGen (collectImports r)
  writeImports fp ip          = Eff.send @ContractGen (writeImports fp ip)

-- ContractGen Pure handler
data ContractGenPure
type ContractGenT = Eff.HandlerT ContractGenPure '[ FST ]
type instance Eff.Handles ContractGenT eff = eff Eff.== ContractGen

runContractGen :: (Monad m) => Eff.EffT ContractGenT m a -> m a
runContractGen = runFS . Eff.runHandlerT . Eff.runEffT

-- | Indentation state
newtype Indent = Indent Int
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)
  deriving newtype Num

instance Semigroup Indent where
  i1 <> i2 = Indent (coerce i1 + coerce i2)

instance Monoid Indent where
  mempty = 0
  mappend = (+)

-- | File system data manipulation effect
class (Monad m) => FileSystem m where
  -- | create a directory
  mkDirFS      :: N.ShortStr -- ^ dir path, <a.b.c.>
               -> m ()
  -- | create a file
  mkDirFileFS  :: N.ShortStr -- ^ file path, <d>
               -> N.ShortStr -- ^ dir path, <a.b.c>
               -> m ()
  mkFilePathFS :: N.ShortStr -- ^ full name of module or record <a.b.d.c>
               -> m FilePath
  putStrFS     :: FilePath -- ^ filepath to write to
               -> [B.Builder] -- ^ content to write to filepath
               -> m ()
  -- | putStrFS followed by carriage return
  putStrLnFS   :: FilePath -- ^ filepath to write to
               -> [B.Builder] -- ^ content to write to filepath
               -> m ()
  newLineFS    :: FilePath
               -> m () -- ^ filepath to write new line to
  appendFS     :: FilePath -- ^ filepath to write to
               -> [B.Builder] -- ^ content to write to filepath
               -> m ()
  tabFS        :: FilePath
               -> m () -- ^ filepath to write indentations to
  delFS        :: FilePath
               -> m () -- ^ filepath to delete indentations from

instance (Monad (t m), Eff.Send FileSystem t m) => FileSystem (Eff.EffT t m) where
  mkDirFS d       = Eff.send @FileSystem (mkDirFS d)
  mkDirFileFS f d = Eff.send @FileSystem (mkDirFileFS f d)
  mkFilePathFS n  = Eff.send @FileSystem (mkFilePathFS n)
  putStrFS fp c   = Eff.send @FileSystem (putStrFS fp c)
  putStrLnFS fp c = Eff.send @FileSystem (putStrLnFS fp c)
  newLineFS fp    = Eff.send @FileSystem (newLineFS fp)
  appendFS fp c   = Eff.send @FileSystem (appendFS fp c)
  tabFS fp        = Eff.send @FileSystem (tabFS fp)
  delFS fp        = Eff.send @FileSystem (delFS fp)

-- FS Pure handler
data FSPure
type FST = Eff.HandlerT FSPure '[]
type instance Eff.Handles FST eff = eff Eff.== FileSystem
type PureFS = [B.Builder]
type VFSP = VFS PureFS

instance (Eff.Error String m, Eff.State (VFSP) m, Eff.State N.Env m, Eff.State FilePreamble m) => FileSystem (FST m) where
  mkDirFS d = do
    putStrFS ((C.unpack . convertString $ d) <> ".mkdir") mempty
    pure ()

  mkDirFileFS f d = do
    fn <- mkFilePathFS (d <> "." <> f)
    putStrFS fn mempty
    pure ()

  mkFilePathFS n = return $ (C.unpack . C.intercalate "/" . C.splitWith (=='.') . fromShort $ n) <> ".java"

  putStrLnFS fp str = putStrFS fp (fmap (<> "\n") str)

  putStrFS fp str = Eff.HandlerT $ Eff.modify @(VFSP) (\ ovfs@VFS{..} ->
                                                        let
                                                          wrWithIndent :: Maybe (FileContent [B.Builder]) -> Maybe (FileContent [B.Builder])
                                                          wrWithIndent = \case
                                                            Just (fc@FileContent{..}) -> Just fc{ fcStore = fcStore
                                                                                                            <> (fmap (G.fromString (replicate (coerce fcIndent) ' ') <>) str)
                                                                                                }
                                                            _ -> Just FileContent{ fcStore = str
                                                                                 , fcIndent = 0
                                                                                 }
                                                        in
                                                          ovfs{ vfs = HM.alter wrWithIndent fp vfs}
                                                     )

  newLineFS fp = appendFS fp ["\n"]

  appendFS fp str = Eff.HandlerT $ Eff.modify @(VFSP) (\ ovfs@VFS{..} ->
                                                         let
                                                          wrNoIndent :: Maybe (FileContent [B.Builder]) -> Maybe (FileContent [B.Builder])
                                                          wrNoIndent = \case
                                                             Just (fc@FileContent{..}) -> Just fc{ fcStore = fcStore
                                                                                                             <> str
                                                                                                 }
                                                             _ -> Just FileContent{ fcStore = str
                                                                                  , fcIndent = 0
                                                                                  }
                                                         in
                                                           ovfs{ vfs = HM.alter wrNoIndent fp vfs}
                                                      )

  tabFS fp = Eff.HandlerT $ Eff.modify @(VFSP) (VFS . HM.update (\ fc@FileContent{..} -> (Just fc{fcIndent = fcIndent + 2})) fp . vfs)

  delFS fp = Eff.HandlerT $ Eff.modify @(VFSP) (VFS . HM.update (\ fc@FileContent{..} -> (Just fc{fcIndent = max 0 (fcIndent - 2)})) fp . vfs)

runFS :: (Monad m) => Eff.EffT FST m a -> m a
runFS = Eff.runHandlerT . Eff.runEffT

-- | all suported effects
type EffsT m r = Eff.EffsT '[ContractGenT, FST, Eff.StateT N.Env, Eff.StateT (VFSP), Eff.StateT FilePreamble, Eff.StateT N.Import, Eff.ExceptT String] m r
type EffsTI r = EffsT Identity r

withPreamble :: (Monad m) => (N.Expr -> m ())
             -> N.Expr
             -> m ()
withPreamble pf = \case
  N.RootE (N.Node{..}) -> void $ mapM (withPreamble pf) childrenList
  x@(N.PreambleE n@(N.Node{}) _) -> pf x
  _ -> pure ()

-- | Iterate each record under a module
-- | and run an action on each
-- |
withRecord :: (Monad m) => (N.Expr -> m ())
           -> N.Expr
           -> m ()
withRecord tf = \case
  e@(N.RecordE n@(N.Node{..})) ->
    do
      tf e
      void $ mapM (withRecord tf) childrenList

  x@(N.RootE n@(N.Node{..})) -> void $ mapM (withRecord tf) childrenList

  x@(N.ModuleE n@(N.Node{..})) -> void $ mapM (withRecord tf) childrenList

  x -> pure ()

