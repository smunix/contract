module Language.Contract.Generator.Java ( run
                                        , contractGen
                                        , Indent(..)
                                        , VFSP(..)
                                        , VFS(..)
                                        , FileContent(..)
                                        ) where

import qualified Debug.Trace as Debug

import Language.Contract.Generator.Effects
import qualified Language.Contract.Generator.Normalize as N

import Control.Monad.IO.Class
import Control.Monad.Trans.Control (liftWith, restoreT)
import Control.Monad.Trans.Class   (lift)
import Data.Functor.Identity
import Data.String.Encode          (convertString)
import GHC.Generics                ( Generic, Generic1 )
import Control.Monad               ( mapM, void, when )
import Control.DeepSeq             ( NFData )
import Data.Data                   ( Data )
import Data.Typeable               ( Typeable )
import Data.Coerce                 ( coerce )
import Data.Bifunctor
import qualified GHC.Exts as G

import qualified Control.Effect as Eff
import qualified Control.Effect.Error as Eff
import qualified Control.Effect.State as Eff
import Data.Coerce (coerce)
import Data.List (foldl')
import qualified System.IO as IO

import qualified Data.HashMap.Strict as HM
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString.Lazy.Char8 as LC
import qualified Data.ByteString.Builder as B
import Data.ByteString.Short as B ( fromShort
                                  , toShort
                                  , ShortByteString
                                  )
import qualified Data.ByteString.Short as Short

import qualified Language.Contract.Syntax as Syn
import qualified Language.Contract.Parser as Parser
import qualified Language.Contract.Parser.Internal as Internal
import Language.Contract.Data hiding (Ident, name)
import qualified Language.Contract.Data as Data

primExprEnv :: N.ExprEnv
primExprEnv = N.ExprEnv . HM.fromList $ [ ("org.contract.field.Bool", N.NoExprE)
                                        , ("org.contract.field.Byte", N.NoExprE)
                                        , ("org.contract.field.Char", N.NoExprE)
                                        , ("org.contract.field.Short", N.NoExprE)
                                        , ("org.contract.field.Int", N.NoExprE)
                                        , ("org.contract.field.Float", N.NoExprE)
                                        , ("org.contract.field.Long", N.NoExprE)
                                        , ("org.contract.field.Double", N.NoExprE)
                                        ]

-- | Closure on record types in the current environment
withTyRec :: N.ExprEnv -- ^ environment to search types from
          -> N.Type -- ^ field type to operate on
          -> (N.Expr -> m ()) -- ^ action for record types
          -> m () -- ^ action for non-record types
          -> m ()
withTyRec (N.ExprEnv eEnvHM) (N.Prim n fn _) isTyRecFn isNotTyRec
  | elem (C.unpack . convertString $ n) ([ "bool"
                                         , "char"
                                         , "byte"
                                         , "i8"
                                         , "u8"
                                         , "short"
                                         , "i16"
                                         , "u16"
                                         , "i32"
                                         , "u32"
                                         , "float"
                                         , "f32"
                                         , "int"
                                         , "f64"
                                         , "i64"
                                         , "u64"
                                         , "long"
                                         , "double"
                                         ] :: [String]) = isNotTyRec
    | otherwise = (case (HM.lookupDefault N.NoExprE fn eEnvHM) of
                      r@(N.RecordE N.Node{..}) -> isTyRecFn r
                      _ -> isNotTyRec
                  )
withTyRec e'@(N.ExprEnv eEnvHM) (N.Ref tp) isTyRecFn isNotTyRec = withTyRec e' tp isTyRecFn isNotTyRec
withTyRec e'@(N.ExprEnv eEnvHM) (N.Array tp) isTyRecFn isNotTyRec = withTyRec e' tp isTyRecFn isNotTyRec
withTyRec e'@(N.ExprEnv eEnvHM) (N.FArray tp i) isTyRecFn isNotTyRec = withTyRec e' tp isTyRecFn isNotTyRec
withTyRec e'@(N.ExprEnv eEnvHM) (N.CArray tp i) isTyRecFn isNotTyRec = withTyRec e' tp isTyRecFn isNotTyRec
withTyRec (N.ExprEnv eEnvHM)  _ _ isNotTyRec = isNotTyRec

typeBuilder :: N.ShortStr -> N.ExprEnv -> N.Type -> B.Builder
typeBuilder prefix (N.ExprEnv eEnvHM) (N.Prim n fn _)
  | C.unpack (convertString n) == "int"    = B.shortByteString "org.contract.field.Int"
  | C.unpack (convertString n) == "bool"   = B.shortByteString "org.contract.field.Bool"
  | C.unpack (convertString n) == "byte"   = B.shortByteString "org.contract.field.Byte"
  | C.unpack (convertString n) == "char"   = B.shortByteString "org.contract.field.Char"
  | C.unpack (convertString n) == "short"  = B.shortByteString "org.contract.field.Short"
  | C.unpack (convertString n) == "int"    = B.shortByteString "org.contract.field.Int"
  | C.unpack (convertString n) == "float"  = B.shortByteString "org.contract.field.Float"
  | C.unpack (convertString n) == "long"   = B.shortByteString "org.contract.field.Long"
  | C.unpack (convertString n) == "double" = B.shortByteString "org.contract.field.Double"
  | C.unpack (convertString n) == "i8"     = B.shortByteString "org.contract.field.Char"
  | C.unpack (convertString n) == "u8"     = B.shortByteString "org.contract.field.Byte"
  | C.unpack (convertString n) == "i16"    = B.shortByteString "org.contract.field.Short"
  | C.unpack (convertString n) == "u16"    = B.shortByteString "org.contract.field.Short"
  | C.unpack (convertString n) == "i32"    = B.shortByteString "org.contract.field.Int"
  | C.unpack (convertString n) == "u32"    = B.shortByteString "org.contract.field.Int"
  | C.unpack (convertString n) == "i64"    = B.shortByteString "org.contract.field.Long"
  | C.unpack (convertString n) == "u64"    = B.shortByteString "org.contract.field.Long"
  | C.unpack (convertString n) == "f32"    = B.shortByteString "org.contract.field.Float"
  | C.unpack (convertString n) == "f64"    = B.shortByteString "org.contract.field.Double"
  | otherwise                              = (case (HM.lookupDefault N.NoExprE fn eEnvHM) of
                                                r@(N.RecordE N.Node{..}) ->
                                                  let
                                                    prefixLen = if Short.null prefix
                                                                then 0
                                                                else 1 + Short.length prefix
                                                  in
                                                    B.byteString . B.drop prefixLen . convertString $ nameFull
                                                _ -> B.shortByteString fn
                                             )

typeBuilder prefix eEnv (N.Ref tp) = "org.contract.field.Ref<" <> typeBuilder prefix eEnv tp <> ">"
typeBuilder prefix eEnv (N.Array tp) = "org.contract.field.array.Dynamic<" <> typeBuilder prefix eEnv tp <> ">"
typeBuilder prefix eEnv (N.FArray tp i) = "org.contract.field.array.Fixed<" <> typeBuilder prefix eEnv tp <> ", org.contract.utils.compile.Long._" <> B.intDec i <> ">"
typeBuilder prefix eEnv (N.CArray tp i) = "org.contract.field.array.Capacity<" <> typeBuilder prefix eEnv tp <> ", org.contract.utils.compile.Long._" <> B.intDec i <> ">"
typeBuilder _ _ x = B.string8 (show x)

newMetaTypeBuilder :: N.ShortStr -> N.ExprEnv -> N.Type -> Int -> B.Builder
newMetaTypeBuilder prefix (N.ExprEnv eEnvHM) (N.Prim n fn _) boff
  | C.unpack (convertString n) == "int"    = B.byteString "new org.contract.field.Int.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "bool"   = B.byteString "new org.contract.field.Bool.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "byte"   = B.byteString "new org.contract.field.Byte.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "char"   = B.byteString "new org.contract.field.Char.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "short"  = B.byteString "new org.contract.field.Short.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "int"    = B.byteString "new org.contract.field.Int.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "float"  = B.byteString "new org.contract.field.Float.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "long"   = B.byteString "new org.contract.field.Long.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "double" = B.byteString "new org.contract.field.Double.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "i8"     = B.byteString "new org.contract.field.Char.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "u8"     = B.byteString "new org.contract.field.Byte.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "i16"    = B.byteString "new org.contract.field.Short.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "u16"    = B.byteString "new org.contract.field.Short.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "i32"    = B.byteString "new org.contract.field.Int.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "u32"    = B.byteString "new org.contract.field.Int.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "i64"    = B.byteString "new org.contract.field.Long.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "u64"    = B.byteString "new org.contract.field.Long.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "f32"    = B.byteString "new org.contract.field.Float.Meta().type(" <> B.intDec boff <> ")"
  | C.unpack (convertString n) == "f64"    = B.byteString "new org.contract.field.Double.Meta().type(" <> B.intDec boff <> ")"
  | otherwise                              = (case (HM.lookupDefault N.NoExprE fn eEnvHM) of
                                                r@(N.RecordE N.Node{..}) ->
                                                  let
                                                    prefixLen = if Short.null prefix
                                                                then 0
                                                                else 1 + Short.length prefix
                                                  in
                                                    B.byteString . B.drop prefixLen . convertString $ nameFull
                                                _ -> B.shortByteString fn
                                             )

newMetaTypeBuilder prefix eEnv (N.Ref tp) boff = "new org.contract.field.Ref.Meta().type(" <> newMetaTypeBuilder prefix eEnv tp 0 <> ", " <> B.intDec boff <> ")"
newMetaTypeBuilder prefix eEnv (N.Array tp) boff = "new org.contract.field.array.Dynamic.Meta().type(" <> B.intDec boff <> ", " <> newMetaTypeBuilder prefix eEnv tp 0 <> ")"
newMetaTypeBuilder prefix eEnv (N.FArray tp i) boff = "new org.contract.field.array.Fixed.Meta().type(" <> B.intDec boff <> ", " <> newMetaTypeBuilder prefix eEnv tp 0 <> ", new org.contract.utils.compile.Long._" <> B.intDec i <> "())"
newMetaTypeBuilder prefix eEnv (N.CArray tp i) boff = "new org.contract.field.array.Capacity.Meta().type(" <> B.intDec boff <> ", "  <> newMetaTypeBuilder prefix eEnv tp 0 <> ", new org.contract.utils.compile.Long._"  <> B.intDec i <> ")"
newMetaTypeBuilder _ _ x _ = B.string8 (show x)

primTypeBuilder :: N.ShortStr -> N.ExprEnv -> N.Type -> Either B.Builder B.Builder
primTypeBuilder prefix (N.ExprEnv eEnvHM) (N.Prim n fn _)
  | C.unpack (convertString n) == "int"    = Right $ B.shortByteString "org.contract.field.Int"
  | C.unpack (convertString n) == "bool"   = Right $ B.shortByteString "org.contract.field.Bool"
  | C.unpack (convertString n) == "byte"   = Right $ B.shortByteString "org.contract.field.Byte"
  | C.unpack (convertString n) == "char"   = Right $ B.shortByteString "org.contract.field.Char"
  | C.unpack (convertString n) == "short"  = Right $ B.shortByteString "org.contract.field.Short"
  | C.unpack (convertString n) == "int"    = Right $ B.shortByteString "org.contract.field.Int"
  | C.unpack (convertString n) == "float"  = Right $ B.shortByteString "org.contract.field.Float"
  | C.unpack (convertString n) == "long"   = Right $ B.shortByteString "org.contract.field.Long"
  | C.unpack (convertString n) == "double" = Right $ B.shortByteString "org.contract.field.Double"
  | C.unpack (convertString n) == "i8"     = Right $ B.shortByteString "org.contract.field.Char"
  | C.unpack (convertString n) == "u8"     = Right $ B.shortByteString "org.contract.field.Byte"
  | C.unpack (convertString n) == "i16"    = Right $ B.shortByteString "org.contract.field.Short"
  | C.unpack (convertString n) == "u16"    = Right $ B.shortByteString "org.contract.field.Short"
  | C.unpack (convertString n) == "i32"    = Right $ B.shortByteString "org.contract.field.Int"
  | C.unpack (convertString n) == "u32"    = Right $ B.shortByteString "org.contract.field.Int"
  | C.unpack (convertString n) == "i64"    = Right $ B.shortByteString "org.contract.field.Long"
  | C.unpack (convertString n) == "u64"    = Right $ B.shortByteString "org.contract.field.Long"
  | C.unpack (convertString n) == "f32"    = Right $ B.shortByteString "org.contract.field.Float"
  | C.unpack (convertString n) == "f64"    = Right $ B.shortByteString "org.contract.field.Double"
  | otherwise                              = (case (HM.lookupDefault N.NoExprE fn eEnvHM) of
                                                r@(N.RecordE N.Node{..}) ->
                                                  let
                                                    prefixLen = if Short.null prefix
                                                                then 0
                                                                else 1 + Short.length prefix
                                                  in
                                                    Right $ B.byteString . B.drop prefixLen . convertString $ nameFull
                                                _ -> Left $ B.shortByteString fn
                                             )
primTypeBuilder prefix eEnv (N.Ref tp) = primTypeBuilder prefix eEnv tp
primTypeBuilder prefix eEnv (N.Array tp) = primTypeBuilder prefix eEnv tp
primTypeBuilder prefix eEnv (N.FArray tp i) = primTypeBuilder prefix eEnv tp
primTypeBuilder prefix eEnv (N.CArray tp i) = primTypeBuilder prefix eEnv tp
primTypeBuilder _ _ x = Left $ B.string8 (show x)

-- | creates an environment for expressions where the current field type has been resolved
--   into a more elaborated form.
mkFieldExprEnv :: N.ShortStr -- ^ enclosing record short name
               -> N.ShortStr -- ^ enclosing record full name
               -> N.Type -- ^ field type to decode
               -> N.ExprEnv -- ^ environment of all existing expressions in the program
               -> Either N.ExprEnv N.ExprEnv
mkFieldExprEnv riName riNameFull tp (N.ExprEnv exprEnv') =
  HM.foldlWithKey'
  (\acc k v ->
      let
        riNameFullBS :: B.ByteString
        riNameFullBS = convertString riNameFull
        riNameBS :: B.ByteString
        riNameBS = convertString riName
        kBS :: B.ByteString
        kBS = convertString k
        tpBS :: Either B.ByteString B.ByteString
        tpBS = case acc of
          Left a -> either (Left . convertString . B.toLazyByteString) (Right . convertString . B.toLazyByteString) $ primTypeBuilder "" a tp
          Right a -> either (Left . convertString . B.toLazyByteString) (Right . convertString . B.toLazyByteString) $ primTypeBuilder "" a tp
        isFieldTyNested :: Either Bool Bool
        isFieldTyNested = bimap (flip B.isSuffixOf (kBS)) (flip B.isSuffixOf (kBS)) (tpBS)
        isFieldTyInRec :: Bool
        isFieldTyInRec = B.isPrefixOf (riNameFullBS) (kBS)
      in
        either
        (\iftn ->
           if isFieldTyInRec && iftn
           then
             case acc of
               Left a -> either
                         (\tpbs -> Left $ (N.ExprEnv $ HM.singleton (convertString tpbs) v) <> a)
                         (\tpbs -> Left $ (N.ExprEnv $ HM.singleton (convertString tpbs) v) <> a)
                         tpBS
               Right a -> either
                          (\tpbs -> Left $ (N.ExprEnv $ HM.singleton (convertString tpbs) v) <> a)
                          (\tpbs -> Left $ (N.ExprEnv $ HM.singleton (convertString tpbs) v) <> a)
                          tpBS
           else
             acc
        )
        (\iftn ->
           if isFieldTyInRec && iftn
           then
             case acc of
               Left a -> either
                         (\tpbs -> Right $ (N.ExprEnv $ HM.singleton (convertString tpbs) v) <> a)
                         (\tpbs -> Right $ (N.ExprEnv $ HM.singleton (convertString tpbs) v) <> a)
                         tpBS
               Right a -> either
                          (\tpbs -> Right $ (N.ExprEnv $ HM.singleton (convertString tpbs) v) <> a)
                          (\tpbs -> Right $ (N.ExprEnv $ HM.singleton (convertString tpbs) v) <> a)
                          tpBS
           else
             acc
        )
        isFieldTyNested
  )
  ((Right primExprEnv) :: Either N.ExprEnv N.ExprEnv)
  (exprEnv')

instance (Monad m, Eff.State VFSP m, Eff.Error String m, Eff.State FilePreamble m, Eff.State N.Env m) => ContractGenRecord (ContractGenRecordT m) where
  beginTopRecord = Eff.HandlerT $ do
    ContractGenRecordInfo{..} <- Eff.get @ContractGenRecordInfo
    let
      N.RecordE (N.Node{..}) = riRecord

    putStrLnFS riFilePath [ "public class " <> B.shortByteString name <> " extends org.contract.field.base.IndexWire {" ]
    pure ()

  beginNestedRecord = Eff.HandlerT $ do
    ContractGenRecordInfo{..} <- Eff.get @ContractGenRecordInfo
    let
      N.RecordE (N.Node{..}) = riRecord

    putStrLnFS riFilePath [ "public static class " <> B.shortByteString name <> " extends org.contract.field.base.IndexWire {" ]
    pure ()

  endRecord = Eff.HandlerT $ do
    ContractGenRecordInfo{..} <- Eff.get @ContractGenRecordInfo
    putStrLnFS riFilePath ["}"]
    pure ()

  beginMeta = Eff.HandlerT $ do
    ri@ContractGenRecordInfo{..} <- Eff.get @ContractGenRecordInfo
    let
      N.RecordE (N.Node{..}) = riRecord
      riName = name
      riNameFull = nameFull
      override = "@Override"
      str = putStrFS riFilePath
      strLn = putStrLnFS riFilePath
      appendStr = appendFS riFilePath
      tab = tabFS riFilePath
      del = delFS riFilePath
      nLn = newLineFS riFilePath
      withIndent action = do
        tab
        action
        del

    -- output nested records first, if any
    withIndent (
      do
        N.Env eEnv@(N.ExprEnv eEnvHM) <- Eff.get @N.Env
        runEachField
          eEnv
          ri
          (eachField
            (pure ())
            (pure ())
            (\ml@N.MemLayout{..} N.Node{..} tp tg -> pure ())
            (\r@N.RecordE{} ->
                do
                  runContractGenRecord
                    (ContractGenRecordInfo { riFilePath = riFilePath
                                           , riRecord = r
                                           })
                    (do
                        beginNestedRecord
                        beginMeta
                        endMeta
                        beginFields
                        endFields
                        beginConstructors
                        endConstructors
                        endRecord
                        pure ()
                    )
                  pure ()
            )
            (pure ())
            (pure ())
          )
      )

    withIndent (
      do
        strLn [ "public static Meta Meta = new Meta();"
              , "public static class Meta {"
              ]
        withIndent (
          do
            N.Env eEnv@(N.ExprEnv eEnvHM) <- Eff.get @N.Env
            runEachField
              eEnv
              ri
              (eachField
                (pure ())
                (do
                    pure()
                )
                (\ml@N.MemLayout{..} N.Node{..} tp tg ->
                   do
                     pure ()
                )
                (\r@(N.RecordE N.Node{..}) -> do
                    pure ()
                )
                (pure ())
                (pure ())
              )
            -- write type metas (effect ?)
            runEachField
              eEnv
              ri
              (eachField
                (pure ())
                (do
                    putStrLnFS riFilePath ["// field meta-descriptors"]
                    pure()
                )
                (\ml@N.MemLayout{..} fld@N.Node{..} tp tg ->
                   do
                     N.Env eEnv <- Eff.get @N.Env
                     let
                       riExprEnv = mkFieldExprEnv
                                   riName
                                   riNameFull
                                   tp
                                   eEnv
                       print = \rieEnv@(N.ExprEnv rieEnvHM) ->
                         do
                           withTyRec
                             rieEnv
                             tp
                             (\ (N.RecordE r@N.Node{..}) ->
                                do
                                  -- field type points to a nested record
                                  let
                                    prefixLen = if Short.null riNameFull
                                                then 0
                                                else 1 + Short.length riNameFull

                                    -- | build a meta.type() constructor for Java
                                    metaTypeBuilder :: B.ByteString -- ^ (module/package) prefix
                                                    -> (N.Node N.Expr, Int, N.Type, N.Node N.Expr) -- ^ (Field, Offset, Type, Ground Record Type)
                                                    -> Bool -- ^ is top-level type (ctor) ?
                                                    -> B.Builder
                                    metaTypeBuilder prefix (fld, off, N.Ref tp', rcd) True =
                                      "new org.contract.field.Ref.Meta().type("
                                      <> (metaTypeBuilder
                                           prefix
                                           (fld, off, tp', rcd)
                                           False
                                         )
                                      <> ", "
                                      <> B.intDec off <> ")"
                                    metaTypeBuilder prefix (fld, off, N.Ref tp', rcd) False =
                                      "new org.contract.field.Ref.Meta().type("
                                      <> (metaTypeBuilder
                                           prefix
                                           (fld, off, tp', rcd)
                                           False
                                         )
                                      <> ", "
                                      <> B.intDec 0 <> ")"
                                    metaTypeBuilder prefix (fld, off, N.Array tp', rcd) True =
                                      "new org.contract.field.array.Dynamic.Meta().type("
                                      <> B.intDec off
                                      <> ", "
                                      <> (metaTypeBuilder
                                           prefix
                                           (fld, off, tp', rcd)
                                           False
                                         )
                                      <> ")"
                                    metaTypeBuilder prefix (fld, off, N.Array tp', rcd) False =
                                      "new org.contract.field.array.Dynamic.Meta().type("
                                      <> B.intDec 0
                                      <> ", "
                                      <> (metaTypeBuilder
                                           prefix
                                           (fld, off, tp', rcd)
                                           False
                                         )
                                      <> ")"
                                    metaTypeBuilder prefix (fld, off, N.FArray tp' sz', rcd) True =
                                      "new org.contract.field.array.Fixed.Meta().type("
                                      <> B.intDec off
                                      <> ", "
                                      <> (metaTypeBuilder
                                           prefix
                                           (fld, off, tp', rcd)
                                           False
                                         )
                                      <> ", new org.contract.utils.compile.Long._"
                                      <> B.intDec sz'
                                      <> "())"
                                    metaTypeBuilder prefix (fld, off, N.FArray tp' sz', rcd) False =
                                      "new org.contract.field.array.Fixed.Meta().type("
                                      <> B.intDec 0
                                      <> ", "
                                      <> (metaTypeBuilder
                                           prefix
                                           (fld, off, tp', rcd)
                                           False
                                         )
                                      <> ", new org.contract.utils.compile.Long._"
                                      <> B.intDec sz'
                                      <> "())"
                                    metaTypeBuilder prefix (fld, off, N.CArray tp' sz', rcd) True =
                                      "new org.contract.field.array.Capacity.Meta().type("
                                      <> B.intDec off
                                      <> ", "
                                      <> (metaTypeBuilder
                                           prefix
                                           (fld, off, tp', rcd)
                                           False
                                         )
                                      <> ", new org.contract.utils.compile.Long._"
                                      <> B.intDec sz'
                                      <> "())"
                                    metaTypeBuilder prefix (fld, off, N.CArray tp' sz', rcd) False =
                                      "new org.contract.field.array.Capacity.Meta().type("
                                      <> B.intDec 0
                                      <> ", "
                                      <> (metaTypeBuilder
                                           prefix
                                           (fld, off, tp', rcd)
                                           False
                                         )
                                      <> ", new org.contract.utils.compile.Long._"
                                      <> B.intDec sz'
                                      <> "())"

                                    metaTypeBuilder prefix (fld, off, tp, rcd) True =
                                      let
                                        isPrefix = B.isPrefixOf (prefix) (convertString $ N.nameFull rcd)
                                        nfrcd
                                          | isPrefix = B.byteString . B.drop prefixLen . convertString $ N.nameFull rcd
                                          | otherwise = B.shortByteString $ N.nameFull rcd
                                      in
                                        B.string8 "new " <> nfrcd <> ".Meta().type("
                                        <> B.intDec off
                                        <> B.string8 ")"

                                    metaTypeBuilder prefix (fld, off, tp, rcd) False =
                                      let
                                        isPrefix = B.isPrefixOf (prefix) (convertString $ N.nameFull rcd)
                                        nfrcd
                                          | isPrefix = B.byteString . B.drop prefixLen . convertString $ N.nameFull rcd
                                          | otherwise = B.shortByteString $ N.nameFull rcd
                                      in
                                        B.string8 "new " <> nfrcd  <> ".Meta().type("
                                        <> B.intDec 0
                                        <> B.string8 ")"

                                  -- create field type descriptor
                                  putStrLnFS riFilePath [ "public final org.contract.type.Type<"
                                                          <> typeBuilder (convertString riNameFull) rieEnv tp
                                                          <> "> "
                                                          <> (B.shortByteString $ N.name fld)
                                                          <> " = "
                                                          <> metaTypeBuilder (convertString riNameFull) (fld, mlSizeOf, tp, r) True
                                                          <> ";"
                                                        ]
                                  pure ()
                             )
                             (do
                                 -- field type _does not_ point to a nested record
                                 putStrLnFS riFilePath [ "public final org.contract.type.Type<"
                                                         <> typeBuilder (convertString riNameFull) rieEnv tp
                                                         <> "> "
                                                         <> (B.shortByteString $ N.name fld)
                                                         <> " = "
                                                         <> newMetaTypeBuilder (convertString riNameFull) rieEnv tp (mlSizeOf)
                                                         <> ";"
                                                       ]
                             )

                     either
                       (print)
                       (print)
                       riExprEnv

                     pure ()
                )
                (\r@(N.RecordE N.Node{..}) -> do
                    pure ()
                )
                (pure ())
                (pure ())
              )
            -- write type desc
            nLn
            strLn ["// type descriptor for record " <> B.shortByteString nameFull <> ""]
            strLn [ "public org.contract.type.Type<" <> B.shortByteString name <> "> Type = type();" ]
            strLn [ "public org.contract.type.Type<" <> B.shortByteString name <> "> type() { return type(0); }" ]
            strLn [ "public org.contract.type.Type<" <> B.shortByteString name <> "> type(final long offset) {" ]
            withIndent (
              do
                strLn [ "return new org.contract.type.Type<" <> B.shortByteString name <> ">(" <> B.shortByteString name <> "::new) {" ]
                withIndent (
                  do
                    let
                      N.RecordE rin@(N.Node{}) = riRecord
                      recName = B.shortByteString (N.name rin)

                    strLn [ "long off = offset;" ]
                    -- offsetOf()
                    strLn [ override ]
                    strLn [ "public long offsetOf() { return off; }" ]
                    -- offsetOf(long)
                    strLn [ override ]
                    strLn [ "public org.contract.type.Type<" <> B.shortByteString name <> "> offsetOf(long offset) {" ]
                    withIndent (
                      do
                        strLn [ "off = offset;" ]
                        strLn [ "return this;" ]
                      )
                    strLn [ "}" ]
                    -- create()
                    strLn [ override ]
                    strLn [ "public " <> recName <> " create(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> index) {" ]
                    withIndent (
                      do
                        strLn [ recName <> " r = super.create(unsafeBuffer, index);" ]
                        strLn [ "r.write(r.index());" ]
                        runEachField
                          eEnv
                          ri
                          (eachField
                            (pure ())
                            (pure ())
                            (\ml@N.MemLayout{..} N.Node{..} tp tg ->
                                do
                                  putStrLnFS riFilePath [ "r." <> B.shortByteString name <> " = " <> B.shortByteString name <> ".create(r.wire(), r.write(r.index() + " <> B.shortByteString name <> ".offsetOf()));"
                                                        ]
                                  pure ()
                            )
                            (\r@N.RecordE{} -> pure ())
                            (pure ())
                            (pure ())
                          )
                        strLn [ "r.write(r.index() + sizeOf());" ]
                        strLn [ "return r;" ]
                      )
                    strLn [ "}" ]
                    -- sizeOf()
                    strLn [ override ]
                    strLn [ "public long sizeOf(long eCount) {" ]
                    withIndent (
                      do
                        strLn [ "long r = 0;" ]
                        runEachField
                          eEnv
                          ri
                          (eachField
                            (pure ())
                            (pure ())
                            (\ml@N.MemLayout{..} N.Node{..} tp tg ->
                                do
                                  putStrLnFS riFilePath [ "r = org.contract.utils.AlignOf.apply(r, "
                                                          <> B.shortByteString name
                                                          <> ".alignOf()) + "
                                                          <> B.shortByteString name
                                                          <> ".sizeOf();"
                                                        ]
                                  pure ()
                            )
                            (\r@N.RecordE{} -> pure ())
                            (pure ())
                            (pure ())
                          )
                        strLn [ "return eCount * org.contract.utils.AlignOf.apply(r, alignOf());" ]
                      )
                    strLn [ "}" ]
                    -- describe()
                    strLn [ override ]
                    strLn [ "public org.contract.type.Desc describe() {" ]
                    withIndent (
                      do
                        strLn [ "return org.contract.type.Desc.record(java.util.Arrays.asList(" ]
                        withIndent(
                          runEachField
                            eEnv
                            ri
                            (eachField
                              (pure ())
                              (pure ())
                              (\ml@N.MemLayout{..} N.Node{..} tp tg ->
                                 do
                                   str [ "org.contract.type.Desc.field(\""
                                         <> B.shortByteString name
                                         <> "\", "
                                         <> B.shortByteString name
                                         <> ".offsetOf(), "
                                         <> B.shortByteString name
                                         <> ".describe())"
                                       ]
                                   pure ()
                              )
                              (\r@N.RecordE{} -> pure ())
                              (appendStr [",\n"])
                              (appendStr ["));\n"])
                            )
                          )
                      )
                    strLn [ "}" ]
                    -- alignOf()
                    strLn [ override ]
                    strLn [ "public long alignOf() {" ]
                    withIndent (
                      do
                        strLn [ "long r = 0;" ]
                        runEachField
                          eEnv
                          ri
                          (eachField
                            (pure ())
                            (pure ())
                            (\ml@N.MemLayout{..} N.Node{..} tp tg ->
                               do
                                 putStrLnFS riFilePath [ "r = Math.max(r, "
                                                         <> B.shortByteString name
                                                         <> ".alignOf());"
                                                       ]
                                 pure ()
                            )
                            (\r@N.RecordE{} -> pure ())
                            (pure ())
                            (pure ())
                          )
                        strLn [ "return r;" ]
                      )
                    strLn [ "}" ]
                    -- from(long, Desc)
                    strLn [ override ]
                    strLn [ "public Boolean from(long off, org.contract.type.Desc desc) {" ]
                    withIndent (
                      do
                        strLn [ "offsetOf(off);" ]
                        nLn
                        strLn [ "boolean r = true;" ]
                        runEachField
                          eEnv
                          ri
                          (eachField
                            (pure ())
                            (pure ())
                            (\ml@N.MemLayout{..} N.Node{..} tp tg ->
                                do
                                  putStrLnFS riFilePath  [ "r &= desc.with(\""
                                                           <> B.shortByteString name
                                                           <> "\", "
                                                           <> B.shortByteString name
                                                           <> "::from);"
                                                         ]
                                  pure()
                            )
                            (\r@N.RecordE{} -> pure ())
                            (pure ())
                            (pure ())
                          )
                        strLn [ "return r;" ]
                      )
                    strLn [ "}" ]
                  )
                strLn [ "};" ]
              )
            strLn [ "}" ]
          )
        strLn [ "}" ]
      )
    pure ()

  endMeta = Eff.HandlerT $ do
    ContractGenRecordInfo{..} <- Eff.get @ContractGenRecordInfo
    let
      N.RecordE (N.Node{..}) = riRecord
    pure ()

  beginFields = Eff.HandlerT $ do
    ri@ContractGenRecordInfo{..} <- Eff.get @ContractGenRecordInfo
    let
      N.RecordE (N.Node{..}) = riRecord
      riNameFull = nameFull
      riName = name
      tab = tabFS riFilePath
      del = delFS riFilePath
      withIndent action = do
        tab
        action
        del

    withIndent (do
                   N.Env eEnv@(N.ExprEnv eEnvHM) <- Eff.get @N.Env
                   runEachField
                     eEnv
                     ri
                     (eachField
                       (pure ())
                       (putStrLnFS riFilePath ["// all fields"])
                       (\ml@N.MemLayout{..} N.Node{..} tp tg ->
                          do
                            N.Env eEnv <- Eff.get @N.Env
                            let
                              riExprEnv = mkFieldExprEnv
                                          riName
                                          riNameFull
                                          tp
                                          eEnv
                            either
                              (id)
                              (id)
                              (bimap
                                (\rieEnv ->
                                   do
                                     let
                                       tyBuilt = typeBuilder riNameFull rieEnv tp
                                     putStrLnFS riFilePath ["public " <> tyBuilt  <> " " <> B.shortByteString name <> ";"]
                                     -- Eff.throw $ "error: type '" <> (LC.unpack . convertString . B.toLazyByteString $ tyBuilt) <> "' in field '" <> (C.unpack . B.fromShort $ nameFull) <> "' is either undefined or ambiguous." 
                                )
                                (\rieEnv ->
                                   do
                                     putStrLnFS riFilePath ["public " <> typeBuilder riNameFull rieEnv tp <> " " <> B.shortByteString name <> ";"]
                                )
                                riExprEnv)
                            pure()
                       )
                       (\r@N.RecordE{} -> pure ())
                       (pure ())
                       (pure ())
                     )
               )
    pure ()

  endFields = Eff.HandlerT $ do
    pure ()

  beginConstructors = Eff.HandlerT $ do
    ri@ContractGenRecordInfo{..} <- Eff.get @ContractGenRecordInfo
    let
      N.RecordE (N.Node{..}) = riRecord
      riName = name
      riNameFull = nameFull
      strLn = putStrLnFS riFilePath
      tab = tabFS riFilePath
      del = delFS riFilePath
      override = "@Override"
      withIndent action = do
        tab
        action
        del

    withIndent(
      do
        -- ctor
        strLn [ "// constructor." ]
        strLn [ "private " <> B.shortByteString name <> "(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> position) {" ]
        withIndent(do
                      strLn [ "super(unsafeBuffer, position);" ]
                  )
        strLn [ "}" ]
        -- attach new buffer
        strLn [ "// process new wire buffer." ]
        strLn [ override ]
        strLn [ "public org.contract.field.base.WireI wire(UnsafeBuffer unsafeBuffer) {" ]
        withIndent(
          do
            N.Env eEnv@(N.ExprEnv eEnvHM) <- Eff.get @N.Env
            runEachField
              eEnv
              ri
              (eachField
                (pure ())
                (pure ())
                (\ml@N.MemLayout{..} N.Node{..} tp tg ->
                   do
                     putStrLnFS riFilePath [ B.shortByteString name <> ".wire(unsafeBuffer);" ]
                     pure ()
                )
                (\r@N.RecordE{} -> pure ())
                (pure ())
                (pure ())
              )
            strLn [ "return super.wire(unsafeBuffer);" ]
          )
        strLn [ "}" ]
      )

    pure ()

  endConstructors = Eff.HandlerT $ do
    ContractGenRecordInfo{..} <- Eff.get @ContractGenRecordInfo
    let
      N.RecordE (N.Node{..}) = riRecord
    pure ()

runContractGenRecord :: (Monad m) => ContractGenRecordInfo
                   -> Eff.EffT ContractGenRecordT m a
                   -> m a
runContractGenRecord ri = Eff.evalState ri . runFS . Eff.runHandlerT . Eff.runEffT

instance (Monad m, Eff.Error String m, Eff.State VFSP m, Eff.State N.Env m, Eff.State FilePreamble m, Eff.State N.Import m) => ContractGen (ContractGenT m) where
  createRecordInModule (N.RecordE r@N.Node{}) m@(N.ModuleE (N.Node{..})) (FilePreamble fh) = Eff.HandlerT $ do
    mkDirFS nameFull
    mkDirFileFS (N.name r) nameFull

  createRecordNested r m = Eff.HandlerT $ pure ()
  createRecordRoot r m = Eff.HandlerT $ pure ()
  writePackageDirective fp m@(N.ModuleE (N.Node{..})) = Eff.HandlerT $ do
    putStrFS fp [ "package "
                , B.shortByteString nameFull
                , ";"
                ]
    newLineFS fp

  writeFilePreamble fp (FilePreamble fh) = Eff.HandlerT $ do
    mapM (\l -> (putStrFS fp [G.fromString l] >> newLineFS fp)) fh
    return ()

  collectImports r = Eff.HandlerT $ do
    let
      tyCollectImport :: N.Type -> N.Import -> N.Import
      tyCollectImport (N.Prim n fn _) acc
        | n == "bool"   = acc <> N.Import (HM.singleton "org.contract.field.Int" 1)
        | n == "byte"   = acc <> N.Import (HM.singleton "org.contract.field.Byte" 1)
        | n == "char"   = acc <> N.Import (HM.singleton "org.contract.field.Char" 1)
        | n == "short"  = acc <> N.Import (HM.singleton "org.contract.field.Short" 1)
        | n == "int"    = acc <> N.Import (HM.singleton "org.contract.field.Int" 1)
        | n == "i32"    = acc <> N.Import (HM.singleton "org.contract.field.Int" 1)
        | n == "u32"    = acc <> N.Import (HM.singleton "org.contract.field.Int" 1)
        | n == "float"  = acc <> N.Import (HM.singleton "org.contract.field.Float" 1)
        | n == "f32"    = acc <> N.Import (HM.singleton "org.contract.field.Float" 1)
        | n == "long"   = acc <> N.Import (HM.singleton "org.contract.field.Long" 1)
        | n == "i64"    = acc <> N.Import (HM.singleton "org.contract.field.Long" 1)
        | n == "u64"    = acc <> N.Import (HM.singleton "org.contract.field.Long" 1)
        | n == "double" = acc <> N.Import (HM.singleton "org.contract.field.Double" 1)
        | n == "f64"    = acc <> N.Import (HM.singleton "org.contract.field.Double" 1)
        | otherwise     = acc
      tyCollectImport (N.Ref tp) acc = tyCollectImport tp (acc <> N.Import (HM.singleton "org.contract.field.Ref" 1))
      tyCollectImport (N.Array tp) acc = tyCollectImport tp (acc <> N.Import (HM.singleton "org.contract.field.array.Dynamic" 1))
      tyCollectImport (N.FArray tp i) acc = tyCollectImport tp (acc
                                                                <> N.Import (HM.singleton ("org.contract.utils.compile.Long._" <> (show i)) 1)
                                                                <> N.Import (HM.singleton "org.contract.field.array.Fixed" 1))
      tyCollectImport (N.CArray tp i) acc = tyCollectImport tp (acc
                                                                <> N.Import (HM.singleton ("org.contract.utils.compile.Long._" <> (show i)) 1)
                                                                <> N.Import (HM.singleton "org.contract.field.array.Capacity" 1))
      tyCollectImport _ acc = acc

      collect :: N.Expr -> N.Import -> N.Import
      collect (N.RootE (N.Node{..})) acc = foldMap (flip collect acc) childrenList
      collect (N.ModuleE (N.Node{..})) acc = foldMap (flip collect acc) childrenList
      collect (N.RecordE (N.Node{..})) acc = foldMap (flip collect acc) childrenList
      collect (N.FieldE (N.Node{..}) tp _) acc = tyCollectImport tp acc
      collect _ acc = acc
    -- Eff.modify @N.Import (\imp -> collect r imp) -- fixme : salumup : removed to produce cleaner Java code
    return ()

  writeImports fp ip = Eff.HandlerT $ do
    HM.foldlWithKey'
      (\ acc k _ ->
         do
           acc
           putStrLnFS fp [ "import " <> G.fromString k <> ";" ]
      )
      (pure ())
      (N.imports ip)

-- | all suported effects
type EffsT m r = Eff.EffsT '[ ContractGenT
                            , FST
                            , Eff.StateT N.Env
                            , Eff.StateT (VFSP)
                            , Eff.StateT FilePreamble
                            , Eff.StateT N.Import
                            , Eff.ExceptT String
                            ]
                 m r
type EffsTI r = EffsT Identity r

-- | run & execute all effects
run :: N.Env
    -> EffsTI a
    -> Either String a
run nenv =
  runIdentity
  . Eff.runError
  . Eff.evalState (N.Import $ HM.fromList [ ("org.agrona.concurrent.UnsafeBuffer", 1)
                                          -- , ("org.contract.field.base.IndexWire", 1)
                                          -- , ("org.contract.field.base.WireI", 1)
                                          -- , ("java.util.Arrays", 1)
                                          -- , ("java.io.ByteArrayOutputStream", 1)
                                          -- , ("java.io.PrintStream", 1)
                                          -- , ("java.io.PrintStream", 1)
                                          -- , ("org.contract.exception.IllegalConversion", 1)
                                          -- , ("org.contract.type.Type", 1)
                                          -- , ("org.contract.type.Desc", 1)
                                          -- , ("org.contract.utils.AlignOf", 1)
                                          -- , ("org.contract.utils.Pointer", 1)
                                          -- , ("org.contract.utils.functional.Thunk0", 1)
                                          -- , ("org.contract.utils.functional.Thunk1", 1)
                                          -- , ("org.contract.utils.functional.Thunk2", 1)
                                          -- , ("org.contract.utils.functional.Thunk3", 1)
                                          -- , ("org.contract.utils.functional.Thunk4", 1)
                                          ])
  . Eff.evalState mempty
  . Eff.evalState mempty
  . Eff.evalState nenv
  . runFS
  . runContractGen

contractGen :: N.Expr
      -> EffsTI (VFSP)
contractGen expr = do
  withPreamble
    setPreamble
    expr

  collectImports
    expr

  withRecord
    (f)
    expr

  Eff.get @VFSP
    where
      setPreamble :: N.Expr -> EffsTI ()
      setPreamble x@(N.PreambleE n@(N.Node{}) content) = do
        Eff.modify @FilePreamble (<> FilePreamble content)

      f :: N.Expr -> EffsTI ()
      f me@(N.RecordE (N.Node{..})) = case parent of
                                        Just (N.NoShow m@(N.ModuleE{})) -> do
                                          -- create file
                                          fp <- mkFilePathFS nameFull
                                          Eff.get @FilePreamble >>= createRecordInModule me m
                                          -- write header
                                          writePackageDirective fp m
                                          Eff.get @FilePreamble >>= writeFilePreamble fp
                                          Eff.get @N.Import >>= writeImports fp
                                          newLineFS fp
                                          -- write record, meta and fields
                                          runContractGenRecord
                                            (ContractGenRecordInfo { riFilePath = fp
                                                             , riRecord = me
                                                             })
                                            (do
                                                beginTopRecord
                                                beginMeta
                                                endMeta
                                                beginFields
                                                endFields
                                                beginConstructors
                                                endConstructors
                                                endRecord
                                            )
                                          pure ()
                                        Just (N.NoShow r@(N.RecordE{})) -> do
                                          createRecordNested me r
                                        Just (N.NoShow r@(N.RootE{})) -> do
                                          createRecordRoot me r
                                        _ -> pure ()
