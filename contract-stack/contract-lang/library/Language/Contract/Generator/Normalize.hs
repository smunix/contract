module Language.Contract.Generator.Normalize ( Expr(..)
                                             , ToExpr(..)
                                             , ExprEnv(..)
                                             , Env(..)
                                             , MemTest(..)
                                             , Import(..)
                                             , ToExprEnv(..)
                                             , Lang(..)
                                             , Node(..)
                                             , NoShow(..)
                                             , ToFullName(..)
                                             , Type(..)
                                             , Tag(..)
                                             , MemLayout(..)
                                             , alignOf
                                             , AddMemLayout(..)
                                             , multipleMemLayout
                                             , (<:>)
                                             , Finalize(..)
                                             , type Str
                                             , type ShortStr
                                             ) where
import qualified Debug.Trace as Debug

import GHC.Types ( Symbol )

import GHC.Generics                ( Generic, Generic1 )
import Control.Monad               ( mapM )
import Control.DeepSeq             ( NFData )
import Data.Data                   ( Data )
import Data.Typeable               ( Typeable )
import Data.Coerce                 ( coerce )
import Data.String.Encode          ( convertString )
import Data.List                   ( foldl' )
import qualified GHC.Exts as G

import qualified Control.Effect as Eff
import qualified Control.Effect.State as Eff
import Data.Monoid ( Sum(..) )
import qualified Data.HashMap.Strict as HM
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString.Builder as B
import Data.ByteString.Short as B ( fromShort
                                  , toShort
                                  , ShortByteString
                                  )

import qualified Language.Contract.Syntax as Syn
import qualified Language.Contract.Parser as Parser
import qualified Language.Contract.Parser.Internal as Internal
import Language.Contract.Data hiding (Ident, name)
import qualified Language.Contract.Data as Data

type Str = C.ByteString
type ShortStr = B.ShortByteString

-- | transform to short name
class ToShortStr t where
  toShortStr :: t -> ShortStr

instance ToShortStr B.Builder where
  toShortStr = toShort . B.pack . L.unpack . B.toLazyByteString

-- | type representation
class ToType t where
  toType :: Maybe Type -> t -> Type

-- | short name
class ToName t where
  toName :: t -> ShortStr

instance ToName ShortStr where
  toName = id

instance (ToName a, ToName b) => ToName (Either a b) where
  toName = either (toName) (toName)

-- | full name
class ToFullName t where
  toFullName :: t -> B.Builder

instance ToFullName ShortStr where
  toFullName = B.shortByteString

instance (ToFullName a, ToFullName b) => ToFullName (Either a b) where
  toFullName = either (toFullName) (toFullName)

-- | hide Show instance
newtype NoShow a = NoShow a
  deriving (Eq, Ord, Typeable, Data, Generic, NFData)
instance Show (NoShow a) where
  showsPrec d _ = showsPrec d "(...)"

-- | Memory layout
data MemLayout where
  MemLayout :: { mlSizeOf :: Int
               , mlAlignOf :: Int
               } -> MemLayout
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

instance Monoid MemLayout where
  mempty = MemLayout 0 0
  mappend = (<>)

infixl 8 <:>
(<:>) :: (Semigroup a) => a -> a -> a
(<:>) = (<>)

alignOf :: Int -> Int -> Int
alignOf v a
  | a == 0 = v
  | v `mod` a == 0 = v
  | otherwise = a * (1 + (v `div` a))

multipleMemLayout :: Int -> MemLayout -> MemLayout
multipleMemLayout n ml@MemLayout{..} = ml{ mlSizeOf = n * mlSizeOf }

instance Semigroup MemLayout where
  MemLayout s a <> MemLayout s' a' = MemLayout s'' a''
    where
      a'' = max a a'
      s'' = alignOf s a' + s'

class Finalize a where
  finalize :: a -> a

instance Finalize MemLayout where
  finalize x@(MemLayout{..}) = x{ mlSizeOf = alignOf mlSizeOf mlAlignOf }

class MemTest (t :: Symbol) m a | m a -> t where
  memTest :: a -> m ()

instance MemTest "io" IO () where
  memTest () = putStrLn "memTest/io"

class AddMemLayout (s :: Symbol) m a where
  addMemLayout :: Node Expr -- ^ enclosing record
               -> ExprEnv
               -> MemLayout
               -> a
               -> m MemLayout

-- | normalized tags
class ToTag a where
  toTag :: a -> Tag

-- | Tree Node (used for AST first pass)
data Node a where
  Node :: { name :: ShortStr
          , nameFull :: ShortStr
          , parent :: Maybe (NoShow a)
          , childrenMap :: HM.HashMap ShortStr a
          , childrenList :: [a]
          }
       -> Node a
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

-- | Our Environment (Types and Exprs)
data Env where
  Env :: { exprEnv :: ExprEnv
         } -> Env
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

instance Semigroup Env where
  Env e <> Env e' = Env $ e <> e'

instance Monoid Env where
  mempty = Env mempty
  mappend = (<>)

-- | Type tree (all types that we know of, also constructed after AST first pass?)
data Type where
  NoType :: Type
  Module :: { tyName :: ShortStr
            , tyFullName :: ShortStr
            , tyExpr :: Maybe Expr
            , tyRepr :: Type
            }
         -> Type
  Prim :: { tyName :: ShortStr
          , tyFullName :: ShortStr
          , tyRepr :: Type
          }
       -> Type
  Rec :: { tyName :: ShortStr
         , tyFullName :: ShortStr
         , tyRepr :: Type
         }
      -> Type
  Ref :: Type
      -> Type
  Array :: Type
        -> Type
  FArray :: Type
         -> !Int
         -> Type
  CArray :: Type
         -> !Int
         -> Type
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

ml8 = MemLayout 1 1
ml16 = MemLayout 2 2
ml32 = MemLayout 4 4
ml64 = MemLayout 8 8

instance (Monad m) => AddMemLayout "type" m Type where
  addMemLayout  rin eEnv@(ExprEnv eEnvHM) ml@MemLayout{..} (Prim n fn r)
    | C.unpack (convertString n) == "bool"   = return $ ml <:> ml8
    | C.unpack (convertString n) == "byte"   = return $ ml <:> ml8
    | C.unpack (convertString n) == "char"   = return $ ml <:> ml8
    | C.unpack (convertString n) == "short"  = return $ ml <:> ml16
    | C.unpack (convertString n) == "int"    = return $ ml <:> ml32
    | C.unpack (convertString n) == "float"  = return $ ml <:> ml32
    | C.unpack (convertString n) == "long"   = return $ ml <:> ml64
    | C.unpack (convertString n) == "double" = return $ ml <:> ml64
    | C.unpack (convertString n) == "i8"     = return $ ml <:> ml8
    | C.unpack (convertString n) == "u8"     = return $ ml <:> ml8
    | C.unpack (convertString n) == "i16"    = return $ ml <:> ml16
    | C.unpack (convertString n) == "u16"    = return $ ml <:> ml16
    | C.unpack (convertString n) == "i32"    = return $ ml <:> ml32
    | C.unpack (convertString n) == "u32"    = return $ ml <:> ml32
    | C.unpack (convertString n) == "i64"    = return $ ml <:> ml64
    | C.unpack (convertString n) == "u64"    = return $ ml <:> ml64
    | C.unpack (convertString n) == "f32"    = return $ ml <:> ml32
    | C.unpack (convertString n) == "f64"    = return $ ml <:> ml64
    | otherwise                              = (<:> mempty) <$> mlM
      where
        prefix = convertString . nameFull $ rin
        filteredExprEnv = HM.filterWithKey
                          (\k v ->
                             B.isSuffixOf (convertString fn) (convertString k) -- isNested
                            && B.isPrefixOf (prefix) (convertString k)
                          )
                          eEnvHM
        isUniq = 1 == HM.size filteredExprEnv
        mlM = if isUniq
              then
                do
                  HM.foldlWithKey'
                    (\a k v ->
                        case v of
                          r@(RecordE Node{..}) ->
                            foldl'
                            (\a c -> case c of
                                FieldE n tp' _ ->
                                  do
                                    a' <- a
                                    addMemLayout @"type" rin eEnv a' tp'
                                _ -> a
                            )
                            (a)
                            childrenList
                          _ -> a
                    )
                    (return mempty)
                    filteredExprEnv
              else
                do
                  return ml -- todo : smunix : ambiguity or record not found; should we throw ?

  addMemLayout rin env'@(ExprEnv eEnv) ml@MemLayout{..} (Ref tp) = return $ ml <:> ml64
  addMemLayout rin env'@(ExprEnv eEnv) ml@MemLayout{..} (Array tp) = return $ ml <:> ml64
  addMemLayout rin env'@(ExprEnv eEnv) ml@MemLayout{..} (FArray tp i) =
    do
      MemLayout tpSz tpAl <- finalize <$> addMemLayout @"type" rin env' mempty tp
      let
        sz = i * tpSz
        al = tpAl
      return $ ml <:> MemLayout sz al

  addMemLayout rin env'@(ExprEnv eEnv) ml@MemLayout{..} (CArray tp i) =
    do
      MemLayout tpSz tpAl <- finalize <$> addMemLayout @"type" rin env' mempty tp
      let
        sz = i * tpSz
        al = tpAl
      return $ ml <:> MemLayout sz al

  addMemLayout rin env'@(ExprEnv eEnv) ml@MemLayout{..} _ = return $ ml <:> mempty

instance ToFullName Type where
  toFullName Prim{..} = B.shortByteString tyFullName
  toFullName (Ref ty) = toFullName ty
  toFullName (Array ty) = toFullName ty
  toFullName (FArray ty _) = toFullName ty
  toFullName (CArray ty _) = toFullName ty
  toFullName _ = mempty

-- | what language will we be using ?
data Lang where
  Java :: Lang
  Cpp :: Lang
  Python :: Lang
  deriving (Enum, Bounded, Eq, Ord, Show, Typeable, Data, Generic, NFData)

instance ToExprEnv Lang where
  toExprEnv Java = te
    where
      te = mempty
        <> newPrelude "bool"    "org.contract.field.Bool"
        <> newPrelude "byte"    "org.contract.field.Byte"
        <> newPrelude "char"    "org.contract.field.Char"
        <> newPrelude "short"   "org.contract.field.Short"
        <> newPrelude "int"     "org.contract.field.Int"
        <> newPrelude "float"   "org.contract.field.Float"
        <> newPrelude "long"    "org.contract.field.Long"
        <> newPrelude "double"  "org.contract.field.Double"
        <> newPrelude "i8"      "org.contract.field.Char"
        <> newPrelude "u8"      "org.contract.field.Byte"
        <> newPrelude "i16"     "org.contract.field.Short"
        <> newPrelude "u16"     "org.contract.field.Short"
        <> newPrelude "i32"     "org.contract.field.Int"
        <> newPrelude "u32"     "org.contract.field.Int"
        <> newPrelude "i64"     "org.contract.field.Long"
        <> newPrelude "u64"     "org.contract.field.Long"
        <> newPrelude "f32"     "org.contract.field.Float"
        <> newPrelude "f64"     "org.contract.field.Double"
        <> newPrelude "string"  "org.contract.field.String"

  toExprEnv Cpp = te
    where
      te = mempty
        <> newPrelude "bool" "bool"
        <> newPrelude "byte" "uint8_t"
        <> newPrelude "char" "char"
        <> newPrelude "short" "uint16_t"
        <> newPrelude "int" "int32_t"
        <> newPrelude "float" "float"
        <> newPrelude "long" "long"
        <> newPrelude "double" "double"
        <> newPrelude "i8" "char"
        <> newPrelude "u8" "uint8_t"
        <> newPrelude "i16" "int16_t"
        <> newPrelude "u16" "uint16_t"
        <> newPrelude "i32" "int32_t"
        <> newPrelude "u32" "uint32_t"
        <> newPrelude "i64" "int64_t"
        <> newPrelude "u64" "uint64_t"
        <> newPrelude "f32" "float"
        <> newPrelude "f64" "double"

  toExprEnv Python = mempty

instance ToName (Spanned Syn.Token) where
  toName (Spanned (Syn.Ident i) _) = toShortStr (B.string8 $ Data.name i)

instance ToName (Syn.Module a) where
  toName (Syn.Module sTok es) = toName sTok

instance ToName (Syn.Record a) where
  toName (Syn.Record sTok es) = toName sTok

instance ToName (Syn.Field a) where
  toName (Syn.Field sTok ty tg) = toName sTok

instance ToName String where
  toName = toShort . C.pack

instance ToName (Syn.File a) where
  toName (Syn.File prmbl es) = toShort ""

instance ToFullName (Spanned Syn.Token) where
  toFullName = B.shortByteString . toName

instance ToFullName (Syn.Type a) where
  toFullName (Syn.Type tk Syn.NoType) = toFullName tk
  toFullName (Syn.Type tk tp) = toFullName tp <> B.string8 "." <> toFullName tk
  toFullName (Syn.Ref ty) = (toFullName ty)
  toFullName (Syn.FArray ty i) = (toFullName ty)
  toFullName (Syn.CArray ty i) = (toFullName ty)
  toFullName (Syn.Array ty) = (toFullName ty)
  toFullName _ = mempty

instance ToTag (Syn.Tag a) where
  toTag (Syn.Tag (Spanned (Syn.Literal (Syn.Integer i) _) _)) = Tag (read i)
  toTag (Syn.Group (Spanned (Syn.Literal (Syn.Integer i) _) _)) = Group (read i)
  toTag _ = NoTag

data Tag where
  NoTag :: Tag
  Tag :: Int -> Tag
  Group :: Int -> Tag
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

-- | accumulated imports
newtype Import = Import { imports :: HM.HashMap String (Sum Int) }
  deriving newtype (Eq, Ord, Show, Monoid, NFData)
  deriving anyclass (Typeable)
  deriving stock (Generic, Data)

instance Semigroup Import where
  Import i <> Import i' = Import $ HM.unionWith (+) i i'

-- | Expr Environment
newtype ExprEnv = ExprEnv (HM.HashMap ShortStr Expr)
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

singletonExprEnv :: ShortStr -> Expr -> ExprEnv
singletonExprEnv = (ExprEnv .) . HM.singleton

instance Semigroup ExprEnv where
  ExprEnv x <> ExprEnv y = ExprEnv $ HM.unionWith (<>) (coerce x) (coerce y)

instance Monoid ExprEnv where
  mempty = ExprEnv HM.empty
  mappend = (<>)

class ToExprEnv a where
  toExprEnv :: a -> ExprEnv

-- ContractGen monad
class (Eff.State Env m) => ContractGen m where
  gen :: m ()

-- | Expr Tree (created after AST first pass)
data Expr  where
  NoExprE   :: Expr
  ListE     :: [Expr]
            -> Expr
  RootE     :: Node Expr
            -> Expr
  PreambleE :: Node Expr
            -> [String]
            -> Expr
  ModuleE   :: Node Expr
            -> Expr
  RecordE   :: Node Expr
            -> Expr
  FieldE    :: Node Expr
            -> Type
            -> Tag
            -> Expr
  PreludeE  :: Node Expr
            -> Expr
  deriving (Eq, Ord, Show, Typeable, Data, Generic, NFData)

instance Semigroup Expr where
  NoExprE <> x = x
  x <> NoExprE = x
  ListE es <> ListE es' = ListE (es <> es')
  ListE es <> e = ListE (es <> [e])
  e <> ListE es = ListE ([e] <> es)
  x <> y = ListE [x, y]

instance Monoid Expr where
  mempty = NoExprE
  mappend = (<>)

newPrelude :: ShortStr -> ShortStr -> ExprEnv
newPrelude s l = singletonExprEnv s (PreludeE (Node{..}))
  where
    name = s
    nameFull = l
    parent = Nothing
    childrenMap = mempty
    childrenList = mempty

instance ToExprEnv Expr where
  toExprEnv (RootE Node{..}) = foldMap (toExprEnv) childrenList
  toExprEnv m@(ModuleE Node{..}) = (singletonExprEnv nameFull m) <> (foldMap (toExprEnv) childrenList)
  toExprEnv r@(RecordE Node{..}) = (singletonExprEnv nameFull r) <> (foldMap (toExprEnv) childrenList)
  toExprEnv f@(FieldE Node{..} _ _) = singletonExprEnv nameFull f
  toExprEnv l@(ListE{}) = undefined -- todo : smunix : potential bug but never encountered (ListE{} are created when merging 2 ExprEnv only)
  toExprEnv _ = singletonExprEnv ("<???>") NoExprE

instance ToFullName Expr where
  toFullName RootE{} = mempty
  toFullName (ModuleE (Node{..})) = B.shortByteString nameFull
  toFullName (RecordE (Node{..})) = B.shortByteString nameFull
  toFullName (FieldE (Node{..}) _ _) = B.shortByteString nameFull
  toFullName _ = mempty

class ToExpr a where
  toExpr :: Lang -> (Maybe (NoShow Expr)) -> a -> Expr

instance (ToExpr a, ToExpr b) => ToExpr (Either a b) where
  toExpr lang pM = either (toExpr lang pM) (toExpr lang pM)

instance ToType (Syn.Type a) where
  toType tyParent t@(Syn.Type tk tp) = Prim { tyName = toName tk
                                            , tyFullName = maybe
                                                           (toShortStr $ (toFullName t))
                                                           (toShortStr . toFullName)
                                                           tyParent
                                            , tyRepr = toType Nothing tp
                                            }
  toType tyParent t@(Syn.Ref ty) = Ref (toType tyParent ty)
  toType tyParent t@(Syn.FArray ty i) = FArray (toType tyParent ty) (unSpan i)
  toType tyParent t@(Syn.CArray ty i) = CArray (toType tyParent ty) (unSpan i)
  toType tyParent t@(Syn.Array ty) = Array (toType tyParent ty)
  toType _ _ = NoType

instance ToExpr (Syn.Field Span) where
  toExpr lang pM f@(Syn.Field sTok ty tg) = e
    where
      sn :: ShortStr
      sn = toName f

      e :: Expr
      e = FieldE n (toType Nothing ty) (toTag tg)

      n :: Node Expr
      n = Node { name = sn
               , nameFull = maybe sn (\ e -> toShortStr $ toFullName (coerce e :: Expr) <> B.string8 "." <> B.shortByteString sn) pM
               , parent = pM
               , childrenMap = HM.empty
               , childrenList = mempty
               }

instance ToExpr (Syn.Module Span) where
  toExpr lang pM m@(Syn.Module sTok es) = e
    where
      sn :: ShortStr
      sn = toName m

      e :: Expr
      e = ModuleE n

      n :: Node Expr
      n = Node { name = sn
               , nameFull = maybe sn (\ e -> case (coerce e :: Expr) of
                                               RootE{} -> sn
                                               eC -> toShortStr $ toFullName eC <> B.string8 "." <> B.shortByteString sn) pM
               , parent = pM
               , childrenMap = csM
               , childrenList = csL
               }

      csM :: HM.HashMap ShortStr Expr
      csM = HM.fromList $ fmap (\ !c ->
                                  let
                                    expr = toExpr lang (Just . coerce $ e) (c)
                                  in
                                    (toShortStr $ toFullName(expr), expr)) (es)

      csL :: [Expr]
      csL = fmap (toExpr lang (Just . coerce $ e)) (es)

instance ToType (Syn.Module a) where
  toType tyParent (Syn.Module sTok es) = Module{..}
    where
      tyName = toName sTok
      tyFullName = maybe (toShortStr $ toFullName sTok) (\tyP -> toShortStr $ toFullName tyP <> B.string8 "." <> toFullName sTok) tyParent
      tyExpr = Nothing
      tyRepr = NoType

instance ToExpr (Syn.Record Span) where
  toExpr lang pM m@(Syn.Record sTok flds) = e
    where
      sn :: ShortStr
      sn = toName m

      e :: Expr
      e = RecordE n

      n :: Node Expr
      n = Node { name = sn
               , nameFull = maybe sn (\ e -> case (coerce e :: Expr) of
                                               RootE{} -> sn
                                               eC -> toShortStr $ toFullName eC <> B.string8 "." <> B.shortByteString sn) pM
               , parent = pM
               , childrenMap = csM
               , childrenList = csL
               }

      csM :: HM.HashMap ShortStr Expr
      csM = HM.fromList $ fmap (\ !c ->
                                  let
                                    expr = toExpr lang (Just . coerce $ e) (c)
                                  in
                                    (toShortStr $ toFullName(expr), expr)) (flds)

      csL :: [Expr]
      csL = fmap (toExpr lang (Just . coerce $ e)) (flds)

instance ToType (Syn.Record a) where
  toType tyParent (Syn.Record sTok es) = Rec{..}
    where
      tyName = toName sTok
      tyFullName = maybe (toShortStr $ toFullName sTok) (\tyP -> toShortStr $ toFullName tyP <> B.string8 "." <> toFullName sTok) tyParent
      tyRepr = NoType

instance ToExpr (Syn.File Span) where
  toExpr lang _ f@(Syn.File p ms) = root
    where
      sn :: ShortStr
      sn = toName f

      root :: Expr
      root = RootE (Node sn sn Nothing csM csL)

      prmb = case p of
               Syn.NoPreambleL -> PreambleE noNode mempty
               Syn.PreambleL hm -> PreambleE noNode (HM.lookupDefault mempty (mapLang lang) hm)
        where
          mapLang :: Lang -> Syn.Lang
          mapLang Cpp = Syn.CppL
          mapLang Java = Syn.JavaL
          mapLang Python = Syn.PythonL

          noNode = Node { name = "preamble"
                        , nameFull = "preamble"
                        , parent = Just $ NoShow root
                        , childrenMap = HM.empty
                        , childrenList = mempty
                        }

      csM :: HM.HashMap ShortStr Expr
      csM = HM.fromList $ fmap (\ !c -> (toName(c), toExpr lang (Just . coerce $ root) (c))) (ms) <> [("preamble", prmb)]

      csL :: [Expr]
      csL = fmap (toExpr lang (Just . coerce $ root)) (ms) <> [prmb]
