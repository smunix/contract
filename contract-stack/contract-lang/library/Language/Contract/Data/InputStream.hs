module Language.Contract.Data.InputStream ( InputStream(..)
                                    , takeByte
                                    , takeChar
                                    , isEmpty
                                    , peekChars
                                    , countLines
                                    , readInputStream
                                    , hReadInputStream
                                    , inputStreamToString
                                    , inputStreamFromString
                                    ) where

import Data.Word (Word8)
import Data.Coerce (coerce)
import Data.String (IsString(..))
import System.IO

import qualified Data.ByteString as BS
import qualified Data.ByteString.UTF8 as Char8

newtype InputStream = InputSteam BS.ByteString deriving (Eq, Ord)

takeByte :: InputStream -> (Word8, InputStream)
takeByte is = (BS.head bs, coerce (BS.tail bs))
  where
    bs = coerce is

takeChar :: InputStream -> (Char, InputStream)
takeChar is = maybe (error "takeChar: no char left") coerce (Char8.uncons (coerce is))

isEmpty :: InputStream -> Bool
isEmpty = BS.null . coerce

peekChars :: Int -> InputStream -> String
peekChars n = Char8.toString . Char8.take n . coerce

readInputStream :: FilePath -> IO InputStream
readInputStream = (coerce <$>) . BS.readFile

hReadInputStream :: Handle -> IO InputStream
hReadInputStream = (coerce <$>) . BS.hGetContents

inputStreamToString :: InputStream -> String
inputStreamToString = Char8.toString . coerce

inputStreamFromString :: String -> InputStream
inputStreamFromString = coerce . Char8.fromString

countLines :: InputStream -> Int
countLines = length . Char8.lines . coerce

instance IsString InputStream where
  fromString = inputStreamFromString
