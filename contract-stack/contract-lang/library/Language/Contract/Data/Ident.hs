module Language.Contract.Data.Ident ( Ident(..)
                              , mkIdent
                              , Name
                              ) where

import GHC.Generics (Generic)

import Control.DeepSeq (NFData)
import Data.Data (Data)
import Data.Typeable (Typeable)

import Data.List (foldl')
import Data.Char (ord)
import Data.String (IsString(..))
import Data.Semigroup as Sem

type Name = String

-- | An identifier
data Ident where
  Ident :: { name :: String -- ^ payload of the identifier
           , raw :: Bool -- ^ whether the identier is raw
           , hash :: {-# UNPACK #-} !Int -- ^ hash for quick comparison
           } -> Ident
  deriving (Data, Typeable, Generic, NFData)

instance Show Ident where
  show = show . name

instance Eq Ident where
  i1 == i2 = hash i1 == hash i2 && name i1 == name i2 && raw i1 == raw i2
  i1 /= i2 = hash i1 /= hash i2 || name i1 /= name i2 || raw i1 /= raw i2

instance Ord Ident where
  compare i1 i2 = case compare (hash i1) (hash i2) of
                    EQ -> compare (raw i1, name i1) (raw i2, name i2)
                    rt -> rt

instance Sem.Semigroup Ident where
  Ident i1 _ _ <> Ident i2 _ _ = mkIdent (i1 <> i2)

instance Monoid Ident where
  mempty = mkIdent ""
  mappend = (Sem.<>)

-- | Smart constructor for making an 'Ident'.
mkIdent :: String -> Ident
mkIdent s = Ident s False (hashString s)
  where
    hashString :: String -> Int
    hashString = foldl' f golden
    f:: Int -> Char -> Int
    f m c = fromIntegral (ord c) * magic + m
    magic = 0xb15b00b
    golden = 1013904242

instance IsString Ident where
  fromString = mkIdent
