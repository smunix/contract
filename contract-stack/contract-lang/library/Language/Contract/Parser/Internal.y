{
{-|
Module      : Language.Contract.Parser.Internal
Description : Contract parser
Copyright   : (c) Providence Salumu, 2019-2020
License     : BSD-style
Maintainer  : Providence.Salumu@smunix.com
Stability   : experimental
Portability : GHC

The parsers in this file are all re-exported to 'Language.Contract.Parser' via the 'Parse' class. 
To get information about transition states and such, run

>  happy --info=happyinfo.txt -o /dev/null src/Language/Contract/Parser/Internal.y

-}
{-# OPTIONS_HADDOCK hide, not-home #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE PartialTypeSignatures #-}

module Language.Contract.Parser.Internal (
  -- * Parsers
  parseModule,
  parseFile,
) where

import Language.Contract.Syntax.Token as Tok
import Language.Contract.Syntax.Ast as Ast

import Language.Contract.Data.Ident        ( Ident(..), mkIdent )
import Language.Contract.Data.Position

import Language.Contract.Parser.Lexer      ( lexNonSpace )
import Language.Contract.Parser.ContractMonad ( pushToken, getPosition, ContractMonad, contractError )
import Language.Contract.Data.Reversed

import Data.Foldable                   ( toList )
import Data.List                       ( (\\), isSubsequenceOf, sort )
import Data.Semigroup                  ( (<>) )
import Data.Monoid                     ( mempty )
import Text.Read                       ( readMaybe )

import Data.List.NonEmpty              ( NonEmpty(..), (<|) )
import qualified Data.List.NonEmpty as N

import qualified Data.HashMap.Strict as HashMap
}

-- named parsers
%name parseModule ModuleSpec
%name parseFile FileSpec

%tokentype { Spanned Tok.Token }
%lexer { lexNonSpace >>= } { Spanned Tok.Eof _ }
%monad { ContractMonad } { >>= } { return }

-- errors
%errorhandlertype explist
%error { expParseError }

%token
  -- Structural symbols.
  '='  { Spanned Tok.Equal               _ }
  '.'  { Spanned Tok.Dot                 _ }
  '?'  { Spanned Tok.Question            _ }
  '@'  { Spanned Tok.At                  _ }
  '|'  { Spanned Tok.Pipe                _ }
  ':'  { Spanned Tok.Colon               _ }
  ';'  { Spanned Tok.SemiColon           _ }
  '{'  { Spanned (Tok.Open Tok.Brace)    _ }
  '}'  { Spanned (Tok.Close Tok.Brace)   _ }
  '('  { Spanned (Tok.Open Tok.Paren)    _ }
  ')'  { Spanned (Tok.Close Tok.Paren)   _ }
  ob   { Spanned (Tok.Open Tok.Bracket)  _ }
  cb   { Spanned (Tok.Close Tok.Bracket) _ }
  lang { Spanned (Tok.Lang)              _ }
  prea { Spanned (Tok.Preamble{})        _ }
  java { Spanned (Tok.Language Tok.Java) _ }
  cpp  { Spanned (Tok.Language Tok.Cpp) _ }
  python { Spanned (Tok.Language Tok.Python) _ }
  '::' { Spanned Tok.ColonColon          _ }

  -- Literals.
  byte   { Spanned (Tok.Literal Tok.Byte{} _)    _ }
  char   { Spanned (Tok.Literal Tok.Char{} _)    _ }
  int    { Spanned (Tok.Literal Tok.Integer{} _) _ }
  float  { Spanned (Tok.Literal Tok.Float{} _)   _ }
  string { Spanned (Tok.Literal Tok.String{} _)  _ }

  -- Keywords used in the language.
  module  { Spanned (Tok.Ident "module") _ }
  record  { Spanned (Tok.Ident "record") _ }
  tag     { Spanned (Tok.Ident "tag")    _ }
  group   { Spanned (Tok.Ident "group")  _ }
  i64     { Spanned (Tok.Ident "i64")    _ }
  i32     { Spanned (Tok.Ident "i32")    _ }
  i16     { Spanned (Tok.Ident "i16")    _ }
  i8      { Spanned (Tok.Ident "i8")     _ }
  bool    { Spanned (Tok.Ident "bool")   _ }
  u64     { Spanned (Tok.Ident "u64")    _ }
  u32     { Spanned (Tok.Ident "u32")    _ }
  u16     { Spanned (Tok.Ident "u16")    _ }
  u8      { Spanned (Tok.Ident "u8")     _ }
  f64     { Spanned (Tok.Ident "f64")    _ }
  f32     { Spanned (Tok.Ident "f32")    _ }

  -- Identifiers.
  '_'    { Spanned (Tok.Ident "_") _ }
  IDENT  { Spanned (Tok.Ident{}) _ }

%nonassoc  '::'

%%

FileSpec :: { Ast.File Span }
  : PreambleSpec ModuleSpec { Ast.File $1 $2 }

PreambleSpec :: { Ast.PreambleL Span }
  : {-emtpy-} { mempty }
  | PreambleSpec PreambleSpec1 { $1 <> $2 }
  | PreambleSpec ';' PreambleSpec1 { $1 <> $3 }

PreambleSpec1  :: { Ast.PreambleL Span }
  : PreambleSpec2 { $1 }
  | PreambleSpec2 ';'  { $1 }

PreambleSpec2  :: { Ast.PreambleL Span }
  : lang '(' PreambleLanguage ')' prea { Ast.PreambleL (HashMap.singleton $3 (tokToPreamble $5)) }

PreambleLanguage :: { Ast.Lang }
  : java   { Ast.JavaL }
  | cpp    { Ast.CppL }
  | python { Ast.PythonL }

ModuleSpec :: { [ Either (Ast.Module Span) (Ast.Record Span) ] }
  : {- empty -} { [] }
  | ModuleSpec1 { $1 }

ModuleSpec1  :: { [ Either (Ast.Module Span) (Ast.Record Span) ] }
  : ModuleSpec1 Module { $2 : $1 }
  | ModuleSpec1 ';' Module { $3 : $1 }
  | ModuleSpec1 ';' { $1 }
  | Module { [$1] }

Module :: { Either (Ast.Module Span) (Ast.Record Span) }
  : module IDENT '{' ModuleContent '}' { Left (Ast.Module $2 $4) }
  | record IDENT '{' RecordContent '}' { Right (Ast.Record $2 $4) }

ModuleContent :: { [ Either (Ast.Module Span) (Ast.Record Span) ] }
  : {- empty -} { [] }
  | ModuleSpec { $1 }

RecordContent :: { [ Either (Ast.Record Span) (Ast.Field Span) ] }
  : RecordContent RecordArm { $2 : $1 }
  | RecordArm { [$1] }

RecordArm :: { Either (Ast.Record Span) (Ast.Field Span) }
  : RecordSpec ';' { Left $1 }
  | FieldSpec ';' { Right $1 }

RecordSpec :: { Ast.Record Span }
  : record IDENT '{' RecordContent '}' { Ast.Record $2 $4 }

FieldSpec :: { Ast.Field Span }
  : Type Identifier TagSpec   { Field $2 $1 $3 }
  | Identifier '::' Type TagSpec   { Field $1 $3 $4 }

TagSpec :: { Ast.Tag Span }
  : {- empty -} { NoTag }
  | tag  '(' int ')' { Tag $3 }
  | group  '(' int ')' { Group $3 }

Type :: { Ast.Type Span }
  : TypeArray { $1 }
  | TypePath { $1 }

TypePath :: { Ast.Type Span }
  : TypeIdentifier { Type $1 NoType }
  | '@' TypePath { Ref $2 }
  | '?' TypePath { Ref $2 }
  | TypePath '.' TypeIdentifier { Type $3 $1 }

TypeArray :: { Ast.Type Span }
  : ob Type cb { Ref $ Array $2 }
  | '@' ob Type cb { Ref $ Array $3 }
  | '?' ob Type cb { Ref $ Array $3 }
  | ob Type '|' int cb { FArray $2 (tokToInt $4) }
  | '@' ob Type '|' int cb { Ref $ FArray $3 (tokToInt $5) }
  | '?' ob Type '|' int cb { Ref $ FArray $3 (tokToInt $5) }
  | ob '|' Type '|' int cb { CArray $3 (tokToInt $ $5) }
  | '@' ob '|' Type '|' int cb { Ref $ CArray $4 (tokToInt $ $6) }
  | '?' ob '|' Type '|' int cb { Ref $ CArray $4 (tokToInt $ $6) }

Identifier :: { Spanned Tok.Token }
  : IDENT { $1 }
  | module { $1 }
  | record { $1 }
  | tag { $1 }
  | group { $1 }
  | i64 { $1 }
  | i32 { $1 }
  | i16 { $1 }
  | i8 { $1 }
  | u64 { $1 }
  | u32 { $1 }
  | u16 { $1 }
  | u8 { $1 }
  | f64 { $1 }
  | f32 { $1 }
  | bool { $1 }

TypeIdentifier :: { Spanned Tok.Token }
  : IDENT { $1 }
  | module { $1 }
  | record { $1 }
  | tag { $1 }
  | group { $1 }
  | i64 { $1 }
  | i32 { $1 }
  | i16 { $1 }
  | i8 { $1 }
  | bool { $1 }
  | u64 { $1 }
  | u32 { $1 }
  | u16 { $1 }
  | u8 { $1 }
  | f64 { $1 }
  | f32 { $1 }

{
-- | Parser for expressions
parseModule :: ContractMonad [ Either (Module Span) (Record Span) ]
parseFile :: ContractMonad (File Span)

-- | Generate a nice looking error message based on expected tokens
expParseError :: (Spanned Tok.Token, [String]) -> ContractMonad a
expParseError tk@((Spanned t sp), exps) = fail $ "Syntax error: unexpected `" ++ show (tk) ++ "'" ++
    case go (sort exps) [] replacements of
      []       -> ""
      [s]      -> " (expected " ++ s ++ ")"
      [s2,s1]  -> " (expected " ++ s1 ++ " or " ++ s2 ++ ")"
      (s : ss) -> " (expected " ++ (reverse ss >>= (++ ", ")) ++ "or " ++ s ++ ")"
  where

  go []     msgs _ = msgs
  go (e:es) msgs rs | e `elem` ignore = go es msgs rs
  go (e:es) msgs [] = go es (e : msgs) []
  go es     msgs ((rep,msg):rs)
    | rep `isSubsequenceOf` es = go (es \\ rep) (msg : msgs) rs
    | otherwise = go es msgs rs

  ignore = words "ntItem ntBlock ntStmt ntPat ntExpr ntTy ntIdent ntPath ntTT \
                  ntArm ntImplItem ntTraitItem ntGenerics ntWhereClause ntArg ntLit"

  replacements = map (\(ks,v) -> (sort ks,v)) $
    [ (expr,                              "an expression"   )

    , (lit,                               "a literal"       )
    , (boolLit,                           "a boolean"       )
    , (byteLit,                           "a byte"          )
    , (charLit,                           "a character"     )
    , (intLit,                            "an int"          )
    , (floatLit,                          "a float"         )
    , (strLit,                            "a string"        )
    , (byteStrLit,                        "a byte string"   )
    , (rawStrLit,                         "a raw string"    )
    , (rawByteStrLit,                     "a raw bytestring")

    , (doc,                               "a doc"           )
    , (outerDoc,                          "an outer doc"    )
    , (innerDoc,                          "an inner doc"    )

    , (identifier,                        "an identifier"   )
    , (lifetime,                          "a lifetime"      )
    ]

  expr :: [String]
  expr = lit ++ identifier ++ lifetime ++
                words "'<' '!' '-' '*' '&' '|' '...' '..=' '..' '::' \
                       '||' '&&' '<<' '(' '[' '{' box break continue \
                       for if loop match move return Self self       \
                       static super unsafe while do default union    \
                       catch auto yield dyn"

  lit = boolLit ++ byteLit ++ charLit ++ intLit ++ floatLit ++ strLit ++
        byteStrLit ++ rawStrLit ++ rawByteStrLit
  boolLit       = words "true false"
  byteLit       = words "byte"
  charLit       = words "char"
  intLit        = words "int"
  floatLit      = words "float"
  strLit        = words "str"
  byteStrLit    = words "byteStr"
  rawStrLit     = words "rawStr"
  rawByteStrLit = words "rawByteStr"

  doc = outerDoc ++ innerDoc
  outerDoc = words "outerDoc"
  innerDoc = words "innerDoc"

  identifier = words "IDENT"
  lifetime = words "LIFETIME"

tokToInt :: Spanned Tok.Token -> Spanned Int
tokToInt = \case
  Spanned (Tok.Literal (Tok.Integer n) _) s -> Spanned (read n) s
  Spanned _ s -> Spanned 0 s

tokToPreamble :: Spanned Tok.Token -> [String]
tokToPreamble = \case
  Spanned (Tok.Preamble (Tok.ByteStrings xs)) s -> xs

toIdent :: Spanned Tok.Token -> Ident
toIdent = \case
  Spanned (Tok.Ident i) _ -> i
  _ -> "unknown"
}
