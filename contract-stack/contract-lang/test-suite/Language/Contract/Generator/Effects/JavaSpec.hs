module Language.Contract.Generator.Effects.JavaSpec ( spec_Instr
                                              ) where

import Test.Tasty.Hspec
import Language.Contract.Generator.Normalize (Finalize(..), (<:>))
import qualified Language.Contract.Generator.Normalize as N
import qualified Language.Contract.Generator.Java as Java

import Control.Monad.IO.Class
import Data.Functor.Identity
import Data.String.Encode          (convertString)
import GHC.Generics                ( Generic, Generic1 )
import Control.Monad               ( mapM, void )
import Control.DeepSeq             ( NFData )
import Data.Data                   ( Data )
import Data.Typeable               ( Typeable )
import Data.Coerce                 ( coerce )
import qualified GHC.Exts as G

import qualified Control.Effect as Eff
import qualified Control.Effect.Error as Eff
import qualified Control.Effect.State as Eff
import Data.Coerce (coerce)

import qualified System.IO as IO

import qualified Data.HashMap.Strict as HM
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString.Lazy.Char8 as LC
import qualified Data.ByteString.Builder as B
import Data.ByteString.Short as B ( fromShort
                                  , toShort
                                  , ShortByteString
                                  )

import qualified Language.Contract.Syntax as Syn
import qualified Language.Contract.Parser as Parser
import qualified Language.Contract.Parser.Internal as Internal
import Language.Contract.Data hiding (Ident, name)
import qualified Language.Contract.Data as Data

spec_Instr :: Spec
spec_Instr = parallel $ do
  -- it "simple effectful instructions" $ do
  --   -- pendingWith "nyi"
  --   yaccFile
  --     program
  --     (N.RootE (N.Node "root" "root" Nothing HM.empty mempty))

  -- it "simple effectful tenv" $ do
  --   -- pendingWith "nyi"
  --   tenvFile
  --     program
  --     (mempty)

  it "imports as monoids" $ do
    let
      imp = mempty
      fields = imp
               <> N.Import (HM.singleton "org.contract.field.Bool" 1)
               <> N.Import (HM.singleton "org.contract.field.Char" 1)
               <> N.Import (HM.singleton "org.contract.field.Int" 1)
      arras = imp
              <> N.Import (HM.singleton "org.contract.utils.compile.Long._30" 1)
              <> N.Import (HM.singleton "org.contract.utils.compile.Long._15" 1)
              <> N.Import (HM.singleton "org.contract.field.Char" 1)

    either
      (\_ -> shouldBe True False)
      (\e ->
         do
           let
             tyCollectImport :: N.Type -> N.Import -> N.Import
             tyCollectImport (N.Prim n fn _) acc
               | n == "bool"   = acc <> N.Import (HM.singleton "org.contract.field.Int" 1)
               | n == "byte"   = acc <> N.Import (HM.singleton "org.contract.field.Byte" 1)
               | n == "char"   = acc <> N.Import (HM.singleton "org.contract.field.Char" 1)
               | n == "short"  = acc <> N.Import (HM.singleton "org.contract.field.Short" 1)
               | n == "int"    = acc <> N.Import (HM.singleton "org.contract.field.Int" 1)
               | n == "i32"    = acc <> N.Import (HM.singleton "org.contract.field.Int" 1)
               | n == "u32"    = acc <> N.Import (HM.singleton "org.contract.field.Int" 1)
               | n == "float"  = acc <> N.Import (HM.singleton "org.contract.field.Float" 1)
               | n == "f32"    = acc <> N.Import (HM.singleton "org.contract.field.Float" 1)
               | n == "long"   = acc <> N.Import (HM.singleton "org.contract.field.Long" 1)
               | n == "i64"    = acc <> N.Import (HM.singleton "org.contract.field.Long" 1)
               | n == "u64"    = acc <> N.Import (HM.singleton "org.contract.field.Long" 1)
               | n == "double" = acc <> N.Import (HM.singleton "org.contract.field.Double" 1)
               | n == "f64"    = acc <> N.Import (HM.singleton "org.contract.field.Double" 1)
               | otherwise     = acc
             tyCollectImport (N.Ref tp) acc = tyCollectImport tp (acc <> N.Import (HM.singleton "org.contract.field.Ref" 1))
             tyCollectImport (N.Array tp) acc = tyCollectImport tp (acc <> N.Import (HM.singleton "org.contract.field.array.Dynamic" 1))
             tyCollectImport (N.FArray tp i) acc = tyCollectImport tp (acc
                                                                       <> N.Import (HM.singleton ("org.contract.utils.compile.Long._" <> (show i)) 1)
                                                                       <> N.Import (HM.singleton "org.contract.field.array.Fixed" 1))
             tyCollectImport (N.CArray tp i) acc = tyCollectImport tp (acc
                                                                       <> N.Import (HM.singleton ("org.contract.utils.compile.Long._" <> (show i)) 1)
                                                                       <> N.Import (HM.singleton "org.contract.field.array.Capacity" 1))
             tyCollectImport _ acc = acc

             collect :: N.Expr -> N.Import -> N.Import
             collect (N.RootE (N.Node{..})) acc = foldMap (flip collect acc) childrenList
             collect (N.ModuleE (N.Node{..})) acc = foldMap (flip collect acc) childrenList
             collect (N.RecordE (N.Node{..})) acc = foldMap (flip collect acc) childrenList
             collect (N.FieldE (N.Node{..}) tp _) acc = tyCollectImport tp acc
             collect _ acc = acc

           let
             expr = N.toExpr N.Java Nothing e
             exprImp = collect expr imp
             foldLst =
               HM.foldlWithKey'
               (\ acc k _ ->
                  [k] <> acc
               )
               ([])
               (N.imports (exprImp))

           -- exprImp `shouldBe` imp
           foldLst `shouldBe` ([ "org.contract.field.Long"
                               , "org.contract.utils.compile.Long._5"
                               , "org.contract.field.array.Capacity"
                               , "org.contract.field.Int"
                               , "org.contract.field.Ref"
                               , "org.contract.field.array.Fixed"
                               , "org.contract.field.Char"
                               , "org.contract.utils.compile.Long._10"
                               , "org.contract.field.array.Dynamic"] :: [String])
      )
      (Parser.run Internal.parseFile (coerce $ programS "imports tests") initPosition)

  it "MemLayout" $ do
    let
      bool = N.MemLayout 1 1
      char = N.MemLayout 1 1
      short = N.MemLayout 2 2
      int = N.MemLayout 4 4
      float = N.MemLayout 4 4
      long = N.MemLayout 8 8

    bool <> char `shouldBe` N.MemLayout 2 1
    finalize (char <> int) `shouldBe` N.MemLayout 8 4
    finalize (int <> char) `shouldBe` N.MemLayout 8 4
    finalize ((int <> char) <> long) `shouldBe` N.MemLayout 16 8
    finalize (int <:> char <:> long) `shouldBe` N.MemLayout 16 8
    finalize (long <> int) `shouldBe` N.MemLayout 16 8
    finalize ((long <> int) <> char) `shouldBe` N.MemLayout 16 8
    finalize (long <:> int <:> char) `shouldBe` N.MemLayout 16 8

  it "list as monoids" $ do
    let
      l1, l2, l3 :: [Int]
      l1 = mempty
      l2 = [0..3]
      l3 = l1 <> l2
      l4 = l2 <> l1
    l3 `shouldBe` l2
    l4 `shouldBe` l2

    let
      b1, b2, b3, b4 :: [B.Builder]
      b1 = mempty
      b1' = [mempty]
      b2 = [B.string8 "Bonjour", B.string8 " ", B.string8 "les zouzous"]
      b3 = b1 <> b2
      b4 = b2 <> b1
      b5 = b1' <> b2
      b6 = b2 <> b1'

    show b3 `shouldBe` "[\"Bonjour\",\" \",\"les zouzous\"]"
    show b4 `shouldBe` "[\"Bonjour\",\" \",\"les zouzous\"]"
    show b5 `shouldBe` "[\"\",\"Bonjour\",\" \",\"les zouzous\"]"
    show b6 `shouldBe` "[\"Bonjour\",\" \",\"les zouzous\",\"\"]"

  it "file content" $ do
    let
        fc1, fc2 :: Java.FileContent [B.Builder]
        fc1 = Java.FileContent{..}
          where
            fcStore = mempty
            fcIndent = 1
        fc2 = Java.FileContent{..}
          where
            fcStore = [B.string8 "Bonjour", B.string8 " ", B.string8 "les zouzous"]
            fcIndent = 2
    fc1 `shouldBe` Java.FileContent { fcStore = [], fcIndent = 1 }
    fc2 `shouldBe` Java.FileContent { fcStore = ["Bonjour"," ","les zouzous"], fcIndent = 2 }
    (fc1 <> fc2) `shouldBe` Java.FileContent { fcStore = ["Bonjour"," ","les zouzous"], fcIndent = 2 }
    (fc2 <> fc1) `shouldBe` Java.FileContent { fcStore = ["Bonjour"," ","les zouzous"], fcIndent = 2 }

  it "java contractGen (simple)" $ do
    let
      simpleProgram :: N.Str
      simpleProgram = "language(java) {%\n"
                      <> "%}\n"
                      <> "module expand {\n"
                      <> "  record A2{\n"
                      <> "    record Nested{\n"
                      <> "      i :: i32;\n"
                      <> "      c :: i8;\n"
                      <> "    };\n"
                      <> "    c :: i8;\n"
                      <> "    i :: i32;\n"
                      <> "    n :: Nested;\n"
                      <> "    d :: f64;\n"
                      <> "  }\n"
                      <> "}\n"
    yaccJava
      simpleProgram
      ((Java.VFS {vfs = HM.fromList [("expand.mkdir",Java.FileContent {fcStore = [], fcIndent = Java.Indent 0}),("expand/A2.java",Java.FileContent {fcStore = ["package ","expand",";","\n","","\n","import org.agrona.concurrent.UnsafeBuffer;\n","\n","public class A2 extends org.contract.field.base.IndexWire {\n","  public static class Nested extends org.contract.field.base.IndexWire {\n","    public static Meta Meta = new Meta();\n","    public static class Meta {\n","      // field meta-descriptors\n","      public final org.contract.type.Type<org.contract.field.Int> i = new org.contract.field.Int.Meta().type(0);\n","      public final org.contract.type.Type<org.contract.field.Char> c = new org.contract.field.Char.Meta().type(4);\n","\n","      // type descriptor for record expand.A2.Nested\n","      public org.contract.type.Type<Nested> Type = type();\n","      public org.contract.type.Type<Nested> type() { return type(0); }\n","      public org.contract.type.Type<Nested> type(final long offset) {\n","        return new org.contract.type.Type<Nested>(Nested::new) {\n","          long off = offset;\n","          @Override\n","          public long offsetOf() { return off; }\n","          @Override\n","          public org.contract.type.Type<Nested> offsetOf(long offset) {\n","            off = offset;\n","            return this;\n","          }\n","          @Override\n","          public Nested create(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> index) {\n","            Nested r = super.create(unsafeBuffer, index);\n","            r.write(r.index());\n","            r.i = i.create(r.wire(), r.write(r.index() + i.offsetOf()));\n","            r.c = c.create(r.wire(), r.write(r.index() + c.offsetOf()));\n","            r.write(r.index() + sizeOf());\n","            return r;\n","          }\n","          @Override\n","          public long sizeOf(long eCount) {\n","            long r = 0;\n","            r = org.contract.utils.AlignOf.apply(r, i.alignOf()) + i.sizeOf();\n","            r = org.contract.utils.AlignOf.apply(r, c.alignOf()) + c.sizeOf();\n","            return eCount * org.contract.utils.AlignOf.apply(r, alignOf());\n","          }\n","          @Override\n","          public org.contract.type.Desc describe() {\n","            return org.contract.type.Desc.record(java.util.Arrays.asList(\n","              org.contract.type.Desc.field(\"i\", i.offsetOf(), i.describe())",",\n","              org.contract.type.Desc.field(\"c\", c.offsetOf(), c.describe())","));\n","          }\n","          @Override\n","          public long alignOf() {\n","            long r = 0;\n","            r = Math.max(r, i.alignOf());\n","            r = Math.max(r, c.alignOf());\n","            return r;\n","          }\n","          @Override\n","          public Boolean from(long off, org.contract.type.Desc desc) {\n","            offsetOf(off);\n","\n","            boolean r = true;\n","            r &= desc.with(\"i\", i::from);\n","            r &= desc.with(\"c\", c::from);\n","            return r;\n","          }\n","        };\n","      }\n","    }\n","    // all fields\n","    public org.contract.field.Int i;\n","    public org.contract.field.Char c;\n","    // constructor.\n","    private Nested(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> position) {\n","      super(unsafeBuffer, position);\n","    }\n","    // process new wire buffer.\n","    @Override\n","    public org.contract.field.base.WireI wire(UnsafeBuffer unsafeBuffer) {\n","      i.wire(unsafeBuffer);\n","      c.wire(unsafeBuffer);\n","      return super.wire(unsafeBuffer);\n","    }\n","  }\n","  public static Meta Meta = new Meta();\n","  public static class Meta {\n","    // field meta-descriptors\n","    public final org.contract.type.Type<org.contract.field.Char> c = new org.contract.field.Char.Meta().type(0);\n","    public final org.contract.type.Type<org.contract.field.Int> i = new org.contract.field.Int.Meta().type(4);\n","    public final org.contract.type.Type<Nested> n = new Nested.Meta().type(8);\n","    public final org.contract.type.Type<org.contract.field.Double> d = new org.contract.field.Double.Meta().type(16);\n","\n","    // type descriptor for record expand.A2\n","    public org.contract.type.Type<A2> Type = type();\n","    public org.contract.type.Type<A2> type() { return type(0); }\n","    public org.contract.type.Type<A2> type(final long offset) {\n","      return new org.contract.type.Type<A2>(A2::new) {\n","        long off = offset;\n","        @Override\n","        public long offsetOf() { return off; }\n","        @Override\n","        public org.contract.type.Type<A2> offsetOf(long offset) {\n","          off = offset;\n","          return this;\n","        }\n","        @Override\n","        public A2 create(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> index) {\n","          A2 r = super.create(unsafeBuffer, index);\n","          r.write(r.index());\n","          r.c = c.create(r.wire(), r.write(r.index() + c.offsetOf()));\n","          r.i = i.create(r.wire(), r.write(r.index() + i.offsetOf()));\n","          r.n = n.create(r.wire(), r.write(r.index() + n.offsetOf()));\n","          r.d = d.create(r.wire(), r.write(r.index() + d.offsetOf()));\n","          r.write(r.index() + sizeOf());\n","          return r;\n","        }\n","        @Override\n","        public long sizeOf(long eCount) {\n","          long r = 0;\n","          r = org.contract.utils.AlignOf.apply(r, c.alignOf()) + c.sizeOf();\n","          r = org.contract.utils.AlignOf.apply(r, i.alignOf()) + i.sizeOf();\n","          r = org.contract.utils.AlignOf.apply(r, n.alignOf()) + n.sizeOf();\n","          r = org.contract.utils.AlignOf.apply(r, d.alignOf()) + d.sizeOf();\n","          return eCount * org.contract.utils.AlignOf.apply(r, alignOf());\n","        }\n","        @Override\n","        public org.contract.type.Desc describe() {\n","          return org.contract.type.Desc.record(java.util.Arrays.asList(\n","            org.contract.type.Desc.field(\"c\", c.offsetOf(), c.describe())",",\n","            org.contract.type.Desc.field(\"i\", i.offsetOf(), i.describe())",",\n","            org.contract.type.Desc.field(\"n\", n.offsetOf(), n.describe())",",\n","            org.contract.type.Desc.field(\"d\", d.offsetOf(), d.describe())","));\n","        }\n","        @Override\n","        public long alignOf() {\n","          long r = 0;\n","          r = Math.max(r, c.alignOf());\n","          r = Math.max(r, i.alignOf());\n","          r = Math.max(r, n.alignOf());\n","          r = Math.max(r, d.alignOf());\n","          return r;\n","        }\n","        @Override\n","        public Boolean from(long off, org.contract.type.Desc desc) {\n","          offsetOf(off);\n","\n","          boolean r = true;\n","          r &= desc.with(\"c\", c::from);\n","          r &= desc.with(\"i\", i::from);\n","          r &= desc.with(\"n\", n::from);\n","          r &= desc.with(\"d\", d::from);\n","          return r;\n","        }\n","      };\n","    }\n","  }\n","  // all fields\n","  public org.contract.field.Char c;\n","  public org.contract.field.Int i;\n","  public Nested n;\n","  public org.contract.field.Double d;\n","  // constructor.\n","  private A2(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> position) {\n","    super(unsafeBuffer, position);\n","  }\n","  // process new wire buffer.\n","  @Override\n","  public org.contract.field.base.WireI wire(UnsafeBuffer unsafeBuffer) {\n","    c.wire(unsafeBuffer);\n","    i.wire(unsafeBuffer);\n","    n.wire(unsafeBuffer);\n","    d.wire(unsafeBuffer);\n","    return super.wire(unsafeBuffer);\n","  }\n","}\n"], fcIndent = Java.Indent 0})]}))

  it "java contractGen (complex)" $ do
    yaccJava
      program
      ((Java.VFS {vfs = HM.fromList [("M1.M2.M3.mkdir",Java.FileContent {fcStore = [], fcIndent = Java.Indent 0}),("M1/M2/M4/R2.java",Java.FileContent {fcStore = ["package ","M1.M2.M4",";","\n","","\n","import java.lang.String;","\n","import java.lang.Long;","\n","import java.lang.Type;","\n","","\n","import org.contract.String;","\n","import org.contract.Long;","\n","import org.contract.Type;","\n","import org.agrona.concurrent.UnsafeBuffer;\n","\n","public class R2 extends org.contract.field.base.IndexWire {\n","  public static Meta Meta = new Meta();\n","  public static class Meta {\n","    // field meta-descriptors\n","    public final org.contract.type.Type<org.contract.field.Int> b = new org.contract.field.Int.Meta().type(0);\n","    public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Ref<M2.M3.R1.R2>, org.contract.utils.compile.Long._5>, org.contract.utils.compile.Long._10>> r2r1m3m2reffarr5carr10 = new org.contract.field.array.Capacity.Meta().type(8, new org.contract.field.array.Fixed.Meta().type(0, new org.contract.field.Ref.Meta().type(M2.M3.R1.R2, 0), new org.contract.utils.compile.Long._5()), new org.contract.utils.compile.Long._10);\n","\n","    // type descriptor for record M1.M2.M4.R2\n","    public org.contract.type.Type<R2> Type = type();\n","    public org.contract.type.Type<R2> type() { return type(0); }\n","    public org.contract.type.Type<R2> type(final long offset) {\n","      return new org.contract.type.Type<R2>(R2::new) {\n","        long off = offset;\n","        @Override\n","        public long offsetOf() { return off; }\n","        @Override\n","        public org.contract.type.Type<R2> offsetOf(long offset) {\n","          off = offset;\n","          return this;\n","        }\n","        @Override\n","        public R2 create(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> index) {\n","          R2 r = super.create(unsafeBuffer, index);\n","          r.write(r.index());\n","          r.b = b.create(r.wire(), r.write(r.index() + b.offsetOf()));\n","          r.r2r1m3m2reffarr5carr10 = r2r1m3m2reffarr5carr10.create(r.wire(), r.write(r.index() + r2r1m3m2reffarr5carr10.offsetOf()));\n","          r.write(r.index() + sizeOf());\n","          return r;\n","        }\n","        @Override\n","        public long sizeOf(long eCount) {\n","          long r = 0;\n","          r = org.contract.utils.AlignOf.apply(r, b.alignOf()) + b.sizeOf();\n","          r = org.contract.utils.AlignOf.apply(r, r2r1m3m2reffarr5carr10.alignOf()) + r2r1m3m2reffarr5carr10.sizeOf();\n","          return eCount * org.contract.utils.AlignOf.apply(r, alignOf());\n","        }\n","        @Override\n","        public org.contract.type.Desc describe() {\n","          return org.contract.type.Desc.record(java.util.Arrays.asList(\n","            org.contract.type.Desc.field(\"b\", b.offsetOf(), b.describe())",",\n","            org.contract.type.Desc.field(\"r2r1m3m2reffarr5carr10\", r2r1m3m2reffarr5carr10.offsetOf(), r2r1m3m2reffarr5carr10.describe())","));\n","        }\n","        @Override\n","        public long alignOf() {\n","          long r = 0;\n","          r = Math.max(r, b.alignOf());\n","          r = Math.max(r, r2r1m3m2reffarr5carr10.alignOf());\n","          return r;\n","        }\n","        @Override\n","        public Boolean from(long off, org.contract.type.Desc desc) {\n","          offsetOf(off);\n","\n","          boolean r = true;\n","          r &= desc.with(\"b\", b::from);\n","          r &= desc.with(\"r2r1m3m2reffarr5carr10\", r2r1m3m2reffarr5carr10::from);\n","          return r;\n","        }\n","      };\n","    }\n","  }\n","  // all fields\n","  public org.contract.field.Int b;\n","  public org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Ref<M2.M3.R1.R2>, org.contract.utils.compile.Long._5>, org.contract.utils.compile.Long._10> r2r1m3m2reffarr5carr10;\n","  // constructor.\n","  private R2(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> position) {\n","    super(unsafeBuffer, position);\n","  }\n","  // process new wire buffer.\n","  @Override\n","  public org.contract.field.base.WireI wire(UnsafeBuffer unsafeBuffer) {\n","    b.wire(unsafeBuffer);\n","    r2r1m3m2reffarr5carr10.wire(unsafeBuffer);\n","    return super.wire(unsafeBuffer);\n","  }\n","}\n"], fcIndent = Java.Indent 0}),("M1/M2/M3/R1.java",Java.FileContent {fcStore = ["package ","M1.M2.M3",";","\n","","\n","import java.lang.String;","\n","import java.lang.Long;","\n","import java.lang.Type;","\n","","\n","import org.contract.String;","\n","import org.contract.Long;","\n","import org.contract.Type;","\n","import org.agrona.concurrent.UnsafeBuffer;\n","\n","public class R1 extends org.contract.field.base.IndexWire {\n","  public static class R2 extends org.contract.field.base.IndexWire {\n","    public static Meta Meta = new Meta();\n","    public static class Meta {\n","      // field meta-descriptors\n","      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Long>>> u64arr = new org.contract.field.Ref.Meta().type(new org.contract.field.array.Dynamic.Meta().type(0, new org.contract.field.Long.Meta().type(0)), 0);\n","      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>>> u64refarr = new org.contract.field.Ref.Meta().type(new org.contract.field.array.Dynamic.Meta().type(0, new org.contract.field.Ref.Meta().type(new org.contract.field.Long.Meta().type(0), 0)), 8);\n","      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>>> u64optarr = new org.contract.field.Ref.Meta().type(new org.contract.field.array.Dynamic.Meta().type(0, new org.contract.field.Ref.Meta().type(new org.contract.field.Long.Meta().type(0), 0)), 16);\n","      public final org.contract.type.Type<org.contract.field.array.Fixed<org.contract.field.Long, org.contract.utils.compile.Long._10>> u64farr10 = new org.contract.field.array.Fixed.Meta().type(24, new org.contract.field.Long.Meta().type(0), new org.contract.utils.compile.Long._10());\n","      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, org.contract.utils.compile.Long._10>>> u64farr10ref = new org.contract.field.Ref.Meta().type(new org.contract.field.array.Fixed.Meta().type(0, new org.contract.field.Long.Meta().type(0), new org.contract.utils.compile.Long._10()), 104);\n","      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, org.contract.utils.compile.Long._10>>> u64farr10opt = new org.contract.field.Ref.Meta().type(new org.contract.field.array.Fixed.Meta().type(0, new org.contract.field.Long.Meta().type(0), new org.contract.utils.compile.Long._10()), 112);\n","      public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, org.contract.utils.compile.Long._10>>> u64optfarr10opt = new org.contract.field.Ref.Meta().type(new org.contract.field.array.Fixed.Meta().type(0, new org.contract.field.Ref.Meta().type(new org.contract.field.Long.Meta().type(0), 0), new org.contract.utils.compile.Long._10()), 120);\n","      public final org.contract.type.Type<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, org.contract.utils.compile.Long._10>> u64reffarr10 = new org.contract.field.array.Fixed.Meta().type(128, new org.contract.field.Ref.Meta().type(new org.contract.field.Long.Meta().type(0), 0), new org.contract.utils.compile.Long._10());\n","      public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.Long, org.contract.utils.compile.Long._10>> u64carr10 = new org.contract.field.array.Capacity.Meta().type(208, new org.contract.field.Long.Meta().type(0), new org.contract.utils.compile.Long._10);\n","      public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, org.contract.utils.compile.Long._5>, org.contract.utils.compile.Long._10>> u64farr5carr10 = new org.contract.field.array.Capacity.Meta().type(288, new org.contract.field.array.Fixed.Meta().type(0, new org.contract.field.Long.Meta().type(0), new org.contract.utils.compile.Long._5()), new org.contract.utils.compile.Long._10);\n","      public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, org.contract.utils.compile.Long._5>, org.contract.utils.compile.Long._10>> testu64farr5carr10 = new org.contract.field.array.Capacity.Meta().type(688, new org.contract.field.array.Fixed.Meta().type(0, new org.contract.field.Long.Meta().type(0), new org.contract.utils.compile.Long._5()), new org.contract.utils.compile.Long._10);\n","      public final org.contract.type.Type<org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, org.contract.utils.compile.Long._5>, org.contract.utils.compile.Long._10>> testu64reffarr5carr10 = new org.contract.field.array.Capacity.Meta().type(1088, new org.contract.field.array.Fixed.Meta().type(0, new org.contract.field.Ref.Meta().type(new org.contract.field.Long.Meta().type(0), 0), new org.contract.utils.compile.Long._5()), new org.contract.utils.compile.Long._10);\n","\n","      // type descriptor for record M1.M2.M3.R1.R2\n","      public org.contract.type.Type<R2> Type = type();\n","      public org.contract.type.Type<R2> type() { return type(0); }\n","      public org.contract.type.Type<R2> type(final long offset) {\n","        return new org.contract.type.Type<R2>(R2::new) {\n","          long off = offset;\n","          @Override\n","          public long offsetOf() { return off; }\n","          @Override\n","          public org.contract.type.Type<R2> offsetOf(long offset) {\n","            off = offset;\n","            return this;\n","          }\n","          @Override\n","          public R2 create(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> index) {\n","            R2 r = super.create(unsafeBuffer, index);\n","            r.write(r.index());\n","            r.u64arr = u64arr.create(r.wire(), r.write(r.index() + u64arr.offsetOf()));\n","            r.u64refarr = u64refarr.create(r.wire(), r.write(r.index() + u64refarr.offsetOf()));\n","            r.u64optarr = u64optarr.create(r.wire(), r.write(r.index() + u64optarr.offsetOf()));\n","            r.u64farr10 = u64farr10.create(r.wire(), r.write(r.index() + u64farr10.offsetOf()));\n","            r.u64farr10ref = u64farr10ref.create(r.wire(), r.write(r.index() + u64farr10ref.offsetOf()));\n","            r.u64farr10opt = u64farr10opt.create(r.wire(), r.write(r.index() + u64farr10opt.offsetOf()));\n","            r.u64optfarr10opt = u64optfarr10opt.create(r.wire(), r.write(r.index() + u64optfarr10opt.offsetOf()));\n","            r.u64reffarr10 = u64reffarr10.create(r.wire(), r.write(r.index() + u64reffarr10.offsetOf()));\n","            r.u64carr10 = u64carr10.create(r.wire(), r.write(r.index() + u64carr10.offsetOf()));\n","            r.u64farr5carr10 = u64farr5carr10.create(r.wire(), r.write(r.index() + u64farr5carr10.offsetOf()));\n","            r.testu64farr5carr10 = testu64farr5carr10.create(r.wire(), r.write(r.index() + testu64farr5carr10.offsetOf()));\n","            r.testu64reffarr5carr10 = testu64reffarr5carr10.create(r.wire(), r.write(r.index() + testu64reffarr5carr10.offsetOf()));\n","            r.write(r.index() + sizeOf());\n","            return r;\n","          }\n","          @Override\n","          public long sizeOf(long eCount) {\n","            long r = 0;\n","            r = org.contract.utils.AlignOf.apply(r, u64arr.alignOf()) + u64arr.sizeOf();\n","            r = org.contract.utils.AlignOf.apply(r, u64refarr.alignOf()) + u64refarr.sizeOf();\n","            r = org.contract.utils.AlignOf.apply(r, u64optarr.alignOf()) + u64optarr.sizeOf();\n","            r = org.contract.utils.AlignOf.apply(r, u64farr10.alignOf()) + u64farr10.sizeOf();\n","            r = org.contract.utils.AlignOf.apply(r, u64farr10ref.alignOf()) + u64farr10ref.sizeOf();\n","            r = org.contract.utils.AlignOf.apply(r, u64farr10opt.alignOf()) + u64farr10opt.sizeOf();\n","            r = org.contract.utils.AlignOf.apply(r, u64optfarr10opt.alignOf()) + u64optfarr10opt.sizeOf();\n","            r = org.contract.utils.AlignOf.apply(r, u64reffarr10.alignOf()) + u64reffarr10.sizeOf();\n","            r = org.contract.utils.AlignOf.apply(r, u64carr10.alignOf()) + u64carr10.sizeOf();\n","            r = org.contract.utils.AlignOf.apply(r, u64farr5carr10.alignOf()) + u64farr5carr10.sizeOf();\n","            r = org.contract.utils.AlignOf.apply(r, testu64farr5carr10.alignOf()) + testu64farr5carr10.sizeOf();\n","            r = org.contract.utils.AlignOf.apply(r, testu64reffarr5carr10.alignOf()) + testu64reffarr5carr10.sizeOf();\n","            return eCount * org.contract.utils.AlignOf.apply(r, alignOf());\n","          }\n","          @Override\n","          public org.contract.type.Desc describe() {\n","            return org.contract.type.Desc.record(java.util.Arrays.asList(\n","              org.contract.type.Desc.field(\"u64arr\", u64arr.offsetOf(), u64arr.describe())",",\n","              org.contract.type.Desc.field(\"u64refarr\", u64refarr.offsetOf(), u64refarr.describe())",",\n","              org.contract.type.Desc.field(\"u64optarr\", u64optarr.offsetOf(), u64optarr.describe())",",\n","              org.contract.type.Desc.field(\"u64farr10\", u64farr10.offsetOf(), u64farr10.describe())",",\n","              org.contract.type.Desc.field(\"u64farr10ref\", u64farr10ref.offsetOf(), u64farr10ref.describe())",",\n","              org.contract.type.Desc.field(\"u64farr10opt\", u64farr10opt.offsetOf(), u64farr10opt.describe())",",\n","              org.contract.type.Desc.field(\"u64optfarr10opt\", u64optfarr10opt.offsetOf(), u64optfarr10opt.describe())",",\n","              org.contract.type.Desc.field(\"u64reffarr10\", u64reffarr10.offsetOf(), u64reffarr10.describe())",",\n","              org.contract.type.Desc.field(\"u64carr10\", u64carr10.offsetOf(), u64carr10.describe())",",\n","              org.contract.type.Desc.field(\"u64farr5carr10\", u64farr5carr10.offsetOf(), u64farr5carr10.describe())",",\n","              org.contract.type.Desc.field(\"testu64farr5carr10\", testu64farr5carr10.offsetOf(), testu64farr5carr10.describe())",",\n","              org.contract.type.Desc.field(\"testu64reffarr5carr10\", testu64reffarr5carr10.offsetOf(), testu64reffarr5carr10.describe())","));\n","          }\n","          @Override\n","          public long alignOf() {\n","            long r = 0;\n","            r = Math.max(r, u64arr.alignOf());\n","            r = Math.max(r, u64refarr.alignOf());\n","            r = Math.max(r, u64optarr.alignOf());\n","            r = Math.max(r, u64farr10.alignOf());\n","            r = Math.max(r, u64farr10ref.alignOf());\n","            r = Math.max(r, u64farr10opt.alignOf());\n","            r = Math.max(r, u64optfarr10opt.alignOf());\n","            r = Math.max(r, u64reffarr10.alignOf());\n","            r = Math.max(r, u64carr10.alignOf());\n","            r = Math.max(r, u64farr5carr10.alignOf());\n","            r = Math.max(r, testu64farr5carr10.alignOf());\n","            r = Math.max(r, testu64reffarr5carr10.alignOf());\n","            return r;\n","          }\n","          @Override\n","          public Boolean from(long off, org.contract.type.Desc desc) {\n","            offsetOf(off);\n","\n","            boolean r = true;\n","            r &= desc.with(\"u64arr\", u64arr::from);\n","            r &= desc.with(\"u64refarr\", u64refarr::from);\n","            r &= desc.with(\"u64optarr\", u64optarr::from);\n","            r &= desc.with(\"u64farr10\", u64farr10::from);\n","            r &= desc.with(\"u64farr10ref\", u64farr10ref::from);\n","            r &= desc.with(\"u64farr10opt\", u64farr10opt::from);\n","            r &= desc.with(\"u64optfarr10opt\", u64optfarr10opt::from);\n","            r &= desc.with(\"u64reffarr10\", u64reffarr10::from);\n","            r &= desc.with(\"u64carr10\", u64carr10::from);\n","            r &= desc.with(\"u64farr5carr10\", u64farr5carr10::from);\n","            r &= desc.with(\"testu64farr5carr10\", testu64farr5carr10::from);\n","            r &= desc.with(\"testu64reffarr5carr10\", testu64reffarr5carr10::from);\n","            return r;\n","          }\n","        };\n","      }\n","    }\n","    // all fields\n","    public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Long>> u64arr;\n","    public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>> u64refarr;\n","    public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Ref<org.contract.field.Long>>> u64optarr;\n","    public org.contract.field.array.Fixed<org.contract.field.Long, org.contract.utils.compile.Long._10> u64farr10;\n","    public org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, org.contract.utils.compile.Long._10>> u64farr10ref;\n","    public org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Long, org.contract.utils.compile.Long._10>> u64farr10opt;\n","    public org.contract.field.Ref<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, org.contract.utils.compile.Long._10>> u64optfarr10opt;\n","    public org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, org.contract.utils.compile.Long._10> u64reffarr10;\n","    public org.contract.field.array.Capacity<org.contract.field.Long, org.contract.utils.compile.Long._10> u64carr10;\n","    public org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, org.contract.utils.compile.Long._5>, org.contract.utils.compile.Long._10> u64farr5carr10;\n","    public org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Long, org.contract.utils.compile.Long._5>, org.contract.utils.compile.Long._10> testu64farr5carr10;\n","    public org.contract.field.array.Capacity<org.contract.field.array.Fixed<org.contract.field.Ref<org.contract.field.Long>, org.contract.utils.compile.Long._5>, org.contract.utils.compile.Long._10> testu64reffarr5carr10;\n","    // constructor.\n","    private R2(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> position) {\n","      super(unsafeBuffer, position);\n","    }\n","    // process new wire buffer.\n","    @Override\n","    public org.contract.field.base.WireI wire(UnsafeBuffer unsafeBuffer) {\n","      u64arr.wire(unsafeBuffer);\n","      u64refarr.wire(unsafeBuffer);\n","      u64optarr.wire(unsafeBuffer);\n","      u64farr10.wire(unsafeBuffer);\n","      u64farr10ref.wire(unsafeBuffer);\n","      u64farr10opt.wire(unsafeBuffer);\n","      u64optfarr10opt.wire(unsafeBuffer);\n","      u64reffarr10.wire(unsafeBuffer);\n","      u64carr10.wire(unsafeBuffer);\n","      u64farr5carr10.wire(unsafeBuffer);\n","      testu64farr5carr10.wire(unsafeBuffer);\n","      testu64reffarr5carr10.wire(unsafeBuffer);\n","      return super.wire(unsafeBuffer);\n","    }\n","  }\n","  public static Meta Meta = new Meta();\n","  public static class Meta {\n","    // field meta-descriptors\n","    public final org.contract.type.Type<org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Int>>> a = new org.contract.field.Ref.Meta().type(new org.contract.field.array.Dynamic.Meta().type(0, new org.contract.field.Int.Meta().type(0)), 0);\n","    public final org.contract.type.Type<org.contract.field.Int> b = new org.contract.field.Int.Meta().type(8);\n","    public final org.contract.type.Type<org.contract.field.Bool> bool = new org.contract.field.Bool.Meta().type(12);\n","    public final org.contract.type.Type<org.contract.field.Int> int = new org.contract.field.Int.Meta().type(16);\n","    public final org.contract.type.Type<org.contract.field.Char> char = new org.contract.field.Char.Meta().type(20);\n","    public final org.contract.type.Type<org.contract.field.Long> i64 = new org.contract.field.Long.Meta().type(24);\n","    public final org.contract.type.Type<org.contract.field.Int> tag = new org.contract.field.Int.Meta().type(32);\n","    public final org.contract.type.Type<org.contract.field.Int> group = new org.contract.field.Int.Meta().type(36);\n","\n","    // type descriptor for record M1.M2.M3.R1\n","    public org.contract.type.Type<R1> Type = type();\n","    public org.contract.type.Type<R1> type() { return type(0); }\n","    public org.contract.type.Type<R1> type(final long offset) {\n","      return new org.contract.type.Type<R1>(R1::new) {\n","        long off = offset;\n","        @Override\n","        public long offsetOf() { return off; }\n","        @Override\n","        public org.contract.type.Type<R1> offsetOf(long offset) {\n","          off = offset;\n","          return this;\n","        }\n","        @Override\n","        public R1 create(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> index) {\n","          R1 r = super.create(unsafeBuffer, index);\n","          r.write(r.index());\n","          r.a = a.create(r.wire(), r.write(r.index() + a.offsetOf()));\n","          r.b = b.create(r.wire(), r.write(r.index() + b.offsetOf()));\n","          r.bool = bool.create(r.wire(), r.write(r.index() + bool.offsetOf()));\n","          r.int = int.create(r.wire(), r.write(r.index() + int.offsetOf()));\n","          r.char = char.create(r.wire(), r.write(r.index() + char.offsetOf()));\n","          r.i64 = i64.create(r.wire(), r.write(r.index() + i64.offsetOf()));\n","          r.tag = tag.create(r.wire(), r.write(r.index() + tag.offsetOf()));\n","          r.group = group.create(r.wire(), r.write(r.index() + group.offsetOf()));\n","          r.write(r.index() + sizeOf());\n","          return r;\n","        }\n","        @Override\n","        public long sizeOf(long eCount) {\n","          long r = 0;\n","          r = org.contract.utils.AlignOf.apply(r, a.alignOf()) + a.sizeOf();\n","          r = org.contract.utils.AlignOf.apply(r, b.alignOf()) + b.sizeOf();\n","          r = org.contract.utils.AlignOf.apply(r, bool.alignOf()) + bool.sizeOf();\n","          r = org.contract.utils.AlignOf.apply(r, int.alignOf()) + int.sizeOf();\n","          r = org.contract.utils.AlignOf.apply(r, char.alignOf()) + char.sizeOf();\n","          r = org.contract.utils.AlignOf.apply(r, i64.alignOf()) + i64.sizeOf();\n","          r = org.contract.utils.AlignOf.apply(r, tag.alignOf()) + tag.sizeOf();\n","          r = org.contract.utils.AlignOf.apply(r, group.alignOf()) + group.sizeOf();\n","          return eCount * org.contract.utils.AlignOf.apply(r, alignOf());\n","        }\n","        @Override\n","        public org.contract.type.Desc describe() {\n","          return org.contract.type.Desc.record(java.util.Arrays.asList(\n","            org.contract.type.Desc.field(\"a\", a.offsetOf(), a.describe())",",\n","            org.contract.type.Desc.field(\"b\", b.offsetOf(), b.describe())",",\n","            org.contract.type.Desc.field(\"bool\", bool.offsetOf(), bool.describe())",",\n","            org.contract.type.Desc.field(\"int\", int.offsetOf(), int.describe())",",\n","            org.contract.type.Desc.field(\"char\", char.offsetOf(), char.describe())",",\n","            org.contract.type.Desc.field(\"i64\", i64.offsetOf(), i64.describe())",",\n","            org.contract.type.Desc.field(\"tag\", tag.offsetOf(), tag.describe())",",\n","            org.contract.type.Desc.field(\"group\", group.offsetOf(), group.describe())","));\n","        }\n","        @Override\n","        public long alignOf() {\n","          long r = 0;\n","          r = Math.max(r, a.alignOf());\n","          r = Math.max(r, b.alignOf());\n","          r = Math.max(r, bool.alignOf());\n","          r = Math.max(r, int.alignOf());\n","          r = Math.max(r, char.alignOf());\n","          r = Math.max(r, i64.alignOf());\n","          r = Math.max(r, tag.alignOf());\n","          r = Math.max(r, group.alignOf());\n","          return r;\n","        }\n","        @Override\n","        public Boolean from(long off, org.contract.type.Desc desc) {\n","          offsetOf(off);\n","\n","          boolean r = true;\n","          r &= desc.with(\"a\", a::from);\n","          r &= desc.with(\"b\", b::from);\n","          r &= desc.with(\"bool\", bool::from);\n","          r &= desc.with(\"int\", int::from);\n","          r &= desc.with(\"char\", char::from);\n","          r &= desc.with(\"i64\", i64::from);\n","          r &= desc.with(\"tag\", tag::from);\n","          r &= desc.with(\"group\", group::from);\n","          return r;\n","        }\n","      };\n","    }\n","  }\n","  // all fields\n","  public org.contract.field.Ref<org.contract.field.array.Dynamic<org.contract.field.Int>> a;\n","  public org.contract.field.Int b;\n","  public org.contract.field.Bool bool;\n","  public org.contract.field.Int int;\n","  public org.contract.field.Char char;\n","  public org.contract.field.Long i64;\n","  public org.contract.field.Int tag;\n","  public org.contract.field.Int group;\n","  // constructor.\n","  private R1(UnsafeBuffer unsafeBuffer, org.contract.utils.Pointer<java.lang.Long> position) {\n","    super(unsafeBuffer, position);\n","  }\n","  // process new wire buffer.\n","  @Override\n","  public org.contract.field.base.WireI wire(UnsafeBuffer unsafeBuffer) {\n","    a.wire(unsafeBuffer);\n","    b.wire(unsafeBuffer);\n","    bool.wire(unsafeBuffer);\n","    int.wire(unsafeBuffer);\n","    char.wire(unsafeBuffer);\n","    i64.wire(unsafeBuffer);\n","    tag.wire(unsafeBuffer);\n","    group.wire(unsafeBuffer);\n","    return super.wire(unsafeBuffer);\n","  }\n","}\n"], fcIndent = Java.Indent 0}),("M1.M2.M4.mkdir",Java.FileContent {fcStore = [], fcIndent = Java.Indent 0})]}))

programS :: (Show a) => a -> N.Str
programS a = preambleStr <> moduleStr
  where
    preambleStr :: N.Str
    preambleStr = "language(java) {%\n"
                  <> "/*\n"
                  <> G.fromString (show a)
                  <> "\n"
                  <> "*/\n"
                  <> "\n"
                  <> "%}\n"
                  <> "language(cpp) {%\n"
                  <> "#  include <iostream>;\n"
                  <> "#  include <cpp/lang/String>;\n"
                  <> "#  include <cpp/lang/Long>;\n"
                  <> "#  include <cpp/lang/Type>;\n"
                  <> "#  include \"local/cpp/lang/Type.H\">;\n"
                  <> "%}\n"
                  <> "language(python) {%\n"
                  <> "import org.py.String\n"
                  <> "import org.py.Long\n"
                  <> "import org.py.Type\n"
                  <> "%}\n"

    moduleStr :: N.Str
    moduleStr = "   module M1 {\n"
                <> "  module M2 {\n"
                <> "    module M3 {\n"
                <> "      record R4 {\n"
                <> "        a :: [int];\n"
                <> "        record R5 {\n"
                <> "          u64arr                :: [u64];\n"
                <> "          u64refarr             :: [@u64] group(20);\n"
                <> "          u64optarr             :: [?u64];\n"
                <> "          u64farr10             :: [u64|10];\n"
                <> "          u64farr10ref          :: @[u64|10];\n"
                <> "          u64farr10opt          :: ?[u64|10];\n"
                <> "          u64optfarr10opt       :: ?[?u64|10];\n"
                <> "          u64reffarr10          :: [@u64|10];\n"
                <> "          u64carr10             :: [|u64|10];\n"
                <> "          u64farr5carr10        :: [|[u64|5]|10];\n"
                <> "          testu64farr5carr10    :: [|[test.u64|5]|10];\n"
                <> "          testu64reffarr5carr10 :: [|[@test.u64|5]|10];\n"
                <> "        };\n"
                <> "        u64arr                :: [u64];\n"
                <> "        u64refarr             :: [@u64] group(20);\n"
                <> "        u64optarr             :: [?u64];\n"
                <> "        u64farr10             :: [u64|10];\n"
                <> "        u64farr10ref          :: @[u64|10];\n"
                <> "        u64farr10opt          :: ?[u64|10];\n"
                <> "        u64optfarr10opt       :: ?[?u64|10];\n"
                <> "        u64reffarr10          :: [@u64|10];\n"
                <> "        u64carr10             :: [|u64|10];\n"
                <> "        u64farr5carr10        :: [|[u64|5]|10];\n"
                <> "        testu64farr5carr10    :: [|[test.u64|5]|10];\n"
                <> "        testu64reffarr5carr10 :: [|[@test.u64|5]|10];\n"
                <> "        b     :: std.int tag(10);\n"
                <> "        bool  :: bool;\n"
                <> "        int   :: int;\n"
                <> "        char  :: char;\n"
                <> "        i64   :: i64;\n"
                <> "        tag   :: int;\n"
                <> "        group :: std.contract.int;\n"
                <> "      }\n"
                <> "    }\n"
                <> "    module M6 {\n"
                <> "      record R7 {\n"
                <> "        b :: int;\n"
                <> "        r2r1m3m2reffarr5carr10 :: [|[@M2.M3.R1.R2|5]|10];\n"
                <> "      }\n"
                <> "    };\n"
                <> "  }\n"
                <> "}\n"

program :: N.Str
program = preambleStr <> moduleStr
  where
    preambleStr :: N.Str
    preambleStr = "language(java) {%\n"
                  <> "import java.lang.String;\n"
                  <> "import java.lang.Long;\n"
                  <> "import java.lang.Type;\n"
                  <> "%}\n"
                  <> "language(cpp) {%\n"
                  <> "#  include <iostream>;\n"
                  <> "#  include <cpp/lang/String>;\n"
                  <> "#  include <cpp/lang/Long>;\n"
                  <> "#  include <cpp/lang/Type>;\n"
                  <> "#  include \"local/cpp/lang/Type.H\">;\n"
                  <> "%}\n"
                  <> "language(java) {%\n"
                  <> "import org.contract.String;\n"
                  <> "import org.contract.Long;\n"
                  <> "import org.contract.Type;\n"
                  <> "%}\n"
                  <> "language(python) {%\n"
                  <> "import org.py.String\n"
                  <> "import org.py.Long\n"
                  <> "import org.py.Type\n"
                  <> "%}\n"

    moduleStr :: N.Str
    moduleStr = "   module M1 {\n"
                <> "  module M2 {\n"
                <> "    module M3 {\n"
                <> "      record R1 {\n"
                <> "        a :: [int];\n"
                <> "        record R2 {\n"
                <> "          u64arr                :: [u64];\n"
                <> "          u64refarr             :: [@u64] group(20);\n"
                <> "          u64optarr             :: [?u64];\n"
                <> "          u64farr10             :: [u64|10];\n"
                <> "          u64farr10ref          :: @[u64|10];\n"
                <> "          u64farr10opt          :: ?[u64|10];\n"
                <> "          u64optfarr10opt       :: ?[?u64|10];\n"
                <> "          u64reffarr10          :: [@u64|10];\n"
                <> "          u64carr10             :: [|u64|10];\n"
                <> "          u64farr5carr10        :: [|[u64|5]|10];\n"
                <> "          testu64farr5carr10    :: [|[test.u64|5]|10];\n"
                <> "          testu64reffarr5carr10 :: [|[@test.u64|5]|10];\n"
                <> "        };\n"
                <> "        b     :: std.int tag(10);\n"
                <> "        bool  :: bool;\n"
                <> "        int   :: int;\n"
                <> "        char  :: char;\n"
                <> "        i64   :: i64;\n"
                <> "        tag   :: int;\n"
                <> "        group :: std.contract.int;\n"
                <> "      }\n"
                <> "    }\n"
                <> "    module M4 {\n"
                <> "      record R2 {\n"
                <> "        b :: int;\n"
                <> "        r2r1m3m2reffarr5carr10 :: [|[@M2.M3.R1.R2|5]|10];\n"
                <> "      }\n"
                <> "    };\n"
                <> "  }\n"
                <> "}\n"

yaccJava :: N.Str -> (Java.VFSP) -> Expectation
yaccJava str vfs = either (\l -> show l `shouldBe` "") (\e ->
                                                           let
                                                             expr' = N.toExpr N.Java Nothing $ e
                                                             env' = N.Env $ N.toExprEnv N.Java <> N.toExprEnv expr'
                                                           in
                                                             (Java.run env' (Java.contractGen expr')) `shouldBe` (Right vfs)
                                                        ) (Parser.run Internal.parseFile (coerce str) initPosition)

yaccFile :: N.Str -> N.Expr -> Expectation
yaccFile str m = either (const $ shouldBe False True) ((shouldBe m) . (N.toExpr N.Java Nothing)) (Parser.run Internal.parseFile (coerce str) initPosition)

tenvFile :: N.Str -> N.ExprEnv -> Expectation
tenvFile str te = either (const $ shouldBe False True) (shouldBe te . (N.toExprEnv N.Java <>) . N.toExprEnv . N.toExpr N.Java Nothing) (Parser.run Internal.parseFile (coerce str) initPosition)
