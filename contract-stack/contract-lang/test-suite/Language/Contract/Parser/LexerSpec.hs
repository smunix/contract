module Language.Contract.Parser.LexerSpec ( spec_Lexer
                                    ) where
import Test.Tasty.Hspec

import qualified Language.Contract.Parser.Lexer as Lexer
import qualified Language.Contract.Data as Data
import qualified Language.Contract.Parser as Parser
import qualified Language.Contract.Syntax as Syn


spec_Lexer :: Spec
spec_Lexer = parallel $ do
    it "preamble" $ do
      lexExpectNoSpans
        (  "language(java) {%\n"
        <> "  import java.lang.String;\n"
        <> "  import java.lang.Long;\n"
        <> "  import java.lang.Type;\n"
        <> "%}\n"
        <> "language(cpp) {%\n"
        <> "#  include <iostream>;\n"
        <> "#  include <cpp/lang/String>;\n"
        <> "#  include <cpp/lang/Long>;\n"
        <> "#  include <cpp/lang/Type>;\n"
        <> "#  include \"local/cpp/lang/Type.H\">;\n"
        <> "%}\n"
        <> "language(java) {%\n"
        <> "  import org.contract.String;\n"
        <> "  import org.contract.Long;\n"
        <> "  import org.contract.Type;\n"
        <> "%}\n"
        <> "language(python) {%\n"
        <> "  import org.py.String\n"
        <> "  import org.py.Long\n"
        <> "  import org.py.Type\n"
        <> "%}\n"
        <> "module M1 {}"
        )
        [ Syn.Lang
        , Syn.Open Syn.Paren
        , Syn.Language Syn.Java
        , Syn.Close Syn.Paren
        , Syn.Space Syn.Whitespace " "
        , Syn.Preamble (Syn.ByteStrings [ ""
                                        , "  import java.lang.String;"
                                        , "  import java.lang.Long;"
                                        , "  import java.lang.Type;"
                                        ]
                       )
        , Syn.Space Syn.Whitespace "\n"
        , Syn.Lang
        , Syn.Open Syn.Paren
        , Syn.Language Syn.Cpp
        , Syn.Close Syn.Paren
        , Syn.Space Syn.Whitespace " "
        , Syn.Preamble (Syn.ByteStrings [ ""
                                        , "#  include <iostream>;"
                                        , "#  include <cpp/lang/String>;"
                                        , "#  include <cpp/lang/Long>;"
                                        , "#  include <cpp/lang/Type>;"
                                        , "#  include \"local/cpp/lang/Type.H\">;"
                                        ]
                       )
        , Syn.Space Syn.Whitespace "\n"
        , Syn.Lang
        , Syn.Open Syn.Paren
        , Syn.Language Syn.Java
        , Syn.Close Syn.Paren
        , Syn.Space Syn.Whitespace " "
        , Syn.Preamble (Syn.ByteStrings [ ""
                                        , "  import org.contract.String;"
                                        , "  import org.contract.Long;"
                                        , "  import org.contract.Type;"
                                        ]
                       )
        , Syn.Space Syn.Whitespace "\n"
        , Syn.Lang
        , Syn.Open Syn.Paren
        , Syn.Language Syn.Python
        , Syn.Close Syn.Paren
        , Syn.Space Syn.Whitespace " "
        , Syn.Preamble (Syn.ByteStrings [ ""
                                        , "  import org.py.String"
                                        , "  import org.py.Long"
                                        , "  import org.py.Type"
                                        ]
                       )
        , Syn.Space Syn.Whitespace "\n"
        , Syn.Ident "module"
        , Syn.Space Syn.Whitespace " "
        , Syn.Ident "M1"
        , Syn.Space Syn.Whitespace " "
        , Syn.Open Syn.Brace
        , Syn.Close Syn.Brace
        ]

    it "simple unspanned lexing" $ do
      lexExpectNoSpans
        ("module contract { record A { a :: i64; } record B { b :: i16; }\n// What comment\n}")
        [ Syn.Ident $ Data.mkIdent "module"
        , Syn.Space Syn.Whitespace " "
        , Syn.Ident $ Data.mkIdent "contract"
        , Syn.Space Syn.Whitespace " "
        , Syn.Open Syn.Brace
        , Syn.Space Syn.Whitespace " "
        , Syn.Ident $ Data.mkIdent "record"
        , Syn.Space Syn.Whitespace " "
        , Syn.Ident $ Data.mkIdent "A"
        , Syn.Space Syn.Whitespace " "
        , Syn.Open Syn.Brace
        , Syn.Space Syn.Whitespace " "
        , Syn.Ident $ Data.mkIdent "a"
        , Syn.Space Syn.Whitespace " "
        , Syn.ColonColon
        , Syn.Space Syn.Whitespace " "
        , Syn.Ident $ Data.mkIdent "i64"
        , Syn.SemiColon
        , Syn.Space Syn.Whitespace " "
        , Syn.Close Syn.Brace
        , Syn.Space Syn.Whitespace " "
        , Syn.Ident $ Data.mkIdent "record"
        , Syn.Space Syn.Whitespace " "
        , Syn.Ident $ Data.mkIdent "B"
        , Syn.Space Syn.Whitespace " "
        , Syn.Open Syn.Brace
        , Syn.Space Syn.Whitespace " "
        , Syn.Ident $ Data.mkIdent "b"
        , Syn.Space Syn.Whitespace " "
        , Syn.ColonColon
        , Syn.Space Syn.Whitespace " "
        , Syn.Ident $ Data.mkIdent "i16"
        , Syn.SemiColon
        , Syn.Space Syn.Whitespace " "
        , Syn.Close Syn.Brace
        , Syn.Space Syn.Whitespace "\n"
        , Syn.Space Syn.Comment " What comment"
        , Syn.Space Syn.Whitespace "\n"
        , Syn.Close Syn.Brace
        ]
    it "tag / fix # lexing" $ do
      lexExpectNoSpanSpaces
        (    "module enclosing {\n"
          <> "  record A {\n"
          <> "    a :: i64 (tag 64);\n"
          <> "    record In {\n"
          <> "      a :: i64 (tag 63);\n"
          <> "      b :: @[f32];\n"
          <> "      c :: [u64|10];\n"
          <> "      d :: [|char|10];\n"
          <> "    }\n"
          <> "  }\n"
          <> "}\n"
        )
        [ Syn.Ident "module",Syn.Ident "enclosing",Syn.Open Syn.Brace,Syn.Ident "record",Syn.Ident "A",Syn.Open Syn.Brace,Syn.Ident "a",Syn.ColonColon,Syn.Ident "i64",Syn.Open Syn.Paren,Syn.Ident "tag",Syn.Literal (Syn.Integer "64") Nothing,Syn.Close Syn.Paren,Syn.SemiColon,Syn.Ident "record",Syn.Ident "In",Syn.Open Syn.Brace,Syn.Ident "a",Syn.ColonColon,Syn.Ident "i64",Syn.Open Syn.Paren,Syn.Ident "tag",Syn.Literal (Syn.Integer "63") Nothing,Syn.Close Syn.Paren,Syn.SemiColon,Syn.Ident "b",Syn.ColonColon,Syn.At,Syn.Open Syn.Bracket,Syn.Ident "f32",Syn.Close Syn.Bracket,Syn.SemiColon,Syn.Ident "c",Syn.ColonColon,Syn.Open Syn.Bracket,Syn.Ident "u64",Syn.Pipe,Syn.Literal (Syn.Integer "10") Nothing,Syn.Close Syn.Bracket,Syn.SemiColon,Syn.Ident "d",Syn.ColonColon,Syn.Open Syn.Bracket,Syn.Pipe,Syn.Ident "char",Syn.Pipe,Syn.Literal (Syn.Integer "10") Nothing,Syn.Close Syn.Bracket,Syn.SemiColon,Syn.Close Syn.Brace,Syn.Close Syn.Brace,Syn.Close Syn.Brace
        ]

    it "non spaces spanned lexing" $ do
      lexExpectNoSpaces
        (    "module contract {\n"
          <> "  record A {\n"
          <> "    a :: i64;\n"
          <> "  }\n"
          <> "  record B {\n"
          <> "    b :: i16;\n"
          <> "  }\n"
          <> "  // What comment\n"
          <> "}\n"
        )
        [Data.Spanned (Syn.Ident "module") (Data.Span (Data.Position 0 1 0) (Data.Position 6 1 6)),Data.Spanned (Syn.Ident "contract") (Data.Span (Data.Position 7 1 7) (Data.Position 15 1 15)),Data.Spanned (Syn.Open Syn.Brace) (Data.Span (Data.Position 16 1 16) (Data.Position 17 1 17)),Data.Spanned (Syn.Ident "record") (Data.Span (Data.Position 20 2 3) (Data.Position 26 2 9)),Data.Spanned (Syn.Ident "A") (Data.Span (Data.Position 27 2 10) (Data.Position 28 2 11)),Data.Spanned (Syn.Open Syn.Brace) (Data.Span (Data.Position 29 2 12) (Data.Position 30 2 13)),Data.Spanned (Syn.Ident "a") (Data.Span (Data.Position 35 3 5) (Data.Position 36 3 6)),Data.Spanned Syn.ColonColon (Data.Span (Data.Position 37 3 7) (Data.Position 39 3 9)),Data.Spanned (Syn.Ident "i64") (Data.Span (Data.Position 40 3 10) (Data.Position 43 3 13)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 43 3 13) (Data.Position 44 3 14)),Data.Spanned (Syn.Close Syn.Brace) (Data.Span (Data.Position 47 4 3) (Data.Position 48 4 4)),Data.Spanned (Syn.Ident "record") (Data.Span (Data.Position 51 5 3) (Data.Position 57 5 9)),Data.Spanned (Syn.Ident "B") (Data.Span (Data.Position 58 5 10) (Data.Position 59 5 11)),Data.Spanned (Syn.Open Syn.Brace) (Data.Span (Data.Position 60 5 12) (Data.Position 61 5 13)),Data.Spanned (Syn.Ident "b") (Data.Span (Data.Position 66 6 5) (Data.Position 67 6 6)),Data.Spanned Syn.ColonColon (Data.Span (Data.Position 68 6 7) (Data.Position 70 6 9)),Data.Spanned (Syn.Ident "i16") (Data.Span (Data.Position 71 6 10) (Data.Position 74 6 13)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 74 6 13) (Data.Position 75 6 14)),Data.Spanned (Syn.Close Syn.Brace) (Data.Span (Data.Position 78 7 3) (Data.Position 79 7 4)),Data.Spanned (Syn.Close Syn.Brace) (Data.Span (Data.Position 98 9 1) (Data.Position 99 9 2))]

    it "simple nested (modules/record) lexing" $ do
      lexExpect
        (    "module enclosing {\n"
          <> "  record A {\n"
          <> "    a :: i64;\n"
          <> "    record In {\n"
          <> "      a :: i64;\n"
          <> "    };\n"
          <> "  }\n"
          <> "  module nested {\n"
          <> "    record C {\n"
          <> "      record In {\n"
          <> "        i :: i64;\n"
          <> "        f :: f64;\n"
          <> "      };\n"
          <> "      i :: i64;\n"
          <> "      f :: f64;\n"
          <> "    }\n"
          <> "  }\n"
          <> "  record B {\n"
          <> "    a :: A.In;\n"
          <> "    b :: nested.C.In;\n"
          <> "  }\n"
          <> "  // What comment\n"
          <> "}\n"
        )
        [Data.Spanned (Syn.Ident "module") (Data.Span (Data.Position 0 1 0) (Data.Position 6 1 6)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 6 1 6) (Data.Position 7 1 7)),Data.Spanned (Syn.Ident "enclosing") (Data.Span (Data.Position 7 1 7) (Data.Position 16 1 16)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 16 1 16) (Data.Position 17 1 17)),Data.Spanned (Syn.Open Syn.Brace) (Data.Span (Data.Position 17 1 17) (Data.Position 18 1 18)),Data.Spanned (Syn.Space Syn.Whitespace "\n  ") (Data.Span (Data.Position 18 1 18) (Data.Position 21 2 3)),Data.Spanned (Syn.Ident "record") (Data.Span (Data.Position 21 2 3) (Data.Position 27 2 9)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 27 2 9) (Data.Position 28 2 10)),Data.Spanned (Syn.Ident "A") (Data.Span (Data.Position 28 2 10) (Data.Position 29 2 11)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 29 2 11) (Data.Position 30 2 12)),Data.Spanned (Syn.Open Syn.Brace) (Data.Span (Data.Position 30 2 12) (Data.Position 31 2 13)),Data.Spanned (Syn.Space Syn.Whitespace "\n    ") (Data.Span (Data.Position 31 2 13) (Data.Position 36 3 5)),Data.Spanned (Syn.Ident "a") (Data.Span (Data.Position 36 3 5) (Data.Position 37 3 6)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 37 3 6) (Data.Position 38 3 7)),Data.Spanned Syn.ColonColon (Data.Span (Data.Position 38 3 7) (Data.Position 40 3 9)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 40 3 9) (Data.Position 41 3 10)),Data.Spanned (Syn.Ident "i64") (Data.Span (Data.Position 41 3 10) (Data.Position 44 3 13)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 44 3 13) (Data.Position 45 3 14)),Data.Spanned (Syn.Space Syn.Whitespace "\n    ") (Data.Span (Data.Position 45 3 14) (Data.Position 50 4 5)),Data.Spanned (Syn.Ident "record") (Data.Span (Data.Position 50 4 5) (Data.Position 56 4 11)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 56 4 11) (Data.Position 57 4 12)),Data.Spanned (Syn.Ident "In") (Data.Span (Data.Position 57 4 12) (Data.Position 59 4 14)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 59 4 14) (Data.Position 60 4 15)),Data.Spanned (Syn.Open Syn.Brace) (Data.Span (Data.Position 60 4 15) (Data.Position 61 4 16)),Data.Spanned (Syn.Space Syn.Whitespace "\n      ") (Data.Span (Data.Position 61 4 16) (Data.Position 68 5 7)),Data.Spanned (Syn.Ident "a") (Data.Span (Data.Position 68 5 7) (Data.Position 69 5 8)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 69 5 8) (Data.Position 70 5 9)),Data.Spanned Syn.ColonColon (Data.Span (Data.Position 70 5 9) (Data.Position 72 5 11)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 72 5 11) (Data.Position 73 5 12)),Data.Spanned (Syn.Ident "i64") (Data.Span (Data.Position 73 5 12) (Data.Position 76 5 15)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 76 5 15) (Data.Position 77 5 16)),Data.Spanned (Syn.Space Syn.Whitespace "\n    ") (Data.Span (Data.Position 77 5 16) (Data.Position 82 6 5)),Data.Spanned (Syn.Close Syn.Brace) (Data.Span (Data.Position 82 6 5) (Data.Position 83 6 6)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 83 6 6) (Data.Position 84 6 7)),Data.Spanned (Syn.Space Syn.Whitespace "\n  ") (Data.Span (Data.Position 84 6 7) (Data.Position 87 7 3)),Data.Spanned (Syn.Close Syn.Brace) (Data.Span (Data.Position 87 7 3) (Data.Position 88 7 4)),Data.Spanned (Syn.Space Syn.Whitespace "\n  ") (Data.Span (Data.Position 88 7 4) (Data.Position 91 8 3)),Data.Spanned (Syn.Ident "module") (Data.Span (Data.Position 91 8 3) (Data.Position 97 8 9)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 97 8 9) (Data.Position 98 8 10)),Data.Spanned (Syn.Ident "nested") (Data.Span (Data.Position 98 8 10) (Data.Position 104 8 16)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 104 8 16) (Data.Position 105 8 17)),Data.Spanned (Syn.Open Syn.Brace) (Data.Span (Data.Position 105 8 17) (Data.Position 106 8 18)),Data.Spanned (Syn.Space Syn.Whitespace "\n    ") (Data.Span (Data.Position 106 8 18) (Data.Position 111 9 5)),Data.Spanned (Syn.Ident "record") (Data.Span (Data.Position 111 9 5) (Data.Position 117 9 11)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 117 9 11) (Data.Position 118 9 12)),Data.Spanned (Syn.Ident "C") (Data.Span (Data.Position 118 9 12) (Data.Position 119 9 13)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 119 9 13) (Data.Position 120 9 14)),Data.Spanned (Syn.Open Syn.Brace) (Data.Span (Data.Position 120 9 14) (Data.Position 121 9 15)),Data.Spanned (Syn.Space Syn.Whitespace "\n      ") (Data.Span (Data.Position 121 9 15) (Data.Position 128 10 7)),Data.Spanned (Syn.Ident "record") (Data.Span (Data.Position 128 10 7) (Data.Position 134 10 13)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 134 10 13) (Data.Position 135 10 14)),Data.Spanned (Syn.Ident "In") (Data.Span (Data.Position 135 10 14) (Data.Position 137 10 16)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 137 10 16) (Data.Position 138 10 17)),Data.Spanned (Syn.Open Syn.Brace) (Data.Span (Data.Position 138 10 17) (Data.Position 139 10 18)),Data.Spanned (Syn.Space Syn.Whitespace "\n        ") (Data.Span (Data.Position 139 10 18) (Data.Position 148 11 9)),Data.Spanned (Syn.Ident "i") (Data.Span (Data.Position 148 11 9) (Data.Position 149 11 10)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 149 11 10) (Data.Position 150 11 11)),Data.Spanned Syn.ColonColon (Data.Span (Data.Position 150 11 11) (Data.Position 152 11 13)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 152 11 13) (Data.Position 153 11 14)),Data.Spanned (Syn.Ident "i64") (Data.Span (Data.Position 153 11 14) (Data.Position 156 11 17)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 156 11 17) (Data.Position 157 11 18)),Data.Spanned (Syn.Space Syn.Whitespace "\n        ") (Data.Span (Data.Position 157 11 18) (Data.Position 166 12 9)),Data.Spanned (Syn.Ident "f") (Data.Span (Data.Position 166 12 9) (Data.Position 167 12 10)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 167 12 10) (Data.Position 168 12 11)),Data.Spanned Syn.ColonColon (Data.Span (Data.Position 168 12 11) (Data.Position 170 12 13)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 170 12 13) (Data.Position 171 12 14)),Data.Spanned (Syn.Ident "f64") (Data.Span (Data.Position 171 12 14) (Data.Position 174 12 17)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 174 12 17) (Data.Position 175 12 18)),Data.Spanned (Syn.Space Syn.Whitespace "\n      ") (Data.Span (Data.Position 175 12 18) (Data.Position 182 13 7)),Data.Spanned (Syn.Close Syn.Brace) (Data.Span (Data.Position 182 13 7) (Data.Position 183 13 8)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 183 13 8) (Data.Position 184 13 9)),Data.Spanned (Syn.Space Syn.Whitespace "\n      ") (Data.Span (Data.Position 184 13 9) (Data.Position 191 14 7)),Data.Spanned (Syn.Ident "i") (Data.Span (Data.Position 191 14 7) (Data.Position 192 14 8)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 192 14 8) (Data.Position 193 14 9)),Data.Spanned Syn.ColonColon (Data.Span (Data.Position 193 14 9) (Data.Position 195 14 11)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 195 14 11) (Data.Position 196 14 12)),Data.Spanned (Syn.Ident "i64") (Data.Span (Data.Position 196 14 12) (Data.Position 199 14 15)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 199 14 15) (Data.Position 200 14 16)),Data.Spanned (Syn.Space Syn.Whitespace "\n      ") (Data.Span (Data.Position 200 14 16) (Data.Position 207 15 7)),Data.Spanned (Syn.Ident "f") (Data.Span (Data.Position 207 15 7) (Data.Position 208 15 8)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 208 15 8) (Data.Position 209 15 9)),Data.Spanned Syn.ColonColon (Data.Span (Data.Position 209 15 9) (Data.Position 211 15 11)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 211 15 11) (Data.Position 212 15 12)),Data.Spanned (Syn.Ident "f64") (Data.Span (Data.Position 212 15 12) (Data.Position 215 15 15)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 215 15 15) (Data.Position 216 15 16)),Data.Spanned (Syn.Space Syn.Whitespace "\n    ") (Data.Span (Data.Position 216 15 16) (Data.Position 221 16 5)),Data.Spanned (Syn.Close Syn.Brace) (Data.Span (Data.Position 221 16 5) (Data.Position 222 16 6)),Data.Spanned (Syn.Space Syn.Whitespace "\n  ") (Data.Span (Data.Position 222 16 6) (Data.Position 225 17 3)),Data.Spanned (Syn.Close Syn.Brace) (Data.Span (Data.Position 225 17 3) (Data.Position 226 17 4)),Data.Spanned (Syn.Space Syn.Whitespace "\n  ") (Data.Span (Data.Position 226 17 4) (Data.Position 229 18 3)),Data.Spanned (Syn.Ident "record") (Data.Span (Data.Position 229 18 3) (Data.Position 235 18 9)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 235 18 9) (Data.Position 236 18 10)),Data.Spanned (Syn.Ident "B") (Data.Span (Data.Position 236 18 10) (Data.Position 237 18 11)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 237 18 11) (Data.Position 238 18 12)),Data.Spanned (Syn.Open Syn.Brace) (Data.Span (Data.Position 238 18 12) (Data.Position 239 18 13)),Data.Spanned (Syn.Space Syn.Whitespace "\n    ") (Data.Span (Data.Position 239 18 13) (Data.Position 244 19 5)),Data.Spanned (Syn.Ident "a") (Data.Span (Data.Position 244 19 5) (Data.Position 245 19 6)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 245 19 6) (Data.Position 246 19 7)),Data.Spanned Syn.ColonColon (Data.Span (Data.Position 246 19 7) (Data.Position 248 19 9)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 248 19 9) (Data.Position 249 19 10)),Data.Spanned (Syn.Ident "A") (Data.Span (Data.Position 249 19 10) (Data.Position 250 19 11)),Data.Spanned Syn.Dot (Data.Span (Data.Position 250 19 11) (Data.Position 251 19 12)),Data.Spanned (Syn.Ident "In") (Data.Span (Data.Position 251 19 12) (Data.Position 253 19 14)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 253 19 14) (Data.Position 254 19 15)),Data.Spanned (Syn.Space Syn.Whitespace "\n    ") (Data.Span (Data.Position 254 19 15) (Data.Position 259 20 5)),Data.Spanned (Syn.Ident "b") (Data.Span (Data.Position 259 20 5) (Data.Position 260 20 6)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 260 20 6) (Data.Position 261 20 7)),Data.Spanned Syn.ColonColon (Data.Span (Data.Position 261 20 7) (Data.Position 263 20 9)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 263 20 9) (Data.Position 264 20 10)),Data.Spanned (Syn.Ident "nested") (Data.Span (Data.Position 264 20 10) (Data.Position 270 20 16)),Data.Spanned Syn.Dot (Data.Span (Data.Position 270 20 16) (Data.Position 271 20 17)),Data.Spanned (Syn.Ident "C") (Data.Span (Data.Position 271 20 17) (Data.Position 272 20 18)),Data.Spanned Syn.Dot (Data.Span (Data.Position 272 20 18) (Data.Position 273 20 19)),Data.Spanned (Syn.Ident "In") (Data.Span (Data.Position 273 20 19) (Data.Position 275 20 21)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 275 20 21) (Data.Position 276 20 22)),Data.Spanned (Syn.Space Syn.Whitespace "\n  ") (Data.Span (Data.Position 276 20 22) (Data.Position 279 21 3)),Data.Spanned (Syn.Close Syn.Brace) (Data.Span (Data.Position 279 21 3) (Data.Position 280 21 4)),Data.Spanned (Syn.Space Syn.Whitespace "\n  ") (Data.Span (Data.Position 280 21 4) (Data.Position 283 22 3)),Data.Spanned (Syn.Space Syn.Comment " What comment") (Data.Span (Data.Position 283 22 3) (Data.Position 298 22 18)),Data.Spanned (Syn.Space Syn.Whitespace "\n") (Data.Span (Data.Position 298 22 18) (Data.Position 299 23 1)),Data.Spanned (Syn.Close Syn.Brace) (Data.Span (Data.Position 299 23 1) (Data.Position 300 23 2)),Data.Spanned (Syn.Space Syn.Whitespace "\n") (Data.Span (Data.Position 300 23 2) (Data.Position 301 24 1))]

    it "simple spanned lexing" $ do
      lexExpect
        (    "module contract {"
          <> "  record A {"
          <> "    a :: i64;"
          <> "  }"
          <> "  record B {"
          <> "    b :: i16;"
          <> "  }\n"
          <> "  // What comment\n"
          <> "}"
        )
        [Data.Spanned (Syn.Ident "module") (Data.Span (Data.Position 0 1 0) (Data.Position 6 1 6)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 6 1 6) (Data.Position 7 1 7)),Data.Spanned (Syn.Ident "contract") (Data.Span (Data.Position 7 1 7) (Data.Position 15 1 15)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 15 1 15) (Data.Position 16 1 16)),Data.Spanned (Syn.Open Syn.Brace) (Data.Span (Data.Position 16 1 16) (Data.Position 17 1 17)),Data.Spanned (Syn.Space Syn.Whitespace "  ") (Data.Span (Data.Position 17 1 17) (Data.Position 19 1 19)),Data.Spanned (Syn.Ident "record") (Data.Span (Data.Position 19 1 19) (Data.Position 25 1 25)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 25 1 25) (Data.Position 26 1 26)),Data.Spanned (Syn.Ident "A") (Data.Span (Data.Position 26 1 26) (Data.Position 27 1 27)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 27 1 27) (Data.Position 28 1 28)),Data.Spanned (Syn.Open Syn.Brace) (Data.Span (Data.Position 28 1 28) (Data.Position 29 1 29)),Data.Spanned (Syn.Space Syn.Whitespace "    ") (Data.Span (Data.Position 29 1 29) (Data.Position 33 1 33)),Data.Spanned (Syn.Ident "a") (Data.Span (Data.Position 33 1 33) (Data.Position 34 1 34)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 34 1 34) (Data.Position 35 1 35)),Data.Spanned Syn.ColonColon (Data.Span (Data.Position 35 1 35) (Data.Position 37 1 37)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 37 1 37) (Data.Position 38 1 38)),Data.Spanned (Syn.Ident "i64") (Data.Span (Data.Position 38 1 38) (Data.Position 41 1 41)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 41 1 41) (Data.Position 42 1 42)),Data.Spanned (Syn.Space Syn.Whitespace "  ") (Data.Span (Data.Position 42 1 42) (Data.Position 44 1 44)),Data.Spanned (Syn.Close Syn.Brace) (Data.Span (Data.Position 44 1 44) (Data.Position 45 1 45)),Data.Spanned (Syn.Space Syn.Whitespace "  ") (Data.Span (Data.Position 45 1 45) (Data.Position 47 1 47)),Data.Spanned (Syn.Ident "record") (Data.Span (Data.Position 47 1 47) (Data.Position 53 1 53)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 53 1 53) (Data.Position 54 1 54)),Data.Spanned (Syn.Ident "B") (Data.Span (Data.Position 54 1 54) (Data.Position 55 1 55)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 55 1 55) (Data.Position 56 1 56)),Data.Spanned (Syn.Open Syn.Brace) (Data.Span (Data.Position 56 1 56) (Data.Position 57 1 57)),Data.Spanned (Syn.Space Syn.Whitespace "    ") (Data.Span (Data.Position 57 1 57) (Data.Position 61 1 61)),Data.Spanned (Syn.Ident "b") (Data.Span (Data.Position 61 1 61) (Data.Position 62 1 62)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 62 1 62) (Data.Position 63 1 63)),Data.Spanned Syn.ColonColon (Data.Span (Data.Position 63 1 63) (Data.Position 65 1 65)),Data.Spanned (Syn.Space Syn.Whitespace " ") (Data.Span (Data.Position 65 1 65) (Data.Position 66 1 66)),Data.Spanned (Syn.Ident "i16") (Data.Span (Data.Position 66 1 66) (Data.Position 69 1 69)),Data.Spanned Syn.SemiColon (Data.Span (Data.Position 69 1 69) (Data.Position 70 1 70)),Data.Spanned (Syn.Space Syn.Whitespace "  ") (Data.Span (Data.Position 70 1 70) (Data.Position 72 1 72)),Data.Spanned (Syn.Close Syn.Brace) (Data.Span (Data.Position 72 1 72) (Data.Position 73 1 73)),Data.Spanned (Syn.Space Syn.Whitespace "\n  ") (Data.Span (Data.Position 73 1 73) (Data.Position 76 2 3)),Data.Spanned (Syn.Space Syn.Comment " What comment") (Data.Span (Data.Position 76 2 3) (Data.Position 91 2 18)),Data.Spanned (Syn.Space Syn.Whitespace "\n") (Data.Span (Data.Position 91 2 18) (Data.Position 92 3 1)),Data.Spanned (Syn.Close Syn.Brace) (Data.Span (Data.Position 92 3 1) (Data.Position 93 3 2))]

lexExpectNoSpans :: String -> [Syn.Token] -> Expectation
lexExpectNoSpans str toks = shouldBe (lexTokenNoSpans $ Data.inputStreamFromString str) (Right toks)

lexTokenNoSpans :: Data.InputStream -> Either Parser.ParseFail [Syn.Token]
lexTokenNoSpans = ((Data.unSpan <$>) <$>) . flip (Parser.run (Lexer.lexTokens Lexer.lexToken)) Data.initPosition

lexToken :: Data.InputStream -> Either Parser.ParseFail [Data.Spanned Syn.Token]
lexToken = flip (Parser.run (Lexer.lexTokens Lexer.lexToken)) Data.initPosition

lexTokenNoSpaces :: Data.InputStream -> Either Parser.ParseFail [Data.Spanned Syn.Token]
lexTokenNoSpaces = flip (Parser.run (Lexer.lexTokens Lexer.lexNonSpace)) Data.initPosition

lexExpect :: String -> [Data.Spanned Syn.Token] -> Expectation
lexExpect str toks = shouldBe (lexToken $ Data.inputStreamFromString str) (Right toks)

lexExpectNoSpaces :: String -> [Data.Spanned Syn.Token] -> Expectation
lexExpectNoSpaces str toks = shouldBe (lexTokenNoSpaces $ Data.inputStreamFromString str) (Right toks)

lexExpectNoSpanSpaces :: String -> [Syn.Token] -> Expectation
lexExpectNoSpanSpaces str toks = shouldBe (fmap (fmap Data.unSpan) $ lexTokenNoSpaces $ Data.inputStreamFromString str) (Right toks)
