module Language.Contract.Parser.ParserSpec ( spec_Parser_1
                                     , spec_Parser_2
                                     , spec_Parser_preamble
                                     ) where
import Test.Tasty.Hspec

import qualified Language.Contract.Parser.Internal as Internal
import qualified Language.Contract.Parser.Lexer as Lexer
import Language.Contract.Data hiding (Ident)
import qualified Language.Contract.Parser as Parser
import Language.Contract.Syntax
import Data.Monoid (mempty)
import Data.Semigroup ((<>))
import Data.HashMap.Strict (fromList)

import Language.Contract.Data (Span)

spec_Parser_preamble :: Spec
spec_Parser_preamble = parallel $ do
    it "java preamble" $ do
      yaccFile
        (  "language(java) {%\n"
        <> "  import java.lang.String;\n"
        <> "  import java.lang.Long;\n"
        <> "  import java.lang.Type;\n"
        <> "%}\n"
        <> "language(cpp) {%\n"
        <> "#  include <iostream>;\n"
        <> "#  include <cpp/lang/String>;\n"
        <> "#  include <cpp/lang/Long>;\n"
        <> "#  include <cpp/lang/Type>;\n"
        <> "#  include \"local/cpp/lang/Type.H\">;\n"
        <> "%}\n"
        <> "language(java) {%\n"
        <> "  import org.contract.String;\n"
        <> "  import org.contract.Long;\n"
        <> "  import org.contract.Type;\n"
        <> "%}\n"
        <> "language(python) {%\n"
        <> "  import org.py.String\n"
        <> "  import org.py.Long\n"
        <> "  import org.py.Type\n"
        <> "%}\n"
        <> "module M1 {}"
        )
        ((File (PreambleL (fromList [(CppL,["","#  include <iostream>;","#  include <cpp/lang/String>;","#  include <cpp/lang/Long>;","#  include <cpp/lang/Type>;","#  include \"local/cpp/lang/Type.H\">;"]),(JavaL,["","  import java.lang.String;","  import java.lang.Long;","  import java.lang.Type;","","  import org.contract.String;","  import org.contract.Long;","  import org.contract.Type;"]),(PythonL,["","  import org.py.String","  import org.py.Long","  import org.py.Type"])])) [Left (Module (Spanned (Ident "M1") (Span (Position 466 23 8) (Position 468 23 10))) [])]))

spec_Parser_1 :: Spec
spec_Parser_1 = parallel $ do
    it "tag / fix # lexing" $ do
      yaccModule
        (    "module M1 {\n"
          <> "  module M2 {\n"
          <> "    module M3 {\n"
          <> "      record R1 {\n"
          <> "        a     :: [int];\n"
          <> "        record R2 {\n"
          <> "          u64arr :: [u64];\n"
          <> "          refu64arr :: [@u64] group(20);\n"
          <> "          refqu64arr :: [?u64];\n"
          <> "          qrefqu64arr :: [?u64];\n"
          <> "          u64farr :: [u64|10];\n"
          <> "          u64reffarr :: @[u64|10];\n"
          <> "          u64qfarr :: ?[u64|10];\n"
          <> "          qu64qfarr :: ?[?u64|10];\n"
          <> "          refu64farr :: [@u64|10];\n"
          <> "          u64carr :: [|u64|10];\n"
          <> "          u64cfarr :: [|[u64|5]|10];\n"
          <> "          u64ucfarr :: [|[std.u64|5]|10];\n"
          <> "          refu64ucfarr :: [|[@std.u64|5]|10];\n"
          <> "        };\n"
          <> "        b     :: std.int tag(10);\n"
          <> "        c     :: std.contract.int;\n"
          <> "        bool  :: bool;\n"
          <> "        int   :: int;\n"
          <> "        char  :: char;\n"
          <> "        i64   :: i64;\n"
          <> "        tag   :: int;\n"
          <> "        group :: int;\n"
          <> "      }\n"
          <> "    }\n"
          <> "    module M4 {\n"
          <> "      record R2 {\n"
          <> "        b :: int;\n"
          <> "      }\n"
          <> "    };\n"
          <> "  }\n"
          <> "}\n"
        )
        [Left (Module (Spanned (Ident "M1") (Span (Position 7 1 7) (Position 9 1 9))) [Left (Module (Spanned (Ident "M2") (Span (Position 21 2 10) (Position 23 2 12))) [Left (Module (Spanned (Ident "M4") (Span (Position 802 31 12) (Position 804 31 14))) [Right (Record (Spanned (Ident "R2") (Span (Position 820 32 14) (Position 822 32 16))) [Right (Field (Spanned (Ident "b") (Span (Position 833 33 9) (Position 834 33 10))) (Type (Spanned (Ident "int") (Span (Position 838 33 14) (Position 841 33 17))) NoType) NoTag)])]),Left (Module (Spanned (Ident "M3") (Span (Position 37 3 12) (Position 39 3 14))) [Right (Record (Spanned (Ident "R1") (Span (Position 55 4 14) (Position 57 4 16))) [Right (Field (Spanned (Ident "group") (Span (Position 763 28 9) (Position 768 28 14))) (Type (Spanned (Ident "int") (Span (Position 772 28 18) (Position 775 28 21))) NoType) NoTag),Right (Field (Spanned (Ident "tag") (Span (Position 741 27 9) (Position 744 27 12))) (Type (Spanned (Ident "int") (Span (Position 750 27 18) (Position 753 27 21))) NoType) NoTag),Right (Field (Spanned (Ident "i64") (Span (Position 719 26 9) (Position 722 26 12))) (Type (Spanned (Ident "i64") (Span (Position 728 26 18) (Position 731 26 21))) NoType) NoTag),Right (Field (Spanned (Ident "char") (Span (Position 696 25 9) (Position 700 25 13))) (Type (Spanned (Ident "char") (Span (Position 705 25 18) (Position 709 25 22))) NoType) NoTag),Right (Field (Spanned (Ident "int") (Span (Position 674 24 9) (Position 677 24 12))) (Type (Spanned (Ident "int") (Span (Position 683 24 18) (Position 686 24 21))) NoType) NoTag),Right (Field (Spanned (Ident "bool") (Span (Position 651 23 9) (Position 655 23 13))) (Type (Spanned (Ident "bool") (Span (Position 660 23 18) (Position 664 23 22))) NoType) NoTag),Right (Field (Spanned (Ident "c") (Span (Position 616 22 9) (Position 617 22 10))) (Type (Spanned (Ident "int") (Span (Position 638 22 31) (Position 641 22 34))) (Type (Spanned (Ident "contract") (Span (Position 629 22 22) (Position 637 22 30))) (Type (Spanned (Ident "std") (Span (Position 625 22 18) (Position 628 22 21))) NoType))) NoTag),Right (Field (Spanned (Ident "b") (Span (Position 582 21 9) (Position 583 21 10))) (Type (Spanned (Ident "int") (Span (Position 595 21 22) (Position 598 21 25))) (Type (Spanned (Ident "std") (Span (Position 591 21 18) (Position 594 21 21))) NoType)) (Tag (Spanned (Literal (Integer "10") Nothing) (Span (Position 603 21 30) (Position 605 21 32))))),Left (Record (Spanned (Ident "R2") (Span (Position 99 6 16) (Position 101 6 18))) [Right (Field (Spanned (Ident "refu64ucfarr") (Span (Position 527 19 11) (Position 539 19 23))) (CArray (FArray (Ref (Type (Spanned (Ident "u64") (Span (Position 551 19 35) (Position 554 19 38))) (Type (Spanned (Ident "std") (Span (Position 547 19 31) (Position 550 19 34))) NoType))) (Spanned 5 (Span (Position 555 19 39) (Position 556 19 40)))) (Spanned 10 (Span (Position 558 19 42) (Position 560 19 44)))) NoTag),Right (Field (Spanned (Ident "u64ucfarr") (Span (Position 485 18 11) (Position 494 18 20))) (CArray (FArray (Type (Spanned (Ident "u64") (Span (Position 505 18 31) (Position 508 18 34))) (Type (Spanned (Ident "std") (Span (Position 501 18 27) (Position 504 18 30))) NoType)) (Spanned 5 (Span (Position 509 18 35) (Position 510 18 36)))) (Spanned 10 (Span (Position 512 18 38) (Position 514 18 40)))) NoTag),Right (Field (Spanned (Ident "u64cfarr") (Span (Position 448 17 11) (Position 456 17 19))) (CArray (FArray (Type (Spanned (Ident "u64") (Span (Position 463 17 26) (Position 466 17 29))) NoType) (Spanned 5 (Span (Position 467 17 30) (Position 468 17 31)))) (Spanned 10 (Span (Position 470 17 33) (Position 472 17 35)))) NoTag),Right (Field (Spanned (Ident "u64carr") (Span (Position 416 16 11) (Position 423 16 18))) (CArray (Type (Spanned (Ident "u64") (Span (Position 429 16 24) (Position 432 16 27))) NoType) (Spanned 10 (Span (Position 433 16 28) (Position 435 16 30)))) NoTag),Right (Field (Spanned (Ident "refu64farr") (Span (Position 381 15 11) (Position 391 15 21))) (FArray (Ref (Type (Spanned (Ident "u64") (Span (Position 397 15 27) (Position 400 15 30))) NoType)) (Spanned 10 (Span (Position 401 15 31) (Position 403 15 33)))) NoTag),Right (Field (Spanned (Ident "qu64qfarr") (Span (Position 346 14 11) (Position 355 14 20))) (Ref (FArray (Ref (Type (Spanned (Ident "u64") (Span (Position 362 14 27) (Position 365 14 30))) NoType)) (Spanned 10 (Span (Position 366 14 31) (Position 368 14 33))))) NoTag),Right (Field (Spanned (Ident "u64qfarr") (Span (Position 313 13 11) (Position 321 13 19))) (Ref (FArray (Type (Spanned (Ident "u64") (Span (Position 327 13 25) (Position 330 13 28))) NoType) (Spanned 10 (Span (Position 331 13 29) (Position 333 13 31))))) NoTag),Right (Field (Spanned (Ident "u64reffarr") (Span (Position 278 12 11) (Position 288 12 21))) (Ref (FArray (Type (Spanned (Ident "u64") (Span (Position 294 12 27) (Position 297 12 30))) NoType) (Spanned 10 (Span (Position 298 12 31) (Position 300 12 33))))) NoTag),Right (Field (Spanned (Ident "u64farr") (Span (Position 247 11 11) (Position 254 11 18))) (FArray (Type (Spanned (Ident "u64") (Span (Position 259 11 23) (Position 262 11 26))) NoType) (Spanned 10 (Span (Position 263 11 27) (Position 265 11 29)))) NoTag),Right (Field (Spanned (Ident "qrefqu64arr") (Span (Position 214 10 11) (Position 225 10 22))) (Ref (Array (Ref (Type (Spanned (Ident "u64") (Span (Position 231 10 28) (Position 234 10 31))) NoType)))) NoTag),Right (Field (Spanned (Ident "refqu64arr") (Span (Position 182 9 11) (Position 192 9 21))) (Ref (Array (Ref (Type (Spanned (Ident "u64") (Span (Position 198 9 27) (Position 201 9 30))) NoType)))) NoTag),Right (Field (Spanned (Ident "refu64arr") (Span (Position 141 8 11) (Position 150 8 20))) (Ref (Array (Ref (Type (Spanned (Ident "u64") (Span (Position 156 8 26) (Position 159 8 29))) NoType)))) (Group (Spanned (Literal (Integer "20") Nothing) (Span (Position 167 8 37) (Position 169 8 39))))),Right (Field (Spanned (Ident "u64arr") (Span (Position 114 7 11) (Position 120 7 17))) (Ref (Array (Type (Spanned (Ident "u64") (Span (Position 125 7 22) (Position 128 7 25))) NoType))) NoTag)]),Right (Field (Spanned (Ident "a") (Span (Position 68 5 9) (Position 69 5 10))) (Ref (Array (Type (Spanned (Ident "int") (Span (Position 78 5 19) (Position 81 5 22))) NoType))) NoTag)])])])])]

spec_Parser_2 :: Spec
spec_Parser_2 = parallel $ do
    it "tag / fix # lexing" $ do
      yaccModule
        (    "module M1 {\n"
          <> "  module M2 {\n"
          <> "    module M3 {\n"
          <> "      record R1 {\n"
          <> "        [int] a;\n"
          <> "        record R2 {\n"
          <> "          [u64] u64arr;\n"
          <> "          [@u64] refu64arr;\n"
          <> "          [?u64] refqu64arr;\n"
          <> "          [?u64] qrefqu64arr;\n"
          <> "          [u64|10] u64farr;\n"
          <> "          @[u64|10] u64reffarr;\n"
          <> "          ?[u64|10] u64qfarr;\n"
          <> "          ?[?u64|10] qu64qfarr;\n"
          <> "          [@u64|10] refu64farr;\n"
          <> "          [|u64|10] u64carr;\n"
          <> "          [|[u64|5]|10] u64cfarr;\n"
          <> "          [|[std.u64|5]|10] u64ucfarr;\n"
          <> "          @[|[@std.u64|5]|10] refu64ucfarr;\n"
          <> "        };\n"
          <> "        std.int b;\n"
          <> "        record R3 {\n"
          <> "          [u64] u64arr tag(10);\n"
          <> "          [@u64] refu64arr group(20);\n"
          <> "          [?u64] refqu64arr;\n"
          <> "          [?u64] qrefqu64arr;\n"
          <> "          [u64|10] u64farr;\n"
          <> "          @[u64|10] u64reffarr;\n"
          <> "          ?[u64|10] u64qfarr;\n"
          <> "          ?[?u64|10] qu64qfarr;\n"
          <> "          [@u64|10] refu64farr;\n"
          <> "          [|u64|10] u64carr;\n"
          <> "          [|[u64|5]|10] u64cfarr;\n"
          <> "          [|[std.u64|5]|10] u64ucfarr;\n"
          <> "          [|[@std.u64|5]|10] refu64ucfarr;\n"
          <> "        };\n"
          <> "        std.contract.int c;\n"
          <> "        bool bool;\n"
          <> "        int int;\n"
          <> "        char char;\n"
          <> "        i64 i64;\n"
          <> "        int tag;\n"
          <> "        int group;\n"
          <> "      }\n"
          <> "    }\n"
          <> "    module M4 {\n"
          <> "      record R2 {\n"
          <> "        int b;\n"
          <> "      }\n"
          <> "    };\n"
          <> "  }\n"
          <> "}\n"
        )
        [Left (Module (Spanned (Ident "M1") (Span (Position 7 1 7) (Position 9 1 9))) [Left (Module (Spanned (Ident "M2") (Span (Position 21 2 10) (Position 23 2 12))) [Left (Module (Spanned (Ident "M4") (Span (Position 1158 46 12) (Position 1160 46 14))) [Right (Record (Spanned (Ident "R2") (Span (Position 1176 47 14) (Position 1178 47 16))) [Right (Field (Spanned (Ident "b") (Span (Position 1193 48 13) (Position 1194 48 14))) (Type (Spanned (Ident "int") (Span (Position 1189 48 9) (Position 1192 48 12))) NoType) NoTag)])]),Left (Module (Spanned (Ident "M3") (Span (Position 37 3 12) (Position 39 3 14))) [Right (Record (Spanned (Ident "R1") (Span (Position 55 4 14) (Position 57 4 16))) [Right (Field (Spanned (Ident "group") (Span (Position 1126 43 13) (Position 1131 43 18))) (Type (Spanned (Ident "int") (Span (Position 1122 43 9) (Position 1125 43 12))) NoType) NoTag),Right (Field (Spanned (Ident "tag") (Span (Position 1109 42 13) (Position 1112 42 16))) (Type (Spanned (Ident "int") (Span (Position 1105 42 9) (Position 1108 42 12))) NoType) NoTag),Right (Field (Spanned (Ident "i64") (Span (Position 1092 41 13) (Position 1095 41 16))) (Type (Spanned (Ident "i64") (Span (Position 1088 41 9) (Position 1091 41 12))) NoType) NoTag),Right (Field (Spanned (Ident "char") (Span (Position 1074 40 14) (Position 1078 40 18))) (Type (Spanned (Ident "char") (Span (Position 1069 40 9) (Position 1073 40 13))) NoType) NoTag),Right (Field (Spanned (Ident "int") (Span (Position 1056 39 13) (Position 1059 39 16))) (Type (Spanned (Ident "int") (Span (Position 1052 39 9) (Position 1055 39 12))) NoType) NoTag),Right (Field (Spanned (Ident "bool") (Span (Position 1038 38 14) (Position 1042 38 18))) (Type (Spanned (Ident "bool") (Span (Position 1033 38 9) (Position 1037 38 13))) NoType) NoTag),Right (Field (Spanned (Ident "c") (Span (Position 1022 37 26) (Position 1023 37 27))) (Type (Spanned (Ident "int") (Span (Position 1018 37 22) (Position 1021 37 25))) (Type (Spanned (Ident "contract") (Span (Position 1009 37 13) (Position 1017 37 21))) (Type (Spanned (Ident "std") (Span (Position 1005 37 9) (Position 1008 37 12))) NoType))) NoTag),Left (Record (Spanned (Ident "R3") (Span (Position 553 22 16) (Position 555 22 18))) [Right (Field (Spanned (Ident "refu64ucfarr") (Span (Position 972 35 30) (Position 984 35 42))) (CArray (FArray (Ref (Type (Spanned (Ident "u64") (Span (Position 961 35 19) (Position 964 35 22))) (Type (Spanned (Ident "std") (Span (Position 957 35 15) (Position 960 35 18))) NoType))) (Spanned 5 (Span (Position 965 35 23) (Position 966 35 24)))) (Spanned 10 (Span (Position 968 35 26) (Position 970 35 28)))) NoTag),Right (Field (Spanned (Ident "u64ucfarr") (Span (Position 932 34 29) (Position 941 34 38))) (CArray (FArray (Type (Spanned (Ident "u64") (Span (Position 921 34 18) (Position 924 34 21))) (Type (Spanned (Ident "std") (Span (Position 917 34 14) (Position 920 34 17))) NoType)) (Spanned 5 (Span (Position 925 34 22) (Position 926 34 23)))) (Spanned 10 (Span (Position 928 34 25) (Position 930 34 27)))) NoTag),Right (Field (Spanned (Ident "u64cfarr") (Span (Position 894 33 25) (Position 902 33 33))) (CArray (FArray (Type (Spanned (Ident "u64") (Span (Position 883 33 14) (Position 886 33 17))) NoType) (Spanned 5 (Span (Position 887 33 18) (Position 888 33 19)))) (Spanned 10 (Span (Position 890 33 21) (Position 892 33 23)))) NoTag),Right (Field (Spanned (Ident "u64carr") (Span (Position 861 32 21) (Position 868 32 28))) (CArray (Type (Spanned (Ident "u64") (Span (Position 853 32 13) (Position 856 32 16))) NoType) (Spanned 10 (Span (Position 857 32 17) (Position 859 32 19)))) NoTag),Right (Field (Spanned (Ident "refu64farr") (Span (Position 829 31 21) (Position 839 31 31))) (FArray (Ref (Type (Spanned (Ident "u64") (Span (Position 821 31 13) (Position 824 31 16))) NoType)) (Spanned 10 (Span (Position 825 31 17) (Position 827 31 19)))) NoTag),Right (Field (Spanned (Ident "qu64qfarr") (Span (Position 798 30 22) (Position 807 30 31))) (Ref (FArray (Ref (Type (Spanned (Ident "u64") (Span (Position 790 30 14) (Position 793 30 17))) NoType)) (Spanned 10 (Span (Position 794 30 18) (Position 796 30 20))))) NoTag),Right (Field (Spanned (Ident "u64qfarr") (Span (Position 767 29 21) (Position 775 29 29))) (Ref (FArray (Type (Spanned (Ident "u64") (Span (Position 759 29 13) (Position 762 29 16))) NoType) (Spanned 10 (Span (Position 763 29 17) (Position 765 29 19))))) NoTag),Right (Field (Spanned (Ident "u64reffarr") (Span (Position 735 28 21) (Position 745 28 31))) (Ref (FArray (Type (Spanned (Ident "u64") (Span (Position 727 28 13) (Position 730 28 16))) NoType) (Spanned 10 (Span (Position 731 28 17) (Position 733 28 19))))) NoTag),Right (Field (Spanned (Ident "u64farr") (Span (Position 706 27 20) (Position 713 27 27))) (FArray (Type (Spanned (Ident "u64") (Span (Position 698 27 12) (Position 701 27 15))) NoType) (Spanned 10 (Span (Position 702 27 16) (Position 704 27 18)))) NoTag),Right (Field (Spanned (Ident "qrefqu64arr") (Span (Position 674 26 18) (Position 685 26 29))) (Ref (Array (Ref (Type (Spanned (Ident "u64") (Span (Position 669 26 13) (Position 672 26 16))) NoType)))) NoTag),Right (Field (Spanned (Ident "refqu64arr") (Span (Position 645 25 18) (Position 655 25 28))) (Ref (Array (Ref (Type (Spanned (Ident "u64") (Span (Position 640 25 13) (Position 643 25 16))) NoType)))) NoTag),Right (Field (Spanned (Ident "refu64arr") (Span (Position 607 24 18) (Position 616 24 27))) (Ref (Array (Ref (Type (Spanned (Ident "u64") (Span (Position 602 24 13) (Position 605 24 16))) NoType)))) (Group (Spanned (Literal (Integer "20") Nothing) (Span (Position 623 24 34) (Position 625 24 36))))),Right (Field (Spanned (Ident "u64arr") (Span (Position 574 23 17) (Position 580 23 23))) (Ref (Array (Type (Spanned (Ident "u64") (Span (Position 569 23 12) (Position 572 23 15))) NoType))) (Tag (Spanned (Literal (Integer "10") Nothing) (Span (Position 585 23 28) (Position 587 23 30)))))]),Right (Field (Spanned (Ident "b") (Span (Position 535 21 17) (Position 536 21 18))) (Type (Spanned (Ident "int") (Span (Position 531 21 13) (Position 534 21 16))) (Type (Spanned (Ident "std") (Span (Position 527 21 9) (Position 530 21 12))) NoType)) NoTag),Left (Record (Spanned (Ident "R2") (Span (Position 92 6 16) (Position 94 6 18))) [Right (Field (Spanned (Ident "refu64ucfarr") (Span (Position 494 19 31) (Position 506 19 43))) (Ref (CArray (FArray (Ref (Type (Spanned (Ident "u64") (Span (Position 483 19 20) (Position 486 19 23))) (Type (Spanned (Ident "std") (Span (Position 479 19 16) (Position 482 19 19))) NoType))) (Spanned 5 (Span (Position 487 19 24) (Position 488 19 25)))) (Spanned 10 (Span (Position 490 19 27) (Position 492 19 29))))) NoTag),Right (Field (Spanned (Ident "u64ucfarr") (Span (Position 453 18 29) (Position 462 18 38))) (CArray (FArray (Type (Spanned (Ident "u64") (Span (Position 442 18 18) (Position 445 18 21))) (Type (Spanned (Ident "std") (Span (Position 438 18 14) (Position 441 18 17))) NoType)) (Spanned 5 (Span (Position 446 18 22) (Position 447 18 23)))) (Spanned 10 (Span (Position 449 18 25) (Position 451 18 27)))) NoTag),Right (Field (Spanned (Ident "u64cfarr") (Span (Position 415 17 25) (Position 423 17 33))) (CArray (FArray (Type (Spanned (Ident "u64") (Span (Position 404 17 14) (Position 407 17 17))) NoType) (Spanned 5 (Span (Position 408 17 18) (Position 409 17 19)))) (Spanned 10 (Span (Position 411 17 21) (Position 413 17 23)))) NoTag),Right (Field (Spanned (Ident "u64carr") (Span (Position 382 16 21) (Position 389 16 28))) (CArray (Type (Spanned (Ident "u64") (Span (Position 374 16 13) (Position 377 16 16))) NoType) (Spanned 10 (Span (Position 378 16 17) (Position 380 16 19)))) NoTag),Right (Field (Spanned (Ident "refu64farr") (Span (Position 350 15 21) (Position 360 15 31))) (FArray (Ref (Type (Spanned (Ident "u64") (Span (Position 342 15 13) (Position 345 15 16))) NoType)) (Spanned 10 (Span (Position 346 15 17) (Position 348 15 19)))) NoTag),Right (Field (Spanned (Ident "qu64qfarr") (Span (Position 319 14 22) (Position 328 14 31))) (Ref (FArray (Ref (Type (Spanned (Ident "u64") (Span (Position 311 14 14) (Position 314 14 17))) NoType)) (Spanned 10 (Span (Position 315 14 18) (Position 317 14 20))))) NoTag),Right (Field (Spanned (Ident "u64qfarr") (Span (Position 288 13 21) (Position 296 13 29))) (Ref (FArray (Type (Spanned (Ident "u64") (Span (Position 280 13 13) (Position 283 13 16))) NoType) (Spanned 10 (Span (Position 284 13 17) (Position 286 13 19))))) NoTag),Right (Field (Spanned (Ident "u64reffarr") (Span (Position 256 12 21) (Position 266 12 31))) (Ref (FArray (Type (Spanned (Ident "u64") (Span (Position 248 12 13) (Position 251 12 16))) NoType) (Spanned 10 (Span (Position 252 12 17) (Position 254 12 19))))) NoTag),Right (Field (Spanned (Ident "u64farr") (Span (Position 227 11 20) (Position 234 11 27))) (FArray (Type (Spanned (Ident "u64") (Span (Position 219 11 12) (Position 222 11 15))) NoType) (Spanned 10 (Span (Position 223 11 16) (Position 225 11 18)))) NoTag),Right (Field (Spanned (Ident "qrefqu64arr") (Span (Position 195 10 18) (Position 206 10 29))) (Ref (Array (Ref (Type (Spanned (Ident "u64") (Span (Position 190 10 13) (Position 193 10 16))) NoType)))) NoTag),Right (Field (Spanned (Ident "refqu64arr") (Span (Position 166 9 18) (Position 176 9 28))) (Ref (Array (Ref (Type (Spanned (Ident "u64") (Span (Position 161 9 13) (Position 164 9 16))) NoType)))) NoTag),Right (Field (Spanned (Ident "refu64arr") (Span (Position 138 8 18) (Position 147 8 27))) (Ref (Array (Ref (Type (Spanned (Ident "u64") (Span (Position 133 8 13) (Position 136 8 16))) NoType)))) NoTag),Right (Field (Spanned (Ident "u64arr") (Span (Position 113 7 17) (Position 119 7 23))) (Ref (Array (Type (Spanned (Ident "u64") (Span (Position 108 7 12) (Position 111 7 15))) NoType))) NoTag)]),Right (Field (Spanned (Ident "a") (Span (Position 74 5 15) (Position 75 5 16))) (Ref (Array (Type (Spanned (Ident "int") (Span (Position 69 5 10) (Position 72 5 13))) NoType))) NoTag)])])])])]

yaccModule :: String -> [Either (Module Span) (Record Span)] -> Expectation
yaccModule str m = Parser.run Internal.parseModule (inputStreamFromString str) initPosition `shouldBe` Right m

yaccFile :: String -> File Span -> Expectation
yaccFile str m = Parser.run Internal.parseFile (inputStreamFromString str) initPosition `shouldBe` Right m
