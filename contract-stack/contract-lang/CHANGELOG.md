# Change log

contract-lang uses [Semantic Versioning][].
The change log is available through the [releases on GitHub][].

[Semantic Versioning]: http://semver.org/spec/v2.0.0.html
[releases on GitHub]: https://github.com/smunix/contract-lang/releases
