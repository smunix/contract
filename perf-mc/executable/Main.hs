import Data.Semigroup ((<>))
import qualified Data.Strings as Str
import Control.Applicative (optional)
import qualified Options.Applicative as Opt
import qualified BenchShow as Bench
import Data.Ord (comparing)
import Data.List (sortBy)

data Config where
  Config :: { cIn :: String
            , cOutPrefix :: Maybe String
            , cOutDir :: Maybe String
            } -> Config
  deriving (Show)

main :: IO ()
main = do
  opts <- Opt.execParser optsParser
  let
    benchConfig = Bench.defaultConfig { Bench.outputDir = cOutDir opts
                                      , Bench.presentation = Bench.Groups Bench.PercentDiff
                                      , Bench.estimator = Bench.Regression
                                      , Bench.selectBenchmarks = \ fn -> either
                                                                         error
                                                                         (reverse . fmap fst . sortBy (comparing snd))
                                                                         (fn
                                                                           (Bench.ColumnIndex 0)
                                                                           (Just Bench.PercentDiff)
                                                                         )
                                      , Bench.classifyBenchmark = (\ name ->
                                                                      let
                                                                        [_, nb, rw, fd, bnch] = Str.sSplitAll "/" name
                                                                      in
                                                                        Just ( bnch
                                                                               <> "/"
                                                                               <> nb
                                                                             , rw
                                                                               <> "/"
                                                                               <> fd
                                                                             )
                                                                  )
                                      }
  print opts
  Bench.report (cIn opts) (cOutPrefix opts) benchConfig
  Bench.graph (cIn opts) (maybe "def" id (cOutPrefix opts)) benchConfig
  where
    versionOption :: Opt.Parser (Config -> Config)
    versionOption = Opt.infoOption "1.0" (Opt.long "version" <> Opt.help "Show version")

    programOptions :: Opt.Parser Config
    programOptions = Config
                     <$> (Opt.option Opt.str ( Opt.long "input"
                                               <> Opt.short 'i'
                                               <> Opt.metavar "FILE"
                                               <> Opt.help "input CSV file"
                                             )
                         )
                     <*> (optional $ Opt.option Opt.str ( Opt.long "prefix"
                                                          <> Opt.short 'p'
                                                          <> Opt.metavar "PREFIX"
                                                          <> Opt.help "prefix for output file"
                                                        )
                         )
                     <*> (optional $ Opt.option Opt.str ( Opt.long "outDir"
                                                          <> Opt.short 'o'
                                                          <> Opt.metavar "DIR"
                                                          <> Opt.help "ouptut directory for reports"
                                                        )
                         )
    optsParser :: Opt.ParserInfo Config
    optsParser =
      Opt.info
      (Opt.helper <*> versionOption <*> programOptions)
      (Opt.fullDesc <> Opt.progDesc "MC vs SBE Perf Benchmarks" <>
      Opt.header "perf - a small benchmark report generator for MC vs SBE")
