module MC.Order ( Order( accumQtySet
                       , accumQtyGet
                       , priceSet
                       , priceGet
                       , symbolSet
                       , symbolGet
                       , textSet
                       , textGet
                       , ui32Set
                       )
                ) where

import GHC.Generics (Generic)
import Control.DeepSeq (NFData(..))

import Foreign.Ptr
import Foreign.C.Types
import Foreign.ForeignPtr

import qualified Data.ByteString.Char8 as C

import Data.Word

import MC.MC

data Order where
  Order :: { orderWire   :: Wire Order
           , orderGet    :: Ptr Order
           , accumQtySet :: Word32 -> IO ()
           , accumQtyGet :: FunPtr (CUInt -> IO ()) -> IO ()
           , priceSet    :: Double -> IO ()
           , priceGet    :: FunPtr (CDouble -> IO ()) -> IO ()
           , symbolSet   :: C.ByteString -> IO ()
           , symbolGet   :: Ptr (Ptr CChar) -> Ptr CULong -> IO ()
           , textSet     :: C.ByteString -> IO ()
           , textGet     :: Ptr (Ptr CChar) -> Ptr CULong -> IO ()
           , ui32Set     :: Word32 -> IO ()
           } -> Order
  deriving (Generic)

instance NFData Order

foreign import ccall "mcOrderAccumQtySet"  mcOrderAccumQtySet :: Ptr Order -> CInt -> IO ()
foreign import ccall "mcOrderAccumQtyGet"  mcOrderAccumQtyGet :: Ptr Order -> FunPtr (CUInt -> IO ()) -> IO ()
foreign import ccall "mcOrderPriceSet"     mcOrderPriceSet    :: Ptr Order -> CDouble -> IO ()
foreign import ccall "mcOrderPriceGet"     mcOrderPriceGet    :: Ptr Order -> FunPtr (CDouble -> IO ()) -> IO ()
foreign import ccall "mcOrderUI32Set"      mcOrderUI32Set     :: Ptr Order -> CUInt -> IO ()
foreign import ccall "mcOrderSymbolSet"    mcOrderSymbolSet   :: Ptr (Wire Order) -> Ptr CChar -> CULong -> IO ()
foreign import ccall "mcOrderSymbolGet"    mcOrderSymbolGet   :: Ptr Order -> Ptr (Ptr CChar) -> Ptr CULong -> IO ()
foreign import ccall "mcOrderTextSet"      mcOrderTextSet     :: Ptr (Wire Order) -> Ptr CChar -> CULong -> IO ()
foreign import ccall "mcOrderTextGet"      mcOrderTextGet     :: Ptr Order -> Ptr (Ptr CChar) -> Ptr CULong -> IO ()

instance Wired Order where
  get w =
    let
      oFp = wirePtr w
      oP  = wireGet w
    in
      Order { orderWire   = w
            , orderGet    = oP
            , accumQtySet = mcOrderAccumQtySet oP . fromIntegral
            , accumQtyGet = mcOrderAccumQtyGet oP
            , priceSet    = mcOrderPriceSet oP . CDouble
            , priceGet    = mcOrderPriceGet oP
            , symbolSet   = (flip C.useAsCStringLen) (\ (ptr, len) -> withForeignPtr oFp (\ wireP -> mcOrderSymbolSet wireP ptr (fromIntegral len)))
            , symbolGet   = mcOrderSymbolGet oP
            , textSet     = (flip C.useAsCStringLen) (\ (ptr, len) -> withForeignPtr oFp (\ wireP -> mcOrderTextSet wireP ptr (fromIntegral len)))
            , textGet     = mcOrderTextGet oP
            , ui32Set     = mcOrderUI32Set oP . fromIntegral
            }
  size = wireSize
