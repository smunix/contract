module SBE.Order ( Order( accumQtySet
                        , accumQtyGet
                        , priceSet
                        , priceGet
                        , symbolSet
                        , symbolGet
                        , textSet
                        , textGet
                        )
                 , allocOrderPtr
                 , releaseOrderPtr
                 ) where

import GHC.Generics (Generic)
import Control.DeepSeq (NFData)

import Foreign.Ptr
import Foreign.C.Types
import Foreign.ForeignPtr

import qualified Data.ByteString.Char8 as C

import Data.Word

import SBE.SBE

data Order where
  Order :: { orderWire   :: Wire Order
           , accumQtySet :: Word32 -> IO ()
           , accumQtyGet :: FunPtr (CUInt -> IO ()) -> IO ()
           , priceSet    :: Double -> IO ()
           , priceGet    :: FunPtr (CDouble -> IO ()) -> IO ()
           , symbolSet   :: C.ByteString -> IO ()
           , symbolGet   :: Ptr CChar -> CULong -> IO Word64
           , textSet     :: C.ByteString -> IO ()
           , textGet     :: Ptr CChar -> CULong -> IO Word64
           } -> Order
  deriving (Generic)

instance NFData Order

foreign import ccall "sbeOrderAccumQtySet"  sbeOrderAccumQtySet :: Ptr Order -> CInt -> IO ()
foreign import ccall "sbeOrderAccumQtyGet"  sbeOrderAccumQtyGet :: Ptr Order -> FunPtr (CUInt -> IO ()) -> IO ()
foreign import ccall "sbeOrderPriceSet"     sbeOrderPriceSet    :: Ptr Order -> CDouble -> IO ()
foreign import ccall "sbeOrderPriceGet"     sbeOrderPriceGet    :: Ptr Order -> FunPtr (CDouble -> IO ()) -> IO ()
foreign import ccall "sbeOrderSymbolSet"    sbeOrderSymbolSet   :: Ptr Order -> Ptr CChar -> CULong -> IO ()
foreign import ccall "sbeOrderSymbolGet"    sbeOrderSymbolGet   :: Ptr Order -> Ptr CChar -> CULong -> IO CULong
foreign import ccall "sbeOrderTextSet"      sbeOrderTextSet     :: Ptr Order -> Ptr CChar -> CULong -> IO ()
foreign import ccall "sbeOrderTextGet"      sbeOrderTextGet     :: Ptr Order -> Ptr CChar -> CULong -> IO CULong
foreign import ccall "sbeNewOrder"          sbeNewOrder         :: Ptr (Wire Order) -> Ptr (MessageHeader Order) -> IO (Ptr Order)
foreign import ccall "&sbeDelOrder"         sbeDelOrder         :: FunPtr (Ptr Order -> IO ())

allocOrderPtr :: Ptr (Wire Order) -> Ptr (MessageHeader Order) -> IO (Ptr Order)
allocOrderPtr = sbeNewOrder

releaseOrderPtr :: FinalizerPtr Order
releaseOrderPtr = sbeDelOrder

instance Wired Order where
  get w =
    let
      oFp = wireGet w
    in
      Order { orderWire   = w
                , accumQtySet = \v -> withForeignPtr oFp (\ orderP -> sbeOrderAccumQtySet orderP . fromIntegral $ v)
                , accumQtyGet = \f -> withForeignPtr oFp (\ orderP -> sbeOrderAccumQtyGet orderP f)
                , priceSet    = \v -> withForeignPtr oFp (\ orderP -> sbeOrderPriceSet orderP . CDouble $ v)
                , priceGet    = \f -> withForeignPtr oFp (\ orderP -> sbeOrderPriceGet orderP f)
                , symbolSet   = (flip C.useAsCStringLen) (\ (ptr, len) -> withForeignPtr oFp (\ orderP -> sbeOrderSymbolSet orderP ptr (fromIntegral len)))
                , symbolGet   = \ ptr len -> withForeignPtr oFp (\ orderP -> fromIntegral <$> sbeOrderSymbolGet orderP ptr len)
                , textSet     = (flip C.useAsCStringLen) (\ (ptr, len) -> withForeignPtr oFp (\ orderP -> sbeOrderTextSet orderP ptr (fromIntegral len)))
                , textGet     = \ ptr len -> withForeignPtr oFp (\ orderP -> fromIntegral <$> sbeOrderTextGet orderP ptr len)
                }
  size = wireSize

