-- You can benchmark your code quickly and effectively with Criterion. See its
-- website for help: <http://www.serpentine.com/criterion/>.
import Criterion.Main
import Control.Monad (replicateM)

import qualified MC.MC as MC
import qualified MC.Order as MC

import qualified SBE.SBE as SBE
import qualified SBE.Order as SBE

import Foreign.C.Types
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Storable

import GHC.Generics (Generic)
import Control.DeepSeq (NFData(..))

import Data.ByteString.Char8 as C

text :: C.ByteString
text = "Free text created by an opportunistic trader"

symbol :: C.ByteString
symbol = "GOOGL"

envOrderMC :: Int -> Benchmark
envOrderMC n = env (fmap MC.get <$> replicateM n MC.newWire :: IO [MC.Order])
               (\os -> bgroup (show n ++ " MC.Orders") [ bench "accumQtySet" (nfAppIO (sequence . fmap (\o -> MC.accumQtySet o 10)) os)
                                                       , bench "priceSet" (nfAppIO (sequence . fmap (\o -> MC.priceSet o 20.0)) os)
                                                       , bench "ui32Set" (nfAppIO (sequence . fmap (\o -> MC.ui32Set o 10)) os)
                                                       , bench "symbolSet" (nfAppIO (sequence . fmap (\o -> MC.symbolSet o symbol)) os)
                                                       , bench "textSet" (nfAppIO (sequence . fmap (\o -> MC.textSet o text)) os)
                                                       ])

envOrderSBE :: Int -> Benchmark
envOrderSBE n = env (fmap SBE.get <$> replicateM n (SBE.newWire SBE.allocOrderPtr SBE.releaseOrderPtr) :: IO [SBE.Order])
               (\os -> bgroup (show n ++ " SBE.Orders") [ bench "accumQtySet" (nfAppIO (sequence . fmap (\o -> SBE.accumQtySet o 10)) os)
                                                        , bench "priceSet" (nfAppIO (sequence . fmap (\o -> SBE.priceSet o 20.0)) os)
                                                        , bench "symbolSet" (nfAppIO (sequence . fmap (\o -> SBE.symbolSet o symbol)) os)
                                                        , bench "textSet" (nfAppIO (sequence . fmap (\o -> SBE.textSet o text)) os)
                                                       ])

data Orders where
  Orders :: { mcos :: [MC.Order]
            , sbos :: [SBE.Order]
            , fpAccumQtyGet :: FunPtr (CUInt -> IO ())
            , fpPriceGet :: FunPtr (CDouble -> IO ())
            , ccharFp :: ForeignPtr (Ptr CChar)
            , culongFp :: ForeignPtr CULong
            , bufferFp :: ForeignPtr CChar
            } -> Orders
  deriving (Generic)

instance NFData Orders where
  rnf os = rnf (mcos os) <> rnf (sbos os)

foreign import ccall "wrapper" mkfpAccumQtyGet :: (CUInt -> IO ()) -> IO (FunPtr (CUInt -> IO ()))
foreign import ccall "wrapper" mkfpPriceGet :: (CDouble -> IO ()) -> IO (FunPtr (CDouble -> IO ()))

perfBench :: Int -> Benchmark
perfBench n = env (do
                      mcOrders <- fmap MC.get <$> replicateM n MC.newWire :: IO [MC.Order]
                      sbeOrders <- fmap SBE.get <$> replicateM n (SBE.newWire SBE.allocOrderPtr SBE.releaseOrderPtr) :: IO [SBE.Order]
                      mcfpAccumGet <- mkfpAccumQtyGet (const $ return ())
                      mcfpPriceGet <- mkfpPriceGet (const $ return ())
                      cchar <- mallocForeignPtr
                      culong <- mallocForeignPtr
                      buffer <- mallocForeignPtrBytes 4096
                      return Orders { mcos = mcOrders
                                    , sbos = sbeOrders
                                    , fpAccumQtyGet = mcfpAccumGet
                                    , fpPriceGet = mcfpPriceGet
                                    , ccharFp = cchar
                                    , culongFp = culong
                                    , bufferFp = buffer
                                    }
                  ) (\os -> bgroup (show n ++ " Orders") [ bgroup "write" [ bgroup "accumQty" [ bench "mc" (nfAppIO (sequence . fmap (\o -> MC.accumQtySet o 10)) (mcos os))
                                                                                              , bench "sbe" (nfAppIO (sequence . fmap (\o -> SBE.accumQtySet o 10)) (sbos os))
                                                                                              ]
                                                                          , bgroup "price" [ bench "mc" (nfAppIO (sequence . fmap (\o -> MC.priceSet o 10)) (mcos os))
                                                                                           , bench "sbe" (nfAppIO (sequence . fmap (\o -> SBE.priceSet o 10)) (sbos os))
                                                                                           ]
                                                                          , bgroup "symbol" [ bench "mc" (nfAppIO (sequence . fmap (\o -> MC.symbolSet o symbol)) (mcos os))
                                                                                            , bench "sbe" (nfAppIO (sequence . fmap (\o -> SBE.symbolSet o symbol)) (sbos os))
                                                                                            ]
                                                                          , bgroup "text" [ bench "mc" (nfAppIO (sequence . fmap (\o -> MC.textSet o text)) (mcos os))
                                                                                          , bench "sbe" (nfAppIO (sequence . fmap (\o -> SBE.textSet o text)) (sbos os))
                                                                                          ]
                                                                          ]
                                                         , bgroup "read" [ bgroup "accumQty" [ bench "mc" (nfAppIO (sequence . fmap (\o -> MC.accumQtyGet o (fpAccumQtyGet os))) (mcos os))
                                                                                             , bench "sbe" (nfAppIO (sequence . fmap (\o -> SBE.accumQtyGet o (fpAccumQtyGet os))) (sbos os))
                                                                                             ]
                                                                         , bgroup "price" [ bench "mc" (nfAppIO (sequence . fmap (\o -> MC.priceGet o (fpPriceGet os))) (mcos os))
                                                                                          , bench "sbe" (nfAppIO (sequence . fmap (\o -> SBE.priceGet o (fpPriceGet os))) (sbos os))
                                                                                          ]
                                                                         , bgroup "symbol" [ bench "mc" (nfAppIO (sequence . fmap (\o -> withForeignPtr (ccharFp os) (\ cchar -> withForeignPtr (culongFp os) (\ culong -> MC.symbolGet o cchar culong)))) (mcos os))
                                                                                           , bench "sbe" (nfAppIO (sequence . fmap (\o -> withForeignPtr (bufferFp os) (\ cchar -> SBE.symbolGet o cchar (fromIntegral 4096)))) (sbos os))
                                                                                           ]
                                                                         , bgroup "text" [ bench "mc" (nfAppIO (sequence . fmap (\o -> withForeignPtr (ccharFp os) (\ cchar -> withForeignPtr (culongFp os) (\ culong -> MC.textGet o cchar culong)))) (mcos os))
                                                                                         , bench "sbe" (nfAppIO (sequence . fmap (\o -> withForeignPtr (bufferFp os) (\ cchar -> SBE.textGet o cchar (fromIntegral 4096)))) (sbos os))
                                                                                         ]
                                                                         ]
                                                         ]
                    )

main :: IO ()
main = defaultMain [ bgroup "PerfBench" [ perfBench 1
                                        , perfBench 10
                                        , perfBench 100
                                        , perfBench 1000
                                        , perfBench 10000
                                        , perfBench 100000
                                        , perfBench 1000000
                                        ]
                   , bgroup "mc/Wire" [ env (MC.newWire :: IO (MC.Wire MC.Order))
                                        (\ w -> bgroup "MC.Wire" [ bench "MC.size" (nfAppIO MC.size w)
                                                                 ])
                                      ]
                   , bgroup "mc/Order" [ envOrderMC 1
                                       , envOrderMC 10
                                       , envOrderMC 100
                                       , envOrderMC 1000
                                       , envOrderMC 10000
                                       , envOrderMC 100000
                                       , envOrderMC 1000000

                                         -- , bgroup "Per Run Order" [ bench "accumQtySet" (perRunEnv (get <$> newWire :: IO Order) (\o -> accumQtySet o 10))
                                         --                          ]
                                       ]
                   , bgroup "sbe/Order" [ envOrderSBE 1
                                        , envOrderSBE 10
                                        , envOrderSBE 100
                                        , envOrderSBE 1000
                                        , envOrderSBE 10000
                                        , envOrderSBE 100000
                                        , envOrderSBE 1000000
                                          -- , bgroup "Per Run Order" [ bench "accumQtySet" (perRunEnv (get <$> newWire :: IO Order) (\o -> accumQtySet o 10))
                                          --                          ]
                                        ]
                   ]
