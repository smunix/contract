-- Tasty makes it easy to test your code. It is a test framework that can
-- combine many different types of tests into one suite. See its website for
-- help: <http://documentup.com/feuerbach/tasty>.
import qualified Test.Tasty
-- Hspec is one of the providers for Tasty. It provides a nice syntax for
-- writing tests. Its website has more info: <https://hspec.github.io>.
import Test.Tasty.Hspec

import qualified MC.MC as MC
import qualified MC.Order as MC

import qualified SBE.SBE as SBE
import qualified SBE.Order as SBE

import Data.Strings (sLen)
import Data.ByteString.Unsafe (unsafePackCStringLen)

import Foreign.C.Types
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Storable

foreign import ccall "wrapper" wrapAccumQtyGet :: (CUInt -> IO ()) -> IO (FunPtr (CUInt -> IO ()))
foreign import ccall "wrapper" wrapPriceGet :: (CDouble -> IO ()) -> IO (FunPtr (CDouble -> IO ()))

main :: IO ()
main = do
    test <- testSpec "perf" spec
    Test.Tasty.defaultMain test
data Env o where
  Env :: { order :: o
         , buffP :: Ptr (Ptr CChar)
         , lenP :: Ptr CULong
         , bufferP :: Ptr CChar
         } -> Env o
spec :: Spec
spec = parallel $ do
  let
    price = 222
    accumQty = 111
    symbol = "GOOGL"
    text = "I am not a free potato"
    bufferLen = 4096
  describe "MC" $ do
    around (\f -> do
               wire <- MC.newWire
               let
                 order = MC.get wire
               buffFP <- mallocForeignPtr
               lenFP <- mallocForeignPtr
               bufferFP <- mallocForeignPtrBytes bufferLen
               withForeignPtr buffFP
                 (\ buffP -> withForeignPtr lenFP
                             (\ lenP -> withForeignPtr bufferFP
                                        (\ bufferP -> f $ Env{..})
                             )
                 )
           ) $ do
      describe "set/get" $ do
        it "price" $ \Env{..} -> do
          MC.priceSet order price
          MC.priceSet order (price+1)
          MC.priceSet order price
          wrapPriceGet (shouldBe price . realToFrac) >>= MC.priceGet order
        it "accumQty" $ \Env{..} -> do
          MC.accumQtySet order accumQty
          wrapAccumQtyGet (shouldBe accumQty . fromIntegral) >>= MC.accumQtyGet order
        it "symbol" $ \Env{..} -> do
          MC.symbolSet order symbol
          MC.symbolGet order buffP lenP
          peek lenP >>= shouldBe (fromIntegral $ sLen symbol)
          ((,) <$> (peek buffP) <*> (fromIntegral <$> peek lenP)) >>= unsafePackCStringLen >>= shouldBe symbol
        it "text" $ \Env{..} -> do
          MC.textSet order text
          MC.textGet order buffP lenP
          peek lenP >>= shouldBe (fromIntegral $ sLen text)
          ((,) <$> (peek buffP) <*> (fromIntegral <$> peek lenP)) >>= unsafePackCStringLen >>= shouldBe text

  describe "SBE" $ do
    around (\f -> do
               wire <- SBE.newWire SBE.allocOrderPtr SBE.releaseOrderPtr
               let
                 order = SBE.get wire
               buffFP <- mallocForeignPtr
               lenFP <- mallocForeignPtr
               bufferFP <- mallocForeignPtrBytes bufferLen
               withForeignPtr buffFP
                 (\ buffP -> withForeignPtr lenFP
                             (\ lenP -> withForeignPtr bufferFP
                                        (\ bufferP -> f $ Env{..})
                             )
                 )
           ) $ do
      describe "set/get" $ do
        it "price" $ \Env{..} -> do
          SBE.priceSet order price
          SBE.priceSet order (price+1)
          SBE.priceSet order price
          wrapPriceGet (shouldBe price . realToFrac) >>= SBE.priceGet order
        it "accumQty" $ \Env{..} -> do
          SBE.accumQtySet order accumQty
          wrapAccumQtyGet (shouldBe accumQty . fromIntegral) >>= SBE.accumQtyGet order
        it "symbol" $ \Env{..} -> do
          SBE.symbolSet order symbol
          byteCount <- SBE.symbolGet order bufferP (fromIntegral bufferLen)
          pendingWith "SBE : setting symbol moves the read pointer to unassigned memory segments"
          byteCount `shouldBe` (fromIntegral $ sLen symbol)
          unsafePackCStringLen (bufferP, fromIntegral byteCount) >>= shouldBe symbol
        it "text" $ \Env{..} -> do
          SBE.textSet order text
          byteCount <- SBE.textGet order bufferP (fromIntegral bufferLen)
          pendingWith "SBE : setting text moves the read pointer to unassigned memory segments"
          byteCount `shouldBe` (fromIntegral $ sLen text)
          unsafePackCStringLen (bufferP, fromIntegral byteCount) >>= shouldBe text
