#! /usr/bin/env sh
java -Dsbe.target.language=CPP -Dsbe.output.dir=../../includes/sbe -jar ../sbe-tool/sbe-all-1.13.3-SNAPSHOT.jar order.xml
find ../../includes/sbe -type f -iname '*.H' -exec sed -e 's@namespace order@namespace sbe@g' -i {} \;
